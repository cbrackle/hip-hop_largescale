//##########################################################################
//
// Program which converts a bp position or range into beads
//
// A "config" file specifying the region and bp to bead mapping is needed.
//
//##########################################################################

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <set>
#include <sstream>

#include "configurations.h"

using namespace std;

int main(int argc, char *argv[]) {

  // get options from command line
  if (argc<6) {
    cout<<"Usage :"<<endl;
    cout<<"Convert a bp position (or range) to a bead position (or range):"<<endl;
    cout<<"       ./bp_to_bead -c configfile -cc CHROM -ss START [-ee END]"<<endl;
    cout<<"where       configfile   HiP-HoP config file with simulated region and number of bp per bead etc."<<endl;
    cout<<"            CHROM        the chromosome."<<endl;
    cout<<"            START        a bp position within the simulated region"<<endl;
    cout<<"            END          optional. if present, a set of beads will be given, otherwise a single bead will be given."<<endl;
    cout<<"                         If a map file is provided, will convert also give all corresoponsing beads within the system:"<<endl;
    cout<<"TO DO : implment a verion which a bed file and converts each line to a range of beads."<<endl;

    exit(EXIT_FAILURE);
  }

  string configfile="",
    chrom="";
  long unsigned int bp_start=0,
    bp_end=0;


  int argi=1;
  while (argi < argc) {

    if ( string(argv[argi]) == "-c" ) {
      // output file
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-c)"<<endl;
        exit(EXIT_FAILURE);
      }
      configfile = string(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-cc" ) {
      // output file
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-cc)"<<endl;
        exit(EXIT_FAILURE);
      }
      chrom = string(argv[argi+1]);
      argi += 2;


    } else if ( string(argv[argi]) == "-ss" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-ss)"<<endl;
        exit(EXIT_FAILURE);
      }
      bp_start = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-ee" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-ee)"<<endl;
        exit(EXIT_FAILURE);
      }
      bp_end = atoi(argv[argi+1]);
      argi += 2;

    } else {
      cerr<<"Error parsing command line (unrecognised option)"<<endl;
      exit(EXIT_FAILURE);
    }

  } 

  if ( configfile=="" ) {
    cerr<<"Must specify a configuration file"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( chrom=="" ) {
    cerr<<"Must specify a chromosome"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( bp_start<=0 ) {
    cerr<<"Must specify a bp_start"<<endl;
    exit(EXIT_FAILURE);
  }


  // set up variables
  ifstream inf;             // file stream for input of dump file
  ofstream ouf;             // file streams for output

  string line;
  stringstream sline;

  bool range_flag=false;

  configuration config;

  set< pair<int,int> > regions;

  set<int> list_of_beads;

  if (bp_end>0) {
    range_flag=true;
  }

  // load config file
  // read the configuration file
  inf.open( configfile.c_str() );
  if ( ! inf.good() ) {
    cout<<" ERROR : File "<<configfile<<"  does not exist."<<endl;
    exit(EXIT_FAILURE);
  }
  getline(inf,line); sline.clear(); sline.str(line);
  sline>>config.chrom;
  getline(inf,line); sline.clear(); sline.str(line);
  sline>>config.regstart;
  getline(inf,line); sline.clear(); sline.str(line);
  sline>>config.regend;
  getline(inf,line); sline.clear(); sline.str(line);
  sline>>config.bp_per_bead;
  inf.close();

  // Do some checks
  if (chrom != config.chrom) {
    cout<<" ERROR : chromosome "<<chrom<<" is not simulated."<<endl;
    exit(EXIT_FAILURE);
  }
  if ( bp_start<config.regstart || bp_start>config.regend ) {
    cout<<" ERROR : START ("<<bp_start<<") is not in simulated region."<<endl;
    exit(EXIT_FAILURE);
  }
  if (range_flag) {
    if (bp_end<bp_start) {
      cout<<" ERROR : END<START bp."<<endl;
      exit(EXIT_FAILURE);
    }
    if ( bp_end<config.regstart || bp_end>config.regend ) {
      cout<<" ERROR : END ("<<bp_end<<") is not in simulated region."<<endl;
      exit(EXIT_FAILURE);
    }
  }


  // get the beads
  if (range_flag) {
    for (int i=config.bp2beadinregion(bp_start);i<=config.bp2beadinregion(bp_end);i++) {
      list_of_beads.insert(i);
    }
  } else {
    list_of_beads.insert(config.bp2beadinregion(bp_start));
  }


for (set<int>::iterator it=list_of_beads.begin();it!=list_of_beads.end();++it) {
  cout<<*it<<" ";
 }



}
