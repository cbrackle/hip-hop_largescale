#!/bin/bash

# Converts a simulated HiC map in gnuplot friendly format into the h5 
# format which can be read by HiCExplorer

# Input format:
#  i j value 
# where i and j are bead numbers (do not have to be integers if bins are used)

if [[ $# != 6 ]]; then
    echo "Script to convert simulated HiC map in gnuplot friendly format (full square with"
    echo "gaps) into the h5 format which can be read by HiCExplorer"
    echo "Usage : ./beadmap2hic.sh mapfile outfilestart bpperbead beadsperbin chrom startbp"
    exit 1
fi

infile=$1
outfile=$2
bp_per_bead=$3
bead_per_bin=$4
chrom=$5
startbp=$6

awk -v cc=$chrom -v ss=$startbp -v bb=$bp_per_bead -v bi=$bead_per_bin '{
if ($1!="" && $1!="#") {bin[$1-0.5*bi]=1;bin[$2-0.5*bi]=1;}
}END{OFS="\t";
bppb=bi*bb
for (i in bin) print cc,int(i*bb)+ss,int(i*bb)+ss+bppb
}' $infile | sort -k1,1 -k2,2n | awk '
BEGIN{b=1}{OFS="\t";print $0,b;b++}' > ${outfile}_bins.bed

awk -v ss=$startbp -v bb=$bp_per_bead \
    -v bi=$bead_per_bin -v ff="${outfile}_bins.bed" 'BEGIN{
while ( (getline<ff)>0 ) {bin[$2]=$4;}
}{
if ($1!="#" && $1!="" && $1<=$2) {OFS="\t";
ii=int(($1-0.5*bi)*bb)+ss;
jj=int(($2-0.5*bi)*bb)+ss;
print bin[ii],bin[jj],$3
}}' $infile | sort -k1,1n -k2,2n > ${outfile}_HiCPro.bed


source ~/activate_conda.sh
conda activate myPy368

hicConvertFormat --matrices ${outfile}_HiCPro.bed \
           --bedFileHicpro ${outfile}_bins.bed \
           --inputFormat hicpro \
           --outFileName ${outfile}.h5 \
           --outputFormat h5 

conda deactivate


rm ${outfile}_HiCPro.bed  ${outfile}_bins.bed


