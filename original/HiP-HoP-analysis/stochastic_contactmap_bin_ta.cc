//##########################################################################
//
// Program for calculating STOCHASTIC binned contact maps from dump files    
// 
// Dump files are assumed to have same number of atoms per frame,
// same number of frames,
// and have line format:
//                        id type xs ys zs ix iy iz
// (xs etc. are in range 0-1 so have to be converted to box coords)
// (assumes id starts from 1 and runs to N)
// 
// Should be able to handle large maps
//
//
//##########################################################################

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <set>
#include <sstream>

#include "configurations.h"
#include "cmaps.h"
#include "random.h"

using namespace std;

int main(int argc, char *argv[]) {

  // get options from command line
  if (argc<11) {
    cout<<"Usage :"<<endl;
    cout<<"       ./stochastic_contactmap_bin_timeav -i infile -o outfile -n N -b B -t THRESH -r RANGE -s SEED -k SKIP -e EVERY -u MULTIPL -g"<<endl;
    cout<<"where       infile    is the input dump file."<<endl;
    cout<<"            outfile   is the output file."<<endl;
    cout<<"            N         is the number of polymer beads (assumes to be the first N)"<<endl;
    cout<<"            B         is the number of beads per bin"<<endl;
    cout<<"            THRESH    is the contact length scale"<<endl;
    cout<<"            SEED      is a seed for the random numbers"<<endl;
    cout<<"            RANGE     (OPTIONAL) how many beads out to go in map"<<endl;
    cout<<"            SKIP      (OPTIONAL) skip this many frames from start of file"<<endl;
    cout<<"            EVERY     (OPTIONAL) consider only every this number of frames"<<endl;
    cout<<"            MULTIPL   (OPTIONAL) multuply the number of attempts by this"<<endl;
    cout<<"            -g        (OPTIONAL) if present output format will be for gnuplot image"<<endl;
    cout<<"Unless option [-g] is used, output is 'i j count' only for one triangle of the map."<<endl;
    exit(EXIT_FAILURE);
  }

  set<string> infiles,
    outfile;
  int Npoly=0,
    beads_per_bin=0,
    seed=0,
    multipl=1,
    skip = 0,
    range = 0,
    every = 1;
  double thresh=0.0;
  bool gnuplot=false;

  int argi=1;
  while (argi < argc) {

    if ( string(argv[argi]) == "-i" ) {
      // input file
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-i)"<<endl;
	exit(EXIT_FAILURE);
      }
      infiles.insert( string(argv[argi+1]) );
      argi += 2;

    } else if ( string(argv[argi]) == "-o" ) {
      // output file
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-o)"<<endl;
	exit(EXIT_FAILURE);
      }
      outfile.insert( string(argv[argi+1]) );
      argi += 2;

    } else if ( string(argv[argi]) == "-n" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-n)"<<endl;
      }
      Npoly = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-b" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-b)"<<endl;
	exit(EXIT_FAILURE);
      }
      beads_per_bin = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-t" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-t)"<<endl;
	exit(EXIT_FAILURE);
      }
      thresh = atof(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-r" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-r)"<<endl;
	exit(EXIT_FAILURE);
      }
      range = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-s" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-s)"<<endl;
	exit(EXIT_FAILURE);
      }
      seed = atof(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-u" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-u)"<<endl;
	exit(EXIT_FAILURE);
      }
      multipl = atof(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-k" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-k)"<<endl;
	exit(EXIT_FAILURE);
      }
      skip = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-e" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-e)"<<endl;
	exit(EXIT_FAILURE);
      }
      every = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-g" ) {
      gnuplot = true;
      argi ++;

    } else {
      cerr<<"Error parsing command line (unrecognised option)"<<endl;
      exit(EXIT_FAILURE);
    }

  } 

  if ( infiles.size()<1 ) {
    cerr<<"No input file specified"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( outfile.size()!=1 ) {
    cerr<<"Must specify exactly one output file"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( Npoly==0 ) {
    cerr<<"Incorrect number of polymer beads"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( beads_per_bin==0 ) {
    cerr<<"Incorrect number of beads per bin"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( thresh==0.0 ) {
    cerr<<"Threshold not specified correctly"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( seed==0 ) {
    cerr<<"Seed not specified correctly"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( every<1 ) {
    cerr<<"Every not specified correctly"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( skip<0 ) {
    cerr<<"Skip not specified correctly"<<endl;
    exit(EXIT_FAILURE);
  }


  // set up variables
  ifstream inf;             // file stream for input of dump file
  ofstream ouf;             // file streams for output

  string line,
    junk;
  stringstream sline;

  randoms myrand;
  myrand.initialize(seed);

  double sep,
    halfbinwidth = 0.5*double(beads_per_bin);

  int attemps; 

  set< pair<int,int> > regions;

  map<pair<int,int>, double> contacts;

  // initialise the system by reading some information from first dump file
  configuration config( *infiles.begin() );

  if ( range == 0 ) {
    range = Npoly;
    attemps = Npoly*(Npoly+1)/2 * multipl;
  } else {
    attemps = (Npoly*(Npoly+1)/2 - (Npoly-range)*(Npoly-range+1)/2)* multipl;
  }

  if ( skip > config.frames_per_file ) {
    cerr<<"Not enough frames in dump to skip "<<skip<<endl;
    exit(EXIT_FAILURE);
  }



  // test output file is not already there
  inf.open( outfile.begin()->c_str() );
  if ( inf.good() ) {
    cout<<" ERROR : File "<<*outfile.begin()<<" already exists. Will not overwrite."<<endl;
    exit(EXIT_FAILURE);
  }
  inf.close();


  // print a message
  cout<<"Generating contact map with "<<beads_per_bin<<" beads per bin"
      <<"from dump files with "<<Npoly<<" polymer beads."<<endl;
  cout<<"Skipping "<<skip<<" frames from the start, looking at every "<<every<<" frame"<<endl;

  
  // loop round dump files
  for (set<string>::iterator fn=infiles.begin(); fn!=infiles.end(); ++fn) {
    cout<<"Reading dump file "<<*fn<<"   "<<endl;;

    inf.open( fn->c_str() );
    if ( !inf.good() ) {
      cout<<" ERROR while trying to open file "<<*fn<<endl;
      exit(EXIT_FAILURE);
    }

    
    // Loop through frames in dump file ---------------------------------------
    for (int timeStep=0;timeStep<config.frames_per_file;timeStep++) {

      // read the frame
      config.read_frame(inf);
      
      if ( timeStep>=skip && timeStep%every == 0 ) {
	cout<<"frame "<<timeStep+1<<" of "<<config.frames_per_file<<"        \r"<<flush;

	// Make attempts
	for (int a=0;a<attemps;a++) {
	  int i = myrand.integer(1,Npoly);
	  int j = myrand.integer(1,Npoly);
	  if ( abs(i-j)<=range ) {
	    sep = config[i].sep_shortest(config[j],config.L);
	    if ( myrand.doub() < exp(-sep/thresh)  ) {
	      contacts[ beadij2binij( i,j,beads_per_bin) ]++;
	    }
	  }
	}

      } // end if

    }// end frames loop
    inf.close();
  
  }
  cout<<endl;
  

  // Output
  ouf.open( outfile.begin()->c_str() );
  ouf<<"# i, j, count"<<endl;
  if (gnuplot) {
    for (int i=0;i<=Npoly;i+=beads_per_bin) {
      for (int j=0;j<=Npoly;j+=beads_per_bin) {
	ouf<<i+halfbinwidth<<" "<<j+halfbinwidth<<" ";
	if (i<=j) {
	  if ( contacts.find( pair<int,int>(i,j) )!=contacts.end() ) {
	    ouf<<contacts[ pair<int,int>(i,j) ]<<endl;
	  } else {
	    ouf<<0.0<<endl;
	  }
	} else {
	  if ( contacts.find( pair<int,int>(j,i) )!=contacts.end() ) {
	    ouf<<contacts[ pair<int,int>(j,i) ]<<endl;
	  } else {
	    ouf<<0.0<<endl;
	  }
	}
      }
      ouf<<endl;
    }
  } else {
    for (map<pair<int,int>, double>::iterator it=contacts.begin(); it!=contacts.end(); ++it) {
      ouf<<it->first.first+halfbinwidth<<" "
	 <<it->first.second+halfbinwidth<<" "
	 <<it->second<<endl;
    }
  }
  ouf.close();

}
