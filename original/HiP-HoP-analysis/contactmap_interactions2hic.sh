#!/bin/bash

# Converts a simulated HiC map in gnuplot friendly format into the h5 
# format which can be read by HiCExplorer

# Input format:
#  i j value 
# where i and j are bead numbers (do not have to be integers if bins are used)

if [[ $# != 6 ]]; then
    echo "Script to convert simulated HiC map in 'i j count' upper triangle only"
    echo "format into the h5 format which can be read by HiCExplorer"
    echo "Usage : ./beadmap2hic.sh mapfile outfilestart bpperbead beadsperbin chrom startbp"
    exit 1
fi

echo "Ooops, have not implemented this yet"
