//##########################################################################
//
// HEADER FOR:
// Functions for contact maps 
//
//##########################################################################


#ifndef CMAPS_H
#define CMAPS_H

using namespace std;

pair<int,int> beadij2binij(const int&,const int&,const int&);

#endif
