//##########################################################################
//
// Header for 
// Set of data structures and functions for describing a simulation 
// confuguraiton
//
//##########################################################################

#ifndef CONFIGURATIONS_H
#define CONFIGURATIONS_H

#include <map>
#include <fstream>
#include <set>
#include <map>

using namespace std;

struct atom {
  // data structure for atoms

  int id,        // atom id
    type,        // atom type
    ix[3];       // image flags

  double x[3];        // atom position

  unsigned int flag : 16; // a (16 bit unsigned) flag for identifying things 

  bool unwrapped;

  // Constructor
atom() : unwrapped(0) {  x[0]=0.0; x[1]=0.0; x[2]=0.0; };

  // Functions
  double sep_shortest(const atom&,const double[3]) const;
  double sep_unwrapped(const atom&,const double[3]) const;
  bool operator<(const atom&) const;
  atom operator+(const atom&) const;
  atom operator-(const atom&) const;
  atom operator/(const int&) const;
  double dot(const atom&) const;
  void unwrap(const double[3]);
  double length() const;

};

struct configuration {
  // data structure for the configuration of a simulation

  double L[3];           // box size
  int N;                 // total number of atoms

  //atom *atoms;    // vector for all atoms

  long int starttime,    // first time step in dump
    timeinterval;        // how often were things output
  int frames_per_file;   // how many frames in file

  string chrom;
  long int regstart,
    regend;
  int bp_per_bead;

  // Functions
  configuration(const string&);   // constructor which reads some info from dump
  configuration();                // alternative constructor which does not read any info
  ~configuration();               // destructor

  const atom &operator[] (const int&) const;
  void read_frame(ifstream&); // read a frame from the dump file.
  void unwrap_coords();           // function which unwraps atom coords
  set<int> get_list_of_type(const int&) const;
  map<int,atom> get_map_of_type(const set<int>&) const;
  atom CoM_unwrapped(const set<int> &) const;
  int bp2beadinregion(const long int &) const;
  
private:
  map<int,atom> at;

};


template<typename Pair>
struct comparesecondofpair {
  // a sctructure for a comparison function for the second of a pair
  bool operator ()(const Pair &a, const Pair &b)
  { return a.second < b.second; }
};

#endif
