//##########################################################################
//
// Program for calculating binned contact maps from dump files (time average)     
// 
// Dump files are assumed to have same number of atoms per frame,
// same number of frames,
// and have line format:
//                        id type xs ys zs ix iy iz
// (xs etc. are in range 0-1 so have to be converted to box coords)
// (assumes id starts from 1 and runs to N)
// 
// Should be able to handle large maps, but can also set a range cut-off
//
// Output is "i j count" just for one triangle
//
//##########################################################################


//##########################################################################
// Binning strategy
// There are two (or probably more) possible ways to bin up contacts:
// 
// (1) In a given cofiguriation, if one or more beads from bin i is with 
//     threshold distance of one or more beads from bin j, count this as
//     a single contact event.
// (2) Every bead in bin i which is within threshold distance of of a bead 
//     in bin j, count it as a contact event. E.g. if bead A in bin i is 
//     within threshold distance of beads A and B in bin j, this is two 
//     events etc.
//
// Here I will use version (2), but I will scale the number of contact events
// so that this can be at most 1 per pair of bins.
// I.e., if there are B beads per bin, in a single configuation, two bins can 
// interact at most B^2 times, so I will scale by 1/B^2.
//##########################################################################

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <set>

#include "configurations.h"
#include "cmaps.h"

using namespace std;

int main(int argc, char *argv[]) {

  // get options from command line
  if (argc<11) {
    cout<<"Usage :"<<endl;
    cout<<"       ./contactmap_bin_timeav -i infile -o outfile -n N -b B -t THRESH -r RANGE -k SKIP -e EVERY"<<endl;
    cout<<"where       infile    is the input dump file."<<endl;
    cout<<"            outfile   is the output file."<<endl;
    cout<<"            N         is the number of polymer beads (assumes to be the first N)"<<endl;
    cout<<"            B         is the number of beads per bin"<<endl;
    cout<<"            THRESH    is the contact threshold"<<endl;
    cout<<"            RANGE     (OPTIONAL) how many beads out to go in map"<<endl;
    cout<<"            SKIP      (OPTIONAL) skip this many frames from start of file"<<endl;
    cout<<"            EVERY     (OPTIONAL) consider only every this number of frames"<<endl;
    cout<<"Output is 'i j count' just for one triangle."<<endl;
    exit(EXIT_FAILURE);
  }

  set<string> infiles,
    outfile;
  int Npoly=0,
    beads_per_bin=0,
    range = 0,
    skip = 0,
    every = 1;
  double thresh=0.0;

  int argi=1;
  while (argi < argc) {

    if ( string(argv[argi]) == "-i" ) {
      // input file
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-i)"<<endl;
	exit(EXIT_FAILURE);
      }
      infiles.insert( string(argv[argi+1]) );
      argi += 2;

    } else if ( string(argv[argi]) == "-o" ) {
      // output file
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-o)"<<endl;
	exit(EXIT_FAILURE);
      }
      outfile.insert( string(argv[argi+1]) );
      argi += 2;

    } else if ( string(argv[argi]) == "-n" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-n)"<<endl;
      }
      Npoly = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-b" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-b)"<<endl;
	exit(EXIT_FAILURE);
      }
      beads_per_bin = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-t" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-t)"<<endl;
	exit(EXIT_FAILURE);
      }
      thresh = atof(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-r" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-r)"<<endl;
	exit(EXIT_FAILURE);
      }
      range = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-k" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-k)"<<endl;
	exit(EXIT_FAILURE);
      }
      skip = atoi(argv[argi+1]);
      argi += 2;

    } else if ( string(argv[argi]) == "-e" ) {
      if (!(argi+1 < argc)) {
        cerr<<"Error parsing command line (-e)"<<endl;
	exit(EXIT_FAILURE);
      }
      every = atoi(argv[argi+1]);
      argi += 2;

    } else {
      cerr<<"Error parsing command line (unrecognised option)"<<endl;
      exit(EXIT_FAILURE);
    }

  } 

  if ( range == 0 ) {
    range = Npoly;
  }

  if ( infiles.size()<1 ) {
    cerr<<"No input file specified"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( outfile.size()!=1 ) {
    cerr<<"Must specify exactly one output file"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( Npoly==0 ) {
    cerr<<"Incorrect number of polymer beads"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( beads_per_bin==0 ) {
    cerr<<"Incorrect number of beads per bin"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( thresh==0.0 ) {
    cerr<<"Threshold not specified correctly"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( range>Npoly ) {
    cerr<<"Range not specified correctly"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( every<1 ) {
    cerr<<"Every not specified correctly"<<endl;
    exit(EXIT_FAILURE);
  }
  if ( skip<0 ) {
    cerr<<"Skip not specified correctly"<<endl;
    exit(EXIT_FAILURE);
  }


  // set up variables
  ifstream inf;             // file stream for input of dump file
  ofstream ouf;             // file streams for output

  double halfbinwidth = 0.5*double(beads_per_bin),
    scale = double(beads_per_bin*beads_per_bin); // 0.5*double(beads_per_bin)*double(beads_per_bin+1); // see details above

  long unsigned int counter;

  map<pair<int,int>, double> contacts;


  // initialise the system by reading some information from first dump file
  configuration config( *infiles.begin() );

  if ( skip > config.frames_per_file ) {
    cerr<<"Not enough frames in dump to skip "<<skip<<endl;
    exit(EXIT_FAILURE);
  }


  // test output file is not already there
  inf.open( outfile.begin()->c_str() );
  if ( inf.good() ) {
    cout<<" ERROR : File "<<*outfile.begin()<<" already exists. Will not overwrite."<<endl;
    exit(0);
  }
  inf.close();

  // print a message
  cout<<"Generating contact map with "<<beads_per_bin<<" beads per bin"
      <<"from dump files with "<<Npoly<<" polymer beads."<<endl;
  cout<<"Ignore interactions further than "<<range<<" beads"<<endl;
  cout<<"Skipping "<<skip<<" frames from the start, looking at every "<<every<<" frame"<<endl;


  // loop round dump files
  counter = 0;
  for (set<string>::iterator fn=infiles.begin(); fn!=infiles.end(); ++fn) {
    cout<<"Reading dump file "<<*fn<<"   ";

    inf.open( fn->c_str() );
    if ( !inf.good() ) {
      cout<<" ERROR while trying to open file "<<*fn<<endl;
      exit(EXIT_FAILURE);
    }

    // Loop through frames in dump file -------------------------------------------------------------
    for (int timeStep=0;timeStep<config.frames_per_file;timeStep++) {

      // read the frame
      config.read_frame(inf);

      if ( timeStep>=skip && timeStep%every == 0 ) {
	counter++;
	// get contacts
	for (int i=0;i<Npoly;i++) {
	  if ( i%100 == 0 ) {
	    cout<<"frame "<<timeStep+1<<" of "<<config.frames_per_file<<"     "
		<<"bead "<<i<<" \r"<<flush;
	  }
	  for (int j=i;j<=i+range&&j<Npoly;j++) {  // i<=j
	    if ( config[i].sep_shortest(config[j],config.L) < thresh ) {
	      contacts[ beadij2binij(i,j,beads_per_bin) ] ++;
	    }
	  }
	}
      } // end if

    } // end frames loop
    inf.close();

  }
  cout<<endl;

  // finish mean
  for (map<pair<int,int>, double>::iterator it=contacts.begin(); it!=contacts.end(); ++it) {
    it->second /= double(counter);
  }


  // Output
  ouf.open( outfile.begin()->c_str() );
  ouf<<"# i, j, count"<<endl;
  for (int i=0;i<Npoly;i++) {
    for (int j=i;j<Npoly;j++) {  // i<j
      if ( contacts.find(beadij2binij(i,j,beads_per_bin))!=contacts.end() ) {
	ouf<<beadij2binij(i,j,beads_per_bin).first+halfbinwidth<<" "
	    <<beadij2binij(i,j,beads_per_bin).second+halfbinwidth<<" "
	    <<contacts[ beadij2binij(i,j,beads_per_bin) ]/scale<<endl;
      }
    }
  }
  ouf.close();


}

