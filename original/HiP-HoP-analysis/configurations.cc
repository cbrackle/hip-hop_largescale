//##########################################################################
//
// Set of data structures and functions for describing a simulation 
// confuguraiton
//
// Dump files are assumed to have same number of atoms per frame,
// and have line format:
//                        id type xs ys zs ix iy iz
// (xs etc. are in range 0-1 so have to be converted to box coords)
// (assumes id starts from 1 and runs to N)
// 
//##########################################################################

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <map>

#include "configurations.h"

using namespace std;


// Member functions of atom

double atom::sep_shortest(const atom &b,const double L[3]) const {
  // return separation of this atom and b
  // looking for shortest arround periodic boundaries

  double dx[3],
    sumSq=0;
    
  for (int i=0;i<3;i++) {
    dx[i]=x[i]-b.x[i]; 
    if (dx[i]>0.5*L[i]) { // make sure its shortest distance
      dx[i]=dx[i]-L[i];
    } else if (dx[i]<-0.5*L[i]) {
      dx[i]=dx[i]+L[i];
    }
    sumSq += dx[i]*dx[i];
  }

  //cout<<sqrt(sumSq)<<" "<<L[0]<<" "<<dx[0]<<" "<<b.x[0]<<endl;
  return sqrt( sumSq );

}

bool atom::operator<(const atom &b) const {
  return id < b.id;
}

atom atom::operator+(const atom &b) const {
  // add atoms vectorially
  // all other variables are set from the first atom
  atom newatom=*this; // start with a copy of the calling instance
  for (int i=0;i<3;i++) {
    newatom.x[i] = x[i]+b.x[i];
  }
  return newatom;
}

atom atom::operator-(const atom &b) const {
  // subtract atoms vectorially
  // all other variables are set from the first atom
  atom newatom=*this; // start with a copy of the calling instance
  for (int i=0;i<3;i++) {
    newatom.x[i] = x[i]-b.x[i];
  }
  return newatom;
}

atom atom::operator/(const int &s) const {
  // Divide by a scalar
  atom newatom=*this; // start with a copy of the calling instance

  for (int i=0;i<3;i++) {
    newatom.x[i] /= double(s);
  }

  return newatom;
}

double atom::dot(const atom &b) const {
  // dot product of two atom positions
  double d = 0.0;
  for (int i=0;i<3;i++) {
    d += x[i]*b.x[i];
  }
  return d;
}

void atom::unwrap(const double L[3]) {
  // unwrap the atom coordinate -- but only if it hasn't been done already

  if (unwrapped==0) {
    unwrapped = 1;
    for (int i=0;i<3;i++) {
      x[i] = x[i] + ix[i]*L[i];
    }
  }
}


double atom::sep_unwrapped(const atom &b,const double L[3]) const {
  // return separation of this atom and b
  // but using unwrapped coords 
  // -- doesn't make sence for proteins, unless it is itself 
  //    at a previous time step

  double Ax[3],
    Bx[3],
    dx,
    sumSq=0;
    
  for (int i=0;i<3;i++) {
    if (unwrapped) {
      Ax[i] = x[i];
      Bx[i] = b.x[i];
    } else {
      Ax[i] = x[i] + ix[i]*L[i];
      Bx[i] = b.x[i] + b.ix[i]*L[i];
    }
  }

  for (int i=0;i<3;i++) {
    dx = Ax[i]-Bx[i]; 
    sumSq += dx*dx;
  }

  return sqrt( sumSq );
}



double atom::length() const {
  // return the length of the vector
  double d = 0.0;
  for (int i=0;i<3;i++) {
    d += x[i]*x[i];
  }
  return sqrt(d);
}




// Member functions of configuration

configuration::configuration(const string &fn) {
  // Constructor which reads some initial info from the file

  ifstream inf;
  string line;
  stringstream sline;
  double dub1,dub2;

  inf.open( fn.c_str() );
  if ( !inf.good() ) {
    cout<<" ERROR while trying to open file "<<fn<<endl;
    exit(0);
  }


  // get number of atoms, box size, and start time
  getline(inf,line);
  getline(inf,line); sline.str(line); sline>>starttime; sline.clear();
  getline(inf,line);
  getline(inf,line); sline.str(line); sline>>N; sline.clear();
  getline(inf,line);
  getline(inf,line); sline.str(line); sline>>dub1>>dub2; L[0]=dub2-dub1; sline.clear();
  getline(inf,line); sline.str(line); sline>>dub1>>dub2; L[1]=dub2-dub1; sline.clear();
  getline(inf,line); sline.str(line); sline>>dub1>>dub2; L[2]=dub2-dub1; sline.clear();
  getline(inf,line);
  for (int i=0;i<N;i++) {getline(inf,line);} // read first frame
  getline(inf,line);
  getline(inf,line); sline.str(line); sline>>timeinterval; sline.clear();
  timeinterval=timeinterval-starttime;
  
  inf.close();

  // set up map of atoms
  for (int i=1;i<=N;i++) {  // atom ids run from 1 to N
    at[i] = atom();
  }

  // get number of frames in the file
  FILE *pee;
  char command[200];
  int flen;

  sprintf(command,"wc -l %s | awk '{ print $1 }'",fn.c_str());
  pee=popen(command,"r");
  fscanf(pee,"%u",&flen);
  pclose(pee);
  flen=flen;

  frames_per_file=flen/(N+9);

}

configuration::configuration() {
  // Constructor which doesn't set anything

}

configuration::~configuration() {
  // Destructor for the configuration struct
}

const atom &configuration::operator[] (const int &i) const {
  // return a constant reference to the ith atom
  return at.at(i);
}

void configuration::read_frame(ifstream &inf)  {
  // function which reads a frame from a dump file

  string line;
  stringstream sline;
  atom new_atom;

  // read 9 lines of header
  for (int i=0;i<9;i++) {getline(inf,line);} // lines of junk

  // read configuration from file
  // lets put them in order of id
  for (int i=0;i<N;i++) {
    getline(inf,line);
    sline.str(line);

    new_atom = atom();  // reset the atom
    sline>>new_atom.id; // ids start at 1; so id-1 is index
    sline>>new_atom.type;
    sline>>new_atom.x[0]>>new_atom.x[1]>>new_atom.x[2];
    sline>>new_atom.ix[0]>>new_atom.ix[1]>>new_atom.ix[2]; 
    // scale coords to box size
    for (int i=0;i<3;i++) {
      new_atom.x[i]*=L[i];
    }
    at.at( new_atom.id ) = new_atom; // copy the atom into the map

    sline.clear();
  }

}

set<int> configuration::get_list_of_type(const int& T) const {
  // return a list of all the ids of the atoms of the specified type
  set<int> list;

  for (int i=0;i<N;i++) {
    if ((*this)[i].type == T) {
      list.insert(i);
    }
  }

  return list;
}

map<int,atom> configuration::get_map_of_type(const set<int> &T) const {
  // return a map with of all of the atoms of the specified type
  // with the id as the key
  map<int,atom> list;

  for (int i=0;i<N;i++) {
    if ( T.find( (*this)[i].type ) != T.end() ) {
      list[i] = (*this)[i];
    }
  }

  return list;
}


atom configuration::CoM_unwrapped(const set<int> &list) const {
  // returns the centre of mass of a list of atoms
  // uses unwrapped coords
  atom theCoM, 
    to_add;
  theCoM.unwrapped=1; // set flag for unwrapped coords

  for (set<int>::const_iterator it=list.begin(); it!=list.end(); ++it) {
    if ( (*this)[*it].unwrapped ) {
      theCoM = theCoM + (*this)[*it];
    } else {
      to_add = (*this)[*it];
      to_add.unwrap( L );
      theCoM = theCoM + to_add;
    }
  }

  theCoM = theCoM / list.size();

  return theCoM;
}


int configuration::bp2beadinregion(const long int &bp) const {
  // given a bp position, return a bead id within the region
  return 1+int( ( bp-regstart )/bp_per_bead );
}
