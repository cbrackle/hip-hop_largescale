//##########################################################################
//
// HEADER FOR:
// Functions for random numbers 
//
//##########################################################################

#ifndef RANDOM_H
#define RANDOM_H

using namespace std;

struct randoms {
  bool initialized;

  randoms();
  void initialize(const int&);
  int integer(const int&, const int&);
  double doub();
  int intFromSet(const set<int> &);

};


#endif
