//##########################################################################
//
// Functions for random numbers 
//
//##########################################################################

#include <utility>
#include <cstdlib>
#include <iostream>
#include <set>

#include "random.h"

//### Memeber functions of random
randoms::randoms() {
 initialized=0;
}

void randoms::initialize(const int &seed) {
  initialized=1;
  srand(seed);
}

int randoms::integer(const int &start, const int &end) {
  // return a random integer between start and end inclusive
  if ( initialized==0 ) {
    cout<<"Error : using random numbers which have not been initialized."<<endl;
    exit(0);
  }
  return start + ( rand() % (int)(end-start+1) ) ;
}

int randoms::intFromSet(const set<int> &theset) {
  // return a random integer from a set of the,
  if ( initialized==0 ) {
    cout<<"Error : using random numbers which have not been initialized."<<endl;
    exit(0);
  }
  set<int>::const_iterator it = theset.begin();
  advance( it, integer( 0,theset.size()-1 ) );
  return *it;
}

double randoms::doub() {
  // return a random double between 0 and 1
  if ( initialized==0 ) {
    cout<<"Error : using random numbers which have not been initialized."<<endl;
    exit(0);
  }
  return (double) rand() / (RAND_MAX) ;
}


