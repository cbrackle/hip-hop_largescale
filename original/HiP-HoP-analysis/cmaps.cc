//##########################################################################
//
// Functions for contact maps 
//
//##########################################################################

#include <utility>

#include "cmaps.h"

using namespace std;

pair<int,int> beadij2binij(const int &i,const int &j,const int &bpp) {
  // get bins and return as pair with i<j
  pair<int,int> binij;

  // make sure i<j
  if (i<=j) {
    binij.first = bpp * int(i/bpp);
    binij.second = bpp * int(j/bpp);
  } else {
    binij.first = bpp * int(j/bpp);
    binij.second = bpp * int(i/bpp);
  }

  return binij;
}
