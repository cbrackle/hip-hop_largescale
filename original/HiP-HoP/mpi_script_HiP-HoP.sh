#!/bin/sh

#$ -N HiPHoP_cell # job name  -- stored in JOB_NAME variable
#$ -V           # use all shell environment variables
#$ -cwd         # run job in working directory
#$ -j y         # merge stdout and stderr to one file

# Choose a queue:
#### #$ -q cdt.7.day   ## -- comment this out, it will go to any available queue

# Choose a parallel environment:
#$ -pe mpi 48          ## -- stored in NSLOTS variable

# Set job runtime
#$ -l h_rt=168:00:00   ## -- request 7 days, will go to 7.day queue (does not affect priority)

module load openmpi/1.10    ## -- specify version explicitly. version 3.0.2 seems not to work (random crashes)

##---- Create local directory and copy there ----##
export MYHOME=`pwd`
export MYSCRATCH=/scratch/cbrackle/${JOB_NAME}

mkdir -p $MYSCRATCH
cp -r ./* ${MYSCRATCH}/
cd $MYSCRATCH


##---- Print some messages ----##
echo -e "\n#############################################################"
date
hostname
echo "Working in directory $MYSCRATCH"
echo -e "#############################################################\n"

TIMEFORMAT=$'\n##################################\nRun time was %0lR\n##################################\n'

##---- Run the simulations (and time them) ----##
time mpirun --use-hwthread-cpus -n $NSLOTS ./run_extruders.py -p params.txt -c CTCF_beads.txt

# the '--use-hwthread-cpus' is now needed since version updates

##---- Print some messages ----##
echo "Job ran on $( hostname ) " 
