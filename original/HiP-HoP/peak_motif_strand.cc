///////////////////////////////////////////////////////////////////////////////
//
// Read bed file with list of motifs and names CTCF peaks
// Keep one motif per peak.
// Assign strand based on highest scoring motif within the peak
// Possibilities are : +
//                     -
//                     .    where the score of a motif on one strand
//                          is only ??% or less stronger than a motif
//                          on the other.
// 
// bed file must have the following columns
//         chr  ss  ee  name  peakscore  strand  motifscore
//
///////////////////////////////////////////////////////////////////////////////

#define DIFFTHRESH 5  // % threshold where difference is considered small

#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <algorithm> 

using namespace std;

struct bedline {

  string line,
    chr,ss,ee,name,peakscore,strand;
  double motifscore;

  bedline(const string &);
  bool operator<(const bedline&) const;

};

bool peekbedline(ifstream &,const string &);


int main(int argc, char *argv[]) {

  if (argc!=3) {
    cout<<"Usage :"<<endl;
    cout<<"       ./peak_motif_strand infile outfile"<<endl;
    cout<<"where       infile    is the input bed file."<<endl;
    cout<<"            outfile   is the output file."<<endl;
    cout<<"and the input bed file must have the following columns:"<<endl;
    cout<<"      chr  ss  ee  name  peakscore  strand  motifscore"<<endl;
    exit(EXIT_FAILURE);
  }

  string infile=string(argv[1]),
    outfile=string(argv[2]);

  // set up variables
  ifstream inf;             // file stream for input of dump file
  ofstream ouf;             // file streams for output

  string line;
  stringstream sline;

  vector<bedline> bedlines;

  double percent_diff,
    percent_thresh=DIFFTHRESH;

  bool allsamestrand;

  // test output file is not already there
  inf.open( outfile.c_str() );
  if ( inf.good() ) {
    cout<<" ERROR : File "<<outfile<<" already exists. Will not overwrite."<<endl;
    exit(0);
  }
  inf.close();

  ouf.open( outfile.c_str() );

  inf.open( infile.c_str() );
  if ( !inf.good() ) {
    cout<<" ERROR while trying to open file "<<infile<<endl;
    exit(EXIT_FAILURE);
  }

  do {
    // read a bed line
    getline(inf,line);
    bedlines.clear();
    bedlines.push_back( bedline(line) );

    // read more if they have the same name
    while ( peekbedline(inf,bedlines.begin()->name) ) {
      // if the next line has the same name, get it
      getline(inf,line);
      bedlines.push_back( bedline(line)  );
    }

    if ( bedlines.size() == 1 ) {
      // There is only one motif in this peak
      ouf<<line<<endl; 
    } else {

      // sort the vector into smallest -> largest
      sort(bedlines.begin(), bedlines.end()); 
 
      allsamestrand=true;
      for (int i=0;i<bedlines.size();i++) {
	for (int j=i+1;j<bedlines.size();j++) {
	  if ( bedlines[i].strand != bedlines[j].strand ) {
	    allsamestrand=false;
	  }
	}
      }
  
      if ( allsamestrand ) {
	// they are all on the same strand, just output the highest scoring
	ouf<<bedlines.back().line<<endl;
      } else {

	if ( bedlines.back().strand == (bedlines.rbegin()+1)->strand ) {
	  // top two are on the same strand, just output the highest scoring
	  ouf<<bedlines.back().line<<endl;
 
	} else {
	  percent_diff = 100*( bedlines.back().motifscore - 
			       (bedlines.rbegin()+1)->motifscore)
	    /bedlines.back().motifscore;

	  if ( percent_diff > percent_thresh ) {
	    // different is big, just output the highest scoring
	    ouf<<bedlines.back().line<<endl;
	  } else {
	    // difference is small -- say its on both
	    ouf<<bedlines.back().chr<<"\t"
		<<bedlines.back().ss<<"\t"
		<<bedlines.back().ee<<"\t"
		<<bedlines.back().name<<"\t"
		<<bedlines.back().peakscore<<"\t"
		<<"."<<"\t"
		<<bedlines.back().motifscore<<endl;
	  }
	}

      }

    }

  } while ( inf.peek() != EOF );
  inf.close();

  ouf.close();

}


bedline::bedline(const string &l) : line(l) {
  stringstream sline;
  sline.str(line);
  sline>>chr>>ss>>ee>>name>>peakscore>>strand>>motifscore;
}

bool bedline::operator<(const bedline &b) const {
  return motifscore<b.motifscore;
}


bool peekbedline(ifstream &ff, const string &nn) {
  // look at the next line in the file
  // compare name with current name
  // return TRUE if they match
  // return FALSE if they match

  string line,
    junk,
    nextname;
  stringstream sline;
  
  // peak at next line - return false if it is EOF
  if ( ff.peek() == EOF ) {
    return false;
  }

  // save the file position
  streampos fileposition = ff.tellg();

  // get next line
  getline(ff,line);
  sline.clear(); sline.str(line);
  sline>>junk>>junk>>junk>>nextname;

  // return to that file position
  ff.seekg( fileposition );

  if ( nextname==nn ) {
    // the next line if from the same peak
    return true;
  } else {
    // the next line is new 
    return false;
  }

}
