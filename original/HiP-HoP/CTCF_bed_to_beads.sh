#!/bin/bash

## Script to convert a CTCF peaks bed file to a list of beads 

if [[ $# != 3 ]];then
    echo "Usage : CTCF_bed_to_beads.sh configfile CTCFbedfile outputfile"
    echo "     where configfile      is HiP-HoP configuration file"
    echo "           CTCFbedfile     bed file of CTCF sites where column 5 is "
    echo "                           the ChIP peak strength and column 6 is the strand"
    echo "           outputfile      is the output file name for the beads list"
    exit 1
fi

configfile=$1
CTCFbedfile=$2
outputfile=$3

if [[ ! -f $configfile ]];then
    echo "Error : cannot find file $$configfile"
    exit 1
fi

if [[ ! -f $CTCFbedfile ]];then
    echo "Error : cannot find file $CTCFbedfile"
    exit 1
fi

if [[ -f $outputfile ]];then
    echo "Error : file $outputfile already exists, will not overwrite"
    exit 1
fi

#################################################################################

# lets say top 10% of CTCFs are always on
# and the minimum prob of occupancy is 0.05
nctcf=$( wc -l $CTCFbedfile | awk '{print $1}' ) 
topscore=$( sort -k5,5nr $CTCFbedfile | head -n $( awk "BEGIN{ print int(0.1*$nctcf) }" ) | tail -n 1 | awk '{print $5 }' )
min=$( awk 'BEGIN{mmin=1e6}{if (mmin>$5) {mmin=$5}}END{print mmin}' $CTCFbedfile )

# get region from config file
regS=$( awk '{if (NR==2) print}' $configfile )
regE=$( awk '{if (NR==3) print}' $configfile )
chr=$( awk '{if (NR==1) print}' $configfile )
bpb=$( awk '{if (NR==4) print}' $configfile )


awk -v aa=$min -v bb=$topscore -v rS=$regS -v rE=$regE -v rC=$chr -v bpb=$bpb '{
bead=0.5*($2+$3)
bead=1+int((bead-rS)/bpb)
strand=$6
prob=0.95*($5-aa)/(bb-aa)+0.05
if (prob>1) {prob=1}
if ($1==rC && $3>=rS && $2<=rE) print bead,strand,prob
}' $CTCFbedfile > temp_${outputfile}


# need to combine multiple CTCF which are on the same bead
# first check max number of peaks per bead
awk '{H[$1]++}END{
for (i in H) {
  if (H[i]>2) {
    print "WARNING - more than 2 peaks per bead",i,H[i]
  }
}
}' temp_${outputfile} 



# the following assumes the max is 2 peaks per bead

sort -k1,1n temp_$outputfile | awk '
function max(x,y) { if (x>y) {return x} else {return y}}
{
if ($1 in stra) {
  # bead is already in list
  if ($2!=stra[$1]) {
    # different strand
    stra[$1]="."
    pr[$1]=max($3,pr[$1])
  } else {
    # same strand
    pr[$1]=$3*(1-pr[$1]) + (1-$3)*pr[$1] + $3*pr[$1]
  }
} else {
stra[$1]=$2; pr[$1]=$3;
}
}END{
for (i in stra) {print i,stra[i],pr[i]}
}' | sort -k1,1n > $outputfile

rm temp_$outputfile
