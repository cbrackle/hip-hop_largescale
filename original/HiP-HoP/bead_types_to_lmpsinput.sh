#!/bin/bash

##-------------------------------------------------------------##
##-- Script to renumber atom types in a lammps input file ---- ##
##-- 
##-- Usage :
##--       ./bead_types_to_lmpsinput.sh beadlist infile outfile
##--        where beadlist is the list of atom types
##--              infile is the first of the lammps input files
##--              outfile is the first of the lammps output file
##--       Assumes input has polymer beads as type 1, and 
##--       protein bead as types >1
##--       Assumes bead ids present in beadlist are polymer 
##--       beads and all others are proteins
##--
##-------------------------------------------------------------##

# check arguments
if [ $# -ne 3 ]; then
    echo ""
    echo " Usage :"
    echo "       ./bead_types_to_lmpsinput.sh beadlist infile outfile"
    echo "        where beadlist is the list of atom types"
    echo "              infile is the first of the lammps input files"
    echo "              outfile is the first of the lammps output file"
    echo " Assumes input has polymer beads as type 1, and"
    echo " protein bead as types >1"
    echo " Assumes bead ids present in beadlist are polymer"
    echo " beads and all others are proteins"
    exit 1
fi

typesfile=$1
infile=$2
outfile=$3

# check files
if ! [ -f $typesfile ]; then
    echo "ERROR :: File $typesfile not found."
    exit 1
fi

if [ -e ${outfile} ]; then
    echo "ERROR :: File ${outfile} already exists."
    exit 1
fi

if ! [  -f ${infile} ]; then
    echo "ERROR :: File ${infile} not found."
    exit 1
fi


# load the types files
#declare -A type
Ndna=0
while read -r line
do
    a=($line)
    if ! [ ${a[0]} == "#" ]; then
	type[${a[0]}]=${a[1]}
	Ndna=$(( $Ndna + 1 ))
    fi
done < $typesfile

# set protein types ---- ACTUALLY THIS IS NOT USED - PROTEINS JUST HAVE Ndnatypes-1 added to their number
Ndnatypes=$( awk 'BEGIN{max=0;}{if ($1!="#"&&$2>max) {max=$2}}END{print max}' $typesfile )
Natoms=$( awk '{if ($2=="atoms") {print $1}}' ${infile} )
i=$(( $Ndna + 1 ))
while [ $i -le $Natoms ]; do
    type[$i]=$(( $Ndnatypes + 1 ))
    i=$(( $i + 1 ))
done
if [ -f tmp_types ]; then
    rm tmp_types
fi
for (( i=1; i<=$Natoms ; i++ )); do
    echo "$i ${type[$i]}" >> tmp_types
done

echo "Reading types file $typesfile"
echo "Editing lammps inputs with $Ndna polymer atoms"
echo "                           $Natoms total atoms"
echo "                           $Ndnatypes polymer atom types"
echo "                           $(( $Ndnatypes + 1 )) total atom types"

# edit files

fn=${infile}
ofn=${outfile}

echo -ne "Converting file $fn to $ofn \r"

# split originalfile into three parts
awk 'BEGIN{A=0;}{
if (A==0) {print;}
if ($1=="Atoms") {A=1;}
}' $fn > tmp_top
awk 'BEGIN{A=0;}{
if (A==1 && $1=="Velocities") {A=0;}
if (A==1&&$0!="") {print;}
if ($1=="Atoms")  {A=1;}
}' $fn > tmp_middle
awk 'BEGIN{B=0;}{
if ($1=="Velocities") {B=1;}
if (B==1) {print;}
}' $fn > tmp_bottom

# sort atoms
sort -g tmp_middle > tmp_mid_srt

# change bead types
paste tmp_types tmp_mid_srt | awk -v Nd=$Ndna -v NdT=$Ndnatypes '{
if ($1<=Nd) {$5=$2;$1="";$2="";print}
else {$5=$5+NdT-1;$1="";$2="";print}
}' > tmp_middle

Ntotaltypes=$( awk 'BEGIN{m=0}{if ($3>m) {m=$3}}END{print m}' tmp_middle )

# edit top of file
awk -v at=$Ntotaltypes '{if ($2=="atom"&&$3=="types") {$1=at}; print}' tmp_top > tmp_top2
awk -v dt=$Ntotaltypes '{ if ($0=="Masses") {print "Masses\n"; for (i=1;i<=dt;i++) {print i,1}; print ""; print "Atoms\n"; exit 1;}; print}' tmp_top2 > tmp_top

# stick them all back together again
cat tmp_top tmp_middle > $ofn
echo "" >> $ofn
cat tmp_bottom >> $ofn

rm tmp_top tmp_top2 tmp_middle tmp_bottom tmp_mid_srt

rm tmp_types
echo ""
echo "Done."
