#!/bin/bash


if [[ $# != 4 ]];then
    echo "Usage : add_crumple_springs.sh typesfile typestring oldlammpsinput newlammpsinput"
    echo "     where typesfile       is the bead types file"
    echo "           typestring      is the bead attribute where there are NO crumple springs (e.g. K27ac)"
    echo "           oldlammpsinput  is the input lammps initial condition file"
    echo "           newlammpsinput  is the outputl ammps initial condition file"
    exit 1
fi

typesfile=$1
typestring=$2
oldlammpsinput=$3
newlammpsinput=$4

if [[ ! -f $typesfile ]];then
    echo "Error : cannot find file $typesfile"
    exit 1
fi

if [[ ! -f $oldlammpsinput ]];then
    echo "Error : cannot find file $oldlammpsinput"
    exit 1
fi

if [[ -f $newlammpsinput ]];then
    echo "Error : file $newlammpsinput already exists"
    exit 1
fi

typeslist=$( awk -v ts=$typestring '{if ($1=="#" && $2=="type") { 
split($0,aa," "); split("",bb, ":")
for (i in aa) {bb[aa[i]]=aa[3]}
if (ts in bb) {line=line " " $3}
}}END{print line}' $typesfile )
typeslist=($typeslist)

if [[ ${#typeslist[@]} == 0 ]]; then
    echo "Error : did not find bead types with $typestring"
    exit 1
fi


echo "Reading file $typesfile"
echo "Adding springs between all beads except those with $typestring"
echo "(that is bead types ${typeslist[@]})"



# lets put an extra next-nearest neighbour spring between
# any triplet of beads where none have the acetylaton mark

awk -v tl="${typeslist[*]}" 'BEGIN{
  N=0;
  split(tl,aa," ")
  for (i in aa) {types[aa[i]]-1}
}{
  if ($1!="#") {N++
    if ($2 in types) {bead[$1]=1} else {bead[$1]=0}
  }
}END{
  count=1;
  for (i=3;i<=N;i++) {
    if (bead[i-2]==0 && bead[i-1]==0 && bead[i]==0) {print count,2,i-2,i; count++}
  }
}' $typesfile > list_of_extra_springs.dat

Nsprings=$( wc -l list_of_extra_springs.dat | awk '{print $1}' )

echo "(which means adding $Nsprings springs)"

awk -v Ns=$Nsprings -v bf="list_of_extra_springs.dat" 'BEGIN{B=0;C=0;
while ( (getline<bf)>0 ) { newbond[$1]=$2 " " $3 " " $4; }
}{

if ($2=="bonds") {oN=$1; $1+=Ns;}
if ($2=="bond" && $3=="types") {$1++}

if ($1=="Bonds") {B=1}

if (B==1 && C==1 && $1=="") { for (i=1;i<=Ns;i++) {print oN+i,newbond[i]} }

if (B==1 && C==0 && $1=="") {C++}

print
if ($1=="Angles") {B=0}
}' $oldlammpsinput > $newlammpsinput


