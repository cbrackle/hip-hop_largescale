/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef COMPUTE_CLASS

ComputeStyle(target2,ComputeTarget2)

#else

#ifndef LMP_TARGET2_CLOSEST_H
#define LMP_TARGET2_CLOSEST_H

#include "compute.h"

namespace LAMMPS_NS {

class ComputeTarget2 : public Compute {
 public:
  ComputeTarget2(class LAMMPS *, int, char **);
  void init();
  void compute_vector();

 private:
  double pfactor;
  int target_hit;
  int prot_tag, target_tag;
  double thresh;
 
};

}

#endif
#endif

/* ERROR/WARNING messages:

E: Illegal ... command

Self-explanatory.  Check the input script syntax and compare to the
documentation for the command.  You can use -echo screen as a
command-line option when running LAMMPS to see the offending line.

*/
