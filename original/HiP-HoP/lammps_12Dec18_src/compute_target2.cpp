/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "string.h"
#include "mpi.h"
#include "compute_target2.h"
#include "atom.h"
#include "update.h"
#include "force.h"
#include "domain.h"
#include "group.h"
#include "error.h"
#include "cmath"
#include <cstdlib>
//#include <iostream>

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

ComputeTarget2::ComputeTarget2(LAMMPS *lmp, int narg, char **arg) :
  Compute(lmp, narg, arg)
{
  if (narg != 7) error->all(FLERR,"Illegal compute target2 command");

  vector_flag = 1;
  size_vector=2;
  // extscalar = 1;

  vector = new double[2];

  target_hit=0;

  prot_tag=atoi(arg[3]);
  target_tag=atoi(arg[4]);
  if ( strcmp(arg[5],"thresh")!=0 ) error->all(FLERR,"Illegal compute target2 command - thresh");
  thresh=atof(arg[6]);

}

/* ---------------------------------------------------------------------- */

void ComputeTarget2::init()
{
 
  
}

/* ---------------------------------------------------------------------- */

void ComputeTarget2::compute_vector()
{
  //invoked_scalar = update->ntimestep;
  invoked_vector = update->ntimestep;

  double **x = atom->x;
  int *tag = atom->tag;
  // int *mask = atom->mask;
  int nlocal = atom->nlocal;
  double box[3];
   box[0] = domain->xprd;
   box[1] = domain->yprd;
   box[2] = domain->zprd;

  int findatom=-1;
  double px, py, pz, p_target[3], p_protein[3];
  double sep[3];

  double target_dist = 0.0;

  // Find atom tagged number target_tag
  px=2e9; py=2e9; pz=2e9; findatom=-1;
  for (int i = 0; i < nlocal; i++) { if (tag[i]==target_tag) findatom=i; }
  if (findatom != -1) {
    px=x[findatom][0];py=x[findatom][1];pz=x[findatom][2];
  }
  MPI_Allreduce(&px,&p_target[0],1,MPI_DOUBLE,MPI_MIN,world);
  MPI_Allreduce(&py,&p_target[1],1,MPI_DOUBLE,MPI_MIN,world);  
  MPI_Allreduce(&pz,&p_target[2],1,MPI_DOUBLE,MPI_MIN,world);


  // Find atom tagged number prot_tag
  px=2e9; py=2e9; pz=2e9; findatom=-1;
  for (int i = 0; i < nlocal; i++) { if (tag[i]==prot_tag) findatom=i; }
  if (findatom != -1) {
    px=x[findatom][0];py=x[findatom][1];pz=x[findatom][2];
  }
  MPI_Allreduce(&px,&p_protein[0],1,MPI_DOUBLE,MPI_MIN,world);
  MPI_Allreduce(&py,&p_protein[1],1,MPI_DOUBLE,MPI_MIN,world);  
  MPI_Allreduce(&pz,&p_protein[2],1,MPI_DOUBLE,MPI_MIN,world);

   for (int j=0;j<3;j++) {
    sep[j]=p_target[j]-p_protein[j];
    if (sep[j]>0.5*box[j]) sep[j]-=box[j];
    if (sep[j]<-0.5*box[j]) sep[j]+=box[j];
  }

  target_dist = sqrt( pow( sep[0] ,2.0) + pow( sep[1] ,2.0) +  pow( sep[2] ,2.0) );

  if (target_dist<thresh) target_hit=1; // All processors have the same value of target_dist, so get the same value for target_hit

  vector[0]=target_dist;
  vector[1]=target_hit;

}
