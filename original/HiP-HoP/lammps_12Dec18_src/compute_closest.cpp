/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "mpi.h"
#include "compute_closest.h"
#include "atom.h"
#include "update.h"
#include "force.h"
#include "domain.h"
#include "group.h"
#include "error.h"
#include "cmath"
//#include <iostream>

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

ComputeClosest::ComputeClosest(LAMMPS *lmp, int narg, char **arg) :
  Compute(lmp, narg, arg)
{
  if (narg != 4) error->all(FLERR,"Illegal compute closest command");

  vector_flag = 1;
  size_vector=2;
  // extscalar = 1;

  whichid = force->inumeric(FLERR,arg[3]);

  vector = new double[2];

}

/* ---------------------------------------------------------------------- */

void ComputeClosest::init()
{
  //pfactor = 0.5 * force->mvv2e;
}

/* ---------------------------------------------------------------------- */

void ComputeClosest::compute_vector()
{
  invoked_scalar = update->ntimestep;

  double **x = atom->x;
  int *tag = atom->tag;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  double box[3];
   box[0] = domain->xprd;
   box[1] = domain->yprd;
   box[2] = domain->zprd;

  double test = 0.0;
  double sep[3];
  double closest=2e9;
  int which=0;
  int firstatom=-1;
  double px=2e9, py=2e9, pz=2e9, pxyz2[3];

  // Find atom tagged number 1
  for (int i = 0; i < nlocal; i++) { if (tag[i]==whichid) firstatom=i; }
  if (firstatom != -1) {
    px=x[firstatom][0];py=x[firstatom][1];pz=x[firstatom][2];
  }
  MPI_Allreduce(&px,&pxyz2[0],1,MPI_DOUBLE,MPI_MIN,world);
  MPI_Allreduce(&py,&pxyz2[1],1,MPI_DOUBLE,MPI_MIN,world);  
  MPI_Allreduce(&pz,&pxyz2[2],1,MPI_DOUBLE,MPI_MIN,world);


  for (int i = 0; i < nlocal; i++) {
    if (mask[i] & groupbit) {

      for (int j=0;j<3;j++) {
      sep[j]=x[i][j]-pxyz2[j];
      if (sep[j]>0.5*box[j]) sep[j]-=box[j];
      if (sep[j]<-0.5*box[j]) sep[j]+=box[j];
      }

      test = sqrt( pow( sep[0] ,2) + 
		   pow( sep[1] ,2) +
		   pow( sep[2] ,2) );
	if (test<closest) {
	  closest = test;
	  which = tag[i];
	}
    }
  }

  cd.dist=closest;
  cd.which=which;

  MPI_Allreduce(&cd,&cd2,1,MPI_DOUBLE_INT,MPI_MINLOC,world);
  vector[0]=cd2.dist;
  vector[1]=cd2.which;

}
