/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "mpi.h"
#include "compute_target.h"
#include "atom.h"
#include "update.h"
#include "force.h"
#include "domain.h"
#include "group.h"
#include "error.h"
#include <cmath>
//#include <iostream>

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

ComputeTarget::ComputeTarget(LAMMPS *lmp, int narg, char **arg) :
  Compute(lmp, narg, arg)
{
  if (narg != 3) error->all(FLERR,"Illegal compute target command");

  scalar_flag = 1;
  extscalar = 1;
}

/* ---------------------------------------------------------------------- */

void ComputeTarget::init()
{
  //pfactor = 0.5 * force->mvv2e;
}

/* ---------------------------------------------------------------------- */

double ComputeTarget::compute_scalar()
{
  invoked_scalar = update->ntimestep;

  double **x = atom->x;
  int *tag = atom->tag;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  double box[3];
   box[0] = domain->xprd;
   box[1] = domain->yprd;
   box[2] = domain->zprd;

   int findatom=-1;
  double px, py, pz, p_target[3], p_protein[3];
  double sep[3];

  double target_dist = 0.0;

  // Find atom tagged number 50
  px=2e9; py=2e9; pz=2e9; findatom=-1;
  for (int i = 0; i < nlocal; i++) { if (tag[i]==50) findatom=i; }
  if (findatom != -1) {
    px=x[findatom][0];py=x[findatom][1];pz=x[findatom][2];
  }
  MPI_Allreduce(&px,&p_target[0],1,MPI_DOUBLE,MPI_MIN,world);
  MPI_Allreduce(&py,&p_target[1],1,MPI_DOUBLE,MPI_MIN,world);  
  MPI_Allreduce(&pz,&p_target[2],1,MPI_DOUBLE,MPI_MIN,world);


  // Find atom tagged number 1
  px=2e9; py=2e9; pz=2e9; findatom=-1;
  for (int i = 0; i < nlocal; i++) { if (tag[i]==1) findatom=i; }
  if (findatom != -1) {
    px=x[findatom][0];py=x[findatom][1];pz=x[findatom][2];
  }
  MPI_Allreduce(&px,&p_protein[0],1,MPI_DOUBLE,MPI_MIN,world);
  MPI_Allreduce(&py,&p_protein[1],1,MPI_DOUBLE,MPI_MIN,world);  
  MPI_Allreduce(&pz,&p_protein[2],1,MPI_DOUBLE,MPI_MIN,world);

  for (int j=0;j<3;j++) {
    sep[j]=p_target[j]-p_protein[j];
    if (sep[j]>0.5*box[j]) sep[j]-=box[j];
    if (sep[j]<-0.5*box[j]) sep[j]+=box[j];
  }

  target_dist = sqrt( pow( sep[0] ,2.0) + pow( sep[1] ,2.0) +  pow( sep[2] ,2.0) );

  scalar = target_dist;
  return scalar;
}
