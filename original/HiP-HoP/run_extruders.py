#!/bin/env python3


### Driver program for LAMMPS with extruders
### Runs in parallel using mpi4py
### Requires LAMMPS to be built with new bond_style harmlj

### Reads in two files specificed at command line:
###     ctcf.dat       -- a list of CTCF beads with probability and direction
###     params.dat     -- a set of parameters for extrusion and switching

### Extruders bind at random to beads i,i+3 (compatible with crumpled polymer)
### Step forward stochastically with some rate (independant of configuration)
### Are halted by correctly oriented CTCFs, but move through otherwise
### Unbind with some rate (independant of CTCFs / polymer configuration)

### CTCF sites are populated stochastically based on probabilities in the file.

### Proteins with the specifed rates, with switching commands in an external file.


## The way mpi4py works is that calls to the lammps library must be 
## made on all processes. The way I have written the code is that any 
## reading/writting to files is done on rank 0. All stochastic stuff 
## is also done on rank 0. Then I broadcast any variables which have 
## changed before the next call to lammps. 


##########################################################################################
##########################################################################################

import random
import sys, getopt
import numpy
import array
from mpi4py import MPI
from lammps import lammps


def idx( id ):
    return 3*id-3

def idy( id ):
    return 3*id-2

def idz( id ):
    return 3*id-1

def sep(l,r,coords,box) :
    dx = coords[idx(l)]-coords[idx(r)]
    if ( abs(dx)>0.5*box[0] ) : 
        dx=box[0]-abs(dx)
    dy = coords[idy(l)]-coords[idy(r)]
    if ( abs(dy)>0.5*box[1] ) : 
        dy=box[1]-abs(dy)
    dz = coords[idz(l)]-coords[idz(r)]
    if ( abs(dz)>0.5*box[2] ) : 
        dz=box[2]-abs(dz)
    if ( numpy.sqrt( dx*dx + dy*dy + dz*dz )>50.0 ) :
        print("********************",dx,dy,dz)
    return numpy.sqrt( dx*dx + dy*dy + dz*dz )



### --------------------------------------------------------------------------------------------
### --------- Main program ---------------------------------------------------------------------
### --------------------------------------------------------------------------------------------


# set up MPI (it as initialised when the library was imported) 
comm = MPI.COMM_WORLD 
me = comm.Get_rank() 
nprocs = comm.Get_size()


### ----- Parse command line --------------------------------

# Initialize command line parameters
quiet = False                          # flag to switch off screen output
runnumber = 1                          # an id number for input and output files
paramsfile = None
ctcffile = None

try:
    opts, args = getopt.getopt(sys.argv[1:],"p:c:qn:",)
except getopt.GetoptError:
    print('run_extruders.sh -p <paramsfile> -c <ctcfile> [-q] [-n <runnumber>]')
    print('      -p <paramsfile> a file with the extruder/switching parameters')
    print('      -c <ctcfile>    a file with the CTCF beads')
    print('      -q              runs quietly without output to screen')
    print('      -n <runnumber>  specifies a run number, default 1.')
    sys.exit(1)
for opt, arg in opts:
    if (opt=="-p"):
        paramsfile = arg
    elif (opt=="-c"):
        ctcffile = arg
    elif (opt=="-q"):
        quiet = True
    elif (opt=="-n"):
        runnumber = int(arg)

if (paramsfile==None or ctcffile==None):
    print("Error: must specify a parameters file and a CTCFs file.")
    sys.exit(1)



### ----- Set parameters and read params file ---------------

# set some parameter values --- all times here are sumlation units (NOT STEPS)
dt = 0.01                                               # time step in simulation time units
septhresh = 4                                           # max distance for bond creation
neighbour_length = 3.5                                  # skin distance for neighbour list
neigh_every = 20                                        # parameter for neigh list rebuild
neigh_delay = 20                                        # parameter for neigh list rebuild
extrdr_bnd_type = 3                                     # bond type of extruders
extrdr_init_sep = 3                                     # initial bead separation of extruders
dump_every_steps = 200000                               # how often to dump (in STEPS)
seeddir = "/Home/cbrackle/work/seeds"                   # directory for seeds

# and load some parameters from the file
Nbeads = None                                           # number of DNA beads
Nextruders = None                                       # number of extruders
update_interval = None                                  # how often to update extruders in simulation time units
extrude_interval = None                                 # how often to move extruders 
on_interval = None                                      # how often attempt to add extruder
off_interval = None                                     # how often attempt to remove extruder
switch_interval = None                                  # how often switch protein
runtime = None                                          # total tun time in simulation time units

mapfile = "extruders_locations_%i.dat"%runnumber      # output file for extruders map

# get rest of parameter values from file
with open(paramsfile, "r") as pf:
    for line in pf:
        a = line.split()
        if a[0]=="nbeads":
            Nbeads = int(a[1])
        elif a[0]=="nextruders":
            Nextruders = int(a[1])
        elif a[0]=="extrude_interval":
            extrude_interval = float(a[1])
        elif a[0]=="update_interval":
            update_interval = float(a[1])
        elif a[0]=="on_interval":
            on_interval = float(a[1])
        elif a[0]=="off_interval":
            off_interval = float(a[1])
        elif a[0]=="switch_interval":
            switch_interval = float(a[1])
        elif a[0]=="runtime":
            runtime = int(a[1])
        elif line[0]!="#" or line=="\n":
            print("Error: unknown entry in parameters file: %s"%a[0])
            sys.exit(1)

if Nbeads==None or \
   Nextruders==None or \
   extrude_interval==None or \
   update_interval==None or \
   on_interval==None or \
   off_interval==None or \
   switch_interval==None or \
   runtime==None:
    print("Error: not all required parameters set in parameters file")
    sys.exit(1)

# set some variables based on the parameters

runtime_steps = int(runtime/dt)                            # runtime in timesteps
update_interval_steps = int(update_interval/dt)            # how often to do an extruder update in timesteps
Tintervals = int(runtime_steps/update_interval_steps)-1    # number of times extruders will be updated

extruders_per_bead = Nextruders/Nbeads
extrude_rate = 1.0/extrude_interval
extrude_rate_steps = extrude_rate*dt                       # rate at which extruders move in inverse timesteps
step_prob = update_interval_steps*extrude_rate_steps    # probability an extruder advances

onrate = 1.0/on_interval
offrate = 1.0/off_interval
onrate_steps = onrate*dt                                   # rate at which extruders attach in inverse timesteps
offrate_steps = offrate*dt                                 # rate at which extruders detach in inverse timesteps
add_prob = update_interval_steps*onrate_steps           # probability an extruder attaches
remove_prob = update_interval_steps*offrate_steps       # probability an extruder detaches

switch_rate = 1.0/switch_interval
switch_steps = switch_rate*dt                              
switch_prob = update_interval_steps*switch_steps        

# get random number seed
with open("%s/seed.%i"%(seeddir,runnumber),"r") as sf:
    seed = int( (sf.read()).split()[3] )
random.seed(seed)

# set up output streams for messages
if me==0: 
    if (quiet == False) :
        ostreams = [sys.stdout, open("extruders_%i.log"%runnumber,"w")]
    else :
        ostreams = [open("extruders_%i.log"%runnumber,"w")]
else:     
    ostreams = []


### ----- Read CTCFs file and set up CTCFs ------------------

# load CTCF information
sitesCTCF = ["0" for i in range(Nbeads+1)]
scoresCTCF = array.array('f', [0.0] * int(Nbeads+1) ) #[0.0 for i in range(Nbeads+1)]
ctcfsitescounter = 0
with open(ctcffile,'r') as cf:
    for line in cf:
        a = line.split()  
        bd = int(a[0])
        if ( bd<1 or bd>Nbeads ):
            print("Error: incorrect entry in ctcfs file (bead id)")
            sys.exit(1)
        if a[1]=="+":
            sitesCTCF[bd] = "F"
        elif a[1]=="-":
            sitesCTCF[bd] = "R"
        elif a[1]==".":
            sitesCTCF[bd] = "B"
        else:
            print("Error: incorrect entry in ctcfs file (direction)")
            sys.exit(1)
        sc = float(a[2])
        if sc<0 or sc>1:
            print("Error: incorrect entry in ctcfs file (probability)")
            sys.exit(1)
        scoresCTCF[bd] = sc
        ctcfsitescounter += 1

# stochastically assign CTCFs 
if me==0:
    CTCF = array.array('h', [0] * int(Nbeads+1) ) #["0" for i in range(Nbeads+1)]
    ctcffilledcounter = 0
    for i in range(0,len(scoresCTCF)):
        if ( sitesCTCF[i]!="0" and random.random() < scoresCTCF[i] ):
            if sitesCTCF[i]=="B":
                # if a peak is BOTH, then select 
                # F,R or B with 1:1:2 ratio 
                # Why? I don't know.
                if random.random() < 0.5:
                    CTCF[i]=2
                else:
                    if random.random() < 0.5:
                        CTCF[i]=1
                    else:
                        CTCF[i]=-1
            else:
                if sitesCTCF[i]=="F":
                    CTCF[i]=1
                elif sitesCTCF[i]=="R":
                    CTCF[i]=-1
            ctcffilledcounter += 1
else:     
    CTCF = None 
CTCF = comm.bcast(CTCF, root=0)


### ----- Set up arrays for run -----------------------------


# set up arrays for extruders
left = array.array('i') 
right = array.array('i') 

# set up arrays for beads, and where CTCFs are
occupied = array.array('H', [0] * int(Nbeads+1) ) 



### ----- Set up outputs ------------------------------------

# write output for where CTCFs were filled
if me==0:
    ctcfoutput = open("CTCF_map_%i.dat"%runnumber,"w")
    for i in range(1,len(CTCF)):
        ctcfoutput.write("%i %s\n"%(i,CTCF[i]))
    ctcfoutput.close()

# set up output file for extruders
if me==0:
    allextrudefile = open(mapfile,"w")
    allextrudefile.write("# timestep, left side, right side\n")



### ----- Set up LAMMPS -------------------------------------

# initialize LAMMPS 
lmp = lammps(cmdargs=["-screen","screen.%i.log"%runnumber,"-log","log.%i.log"%runnumber])
#lmp = lammps(cmdargs=["-log","log.%i.log"%runnumber])

lmp.commands_list(["units lj",
                   "atom_style angle",
                   "boundary p p p"])

# set some LAMMPS parameters

# run number
lmp.command("variable runnumber equal %i"%runnumber)

# neighbour parameters
lmp.commands_list(["neighbor %f bin"%neighbour_length,
                   "neigh_modify every %i delay %i check yes"%(neigh_every,neigh_delay)]),
#"comm_modify cutoff %f"%(neighbour_length+2.5)])

#set seed
lmp.command("variable seed equal %i"%seed)
# and set up a random number generator for switching
lmp.command("variable switchseed equal floor(random(1000,99999,%i))"%(seed*2))

# set from file: initial conditions / groups / force field / fixes / outputs
lmp.file("in.initial")

# time step
lmp.command("timestep %f"%dt)

# switching rate
lmp.command("variable k equal %f "%switch_prob)


# get some parameters from LAMMPS
boxsize = [ lmp.extract_global("boxxhi",1)-lmp.extract_global("boxxlo",1) , lmp.extract_global("boxyhi",1)-lmp.extract_global("boxylo",1), lmp.extract_global("boxzhi",1)-lmp.extract_global("boxzlo",1) ]

initialNbonds =  lmp.extract_global("nbonds",0) 

totalatoms = lmp.extract_global("natoms",0) 
Nprots = totalatoms - Nbeads


### ----- Write some messages to scree and/or log file ------

# write a message
if me==0:
    for messages in ostreams:
        messages.write("#*****************************************************************************************\n")
        messages.write("#  Running simulation %i on %i processors with %i polymer beads\n"%(runnumber,nprocs,Nbeads))
        messages.write("#                                              %i switching proteins\n"%Nprots)
        messages.write("#                                              %i extruders\n"%Nextruders)
        messages.write("#                                          for %i time steps\n"%runtime_steps)
        messages.write("#\n")
        messages.write("#  extruders are updated every %i timesteps\n"%update_interval_steps)
        messages.write("#  extruders advance every %f timesteps\n"%(1.0/extrude_rate_steps))
        messages.write("#  extruders detach after %f timesteps\n"%(1.0/offrate_steps))
        messages.write("#     after travelling on average %f beads\n"%(extrude_rate/offrate))
        messages.write("#  free extruders attach after %f timesteps\n"%(1.0/onrate_steps))
        messages.write("#  bridges switch on average every %f timesteps\n"%(1.0/switch_steps))
        messages.write("#\n")
        messages.write("# Probabilities: (try to keep these small)\n")
        messages.write("#  step_prob = %f\n"%step_prob)
        messages.write("#  add_prob = %f\n"%add_prob)
        messages.write("#  remove_prob = %f\n"%remove_prob)
        messages.write("#  switch_prob = %f       (%f proteins switch on each attempt)\n"%(switch_prob,switch_prob*Nprots))
        messages.write("#  random seed = %i\n"%seed)
        messages.write("#\n")
        messages.write("#  %i out of %i CTCF sites have been stochatically filled\n"%(ctcffilledcounter,ctcfsitescounter))
        messages.write("#*****************************************************************************************\n\n")


### ----- Do an intial run without extruders or sticky beads ----------------

lmp.command("run %i"%update_interval_steps)


if ( len(left) != lmp.extract_global("nbonds",0)-initialNbonds ) :
    print("ERROR - lost some bonds")
    sys.exit(1)


### ----- Switch on bead attractions ------------------------

## Actually, lets run with extruders only for the first million steps
#lmp.file("in.attraction")


### ----- Switch on dumping ------------------

lmp.command("dump dmp all custom %i dump.%i.DNA id type xs ys zs ix iy iz"%(dump_every_steps,runnumber))



### ----- Do the rest of the run ----------------------------

# run subsequent steps, updating according to step_prob
for step in range(1,Tintervals):
    if (me == 0 and quiet == False and step%1==0):
        sys.stdout.write("running %i   ( %.2f %% done )     \r"%(step*update_interval_steps,100.0*(step)/float(Tintervals)))
        sys.stdout.flush()
        allextrudefile.flush() # also flush the buffer for the extruders log

    # switch on attraction after 1 million steps
    if step*update_interval_steps==1e6:
        lmp.file("in.attraction")

    coords = lmp.gather_atoms("x",1,3)

    # output extruders positions
    if me==0:
        for i in range(0,len(left)): 
            allextrudefile.write("%i %i %i\n" %(step*update_interval_steps,left[i],right[i]) )


    # count the number of lammps commands issued in this round
    flag_didAcommand = 0

    # remove extruders
    commandslist = []     
    if me==0:
        for j in range(0,len(left)):
            if ( random.random() < remove_prob ):
                commandslist.append( "group newbond id %i %i" % (left[j],right[j]) )   # delete the old bonds
                commandslist.append("delete_bonds newbond bond %i remove special"%extrdr_bnd_type) # need special so that bond lists are recalculated properly
                commandslist.append("group newbond delete")
                occupied[left[j]]=0
                occupied[right[j]]=0
                left[j]=-1
                right[j]=-1
        left = array.array('i',[value for value in left if value != -1])
        right = array.array('i',[value for value in right if value != -1])
    # collect arrays and commands list
    left = comm.bcast(left, root=0) # left can change size, so have to use slow pickle version bcast    
    right = comm.bcast(right, root=0) # right can change size, so have to use slow pickle version bcast    
    comm.Bcast(occupied, root=0) #occupied = comm.bcast(occupied, root=0)  
    commandslist = comm.bcast(commandslist, root=0)     
    # now do lammps commands on all procs   
    if len(commandslist)>0:
        #commandslist.append("delete_bonds newbond bond %i remove special"%extrdr_bnd_type) # need special so that bond lists are recalculated properly
        #commandslist.append("group newbond delete")
        lmp.commands_list(commandslist)
        flag_didAcommand += 1 


    # update extruders 
    commandsREMlist = []
    commandsADDlist = []
    if me==0:
        for j in range(0,len(left)):      # left and right get updated here, but the length and order do not change
            lastleft = left[:]
            lastright = right[:]
            if ( left[j]-1>0 and occupied[left[j]-1]==0 and CTCF[left[j]-1]!=1 and CTCF[left[j]-1]!=2 and 
                 random.random() < step_prob ):
                occupied[left[j]] = 0
                left[j] -= 1
                occupied[left[j]] = 1
            if ( right[j]+1<=Nbeads and occupied[right[j]+1]==0 and CTCF[right[j]+1]!=-1 and CTCF[right[j]+1]!=2 and 
                 random.random() < step_prob):
                occupied[right[j]] = 0
                right[j] += 1
                occupied[right[j]] = 1
            if ( lastleft[j]!=left[j] or lastright[j]!=right[j] ):
                if ( sep(left[j],right[j],coords,boxsize) > septhresh ):
                    print("ERROR - bond cannot be made. Separation is ",sep(left[j],right[j],coords,boxsize),boxsize[0],boxsize[1],boxsize[2])
                    print(left[j],right[j],sep(left[j],right[j],coords,boxsize),"update")
                    sys.exit(1)
                commandsREMlist.append("group newbond id %i %i" % (lastleft[j],lastright[j]) ) # delete the old bonds
                commandsREMlist.append("delete_bonds newbond bond %i remove special"%extrdr_bnd_type) # need special so that bond lists are recalculated properly
                commandsREMlist.append("group newbond delete")

                #commandsADDlist.append("create_bonds single/bond %i %i %i special no"%(extrdr_bnd_type,left[j],right[j])) # special no here, but special yes for the last bond to be added
                commandsADDlist.append( "group newbond id %i %i" % (left[j],right[j]) ) # add a new bonds                 
                commandsADDlist.append( "create_bonds many newbond newbond %i 0.0 %f"%(extrdr_bnd_type,septhresh) )                 
                commandsADDlist.append( "group newbond delete" )
    # collect arrays and commands list
    # (can use fast Bcast version since left and right do not change size) 
    comm.Bcast(left, root=0) #left = comm.bcast(left, root=0)     
    comm.Bcast(right, root=0) #right = comm.bcast(right, root=0)  
    comm.Bcast(occupied, root=0) #occupied = comm.bcast(occupied, root=0)       
    commandsREMlist = comm.bcast(commandsREMlist, root=0) 
    commandsADDlist = comm.bcast(commandsADDlist, root=0) 
    # now do lammps commands on all procs
    if len(commandsREMlist)>0:
        #commandsREMlist.append("delete_bonds newbond bond %i remove special"%extrdr_bnd_type) # need special so that bond lists are recalculated properly
        #commandsREMlist.append("group newbond delete")
        lmp.commands_list(commandsREMlist)
        #commandsADDlist[-1] = commandsADDlist[-1].replace("special no","special yes") # recalculate special list just on the last change to the bonds list
        lmp.commands_list(commandsADDlist)
        flag_didAcommand += 1 

    # add extruders
    commandslist = []     
    if me==0:
        for i in range(0,Nextruders-len(left)):
            if ( random.random() < add_prob ):
                where = random.randrange(1,Nbeads-1-extrdr_init_sep)
                if ( occupied[ where:where+extrdr_init_sep+1 ] == array.array('H',[0]*(extrdr_init_sep+1)) and 
                     CTCF[ where:where+extrdr_init_sep+1 ] == array.array('h',[0]*(extrdr_init_sep+1)) ):
                    left.append(where)
                    right.append(where+extrdr_init_sep)
                    occupied[ left[-1] ]=1
                    occupied[ right[-1] ]=1
                    #commandslist.append("create_bonds single/bond %i %i %i special no" % (extrdr_bnd_type,left[-1],right[-1]) ) # add the new bond
                    commandslist.append( "group newbond id %i %i" % (left[-1],right[-1]) ) # add a new bonds                 
                    commandslist.append( "create_bonds many newbond newbond %i 0.0 %f"%(extrdr_bnd_type,septhresh) )                 
                    commandslist.append( "group newbond delete" )
    # collect arrays and commands list 
    left = comm.bcast(left, root=0) # left can change size, so have to use slow pickle version bcast
    right = comm.bcast(right, root=0) # right can change size, so have to use slow pickle version bcast    
    comm.Bcast(occupied, root=0) #occupied = comm.bcast(occupied, root=0)
    commandslist = comm.bcast(commandslist, root=0)     
    # now do lammps commands on all procs
    if len(commandslist)>0:
        # recalculate special list just on the last change to the bonds list
        #commandslist[-1] = commandslist[-1].replace("special no","special yes") 
        lmp.commands_list(commandslist)
        flag_didAcommand += 1 


    # Check we have the right number of bonds
    if ( len(left) != lmp.extract_global("nbonds",0)-initialNbonds ) :
        print("ERROR - lost some bonds (add)")
        sys.exit(1)

    # Check we have the right number of extruders
    if ( len(left)>Nextruders ) :
        print("ERROR - too many extruders")
        sys.exit(1)

    # do switching
    lmp.file("switch.lam")

    # do the run
    #comm.Barrier()     
    if flag_didAcommand == 0:    
        # if nothing has changed, can skip setup before run         
        lmp.command("run %i pre no post no"%update_interval_steps)     
    else: # in both cases do set 'post no' to skip calculating timing stats     
        lmp.command("run %i post no"%update_interval_steps)


# run a final timestep with post set to yes to calculate timing stats 
lmp.command("run 1 pre no post yes")
 

# remove all extruder bonds before writting restart
commandsREMlist = []
commandsREMlist.append("group newbond id <> 1 %i" %(Nbeads) ) # delete the old bonds
commandsREMlist.append("delete_bonds newbond bond %i remove special"%extrdr_bnd_type) # need special so that bond lists are recalculated properly
commandsREMlist.append("group newbond delete")

lmp.command("write_restart end.%i.restart"%runnumber)
lmp.close()  

if me==0:
    allextrudefile.close()

if quiet == False and me==0:
    print("")
    print("Sucsess!")

sys.exit(0)


