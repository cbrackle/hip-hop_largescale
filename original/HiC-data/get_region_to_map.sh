#!/bin/bash

if [[ ! "$#" == 5 ]]; then
echo "Convert normalized HiC data to a region map which gnuplot can read."
echo "Usage : ./get_region_to_map.sh start end binwidth inputfile outputfile"
exit 1
fi

start=$1
end=$2
binwidth=$3
file=$4
outfile=$5      #{file%.*}-${start}-${end}_map.dat

if [[ ! -f $file ]]; then
    echo "Error : file $file not found."
    exit 1
fi

re='^[0-9]+$'
if ! [[ $start =~ $re ]] ; then
    echo "Error: $start is not a number"
    exit 1
fi
if ! [[ $end =~ $re ]] ; then
    echo "Error: $end is not a number"
    exit 1
fi
if ! [[ $binwidth =~ $re ]] ; then
    echo "Error: $binwidth is not a number"
    exit 1
fi

if [[ -f $outfile ]]; then
    echo "Error : file $outfile already exists."
    exit 1
fi

awk -v s=$start -v e=$end '{
if ($1>=s && $1<=e && $2>=s && $2<=e) print
}' $file > tmp

octave<<EOF
s=${start};
w=${binwidth};
a=load("tmp");
M=max( max( a(:,1) ), max( a(:,2) ) );
M=1+(M-s)/w;
z=nan(M);
for i=1:length(a(:,3))
   z( 1+(a(i,1)-s)/w , 1+(a(i,2)-s)/w )=a(i,3);
   z( 1+(a(i,2)-s)/w , 1+(a(i,1)-s)/w )=z( 1+(a(i,1)-s)/w , 1+(a(i,2)-s)/w );
end
fi = fopen('${outfile}', 'w');
for i=1:M
for j=1:M
   line=sprintf("%i %i %3.5f",(i-1)*w+s,(j-1)*w+s,z(i,j));
   fdisp(fi,line)
end
   fdisp(fi,"")
end
fclose(fi);
EOF
rm tmp


echo "set term pdfcairo enhanced color size 10cm,6cm font 'Helvetica,10'

set output '${outfile%.*}.pdf'
set logscale cb
#set cbrange [.001:1000]
set size square
set palette defined (0 'white', 0.333 'yellow', 0.666 'red', 1 'black' )
p [${start}/1e6:${end}/1e6][${start}/1e6:${end}/1e6] \"< awk '{if (\$3==\\\"NaN\\\") {\$3=0} print}' ${outfile}\" u (\$1/1e6):(\$2/1e6):((\$3))  w image notitle" > test

gnuplot test

rm test

