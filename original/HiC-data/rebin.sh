#!/bin/bash

if [[ ! "$#" == 6 ]]; then
echo "Convert normalized HiC data to a region map which gnuplot can read."
echo "Usage : ./rebin.sh start end OLDbinwidth NEWbinwidth inputfile outputfile"
echo "Where all sizes are in bp"
exit 1
fi

start=$1
end=$2
oldBinW=$3
newBinW=$4
infile=$5
outfile=$6

re='^[0-9]+$'
if ! [[ $start =~ $re ]] ; then
    echo "Error: $start is not a number"
    exit 1
fi
if ! [[ $end =~ $re ]] ; then
    echo "Error: $end is not a number"
    exit 1
fi

if ! [[ $oldBinW =~ $re ]] ; then
    echo "Error: $oldBinW is not a number"
    exit 1
fi
if ! [[ $newBinW =~ $re ]] ; then
    echo "Error: $newBinW is not a number"
    exit 1
fi



if [[ ! -f $infile ]]; then
    echo "Error : file $infile not found."
    exit 1
fi

if [[ -f $outfile ]]; then
    echo "Error : file $outfile already exists."
    exit 1
fi

check=$(
awk -v Ob=$oldBinW -v Nb=$newBinW 'BEGIN{if ( Nb/Ob - int(Nb/Ob) != 0 ) {print 0} else {print 1}}'
)

if [[ $check == 0 ]]; then
    echo "Error : new bin width must be multiple of old bin width."
    exit 1
fi



requiredL=$(( ($end-$start) / $newBinW ))


octave<<EOF

a=load('${infile}');
startB=$start;
endB=$end;
delta=${oldBinW};  #a(1,2)-a(1,1);
newdelta=${newBinW};

L=length([startB:newdelta:endB]);
# get values
val=zeros(L);
for k=1:length(a(:,3))
i=floor((a(k,1)-startB)/newdelta)+1;
j=floor((a(k,2)-startB)/newdelta)+1;
val(i,j)+=a(k,3);
if (i!=j) 
val(j,i)+=a(k,3);
end
end

for i=1:L
for j=1:L
if (val(i,j)==0) 
val(i,j)=NaN;
end
end
end

fi = fopen('${outfile}', 'w');
for i=1:${requiredL}
for j=1:${requiredL}
   line=sprintf("%i %i %3.5f",(i-1)*newdelta+startB,(j-1)*newdelta+startB,val(i,j));
   fdisp(fi,line)
end
   fdisp(fi,"")
end
fclose(fi);

EOF
