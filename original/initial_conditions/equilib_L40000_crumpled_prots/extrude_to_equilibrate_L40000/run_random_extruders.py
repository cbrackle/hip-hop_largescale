#!/bin/env python3

from mpi4py import MPI

import random
import sys, getopt
import numpy
from lammps import lammps


def idx( id ):
    return 3*id-3

def idy( id ):
    return 3*id-2

def idz( id ):
    return 3*id-1

def sep(l,r,coords,box) :
    dx = coords[idx(l)]-coords[idx(r)]
    if ( abs(dx)>0.5*box[0] ) : 
        dx=box[0]-abs(dx)
    dy = coords[idy(l)]-coords[idy(r)]
    if ( abs(dy)>0.5*box[1] ) : 
        dy=box[1]-abs(dy)
    dz = coords[idz(l)]-coords[idz(r)]
    if ( abs(dz)>0.5*box[2] ) : 
        dz=box[2]-abs(dz)
    if ( numpy.sqrt( dx*dx + dy*dy + dz*dz )>50.0 ) :
        print("********************",dx,dy,dz)
    return numpy.sqrt( dx*dx + dy*dy + dz*dz )

#def get_nbonds(self):
#    return self.lib.lammps_get_nbonds(self.lmp)

#########################################################################################################
## Start main program

# set up MPI (it as initialised when the library was imported)

comm = MPI.COMM_WORLD
me = comm.Get_rank()
nprocs = comm.Get_size()

## The way MPI works is that calls to the lammps library must be
## made on all processes.
## The way I have written the python code is that any reading/writting
## to files is done on rank 0. Any stochastic stuff is done on rank 
## zero. Then need to broadcast and variables which have changed.

# add the get_nbonds function to the lmp instance of the lammps class
#lammps.get_nbonds = get_nbonds


quiet = False                          # flag to switch off screen output
runnumber = 1                          # an id number for input and output files

try:
    opts, args = getopt.getopt(sys.argv[1:],"qn:",)
except getopt.GetoptError:
    print('test.py [-q] [-n <runnumber>]')
    print('      -q runs quietly without output to screen')
    print('      -n <runnumber> specifies a run number, default 1.')
    sys.exit(2)
for opt, arg in opts:
    if (opt=="-q"):
        quiet = True
    elif (opt=="-n"):
        runnumber = int(arg)



# Brownian time for a DNA bead is 2 simulation time units

# parameter values
dt = 0.01                                           # time step in simulation time units
septhresh=4.5                                       # max distance for bond creation
runtime = 2e4                                      # total tun time in simulation time units
Nbeads = 40000                                      # number of DNA beads
extruders_per_bead = 300.0/40000.0                   # to set total number of extruders
extrude_rate = 1.0/12.5                            # rate at which extruders move in inverse simulation time units
update_interval = 10                               # how often to update extruders in simulation time units
onrate = 1.0/1.0                                 # rate at which extruders attach in inverse simulation time units
offrate = 1.0/1250.0                               # rate at which extruders detach in inverse simulation time units
mapfile = "extruders_locations_%i.dat"%runnumber    # output file for extruders map

regionstart = 50                                    # set the region where the extruders will be
regionend = 40000-50                                # set the region where the extruders will be

# set some variables
runtime_steps = int(runtime/dt)                            # runtime in timesteps
Nextruders = int(Nbeads*extruders_per_bead)                # number of extruders in system
update_interval_steps = int(update_interval/dt)            # how often to do an extruder update in timesteps
Tintervals = int(runtime_steps/update_interval_steps)-1    # number of times extruders will be updated
extrude_rate_steps = extrude_rate*dt                       # rate at which extruders move in inverse timesteps
onrate_steps = onrate*dt                                   # rate at which extruders attach in inverse timesteps
offrate_steps = offrate*dt                                 # rate at which extruders detach in inverse timesteps

step_prob = update_interval_steps*extrude_rate_steps    # probability an extruder advances
add_prob = update_interval_steps*onrate_steps           # probability an extruder attaches
remove_prob = update_interval_steps*offrate_steps       # probability an extruder detaches

# set up arrays for extruders
left = []
right = []

# random numbers for extruders
seedfile = open("/Home/cbrackle/work/seeds/seed.%i"%runnumber,"r")
#seedfile = open("/home/cbrackle/seeds/seed.%i"%runnumber,"r")
seedline = seedfile.read()
seedfile.close()
seedline = seedline.split(" ")
seed = int(seedline[3])
random.seed(seed)

# set up arrays for beads, and where CTCFs are
occupied=[0 for i in range(Nbeads+1)]

# load CTCF information
sitesCTCF=["0" for i in range(Nbeads+1)]
scoresCTCF=[0.0 for i in range(Nbeads+1)]
#with open("loop_map.dat",'r') as lp:
#    for line in lp.readlines():
#        a=line.split()
#        sitesCTCF[int(a[0])]=a[1]
#        scoresCTCF[int(a[0])]=a[2]

# set up CTCFs stochastically
CTCF=["0" for i in range(Nbeads+1)]
if me==0:
    for i in range(0,len(scoresCTCF)):
        if (random.random() < scoresCTCF[i]):
            CTCF[i]=sitesCTCF[i]
    ctcfoutput = open("CTFCmap_%i.dat"%runnumber,"w")
    for i in range(1,len(CTCF)):
        ctcfoutput.write("%i %s\n"%(i,CTCF[i]))
    ctcfoutput.close()
else:
    CTCF = None
CTCF = comm.bcast(CTCF, root=0)


# set up lammps and load initial input file
#lmp = lammps()
lmp = lammps(comm=comm,cmdargs=["-screen","screen.%i.log"%runnumber,"-log","log.%i.log"%runnumber])
lmp.command("variable runnumber equal %i"%runnumber)
lmp.file("in.initial")
lmp.command("timestep %f"%dt)
boxsize = [ lmp.extract_global("boxxhi",1)-lmp.extract_global("boxxlo",1) , lmp.extract_global("boxyhi",1)-lmp.extract_global("boxylo",1), lmp.extract_global("boxzhi",1)-lmp.extract_global("boxzlo",1) ]


# some messages
if me==0:
    if quiet == False:
        ostreams = [sys.stdout, open("extruders_%i.log"%runnumber,"w")]
    else:
        ostreams = [open("extruders_%i.log"%runnumber,"w")]
else:
    ostreams = []

for messages in ostreams:
    messages.write("#******************************************************************************************************************************************\n")
    messages.write("#  Running simulation %i with %i polymer beads and %i extruders for %i time steps\n"%(runnumber,Nbeads,Nextruders, runtime_steps))
    messages.write("#  running on %i processors\n"%nprocs)
    messages.write("#\n")
    messages.write("#  extruders are updated every %i timesteps\n"%update_interval_steps)
    messages.write("#  extruders advance every %i timesteps\n"%(1.0/extrude_rate_steps))
    messages.write("#  extruders detach after %f timesteps\n"%(1.0/offrate_steps))
    messages.write("#  extruders detach after travelling %f beads\n"%(extrude_rate/offrate))
    messages.write("#  free extruders attach after %f timesteps\n"%(1.0/onrate_steps))
    messages.write("#\n")
    messages.write("#  step_prob = %f\n"%step_prob)
    messages.write("#  add_prob = %f\n"%add_prob)
    messages.write("#  remove_prob = %f\n"%remove_prob)
    messages.write("#  random seed = %i\n"%seed)
    messages.write("#******************************************************************************************************************************************\n\n")

    messages.write("#\n# step, number of extruders\n")


# set up output file for extruders
if me==0:
    allextrudefile = open(mapfile,"w")
    allextrudefile.write("# timestep, left side, right side")


# do an initial run without extruders
lmp.command("run %i"%update_interval_steps)

if ( len(left) != lmp.extract_global("nbonds",0)-Nbeads+1 ) :
    print("ERROR - lost some bonds")
    sys.exit()



# run subsequent steps, updating according to step_prob
for step in range(0,Tintervals):
    if (me==0 and quiet == False):
        sys.stdout.write("running %i   ( %.2f %% done )     \r"%(step*update_interval_steps,100.0*(step)/float(Tintervals)))
        sys.stdout.flush()

    count_commands = 0

    coords = lmp.gather_atoms("x",1,3)

    # output extruders positions
    if me==0:
        for i in range(0,len(left)): 
            allextrudefile.write("%i %i %i\n" %(step*update_interval_steps,left[i],right[i]) )

    # remove extruders on proc 1
    commandslist = []
    if me==0:
        # remove extruders
        for j in range(0,len(left)):
            if ( random.random() < remove_prob ):
                commandslist.append( "group newbond id %i %i" % (left[j],right[j]) )
                commandslist.append( "delete_bonds newbond bond 2 remove special" )
                commandslist.append( "group newbond delete" )
                occupied[left[j]]=0
                occupied[right[j]]=0
                left[j]=-1
                right[j]=-1
        left[:] = (value for value in left if value != -1)
        right[:] = (value for value in right if value != -1)
    left = comm.bcast(left, root=0)
    right = comm.bcast(right, root=0)
    occupied = comm.bcast(occupied, root=0)
    commandslist = comm.bcast(commandslist, root=0)
    # now do lammps commands on all procs
    for cc in commandslist:
        lmp.command(cc)
        count_commands += 1

    if ( len(left) != lmp.extract_global("nbonds",0)-Nbeads+1 ) :
        print("ERROR - lost some bonds (remove)")
        sys.exit()

    # update extruders
    commandslist = []
    if me==0:
        for j in range(0,len(left)):      # left and right get updated here, but the length and order do not change
            lastleft = list(left)
            lastright = list(right)
            if ( left[j]-1>0 and occupied[left[j]-1]==0 and CTCF[left[j]-1]!="F" and random.random() < step_prob ):
                occupied[left[j]] = 0
                left[j] -= 1
                occupied[left[j]] = 1
            if ( right[j]+1<=Nbeads and occupied[right[j]+1]==0 and CTCF[right[j]+1]!="R" and random.random() < step_prob):
                occupied[right[j]] = 0
                right[j] += 1
                occupied[right[j]] = 1
            if ( lastleft[j]!=left[j] or lastright[j]!=right[j] ):
                if ( sep(left[j],right[j],coords,boxsize) > septhresh ):
                    print("ERROR - bond cannot be made. Separation is ",sep(left[j],right[j],coords,boxsize),boxsize[0],boxsize[1],boxsize[2])
                    print(left[j],right[j],sep(left[j],right[j],coords,boxsize),"update")
                    sys.exit()
                commandslist.append( "group dbond id %i %i" % (lastleft[j],lastright[j]) ) # delete the bond
                commandslist.append( "delete_bonds dbond bond 2 remove special" ) # delete old bond; need special so that bond lists are recalculated properly
                commandslist.append( "group dbond delete" )
                commandslist.append( "group newbond id %i %i" % (left[j],right[j]) ) # add a new bonds
                commandslist.append( "create_bonds many newbond newbond 2 0.0 %f"%septhresh )
                commandslist.append( "group newbond delete" )
    left = comm.bcast(left, root=0)
    right = comm.bcast(right, root=0)
    occupied = comm.bcast(occupied, root=0)
    commandslist = comm.bcast(commandslist, root=0)
    # now do lammps commands on all procs
    for cc in commandslist:
        lmp.command(cc)
        count_commands += 1

    if ( len(left) != lmp.extract_global("nbonds",0)-Nbeads+1 ) :
        print("ERROR - lost some bonds (update)",len(left),lmp.get_nbonds()-Nbeads+1)
        sys.exit()

    # add extruders
    commandslist = []
    if me==0:
        for i in range(0,Nextruders-len(left)):
            if ( random.random() < add_prob ):
                where = random.randrange(regionstart,regionend-2)
                if ( occupied[ where ]==0 and occupied[ where+1 ]==0 and occupied[ where+2 ]==0 and CTCF[ where ]=="0" and CTCF[ where+1 ]=="0" and CTCF[ where+2 ]=="0" ):
                    left.append(where)
                    right.append(where+2)
                    occupied[ left[-1] ]=1
                    occupied[ right[-1] ]=1
                    commandslist.append( "group newbond id %i %i" % (left[-1],right[-1]) )
                    commandslist.append( "create_bonds many newbond newbond 2 0.0 %f"%septhresh )
                    commandslist.append( "group newbond delete" )
    left = comm.bcast(left, root=0)
    right = comm.bcast(right, root=0)
    occupied = comm.bcast(occupied, root=0)
    commandslist = comm.bcast(commandslist, root=0)
    # now do lammps commands on all procs
    for cc in commandslist:
        lmp.command(cc)
        count_commands += 1


    if ( len(left) != lmp.extract_global("nbonds",0)-Nbeads+1 ) :
        print("ERROR - lost some bonds (add)")
        sys.exit()

    if ( len(left)>Nextruders ) :
        print("ERROR - too many extruders")
        sys.exit()

    # do the run
    comm.Barrier()
    if count_commands == 0:    # if nothing has changed, can skip setup before run 
        lmp.command("run %i pre no post no"%update_interval_steps)
    else: # in both cases do set 'post no' to skip calculating timing stats
        lmp.command("run %i post no"%update_interval_steps)

 
# run a final timestep with post set to yes to calculate timing stats
lmp.command("run 1 pre no post yes")

if (me==0 and quiet == False ):
    print("")
    print("Sucsess!")

if me==0:
    allextrudefile.close()

lmp.command("write_restart end.%i.restart"%runnumber)
lmp.command("write_data end.%i.restart.data"%runnumber)
lmp.close()  

MPI.Finalize()
sys.exit(0)


