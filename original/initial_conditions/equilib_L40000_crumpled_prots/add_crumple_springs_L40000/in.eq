###
# Box and units  (use LJ units and periodic boundaries)
###

variable runnumber index 5 #3 4 5 #1 2

units lj

atom_style angle 

boundary p p p

###
# Pair interactions require lists of neighbours to be calculated
###
neighbor 1.5 bin
#neighbor 5 bin
neigh_modify every 1 delay 1 check yes 

### READ "start" data file ###
read_data lammps.Xtra-springs.input.${runnumber}


######################

######################
### reset timestep ###
reset_timestep 0 
######################

###
# Define groups (atom type 1 is group 'all')
###
group all type 1 2
group dna type 1 
group prots type 2 



###################################################################
# Stiffness term
#
# E = K * (1+cos(theta)), K>0
#
angle_style cosine 
angle_coeff 1  3.0


###################################################################

###################################################################
# Pair interaction between non-bonded atoms

pair_style  lj/cut 1.12246152962189
pair_modify shift yes
pair_coeff * * 1.0 1.0 1.12246152962189


###################################################################
# Pair interaction between bonded atoms

bond_style hybrid fene harmonic
bond_coeff   1 fene 30.0   1.6   1.0   1.0
bond_coeff   2 none
special_bonds fene #<=== I M P O R T A N T (new command)


###################################################
###
# Set up fixes
###
include /Home/cbrackle/work/seeds/seed.${runnumber} # Load a seed for the Langevin integrator from a file
#include /home/cbrackle/seeds/seed.${runnumber}

fix 1 dna nve   ###NVE ensemble
fix 2 dna langevin   1.0 1.0 0.5  ${seed}


###################################################

###
# Output
###
variable t equal step
variable cbtemp equal temp
variable cbepair equal epair
variable cbemol equal emol
variable cbebond equal ebond
variable cbeangle equal eangle


dump dmp all custom 20000 dump.${runnumber}.DNA id type xs ys zs ix iy iz 
#dump dmp all custom 5000 dump.${runnumber}.DNA id type xs ys zs ix iy iz 
##### Sample thermodynamic info  (temperature, energy, pressure, etc.) #####
thermo 10000
thermo_style   custom   step  temp  epair  emol  
############################################################################


###
# set timestep of integrator
###
timestep 0.01

# Initial run for a bit 
run 100000

# First lets push everything appart a bit
pair_style   soft 2.5
pair_modify  shift yes
pair_coeff * * 50 2.5
 
special_bonds lj 0.0 0.0 1.0

angle_coeff 1  10.0

timestep 0.005
run 20000

# now switch back to normal interactions
# and lets add the crumpling springs slowly
# basically we dont want any tangles

# lets lower the langevin damp time so diffusion is slower
unfix 2
fix 2 dna langevin   1.0 1.0 0.1  ${seed}
timestep 0.001

special_bonds fene

pair_style  lj/cut 1.12246152962189
pair_modify shift yes
pair_coeff * * 1.0 1.0 1.12246152962189
angle_coeff 1  3.0

bond_coeff   2 harmonic 1.0 1.1
run 20000
bond_coeff   2 harmonic 5.0 1.1
run 20000
bond_coeff   2 harmonic 50.0 1.1
run 20000
bond_coeff   2 harmonic 200.0 1.1

# now put verything back to normal
timestep 0.01
unfix 2
fix 2 dna langevin   1.0 1.0 0.5  ${seed}


fix bb all balance 5000 1.08 shift xyz 100 1.01 

run 1000000

write_restart DNA_eq.${runnumber}.restart
write_data DNA_eq.${runnumber}.restart.data nocoeff

clear
next runnumber
jump in.eq
