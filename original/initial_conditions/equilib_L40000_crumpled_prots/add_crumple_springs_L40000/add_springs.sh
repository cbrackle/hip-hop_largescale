#!/bin/bash

if [[ $# != 3 ]]; then
    echo "Usage : ./add_springs.sh N input output"
    echo "       where N        is the number of polymer beads "
    echo "             input    is the input lammps file"
    echo "             output   is the output lammps file"
    echo "            (assumes frist N beads are polymer)"
    exit 1
fi

Npoly=$1
infile=$2
outfile=$3

re='^[0-9]+$'
if ! [[ $Npoly =~ $re ]] ; then
   echo "Error: $Npoly is not an integer" >&2
   exit 1
fi

if ! [[ -f $infile ]]; then
    echo "Error: can't find file $infile"
    exit 1
fi

if [[ -f $outfile ]]; then
    echo "Error: File $outfile already exists. Will not overwrite."
    exit 1
fi

# lets put an extra next-nearest neighbour spring between
# all triplets of beads

awk -v N=$Npoly 'BEGIN{count=1;
for (i=3;i<=N;i++) {
print count,2,i-2,i; count++
}}' > list_of_extra_springs.dat

Nsprings=$( wc -l list_of_extra_springs.dat | awk '{print $1}' )

awk -v Ns=$Nsprings -v bf="list_of_extra_springs.dat" 'BEGIN{B=0;C=0;
while ( (getline<bf)>0 ) { newbond[$1]=$2 " " $3 " " $4; }
}{

if ($2=="bonds") {oN=$1; $1+=Ns;}
if ($2=="bond" && $3=="types") {$1++}

if ($1=="Bonds") {B=1}

if (B==1 && C==1 && $1=="") { for (i=1;i<=Ns;i++) {print oN+i,newbond[i]} }

if (B==1 && C==0 && $1=="") {C++}

print
if ($1=="Angles") {B=0}
}' $infile > $outfile


