
Scripts and Code for Running HiP-HoP Simulations and Analysing the Output
=========================================================================

#### HiP-HoP

programs for running HiP-HoP simulations

#### HiC-data  

scripts for pulling HiC data from the server

#### data_GM12878  

scripts for downloading data and calling peaks etc

#### initial_conditions

scripts for generating initial conditions

#### HiP-HoP-analysis

programs for analysing output from simulations

#### example

a full example

--------------------------------

### Where to start

First look in HiP-HoP/README for detailes of the code and compilation
Then data_GM12878/README_* for how to get the data
Then example/README_SETUPandRUN for a full example simulation
