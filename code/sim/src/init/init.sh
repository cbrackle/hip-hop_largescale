#!/bin/bash
# A script to run LAMMPS starting from a rosette-like polymer chain

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Check arguments
if [[ $# != 3 ]]; then 
    echo "Usage: init.sh chrom run out_dir"
    exit 1
fi

chrom=$1   # Chromosome
run=$2     # Trial number
out_dir=$3 # Output directory

nextruders=450 # 600 (roughly 0.76% of the chromosome length)

genome="hg19"
#genome="hg38"
chr=${chrom:3}
chrom_size_file="${sh_dir}/../../../../genome/${genome}/${genome}.chrom.sizes"
pyx=python3

####################

# 1. Get the number of beads and box size

# Get chromosome size and determine the number of beads
bp_per_bead=1000 # 1 kb resolution
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chrom_size_file)
nbeads=$($pyx -c "
import math
print(int(math.ceil($chrom_size_bp/$bp_per_bead.)))")
#nbeads=33405

name="${genome}_chr_${chr}_Nb_${nbeads}_Ne_${nextruders}_run_${run}"
#name="dna_Nb_${nbeads}_Ne_${nextruders}_run_${run}"
out_dir="${out_dir}/${name}"

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Compute the box size
# Bead size (linear compaction of chromatin)
# sigma = 20 nm -- this is based on Pax6 work which used FISH data to
#                  approximate the bead size, finding sigma = 21.8nm
#
# Density of chromatin in a human nucleus
# Size of a human nucleus (e.g. lymphocyte) = 10 \mu m
# Number of bp in human genome (for diploid cells) = 6.5 Gbp
# Density of chromatin phi = 6.5 Mbp/\mu m^3 = 0.0065 bp/nm^3
sigma=20.0
phi=0.0065
box_size=$($pyx -c "
print(int(round(($nbeads*$bp_per_bead/$phi)**(1./3.)/$sigma)))")

echo "Creating a polymer chain for $chrom with $nbeads beads"
echo "Using box size = $box_size sigma"

# Determine the boundary locations
lo=$($pyx -c "print(-int(${box_size}/2.0))")
hi=$($pyx -c "print(int(${box_size}/2.0))")
xlo=$lo; xhi=$hi
ylo=$lo; yhi=$hi
zlo=$lo; zhi=$hi

####################

# 2. Generate a rosette chain

traj_file="${out_dir}/traj.dat"
radius=$($pyx -c "print(int((${box_size}*0.999)/10.0)*5)")
height=1.2
gap=0.2
petals=6
beads_per_turn=70

# Create the rosette cylinder and store the coordinates to a file
awk -v r=$radius -v h=$height -v w=$gap -v k=$petals -v n=$beads_per_turn \
    -v nbeads=$nbeads 'BEGIN {
pi=atan2(0.0,-1.0)
for (i=0;i<nbeads;i++) {
  x=r*cos(i/n)*((1-w)*cos(k*i/n)**2+w)
  y=r*sin(i/n)*((1-w)*cos(k*i/n)**2+w)
  z=h*i/(2.0*pi)/n
  d=sqrt((x-lx)**2+(y-ly)**2+(z-lz)**2)
  print x,y,z,d
  lx=x; ly=y; lz=z
}}' > $traj_file
echo "Done generating rosettes"

# Find the height of the cylinder
lz=$(awk '{if($3>num){num=$3}} END {print num}' $traj_file)

# Shift the cylinder so that its centre is at the origin
awk -v lz=$lz '{print $1,$2,$3-0.5*lz}' $traj_file > $traj_file.tmp 
mv $traj_file.tmp $traj_file

ixlo=$xlo; ixhi=$xhi
iylo=$ylo; iyhi=$yhi
izlo=$($pyx -c "print(-(int($lz/2.0)+5))")
izhi=$($pyx -c "print(int($lz/2.0)+5)")

nbonds=$(($nbeads-1))
nangles=$(($nbeads-2))

####################

# 3. Create LAMMPS bead file

init_beadfile="init_${name}.in"
file="${out_dir}/${init_beadfile}"
echo "LAMMPS data file via

${nbeads} atoms
1 atom types
${nbonds} bonds
1 bond types
${nangles} angles
1 angle types

${ixlo} ${ixhi} xlo xhi
${iylo} ${iyhi} ylo yhi
${izlo} ${izhi} zlo zhi

Masses

1 1

Atoms # angle
" > $file

awk '{i++; print i,1,1,$1,$2,$3,0,0,0}' $traj_file >> $file
rm $traj_file

awk -v nbonds=$nbonds -v nangles=$nangles 'BEGIN {
  print ""
  print "Bonds"
  print ""
  for (i=1;i<=nbonds;i++) {print i,1,i,i+1}
  print ""
  print "Angles"
  print ""
  for (i=1;i<=nangles;i++) {print i,1,i,i+1,i+2}
}' >> $file

####################

# 4. Copy and modify LAMMPS scripts

# Potentials

persist_length=4.0 # 4.0

# Generate random seeds

function get_rand(){
    # Generate a 4-byte random integer using urandom
    # Need to make sure the seed is positive and less than 900 million!
    # Only use the leftmost 29 bits of the integer (since 2^29 = 536870912)
    rand=$($pyx -c "print(($(od -vAn -N4 -tu4 < /dev/urandom) >> 3)+1)")
    echo $rand
}

prep1_seed=$(get_rand)
prep2_seed=$(get_rand)
prep3_seed=$(get_rand)
prep4_seed=$(get_rand)
extrude_seed=$(get_rand)
extrude_prob_seed=$(get_rand)
crumple_seed_1=$(get_rand)
crumple_seed_2=$(get_rand)
crumple_seed_3=$(get_rand)

# Set run time (in timesteps - note that the size of each timestep changes
# in different scripts)
prep1_run_soft_1=30000 # 30000 # dt = 0.0001, 0.001, 0.01
prep1_run_soft_2=10000 # 10000 # dt = 0.01
prep1_run_lj_1=1000 # 1000 # dt = 0.0001
prep1_run_lj_2=10000 # 10000 # dt = 0.01
prep1_run_fene_1=1000 # 1000 # dt = 0.0001
prep1_run_fene_2=10000 # 10000 # dt = 0.01
prep1_thermofreq=1000 # 1000
prep1_printfreq=5000 # 5000

prep2_run=1000000 # 1000000 # dt = 0.01
prep2_thermofreq=10000 # 10000
prep2_printfreq=50000 # 50000

prep3_run=500000 # 500000 # dt = 0.01
prep3_thermofreq=10000 # 10000
prep3_printfreq=50000 # 50000

prep4_run=2500000 # 2500000 # dt = 0.01
prep4_thermofreq=10000 # 10000
prep4_printfreq=50000 # 50000
prep4_balancefreq=10000 # 10000

extrude_thermofreq=10000 # 10000
extrude_printfreq=10000 # 10000
extrude_balancefreq=10000 # 10000

crumple_bond_type=2 #2
crumple_run_1=100000 # 100000 # dt = 0.01
crumple_run_2=10000 # 10000 # dt = 0.005
crumple_run_3=20000 # 20000 # dt = 0.001
crumple_run_4=1000000 # 1000000 # dt = 0.01
crumple_thermofreq=10000 # 10000
crumple_printfreq=20000 # 20000
crumple_balancefreq=10000 # 10000

# Set extrusion parameters
extrude_dt=0.01 # 0.01
extrude_run=20000 # 20000 in LJ time
sep_thres=4.8 # 4.8
extrude_buffer=50 # 50
extrude_bond_type=2 # 2
update_time=10.0 # 10.0
on_time=1.0 # 1.0
off_time=1250.0 # 1250.0
extrude_time=12.5 # 12.5

# Set file names
prep1_lam="prep1_${name}.lam"
prep1_log="prep1_${name}.log"
prep1_restartfile="prep1_${name}.restart"
prep1_trajfile="prep1_${name}.lammpstrj"
prep1_beadfile="prep1_${name}.out"

prep2_lam="prep2_${name}.lam"
prep2_log="prep2_${name}.log"
prep2_restartfile="prep2_${name}.restart"
prep2_trajfile="prep2_${name}.lammpstrj"
prep2_beadfile="prep2_${name}.out"

prep34_lam="prep34_${name}.lam"
prep34_log="prep34_${name}.log"
prep3_restartfile="prep3_${name}.restart"
prep3_trajfile="prep3_${name}.lammpstrj"
prep3_beadfile="prep3_${name}.out"

prep4_restartfile="prep4_${name}.restart"
prep4_trajfile="prep4_${name}.lammpstrj"
prep4_beadfile="prep4_${name}.out"

extrude_lam="extrude_${name}.lam"
extrude_log="extrude_${name}.log"
extrude_slog="extrude_${name}.slog"
extrude_pylog="extrude_${name}.pylog"
extrude_restartfile="extrude_${name}.restart"
extrude_trajfile="extrude_${name}.lammpstrj"
extrude_beadfile="extrude_${name}.out"
extrude_params="extrude-params_${name}.txt"
extrude_posfile="extrude-pos_${name}.dat"

crumple_lam="crumple_${name}.lam"
crumple_log="crumple_${name}.log"
crumple_restartfile="crumple_${name}.restart"
crumple_trajfile="crumple_${name}.lammpstrj"
crumple_inbeadfile="crumple_${name}.in"
crumple_outbeadfile="crumple_${name}.out"

# Copy the template LAMMPS scripts
run_sh="run_${name}.sh"
prep1_lam_path="${out_dir}/${prep1_lam}"
prep2_lam_path="${out_dir}/${prep2_lam}"
prep34_lam_path="${out_dir}/${prep34_lam}"
extrude_lam_path="${out_dir}/${extrude_lam}"
extrude_params_path="${out_dir}/${extrude_params}"
crumple_lam_path="${out_dir}/${crumple_lam}"
run_sh_path="${out_dir}/${run_sh}"
cp ${sh_dir}/prep1.lam $prep1_lam_path
cp ${sh_dir}/prep2.lam $prep2_lam_path
cp ${sh_dir}/prep34.lam $prep34_lam_path
cp ${sh_dir}/extrude.lam $extrude_lam_path
cp ${sh_dir}/extrude_params.txt $extrude_params_path
cp ${sh_dir}/crumple.lam $crumple_lam_path
cp ${sh_dir}/run.sh $run_sh_path

# Modify prep_1.lam
file=$prep1_lam_path
perl -pi -e "s/IXLO/${ixlo}/g" $file
perl -pi -e "s/IXHI/${ixhi}/g" $file
perl -pi -e "s/IYLO/${iylo}/g" $file
perl -pi -e "s/IYHI/${iyhi}/g" $file
perl -pi -e "s/IZLO/${izlo}/g" $file
perl -pi -e "s/IZHI/${izhi}/g" $file
perl -pi -e "s/PERSIST_LENGTH/${persist_length}/g" $file
perl -pi -e "s/PREP1_INBEADFILE/${init_beadfile}/g" $file
perl -pi -e "s/PREP1_THERMOFREQ/${prep1_thermofreq}/g" $file
perl -pi -e "s/PREP1_PRINTFREQ/${prep1_printfreq}/g" $file
perl -pi -e "s/PREP1_RUN_SOFT_1/${prep1_run_soft_1}/g" $file
perl -pi -e "s/PREP1_RUN_SOFT_2/${prep1_run_soft_2}/g" $file
perl -pi -e "s/PREP1_RUN_LJ_1/${prep1_run_lj_1}/g" $file
perl -pi -e "s/PREP1_RUN_LJ_2/${prep1_run_lj_2}/g" $file
perl -pi -e "s/PREP1_RUN_FENE_1/${prep1_run_fene_1}/g" $file
perl -pi -e "s/PREP1_RUN_FENE_2/${prep1_run_fene_2}/g" $file
perl -pi -e "s/PREP1_RESTARTFILE/${prep1_restartfile}/g" $file
perl -pi -e "s/PREP1_OUTBEADFILE/${prep1_beadfile}/g" $file
perl -pi -e "s/PREP1_TRAJFILE/${prep1_trajfile}/g" $file
perl -pi -e "s/PREP1_SEED/${prep1_seed}/g" $file

# Modify prep_2.lam
file=$prep2_lam_path
if (( $(bc <<< "${ixlo} < ${xlo}") )); then
    perl -pi -e "s/MXLO/${ixlo}/g" $file
else
    perl -pi -e "s/MXLO/${xlo}/g" $file
fi
if (( $(bc <<< "${ixhi} > ${xhi}") )); then
    perl -pi -e "s/MXHI/${ixhi}/g" $file
else
    perl -pi -e "s/MXHI/${xhi}/g" $file
fi
if (( $(bc <<< "${iylo} < ${ylo}") )); then
    perl -pi -e "s/MYLO/${iylo}/g" $file
else
    perl -pi -e "s/MYLO/${ylo}/g" $file
fi
if (( $(bc <<< "${iyhi} > ${yhi}") )); then
    perl -pi -e "s/MYHI/${iyhi}/g" $file
else
    perl -pi -e "s/MYHI/${yhi}/g" $file
fi
if (( $(bc <<< "${izlo} < ${zlo}") )); then
    perl -pi -e "s/MZLO/${izlo}/g" $file
else
    perl -pi -e "s/MZLO/${zlo}/g" $file
fi
if (( $(bc <<< "${izhi} > ${zhi}") )); then
    perl -pi -e "s/MZHI/${izhi}/g" $file
else
    perl -pi -e "s/MZHI/${zhi}/g" $file
fi
perl -pi -e "s/IXLO/${ixlo}/g" $file
perl -pi -e "s/IXHI/${ixhi}/g" $file
perl -pi -e "s/IYLO/${iylo}/g" $file
perl -pi -e "s/IYHI/${iyhi}/g" $file
perl -pi -e "s/IZLO/${izlo}/g" $file
perl -pi -e "s/IZHI/${izhi}/g" $file
perl -pi -e "s/XLO/${xlo}/g" $file
perl -pi -e "s/XHI/${xhi}/g" $file
perl -pi -e "s/YLO/${ylo}/g" $file
perl -pi -e "s/YHI/${yhi}/g" $file
perl -pi -e "s/ZLO/${zlo}/g" $file
perl -pi -e "s/ZHI/${zhi}/g" $file
perl -pi -e "s/PERSIST_LENGTH/${persist_length}/g" $file
perl -pi -e "s/PREP2_INBEADFILE/${prep1_beadfile}/g" $file
perl -pi -e "s/PREP2_RUN/${prep2_run}/g" $file
perl -pi -e "s/PREP2_THERMOFREQ/${prep2_thermofreq}/g" $file
perl -pi -e "s/PREP2_PRINTFREQ/${prep2_printfreq}/g" $file
perl -pi -e "s/PREP2_RESTARTFILE/${prep2_restartfile}/g" $file
perl -pi -e "s/PREP2_OUTBEADFILE/${prep2_beadfile}/g" $file
perl -pi -e "s/PREP2_TRAJFILE/${prep2_trajfile}/g" $file
perl -pi -e "s/PREP2_SEED/${prep2_seed}/g" $file

# Modify prep_34.lam
file=$prep34_lam_path
perl -pi -e "s/XLO/${xlo}/g" $file
perl -pi -e "s/XHI/${xhi}/g" $file
perl -pi -e "s/YLO/${ylo}/g" $file
perl -pi -e "s/YHI/${yhi}/g" $file
perl -pi -e "s/ZLO/${zlo}/g" $file
perl -pi -e "s/ZHI/${zhi}/g" $file
perl -pi -e "s/PERSIST_LENGTH/${persist_length}/g" $file
perl -pi -e "s/PREP3_INBEADFILE/${prep2_beadfile}/g" $file
perl -pi -e "s/PREP3_RUN/${prep3_run}/g" $file
perl -pi -e "s/PREP3_THERMOFREQ/${prep3_thermofreq}/g" $file
perl -pi -e "s/PREP3_PRINTFREQ/${prep3_printfreq}/g" $file
perl -pi -e "s/PREP3_RESTARTFILE/${prep3_restartfile}/g" $file
perl -pi -e "s/PREP3_OUTBEADFILE/${prep3_beadfile}/g" $file
perl -pi -e "s/PREP3_TRAJFILE/${prep3_trajfile}/g" $file
perl -pi -e "s/PREP3_SEED/${prep3_seed}/g" $file
perl -pi -e "s/PREP4_RUN/${prep4_run}/g" $file
perl -pi -e "s/PREP4_THERMOFREQ/${prep4_thermofreq}/g" $file
perl -pi -e "s/PREP4_PRINTFREQ/${prep4_printfreq}/g" $file
perl -pi -e "s/PREP4_BALANCEFREQ/${prep4_balancefreq}/g" $file
perl -pi -e "s/PREP4_RESTARTFILE/${prep4_restartfile}/g" $file
perl -pi -e "s/PREP4_OUTBEADFILE/${prep4_beadfile}/g" $file
perl -pi -e "s/PREP4_TRAJFILE/${prep4_trajfile}/g" $file
perl -pi -e "s/PREP4_SEED/${prep4_seed}/g" $file

# Modify extrude.lam
file=$extrude_lam_path
perl -pi -e "s/XLO/${xlo}/g" $file
perl -pi -e "s/XHI/${xhi}/g" $file
perl -pi -e "s/YLO/${ylo}/g" $file
perl -pi -e "s/YHI/${yhi}/g" $file
perl -pi -e "s/ZLO/${zlo}/g" $file
perl -pi -e "s/ZHI/${zhi}/g" $file
perl -pi -e "s/PERSIST_LENGTH/${persist_length}/g" $file
perl -pi -e "s/EXTRUDE_INBEADFILE/${prep4_beadfile}/g" $file
perl -pi -e "s/EXTRUDE_THERMOFREQ/${extrude_thermofreq}/g" $file
perl -pi -e "s/EXTRUDE_PRINTFREQ/${extrude_printfreq}/g" $file
perl -pi -e "s/EXTRUDE_BALANCEFREQ/${extrude_balancefreq}/g" $file
perl -pi -e "s/EXTRUDE_TRAJFILE/${extrude_trajfile}/g" $file
perl -pi -e "s/EXTRUDE_SEED/${extrude_seed}/g" $file

# Modify extrusion parameter file
file=$extrude_params_path
echo \
"nbeads ${nbeads}
nextruders ${nextruders}
dt ${extrude_dt}
runtime ${extrude_run}
sep_thres ${sep_thres}
seed ${extrude_prob_seed}
extrude_buffer ${extrude_buffer}
extrude_bond_type ${extrude_bond_type}
update_time ${update_time}
on_time ${on_time}
off_time ${off_time}
extrude_time ${extrude_time}
extrude_lammps_file ${extrude_lam}
extrude_pos_file ${extrude_posfile}
extrude_log_file ${extrude_pylog}
lammps_log_file ${extrude_log}
lammps_screen_file ${extrude_slog}
restart_file ${extrude_restartfile}
end_file ${extrude_beadfile}" > $file

# Modify crumple.lam
file=$crumple_lam_path
perl -pi -e "s/XLO/${xlo}/g" $file
perl -pi -e "s/XHI/${xhi}/g" $file
perl -pi -e "s/YLO/${ylo}/g" $file
perl -pi -e "s/YHI/${yhi}/g" $file
perl -pi -e "s/ZLO/${zlo}/g" $file
perl -pi -e "s/ZHI/${zhi}/g" $file
perl -pi -e "s/CRUMPLE_RUN_1/${crumple_run_1}/g" $file
perl -pi -e "s/CRUMPLE_RUN_2/${crumple_run_2}/g" $file
perl -pi -e "s/CRUMPLE_RUN_3/${crumple_run_3}/g" $file
perl -pi -e "s/CRUMPLE_RUN_4/${crumple_run_4}/g" $file
perl -pi -e "s/PERSIST_LENGTH/${persist_length}/g" $file
perl -pi -e "s/CRUMPLE_INBEADFILE/${crumple_inbeadfile}/g" $file
perl -pi -e "s/CRUMPLE_OUTBEADFILE/${crumple_outbeadfile}/g" $file
perl -pi -e "s/CRUMPLE_THERMOFREQ/${crumple_thermofreq}/g" $file
perl -pi -e "s/CRUMPLE_PRINTFREQ/${crumple_printfreq}/g" $file
perl -pi -e "s/CRUMPLE_BALANCEFREQ/${crumple_balancefreq}/g" $file
perl -pi -e "s/CRUMPLE_TRAJFILE/${crumple_trajfile}/g" $file
perl -pi -e "s/CRUMPLE_RESTARTFILE/${crumple_restartfile}/g" $file
perl -pi -e "s/CRUMPLE_SEED_1/${crumple_seed_1}/g" $file
perl -pi -e "s/CRUMPLE_SEED_2/${crumple_seed_2}/g" $file
perl -pi -e "s/CRUMPLE_SEED_3/${crumple_seed_3}/g" $file

# Modify run script
file=$run_sh_path
perl -pi -e "s/PREP1_LAM/${prep1_lam}/g" $file
perl -pi -e "s/PREP1_LOG/${prep1_log}/g" $file
perl -pi -e "s/PREP2_LAM/${prep2_lam}/g" $file
perl -pi -e "s/PREP2_LOG/${prep2_log}/g" $file
perl -pi -e "s/PREP34_LAM/${prep34_lam}/g" $file
perl -pi -e "s/PREP34_LOG/${prep34_log}/g" $file
perl -pi -e "s/EXTRUDE_PARAMS/${extrude_params}/g" $file
perl -pi -e "s/EXTRUDE_OUTBEADFILE/${extrude_beadfile}/g" $file
perl -pi -e "s/CRUMPLE_BOND_TYPE/${crumple_bond_type}/g" $file
perl -pi -e "s/CRUMPLE_INBEADFILE/${crumple_inbeadfile}/g" $file
perl -pi -e "s/CRUMPLE_OUTBEADFILE/${crumple_outbeadfile}/g" $file
perl -pi -e "s/CRUMPLE_LAM/${crumple_lam}/g" $file
perl -pi -e "s/CRUMPLE_LOG/${crumple_log}/g" $file
perl -pi -e "s/NBEADS/${nbeads}/g" $file
