#!/bin/env python3
# extrude.py
# A script which runs LAMMPS coupled with loop extrusion

from mpi4py import MPI
import sys
import random
from array import array
import math
from lammps import lammps

###################

# Classes and functions to handle the parameters and the bond list
class ExtrudeParams(object):
    def __init__(self):
        self.nbeads = None
        self.nextruders = None
        self.dt = None
        self.runtime = None
        self.sepThres = None
        self.seed = None
        self.extrudeBuffer = None
        self.extrudeBondType = None
        self.updateTime = None
        self.onTime = None
        self.offTime = None
        self.extrudeTime = None
        self.extrudePosFile = None
        self.extrudeLAMMPSFile = None
        self.extrudeLogFile = None
        self.LAMMPSLogFile = None
        self.LAMMPSScreenFile = None
        self.restartFile = None
        self.endFile = None
    
    def readParams(self, paramFile):
        with open(paramFile, 'r') as reader:
            for line in reader:
                if (line.startswith('#') or line == '\n'): continue
                args = line.split()
                if (len(args) < 2):
                    print("Error: not enough arguments (need 2)")
                    sys.exit(1)
                field, value = args[0], args[1]
                if (field == "nbeads"):
                    self.nbeads = int(value)                    
                elif (field == "nextruders"):
                    self.nextruders = int(value)
                elif (field == "dt"):
                    self.dt = float(value)
                elif (field == "runtime"):
                    self.runtime = int(value)
                elif (field == "sep_thres"):
                    self.sepThres = float(value)
                elif (field == "seed"):
                    self.seed = int(value)
                elif (field == "extrude_buffer"):
                    self.extrudeBuffer = int(value)
                elif (field == "extrude_bond_type"):
                    self.extrudeBondType = int(value)
                elif (field == "update_time"):
                    self.updateTime = float(value)
                elif (field == "on_time"):
                    self.onTime = float(value)
                elif (field == "off_time"):
                    self.offTime = float(value)
                elif (field == "extrude_time"):
                    self.extrudeTime = float(value)
                elif (field == "extrude_lammps_file"):
                    self.extrudeLAMMPSFile = value
                elif (field == "extrude_pos_file"):
                    self.extrudePosFile = value
                elif (field == "extrude_log_file"):
                    self.extrudeLogFile = value
                elif (field == "lammps_log_file"):
                    self.LAMMPSLogFile = value
                elif (field == "lammps_screen_file"):
                    self.LAMMPSScreenFile = value
                elif (field == "restart_file"):
                    self.restartFile = value
                elif (field == "end_file"):
                    self.endFile = value
                else:
                    print("Error: unknown entry in parameter file: %s" % line)
                    sys.exit(1)
        if (self.nbeads == None or self.nextruders == None or 
            self.dt == None or self.runtime == None or self.sepThres == None or
            self.seed == None or self.extrudeBuffer == None or 
            self.updateTime == None or self.onTime == None or
            self.offTime == None or self.extrudeTime == None or
            self.extrudeLAMMPSFile == None or self.extrudePosFile == None or
            self.extrudeLogFile == None or self.LAMMPSLogFile == None or
            self.LAMMPSScreenFile == None or self.restartFile == None or 
            self.endFile == None):
            return False
        return True

def deleteBond(commands, occupied, s1, s2, bondType):
    commands.append("group db id %d %d" % (s1, s2))
    commands.append("delete_bonds db bond %d remove special" % bondType)
    commands.append("group db delete")
    occupied[s1] = 0
    occupied[s2] = 0

def addBond(commands, occupied, s1, s2, bondType, thres):
    commands.append("group nb id %d %d" % (s1, s2))
    commands.append("create_bonds many nb nb %d 0.0 %f" % (bondType, thres))
    commands.append("group nb delete")
    occupied[s1] = 1
    occupied[s2] = 1

def idx(index): 
    return 3*index-3

def idy(index):
    return 3*index-2

def idz(index):
    return 3*index-1

def dist(s1, s2, coords, boxsize):
    absdx = abs(coords[idx(s1)]-coords[idx(s2)])
    absdy = abs(coords[idy(s1)]-coords[idy(s2)])
    absdz = abs(coords[idz(s1)]-coords[idz(s2)])
    if (absdx > 0.5*boxsize[0]):
        absdx = boxsize[0]-absdx
    if (absdy > 0.5*boxsize[1]):
        absdy = boxsize[1]-absdy
    if (absdz > 0.5*boxsize[2]):
        absdz = boxsize[2]-absdz
    return math.sqrt(absdx*absdx+absdy*absdy+absdz*absdz)

###################

# Start the main program

# Set up MPI 
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

# Check arguments
args = sys.argv
if (len(args) != 3):
    print("extrude.py paramsFile quiet")
    sys.exit(1)

paramsFile = args[1]    # Parameter file
quiet = bool(int(args[2]))   # Print to screen or not

####################

# Read parameter values from file

params = ExtrudeParams()
if (rank == 0):
    params.readParams(paramsFile)
params = comm.bcast(params, root=0)
        
# Set parameters
nbeads = params.nbeads
nextruders = params.nextruders
runtime = params.runtime
dt = params.dt
sepThres = params.sepThres
seed = params.seed
updateTime = params.updateTime
extrudeRate = 1.0/params.extrudeTime
onRate = 1.0/params.onTime
offRate = 1.0/params.offTime
extrudeBuffer = params.extrudeBuffer
extrudeBondType = params.extrudeBondType
regionStart = extrudeBuffer
regionEnd = nbeads-extrudeBuffer
extrudeLAMMPSFile = params.extrudeLAMMPSFile
extrudePosFile = params.extrudePosFile
extrudeLogFile = params.extrudeLogFile
LAMMPSLogFile = params.LAMMPSLogFile
LAMMPSScreenFile = params.LAMMPSScreenFile
restartFile = params.restartFile
endFile = params.endFile

# Convert time/rate variables in units of timesteps
runtimeStep = int(runtime/dt)
updateTimeStep = int(updateTime/dt)
numOfIntervals = int(runtimeStep/updateTimeStep)
extrudeRateStep = extrudeRate*dt
onRateStep = onRate*dt
offRateStep = offRate*dt

stepProb = updateTimeStep*extrudeRateStep
addProb = updateTimeStep*onRateStep
removeProb = updateTimeStep*offRateStep

# Set up random generator
random.seed(seed)

# Arrays for extruders' locations
left = array('i')
right = array('i')
occupied = array('H',[0]*int(nbeads+1))

# No CTCF on chromatin for equilibration

####################

# Output messages about the simulation to be conducted

ostreams = []
if (rank == 0):
    ostreams.append(open(extrudeLogFile,"w"))
    if (not quiet):
        ostreams.append(sys.stdout)

for ostream in ostreams:
    ostream.write("Running loop extrusion simulation ...\n")
    ostream.write("Number of processors: %d\n" % nprocs)
    ostream.write("Number of polymer beads: %d\n" % nbeads)
    ostream.write("Number of extruders: %d\n" % nextruders)
    ostream.write("Total timesteps: %d\n" % runtimeStep)
    ostream.write("\n")
    ostream.write("Extruders are updated every %d timesteps\n" % 
                  updateTimeStep)
    ostream.write("Extruders advance every %.1f timesteps\n" % 
                  (1.0/extrudeRateStep))
    ostream.write("Extruders detach after %.1f timesteps\n" % 
                  (1.0/offRateStep))
    ostream.write("Extruders detach after travelling %.1f beads\n" %
                  (extrudeRate/offRate))
    ostream.write("Free extruders attach after %.1f timesteps\n" % 
                  (1.0/onRateStep))
    ostream.write("\n")
    ostream.write("Probabilities:\n")
    ostream.write("Step prob = %f\n" % stepProb)
    ostream.write("Add prob = %f\n" % addProb)
    ostream.write("Remove prob = %f\n" % removeProb)
    ostream.write("Seed = %d\n" % seed)
    ostream.write("\n")

####################

# Set up LAMMPS

lmp = lammps(comm=comm, cmdargs=["-screen", LAMMPSScreenFile,
                                 "-log", LAMMPSLogFile])
lmp.file(extrudeLAMMPSFile)
lmp.command("timestep %f" % dt)

# Get the box size
boxsize = [lmp.extract_global("boxxhi",1)-lmp.extract_global("boxxlo",1),
           lmp.extract_global("boxyhi",1)-lmp.extract_global("boxylo",1),
           lmp.extract_global("boxzhi",1)-lmp.extract_global("boxzlo",1)]

####################

# Do extrusion

extrudeWriter = None
if (rank == 0):
    extrudeWriter = open(extrudePosFile, "w")
    extrudeWriter.write("# Timestep, Left, Right\n")

# Do an initial run without extruders
lmp.command("run %i" % updateTimeStep)

if (len(left) != lmp.extract_global("nbonds",0)-nbeads+1):
    print("Error: lost some bonds")
    sys.exit(1)

# Run subsequent steps, updating according to stepProb
for step in range(1,numOfIntervals):
    
    # Output timestep info
    if (rank == 0 and not quiet):
        sys.stdout.write("Running step %d (%.2f done)\n" % 
                         (step*updateTimeStep,
                          100.0*step/float(numOfIntervals)))
        sys.stdout.flush()
    
    # Get the bead coordinates from LAMMPS
    coords = lmp.gather_atoms("x",1,3)

    # Output extruder positions
    if (rank == 0):
        for l, r in zip(left,right):
            extrudeWriter.write("%d %d %d\n" % (step*updateTimeStep, l, r))
    
    countCommand = 0
    commands = []
    if (rank == 0):
        # Remove extruders
        for i in range(len(left)):
            if (random.random() < removeProb):
                deleteBond(commands, occupied, left[i], right[i], 
                           extrudeBondType)
                left[i] = -1
                right[i] = -1
        left = array('i',[value for value in left if value != -1])
        right = array('i', [value for value in right if value != -1])
        
        # Update extruders
        for i in range(len(left)):
            prevLeft = left[i]
            prevRight = right[i]
            ldown = left[i]-1
            rup = right[i]+1
            if (ldown > 0 and occupied[ldown] == 0 and 
                random.random() < stepProb):
                left[i] = ldown
            if (rup <= nbeads and occupied[rup] == 0 and 
                random.random() < stepProb):
                right[i] = rup
            if (prevLeft != left[i] or prevRight != right[i]):
                sep = dist(left[i], right[i], coords, boxsize)
                if (sep > sepThres):
                    print("Error: bond cannot be made."
                          "Separation is too large: d = %.2f" % sep)
                    sys.exit(1)
                deleteBond(commands, occupied, prevLeft, prevRight, 
                           extrudeBondType)
                addBond(commands, occupied, left[i], right[i], 
                        extrudeBondType, sepThres)
    
        # Add extruders
        for i in range(nextruders-len(left)):
            if (random.random() < addProb):
                s = random.randrange(regionStart,regionEnd-2)
                sp1 = s+1
                sp2 = s+2
                if (occupied[s] == 0 and occupied[sp1] == 0 and
                    occupied[sp2] == 0):
                    left.append(s)
                    right.append(sp2)
                    addBond(commands, occupied, s, sp2, 
                            extrudeBondType, sepThres)
    
    left = comm.bcast(left, root=0)
    right = comm.bcast(right, root=0)
    comm.Bcast(occupied, root=0)
    commands = comm.bcast(commands, root=0)

    if (len(commands) > 0):
        lmp.commands_list(commands)
        countCommand += len(commands)
    
    if (len(left) != lmp.extract_global("nbonds",0)-nbeads+1):
        print("Error: lost some bonds")
        sys.exit(1)

    if (len(left) > nextruders):
        print("Error: too many extruders")
        sys.exit(1)

    # Make sure all procs have updated the bond list before the next run
    comm.Barrier()

    # Do the run
    lmp.command("run %d post no" % updateTimeStep)

# Run a final timestep with post set to yes to calculate timing stats
lmp.command("run 1")

# Remove all the extruder bonds before writing restart file
commands = []
commands.append("group db id <> 1 %d" % nbeads)
commands.append("delete_bonds db bond %d remove special" % extrudeBondType)
commands.append("group db delete")
lmp.commands_list(commands)

if (rank == 0 and not quiet):
    print("Done")

if (rank == 0):
    for ostream in ostreams:
        ostream.close()
    extrudeWriter.close()

# Write restart file and final output file
lmp.command("write_restart %s" % restartFile)
lmp.command("write_data %s nocoeff" % endFile)
lmp.close()

####################

# Clean up

MPI.Finalize()
