#!/bin/bash
# A script to add springs between next to nearest neighbours

# Check arguments
if [[ $# != 4 ]]; then
    echo "Usage: ./add_crumpled_springs.sh npolybeads bondtype infile outfile"
    exit 1
fi

npolybeads=$1
bondtype=$2
infile=$3
outfile=$4

if [[ ! -f $infile ]]; then
    echo "Error: cannot find the file ${infile}"
    exit 1
fi

if [[ -f $outfile ]]; then
    echo "Error: file ${outfile} already exists"
    exit 1
fi

# Check if the outfile directory exists
outdir=$(dirname $outfile)
if [[ ! -d $outdir ]]; then
    mkdir -p $outdir
fi

spring_file="${outdir}/springs.dat"

awk -v n=$npolybeads -v btype=$bondtype 'BEGIN {
count=1
for (i=3;i<=n;i++) {
  print count,btype,i-2,i; count++
}}' > $spring_file

nsprings=$(wc -l $spring_file | awk '{print $1}')

awk -v nsprings=$nsprings -v btype=$bondtype -v springfile=$spring_file '
BEGIN{
  readBond=0
  readNewBond=0
  while ((getline<springfile)>0) {newbond[$1]=$2 " " $3 " " $4}
}{
  if ($2=="bonds") {nbonds=$1; $1+=nsprings}
  if ($2=="bond" && $3=="types") {$1=btype}
  if ($1=="Atoms") {skip=0}
  if ($1=="Bonds") {readBond=1}
  if (readBond==1 && readNewBond==1 && $1=="") {
    for (i=1;i<=nsprings;i++) {print nbonds+i,newbond[i]}
  }
  if (readBond==1 && readNewBond==0 && $1=="") {readNewBond=1}
  if ($1=="Angles") {readBond=0}
  if ($1=="Pair") {skip=1}
  if (skip==0) {
    print
  }
}' $infile > $outfile

