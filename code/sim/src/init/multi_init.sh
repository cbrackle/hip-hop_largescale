#!/bin/bash
# multi_init.sh

if [[ $# != 5 ]]; then
    echo "Usage: multi_init.sh chrom run_start run_end run_inc dir"
    exit 1
fi

chrom=$1
run_start=$2
run_end=$3
run_inc=$4
dir=$5

for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    ./init.sh $chrom $run $dir
done
