#!/bin/bash
# A script to run the initial equilibration of the chromosome with LAMMPS

if [[ $# != 1 ]]; then
    echo "Usage: nprocs"
    exit 1
fi

nprocs=$1

lmp_mpi="./programs/lmp_mpi"
extrude_py="./programs/extrude.py"

# Run initial equilibration 
mpirun -np $nprocs $lmp_mpi -in PREP1_LAM -log PREP1_LOG &&
mpirun -np $nprocs $lmp_mpi -in PREP2_LAM -log PREP2_LOG &&
mpirun -np $nprocs $lmp_mpi -in PREP34_LAM -log PREP34_LOG &&

# Run random extrusion
mpirun -np $nprocs $extrude_py EXTRUDE_PARAMS 0 &&

# Add crumple springs
./programs/add_crumpled_springs.sh NBEADS CRUMPLE_BOND_TYPE EXTRUDE_OUTBEADFILE CRUMPLE_INBEADFILE &&
mpirun -np $nprocs $lmp_mpi -in CRUMPLE_LAM -log CRUMPLE_LOG
