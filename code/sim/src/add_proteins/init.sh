#!/bin/bash
# init.sh
# A script to create LAMMPS scripts for adding proteins to an equilibrated 
# polymer chain

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Check arguments
if [[ $# != 3 ]]; then 
    echo "Usage: init.sh chrom_bead_file run out_dir"
    exit 1
fi

chrom_bead_file=$1 # Equilibrated polymer output file
run=$2             # Trial number
out_dir=$3         # Output directory

if [[ ! -f $chrom_bead_file ]]; then
    echo "Error: cannot find the file $chrom_bead_file"
    exit 1
fi

nproteins=10000 # 5000
name=$(basename $chrom_bead_file)
name=${name#*_}
name=${name%_run*}
name="${name}_Np_${nproteins}_run_${run}"

out_dir="${out_dir}/${name}"

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

####################

# 1. Copy and modify LAMMPS scripts

# Get number of beads and box dimensions
nbeads=$(awk '{if(NF==2 && $2=="atoms"){print $1}}' $chrom_bead_file)
xlo=$(printf "%f" $(grep 'xlo xhi' $chrom_bead_file | awk '{print $1}'))
xhi=$(printf "%f" $(grep 'xlo xhi' $chrom_bead_file | awk '{print $2}'))
ylo=$(printf "%f" $(grep 'ylo yhi' $chrom_bead_file | awk '{print $1}'))
yhi=$(printf "%f" $(grep 'ylo yhi' $chrom_bead_file | awk '{print $2}'))
zlo=$(printf "%f" $(grep 'zlo zhi' $chrom_bead_file | awk '{print $1}'))
zhi=$(printf "%f" $(grep 'zlo zhi' $chrom_bead_file | awk '{print $2}'))

# Potentials
persist_length=4.0 # 4.0

# Generate random seeds
function get_rand(){
    # Generate a 4-byte random integer using urandom
    # Need to make sure the seed is positive and less than 900 million!
    # Only use the leftmost 29 bits of the integer (since 2^29 = 536870912)
    rand=$(python -c "print ($(od -vAn -N4 -tu4 < /dev/urandom) >> 3)+1")
    echo $rand
}

protein_gen_seed=$(get_rand)
protein_prot_seed=$(get_rand)
protein_poly_seed=$(get_rand)

# Set run time (in timesteps - note that the size of each timestep changes
# in different scripts)
protein_run_1=5000 # 5000 # dt = 0.001
protein_run_2=5000 # 5000 # dt = 0.01
protein_run_3=5000 # 5000 # dt = 0.01
protein_run_4=100000 # 100000 # dt = 0.01
protein_thermofreq=10000 # 10000
protein_printfreq=10000 # 10000

# Set file names
crumple_outbeadfile="crumple_${name}.out"
protein_lam="protein_${name}.lam"
protein_log="protein_${name}.log"
protein_restartfile="protein_${name}.restart"
protein_trajfile="protein_${name}.lammpstrj"
protein_inbeadfile="protein_${name}.in"
protein_outbeadfile="protein_${name}.out"

# Copy the template LAMMPS scripts
run_sh="run_${name}.sh"
protein_lam_path="${out_dir}/${protein_lam}"
run_sh_path="${out_dir}/${run_sh}"
cp ${sh_dir}/protein.lam $protein_lam_path
cp ${sh_dir}/run.sh $run_sh_path
cp $chrom_bead_file ${out_dir}/$crumple_outbeadfile

# Modify protein.lam
file=$protein_lam_path
perl -pi -e "s/XLO/${xlo}/g" $file
perl -pi -e "s/XHI/${xhi}/g" $file
perl -pi -e "s/YLO/${ylo}/g" $file
perl -pi -e "s/YHI/${yhi}/g" $file
perl -pi -e "s/ZLO/${zlo}/g" $file
perl -pi -e "s/ZHI/${zhi}/g" $file
perl -pi -e "s/PROTEIN_RUN_1/${protein_run_1}/g" $file
perl -pi -e "s/PROTEIN_RUN_2/${protein_run_2}/g" $file
perl -pi -e "s/PROTEIN_RUN_3/${protein_run_3}/g" $file
perl -pi -e "s/PROTEIN_RUN_4/${protein_run_4}/g" $file
perl -pi -e "s/PERSIST_LENGTH/${persist_length}/g" $file
perl -pi -e "s/PROTEIN_INBEADFILE/${protein_inbeadfile}/g" $file
perl -pi -e "s/PROTEIN_OUTBEADFILE/${protein_outbeadfile}/g" $file
perl -pi -e "s/PROTEIN_THERMOFREQ/${protein_thermofreq}/g" $file
perl -pi -e "s/PROTEIN_PRINTFREQ/${protein_printfreq}/g" $file
perl -pi -e "s/PROTEIN_TRAJFILE/${protein_trajfile}/g" $file
perl -pi -e "s/PROTEIN_RESTARTFILE/${protein_restartfile}/g" $file
perl -pi -e "s/PROTEIN_PROT_SEED/${protein_prot_seed}/g" $file
perl -pi -e "s/PROTEIN_POLY_SEED/${protein_poly_seed}/g" $file

# Modify run script
file=$run_sh_path
perl -pi -e "s/CRUMPLE_OUTBEADFILE/${crumple_outbeadfile}/g" $file
perl -pi -e "s/PROTEIN_LAM/${protein_lam}/g" $file
perl -pi -e "s/PROTEIN_LOG/${protein_log}/g" $file
perl -pi -e "s/PROTEIN_INBEADFILE/${protein_inbeadfile}/g" $file
perl -pi -e "s/PROTEIN_OUTBEADFILE/${protein_outbeadfile}/g" $file
perl -pi -e "s/PROTEIN_GEN_SEED/${protein_gen_seed}/g" $file
perl -pi -e "s/NPROTEINS/${nproteins}/g" $file
perl -pi -e "s/NBEADS/${nbeads}/g" $file
