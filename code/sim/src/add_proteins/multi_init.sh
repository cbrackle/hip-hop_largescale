#!/bin/bash
# multi_init.sh

if [[ $# != 5 ]]; then
    echo "Usage: multi_init.sh run_start run_end run_inc in_dir out_dir"
    exit 1
fi

run_start=$1
run_end=$2
run_inc=$3
in_dir=$4
out_dir=$5

chrom="chr21" # "chr22" "chr21" "chr20" "chr11" "chr18" "chr19"
genome="hg19" # "hg38"
chr=${chrom:3}
run=$run_start
Nb=33830 # 33830 33405 51305 48130 63026 79307 45000
Np=10000 # 5000
Ne=260 # 250 620 390 370 480 600 390 590 450 340

for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    echo "Creating files for run = $run"
    equil_run=$(python -c "print ((${run}-1)%10)+1")
    #equil_name="dna_Nb_${Nb}_Np_${Np}_Ne_${Ne}_run_${equil_run}"
    #equil_name="${genome}_chr_${chr}_Ne_${Ne}_run_${equil_run}"
    equil_name="${genome}_chr_${chr}_Nb_${Nb}_Ne_${Ne}_run_${equil_run}"
    equil_file="${in_dir}/${equil_name}/crumple_${equil_name}.out"
    ./init.sh $equil_file $run $out_dir
done
