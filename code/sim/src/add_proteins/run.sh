#!/bin/bash
# A script to add proteins to LAMMPS chromosome simulations

if [[ $# != 1 ]]; then
    echo "Usage: nprocs"
    exit 1
fi

nprocs=$1

lmp_mpi="./programs/lmp_mpi"

# Add proteins
./programs/add_proteins.sh NPROTEINS PROTEIN_GEN_SEED CRUMPLE_OUTBEADFILE PROTEIN_INBEADFILE &&
mpirun -np $nprocs $lmp_mpi -in PROTEIN_LAM -log PROTEIN_LOG &&

# Remove extra springs
./programs/remove_crumpled_springs.sh NBEADS PROTEIN_OUTBEADFILE PROTEIN_OUTBEADFILE.tmp &&
mv PROTEIN_OUTBEADFILE.tmp PROTEIN_OUTBEADFILE
