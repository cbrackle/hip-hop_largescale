#!/bin/bash
# A script to remove springs between next to nearest neighbours

# Check arguments
if [[ $# != 3 ]]; then
    echo "Usage: ./remove_crumpled_springs.sh npolybeads infile outfile"
    exit 1
fi

npolybeads=$1
infile=$2
outfile=$3

if [[ ! -f $infile ]]; then
    echo "Error: cannot find the file ${infile}"
    exit 1
fi

if [[ -f $outfile ]]; then
    echo "Error: file ${outfile} already exists"
    exit 1
fi

# Check if the outfile directory exists
outdir=$(dirname $outfile)
if [[ ! -d $outdir ]]; then
    mkdir -p $outdir
fi

awk -v n=$npolybeads 'BEGIN {readBond=0} {
  if ($2=="bonds") {$1=(n-1)}
  if ($2=="bond" && $3=="types") {$1--}
  if ($1=="Bonds") {
    readBond=1
    print
    print ""
    for (i=1;i<n;i++) {print i,1,i,i+1}
    print ""
  }
  if ($1=="Angles") {readBond=0}
  if (readBond==0) {print}
}' $infile > $outfile
