#!/bin/bash
# add_proteins.sh
# A script to add protein beads to a LAMMPS bead file

# Check arguments
if [[ $# != 4 ]]; then
    echo "Usage: add_proteins.sh nprot seed infile outfile"
    exit 1
fi

nprot=$1
seed=$2
infile=$3
outfile=$4

if [[ ! -f $infile ]]; then
    echo "Error: cannot find the file ${infile}"
fi

if [[ -f $outfile ]]; then
    echo "Error: the file ${outfile} already exists"
    exit 1
fi

# Check if the outfile directory exists
outdir=$(dirname $outfile)
if [[ ! -d $outdir ]]; then
    mkdir -p $outdir
fi

# Get the box size
xlo=$(printf "%f" $(grep 'xlo xhi' $infile | awk '{print $1}'))
xhi=$(printf "%f" $(grep 'xlo xhi' $infile | awk '{print $2}'))
ylo=$(printf "%f" $(grep 'ylo yhi' $infile | awk '{print $1}'))
yhi=$(printf "%f" $(grep 'ylo yhi' $infile | awk '{print $2}'))
zlo=$(printf "%f" $(grep 'zlo zhi' $infile | awk '{print $1}'))
zhi=$(printf "%f" $(grep 'zlo zhi' $infile | awk '{print $2}'))

# Generate a list of random coordinates within the box
coords_file="${outdir}/protein_coords.dat"
python -c "
from __future__ import print_function
import random
random.seed(${seed})
buff = 1.0
lx = ${xhi}-${xlo}-buff
ly = ${yhi}-${ylo}-buff
lz = ${zhi}-${zlo}-buff
for i in range(${nprot}):
    x = (random.random()-0.5)*lx
    y = (random.random()-0.5)*ly
    z = (random.random()-0.5)*lz
    print('%d %.16e %.16e %.16e' % (i,x,y,z))
" > $coords_file

# Create the output bead file
outfile="${outdir}/${outfile}"
awk -v nprot=$nprot -v coordsfile=$coords_file 'BEGIN {
# Read the protein bead coordinates
while ((getline<coordsfile)>0) {prot[$1]=$2" "$3" "$4}
}{
if (readBead==1) {
  if ($1!="" && countBead<nold) {
    countBead++ 
  } else if (countBead==nold) {
    for (i=0;i<nprot;i++) {
      print i+1+nold,i+2,2,prot[i],"0 0 0"
    }
    readBead=0
  }
} else if (readVelocity==1) {
  if ($1!="" && countVelocity<nold) {
    countVelocity++
  } else if (countVelocity==nold) {
    for (i=0;i<nprot;i++) {
      printf "%d %.16e %.16e %.16e\n",i+nold+1,0,0,0
    }
    readVelocity=0
  }
} else if (readMass==1) {
  if ($1!="" && countMass<1) {
    countMass++
  } else if (countMass==1) {
    print "2 1"
    readMass=0
  }
}
if ($2=="atom" && $3=="types") {$1++}
if ($2=="atoms") {nold=$1; $1+=nprot; nnew=$1}
if ($1=="Masses") {readMass=1}
if ($1=="Atoms") {readBead=1; readMass=0; skip=0}
if ($1=="Velocities") {readVelocity=1; readBead=0}
if ($1=="Pair") {skip=1}
if (skip==0) {
  print
}}' $infile > $outfile

rm $coords_file
