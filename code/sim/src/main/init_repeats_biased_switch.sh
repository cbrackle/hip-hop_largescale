#!/bin/bash
# A script to run LAMMPS starting from an equilibrated conformation

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Check arguments
if [[ $# != 4 ]]; then 
    echo "Usage: init.sh chrom equil_bead_file run out_dir"
    exit 1
fi

chrom=$1           # Chromosome
equil_bead_file=$2 # Position file from equilibration
run=$3             # Trial number
out_dir=$4         # Output directory

# Do the whole chromosome
#regstart=0
#regend=$nbp
regstart=6500000
regend=16500000

genome="hg19"
cell="GM12878"
chr=${chrom:3}
name="${cell}_${genome}_chr_${chr}_${regstart}-${regend}_run_${run}"
out_dir="${out_dir}/${name}"
chrom_size_file="${sh_dir}/../../../genome/hg19/hg19.chrom.sizes"

# Get the number of polymer beads
bp_per_bead=1000
nregbeads=$(python -c "print ($regend-$regstart)/$bp_per_bead")
npolybeads=$(awk '{if(NF==9&&$3==1){a++}}END{print a}' $equil_bead_file)
#nbp=$(awk -v ch=$chrom '{if($1==ch) {print $2}}' $chrom_size_file)
#npolybeads=$(python -c "
#import math
#print int(math.ceil(${nbp}/float(${bp_per_bead})))")

# Path to programs
program_data2beads="${sh_dir}/../bin/main/exe/data_to_beads"
program_peakmotifstrand="${sh_dir}/../bin/main/exe/peak_motif_strand"
program_ctcfbed2beads="${sh_dir}/../bin/main/exe/ctcf_bed_to_beads"
#program_beadlist2lammps="${sh_dir}/bead_types_to_lmpsinput.sh"
program_beadlist2lammps="${sh_dir}/bead_types_to_lmpsinput_repeats.sh"
program_addsprings="${sh_dir}/add_crumple_springs.sh"

# Path to data
path2data="${sh_dir}/../../../data_GM12878/${genome}/"
k27ac_bed="${path2data}/histone/H3K27ac_epicpeaks.bed"
k27me3_bed="${path2data}/histone/H3K27me3_epicpeaks.bed"
k9me3_bed="${path2data}/histone/H3K9me3_epicpeaks.bed"
het_bed="${path2data}/het/heterochromatin.bed"
#dnase_bed="${path2data}/old_dnase/DNase_peaks.bed"
dnase_bed="${path2data}/dnase/DNase_finalpeaks.bed"
atac_bed="${path2data}/atac/ATAC_finalpeaks.bed"
#ctcf_bed="${path2data}/old_ctcf/CTCF_peaks_withstrand.bed"
ctcf_bed="${path2data}/ctcf/CTCF_Rad21_peaks_withstrand.bed"

# Unzip the data if needed
if [[ -f ${k27ac_bed}.gz ]]; then
    gzip -d ${k27ac_bed}.gz
elif [[ ! -f $k27ac_bed ]]; then
    echo "Error: cannot find the file ${k27ac_bed}"
    exit 1
fi

if [[ -f ${k27me3_bed}.gz ]]; then
    gzip -d ${k27me3_bed}.gz
elif [[ ! -f $k27me3_bed ]]; then
    echo "Error: cannot find the file ${k27me3_bed}"
    exit 1
fi

if [[ -f ${k9me3_bed}.gz ]]; then
    gzip -d ${k9me3_bed}.gz
elif [[ ! -f $k9me3_bed ]]; then
    echo "Error: cannot find the file ${k9me3_bed}"
    exit 1
fi

if [[ -f ${het_bed}.gz ]]; then
    gzip -d ${het_bed}.gz
elif [[ ! -f $het_bed ]]; then
    echo "Error: cannot find the file ${het_bed}"
    exit 1
fi

if [[ -f ${dnase_bed}.gz ]]; then
    gzip -d ${dnase_bed}.gz
elif [[ ! -f $dnase_bed ]]; then
    echo "Error: cannot find the file ${dnase_bed}"
    exit 1
fi

if [[ -f ${atac_bed}.gz ]]; then
    gzip -d ${atac_bed}.gz
elif [[ ! -f $atac_bed ]]; then
    echo "Error: cannot find the file ${atac_bed}"
    exit 1
fi

if [[ -f ${ctcf_bed}.gz ]]; then
    gzip -d ${ctcf_bed}.gz
elif [[ ! -f $ctcf_bed ]]; then
    echo "Error: cannot find the file ${ctcf_bed}"
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

####################

# 1. Generate bead lists

# Convert bed data files to a list of beads
# Create the config file for generating the bead list
config_file="${out_dir}/config_${name}.txt"
echo "${chrom}
${regstart}
${regend}
${bp_per_bead}
ACT ${atac_bed}
OPEN ${k27ac_bed}
POLY ${k27me3_bed}
HET ${k9me3_bed}" > $config_file

$program_data2beads $config_file $out_dir

# Get CTCF bead list
# Pick the motif/strand with the highest motif score within a peak
#ctcf_bed="${path2data}/ctcf/CTCF_peaks_withstrand.bed"
ctcf_prob_file="CTCF-prob_${name}.dat"
ctcf_map_file="CTCF-map_${name}.dat"
ctcf_prob_file_path="${out_dir}/${ctcf_prob_file}"
$program_peakmotifstrand $ctcf_bed $ctcf_prob_file_path
$program_ctcfbed2beads $config_file $ctcf_prob_file_path \
    $ctcf_prob_file_path.tmp
mv $ctcf_prob_file_path.tmp $ctcf_prob_file_path

####################

# 2. Get the initial condition file
bead_type_file="${out_dir}/bead_type.dat"
full_bead_type_file="${out_dir}/full_bead_type.dat"
repeat_index_file="${out_dir}/repeat_index.dat"
crumpled_bond_type=2
init_bead_file="extrude_${name}.in"
init_bead_file_path="${out_dir}/${init_bead_file}"
gap=1000
$program_beadlist2lammps $gap $bead_type_file $equil_bead_file \
    $init_bead_file_path $repeat_index_file $out_dir

# Modify the bead type file to include the repeats
nrepeats=$(wc -l $repeat_index_file | awk '{print $1}')
# Add a new bead type for spacer regions
npolytypes=$(awk '$2=="Type"' $bead_type_file | \
    awk 'BEGIN {nt=0} {if($3>nt){nt=$3}} END {print nt}')
npolytypes=$(($npolytypes+1))
# Modify the bead type for spacer regions
awk -v repeatfile=$repeat_index_file -v nb=$npolybeads -v stype=$npolytypes \
'BEGIN {
nrep=0
while ((getline<repeatfile)>0) {
  start[nrep]=$2
  end[nrep]=$3
  nrep+=1
}
for ( i=0; i<nrep; i++ ) {
  prev = (i-1)<0 ? 1 : end[i-1]+1
  for ( j=prev; j<start[i]; j++ ) {
    spacer[j]=1
  }
  for ( j=start[i]; j<=end[i]; j++ ) {
    spacer[j]=0
  }
}
for ( j=end[nrep-1]+1; j<=nb; j++ ) {
  spacer[j]=1
}} {
if (NR==4) {$1=(stype+1)};
if ($1=="Masses") {
  print
  print ""
  m=1
  for ( j=1; j<=(stype+1); j++ ) {
    print j,1
  }
  print ""
}
if ($1=="Atoms") {m=0}
if (NF==9&&$1<=nb&&spacer[$1]==1) {$3=stype}
else if (NF==9&&$1>nb) {$3=stype+1}
if (m==0) {print}
}' $init_bead_file_path > $init_bead_file_path.tmp
mv $init_bead_file_path.tmp $init_bead_file_path
echo "# List of beads with ${npolytypes} different types." > $full_bead_type_file
echo "# Type ${npolytypes} : !ACT !HET !OPEN !POLY SPACER" >> $full_bead_type_file
awk '{if(NR>1&&$1=="#"){print}}' $bead_type_file >> $full_bead_type_file
awk -v n=$npolybeads '{if(NF==9&&$1<=n){print $1,$3}}' $init_bead_file_path >> $full_bead_type_file

# Modify the CTCF file to include the repeats
> $ctcf_prob_file_path.tmp
for (( i=1; $i <= $nrepeats; i++ ))
do
    index=$(awk -v rep=$i '{if($1==rep){print $2-1}}' $repeat_index_file)
    if [[ $index > 0 ]]; then
	echo "$index 0 0 1" >> $ctcf_prob_file_path.tmp
    fi
    awk -v start=$index '{print $1+start,$2,$3,$4}' $ctcf_prob_file_path >> $ctcf_prob_file_path.tmp
    index=$(awk -v rep=$i '{if($1==rep){print $3+1}}' $repeat_index_file)
    if [[ $index < $npolybeads ]]; then
	echo "$index 0 0 1" >> $ctcf_prob_file_path.tmp
    fi
done
mv $ctcf_prob_file_path.tmp $ctcf_prob_file_path

# Add crumpled springs
$program_addsprings $full_bead_type_file OPEN $crumpled_bond_type \
    $init_bead_file_path $init_bead_file_path.tmp $out_dir
mv $init_bead_file_path.tmp $init_bead_file_path

####################

# 3. Count bead types

npolytypes=$(awk '$2=="Type"' $full_bead_type_file | \
    awk 'BEGIN {nt=0} {if($3>nt){nt=$3}} END {print nt}')

awk '{
if ($2=="Type") {
  if ($5=="ACT") act[$3]=1
  if ($6=="HET") het[$3]=1
  if ($7=="OPEN") open[$3]=1
  if ($8=="POLY") poly[$3]=1
}
if ($1!="#") {
  if ($2 in act) {a++}
  if ($2 in open) {o++}
  if ($2 in poly) {p++}
  if ($2 in het) {h++}
}} END {
  print a " active beads"
  print o " open beads"
  print p " polycomb beads"
  print h " heterochrom beads"
}' $full_bead_type_file

####################

# 4. Set simulation parameters

# Potentials

persist_length=4.0 #4.0

# Generate random seeds

function get_rand(){
    # Generate a 4-byte random integer using urandom
    # Need to make sure the seed is positive and less than 900 million!
    # Only use the leftmost 29 bits of the integer (since 2^29 = 536870912)
    rand=$(python -c "print ($(od -vAn -N4 -tu4 < /dev/urandom) >> 3)+1")
    echo $rand
}

extrude_prob_seed=$(get_rand)
extrude_seed=$(get_rand)
switch_seed=$(get_rand)

# Set run time (in timesteps - note that the size of each timestep changes
# in different scripts)
extrude_thermofreq=200000 # 200000
extrude_printfreq=200000 # 200000
extrude_balancefreq=5000 # 5000

# Get box dimensions
xlo=$(printf "%f" $(grep 'xlo xhi' $equil_bead_file | awk '{print $1}'))
xhi=$(printf "%f" $(grep 'xlo xhi' $equil_bead_file | awk '{print $2}'))
ylo=$(printf "%f" $(grep 'ylo yhi' $equil_bead_file | awk '{print $1}'))
yhi=$(printf "%f" $(grep 'ylo yhi' $equil_bead_file | awk '{print $2}'))
zlo=$(printf "%f" $(grep 'zlo zhi' $equil_bead_file | awk '{print $1}'))
zhi=$(printf "%f" $(grep 'zlo zhi' $equil_bead_file | awk '{print $2}'))

nbeads=$(awk '{if($2=="atoms"){print $1}}' $equil_bead_file)
nprotbeads=$(($nbeads-$npolybeads))

nprottypes=3 # 3
switch=1  # proteins are switching (1) or not (0)
inact_prot=0 # switch on (1) or off (1) general inactive proteins
nact=1000 # 800 Number of active binders
npcmb=500 # 500 Number of polycomb binders
nhet=2500 # 1200 Number of heterochromatin binders
ninact=0 # 2000 Number of general inactive binders
if [[ $inact_prot == 1 ]]; then
    nprottypes=$(($nprottypes+1))
fi

echo "Number of polymer beads: ${npolybeads}"
echo "Number of protein beads: ${nprotbeads}"
echo "Total number of beads: ${nbeads}"

# Check that the equilibration bead file has enough protein beads
if (( $(($nact+$npcmb+$nhet)) > $nprotbeads )); then
    echo "Error: not enough protein beads in equilibration sample"
    echo "Got ${nprotbeads} protein beads,"
    echo "but need at least protein beads."
    exit 1
fi

# Protein-chromatin and chromatin-chromatin interactions
non_specific=1 # Switch on (1) or off (1) non-specific chromatin interactions
no_mark_int=0 # Interaction between non-marked beads (1) or
              # between non-acetylated beads (0)
cutoff=1.8
sigma=1.0
norm=$(python -c "
print '%.16f' % (1.0+4.0*(($sigma/$cutoff)**12.0-($sigma/$cutoff)**6.0))")
e_act_sact=7.0
e_act_act=3.0
e_pcmb_pcmb=7.0
e_het_het=3.0
e_inact_inact=3.0
e_chrom_chrom=0.4
e_act_sact=$(python -c "print '%.16f' % ($e_act_sact/$norm)")
e_act_act=$(python -c "print '%.16f' % ($e_act_act/$norm)")
e_pcmb_pcmb=$(python -c "print '%.16f' % ($e_pcmb_pcmb/$norm)")
e_het_het=$(python -c "print '%.16f' % ($e_het_het/$norm)")
e_inact_inact=$(python -c "print '%.16f' % ($e_inact_inact/$norm)")
e_chrom_chrom=$(python -c "print '%.16f' % ($e_chrom_chrom/$norm)")

echo "Interaction energies (in kT):"
echo "Active TFs <-> ATAC/Dnase              = $e_act_sact"
echo "Active TFs <-> K27ac                   = $e_act_act"
echo "Polycomb TFs <-> K27me3                = $e_pcmb_pcmb"
echo "Heterochrom TFs <-> K9me3              = $e_het_het"
echo "Inactive TFs <-> K9me3/K27me3/unmarked = $e_inact_inact"
echo "Chromatin <-> Chromatin                = $e_chrom_chrom"

# Set extrusion parameters
neigh_length=5.0 # 5.0
neigh_every=20 # 20
neigh_delay=20 # 20
nextruders=300 # 300 (roughly 0.5% of the chromosome length)
extrude_dt=0.01 # 0.01
extrude_run=500000 # 500000 in LJ time
sep_thres=4.8 # 4.8
extrude_buffer=50 # 50
extrude_bond_type=3 # 3
extrude_init_sep=3 # 3 - initial separation (in beads) of extruder spring
update_time=50 # 50 in LJ time
on_time=50 # 50 in LJ time
off_time=40000 # 40000 in LJ time
extrude_time=500 # 500 in LJ time

# Set proteins parameters
prot_attract_start_time=50000 # 10000 in LJ time
chrom_attract_start_time=10000 # 10000 in LJ time
#switch_time=100000 # 1000 in LJ time
act_switch_time=10000 # 1000 in LJ time 
inact_switch_time=100000 # 100000 in LJ time

if [[ $switch == 0 ]]; then
    act_switch_time=$extrude_run
    inact_switch_time=$extrude_run
fi

# Set file names
prot_attract_lam="prot-attract_${name}.lam"
chrom_attract_lam="chrom-attract_${name}.lam"
switch_lam="switch_${name}.lam"
extrude_lam="extrude_${name}.lam"
extrude_log="extrude_${name}.log"
extrude_screenlog="extrude_${name}.slog"
extrude_pylog="extrude_${name}.pylog"
extrude_restartfile="extrude_${name}.restart"
extrude_trajfile="extrude_${name}.lammpstrj"
extrude_beadfile="extrude_${name}.out"
extrude_params="extrude-params_${name}.txt"
extrude_posfile="extrude-pos_${name}.dat"

# Copy the template LAMMPS scripts
run_sh="run_${name}.sh"
prot_attract_lam_path="${out_dir}/${prot_attract_lam}"
chrom_attract_lam_path="${out_dir}/${chrom_attract_lam}"
switch_lam_path="${out_dir}/${switch_lam}"
extrude_lam_path="${out_dir}/${extrude_lam}"
extrude_params_path="${out_dir}/${extrude_params}"
run_sh_path="${out_dir}/${run_sh}"
cp run.sh $run_sh_path

# Set bead types
# Need 2*nprottypes for switching on/off proteins (half on, half off)
ntotaltypes=$(($npolytypes+2*$nprottypes))

# Protein types
tt=$(($npolytypes+1))
on_act_type=$tt
tt=$(($tt+1))
on_pcmb_type=$tt
tt=$(($tt+1))
on_het_type=$tt
tt=$(($tt+1))
if [[ $inact_prot == 1 ]]; then
    on_inact_type=$tt
    tt=$(($tt+1))
fi
off_act_type=$tt
tt=$(($tt+1))
off_pcmb_type=$tt
tt=$(($tt+1))
off_het_type=$tt
if [[ $inact_prot == 1 ]]; then
    tt=$(($tt+1))
    off_inact_type=$tt
fi

# Polymer bead types with ATAC/DNase signal
poly_strong_active_types=$(awk '{
if ($1=="#" && $5=="ACT") {type[$3]=1}} END {
for (i in type) {tt=tt " " i}
print tt
}' $full_bead_type_file)

# Polymer bead types with heterochromatin (H3K9me3) signal
poly_het_types=$(awk '{
if ($1=="#" && $6=="HET") {type[$3]=1}} END {
for (i in type) {tt=tt " " i}
print tt
}' $full_bead_type_file)

# Polymer bead types with open (H3K27ac) signal
poly_weak_active_types=$(awk '{
if ($1=="#" && $7=="OPEN") {type[$3]=1}} END {
for (i in type) {tt=tt " " i}
print tt
}' $full_bead_type_file)

# Polymer bead types with polycomb (H3K27me3) signal
poly_pcmb_types=$(awk '{
if ($1=="#" && $8=="POLY") {type[$3]=1}} END {
for (i in type) {tt=tt " " i}
print tt
}' $full_bead_type_file)

# Polymer bead types with no acetyl (H3K27ac) signal
non_acetyl_types=$(awk '{
if ($1=="#" && $7=="!OPEN" && $9!="SPACER") {type[$3]=1}} END {
for (i in type) {tt=i "," tt}
print tt
}' $full_bead_type_file)
non_acetyl_types=$(python -c "
a=[$non_acetyl_types]
list.sort(a)
for i in a: 
    print i,")

# Polymer bead types with non-active signal (K9me3, K27me3, no mark)
poly_inact_types=$(echo 1 $poly_pcmb_types $poly_het_types)

####################

# 5. Generate the LAMMPS script

# Notes:
# - Only need 2*nprottypes-1 extra atom types as the equilibrated sample
#   has already reserved an atom type for proteins
echo \
"# extrude.lam
##################################################
##################################################
# Main simulation - switching proteins + loop extrusion + crumpled chain
##################################################
##################################################

# Simulation basic setup

units lj
atom_style angle
boundary p p p
package omp 1
comm_style tiled
neighbor ${neigh_length} bin
neigh_modify every ${neigh_every} delay ${neigh_delay} check yes

read_data ${init_bead_file} & 
extra/atom/types $((2*nprottypes-1)) extra/bond/types 1 & 
extra/special/per/atom 2 extra/bond/per/atom 1

##################################################

# Define groups 

group all type $(eval echo {1..$ntotaltypes})
group poly type $(eval echo {1..$npolytypes})
group prot type $(eval echo {$((npolytypes+1))..$ntotaltypes})
" > $extrude_lam_path
for (( i=$((npolytypes+2)); i<=$ntotaltypes; i++ ))
do 
    echo "mass $i 1.0" >> $extrude_lam_path
done

# For switching proteins
if [[ $switch == 1 ]]; then # Proteins are switching
    # Atom id of the starting and end atom for each protein group
    on_act_start=$((1+$npolybeads))
    on_act_end=$(($npolybeads+$nact/2))
    off_act_start=$((1+$on_act_end))
    off_act_end=$(($npolybeads+$nact))
    on_pcmb_start=$((1+$off_act_end))
    on_pcmb_end=$(($off_act_end+$npcmb/2))
    off_pcmb_start=$((1+$on_pcmb_end))
    off_pcmb_end=$(($off_act_end+$npcmb))
    on_het_start=$((1+$off_pcmb_end))
    on_het_end=$(($off_pcmb_end+$nhet/2))
    off_het_start=$((1+$on_het_end))
    off_het_end=$(($off_pcmb_end+$nhet))
    if [[ $inact_prot == 1 ]]; then
	on_inact_start=$((1+$off_het_end))
	on_inact_end=$(($off_het_end+$ninact/2))
	off_inact_start=$((1+$on_inact_end))
	off_inact_end=$(($off_het_end+$ninact))
	other_start=$((1+$off_inact_end))
    else
	other_start=$((1+$off_het_end))
    fi
    other_end=$nbeads
    
    # Assign protein beads to groups and remove extra beads
    echo "
# Assign proteins to a specific group
# Active groups
group totype id <> ${on_act_start} ${on_act_end}
set group totype type ${on_act_type}
group totype delete
group totype id <> ${off_act_start} ${off_act_end}
set group totype type ${off_act_type}
group totype delete

# Polycomb groups
group totype id <> ${on_pcmb_start} ${on_pcmb_end}
set group totype type ${on_pcmb_type}
group totype delete
group totype id <> ${off_pcmb_start} ${off_pcmb_end}
set group totype type ${off_pcmb_type}
group totype delete

# Heterochromatin groups
group totype id <> ${on_het_start} ${on_het_end}
set group totype type ${on_het_type}
group totype delete
group totype id <> ${off_het_start} ${off_het_end}
set group totype type ${off_het_type}
group totype delete" >> $extrude_lam_path

    if [[ $inact_prot == 1 ]]; then
	echo "
# Inactive groups
group totype id <> ${on_inact_start} ${on_inact_end}
set group totype type ${on_inact_type}
group totype delete
group totype id <> ${off_inact_start} ${off_inact_end}
set group totype type ${off_inact_type}
group totype delete" >> $extrude_lam_path
    fi

    if [[ $other_start < $other_end ]]; then
	echo "
# Delete other groups
group todel id <> ${other_start} ${other_end}
delete_atoms group todel" >> $extrude_lam_path
    fi
else # Proteins are not switching
    # Atom id of the starting and end atom for each protein group
    on_act_start=$((1+$npolybeads))
    on_act_end=$(($npolybeads+$nact))
    on_pcmb_start=$((1+$on_act_end))
    on_pcmb_end=$(($on_act_end+$npcmb))
    on_het_start=$((1+$on_pcmb_end))
    on_het_end=$(($on_pcmb_end+$nhet))
    if [[ $inact_prot ]]; then 
	on_inact_start=$((1+$on_het_start))
	on_inact_end=$(($on_het_end+$ninact))
	other_start=$((1+$on_inact_end))
    else
	other_start=$((1+$on_het_end))
    fi
    other_end=$nbeads
    
    # Assign protein beads to groups and remove extra beads
    echo "
# Assign proteins to a specific group
# Active groups
group totype id <> ${on_act_start} ${on_act_end}
set group totype type ${on_act_type}
group totype delete

# Polycomb groups
group totype id <> ${on_pcmb_start} ${on_pcmb_end}
set group totype type ${on_pcmb_type}
group totype delete

# Heterochromatin groups
group totype id <> ${on_het_start} ${on_het_end}
set group totype type ${on_het_type}
group totype delete" >> $extrude_lam_path

    if [[ $inact_prot == 1 ]]; then
echo "
# Inactive groups
group totype id <> ${on_inact_start} ${on_inact_end}
set group totype type ${on_inact_type}
group totype delete
group totype id <> ${off_inact_start} ${off_inact_end}
set group totype type ${off_inact_type}
group totype delete" >> $extrude_lam_path
    fi

    if [[ $other_start < $other_end ]]; then
	echo "
# Delete other groups
group todel id <> ${other_start} ${other_end}
delete_atoms group todel" >> $extrude_lam_path
    fi
fi    

echo "
##################################################

# Simulation box

change_box all x final ${xlo} ${xhi} y final ${ylo} ${yhi} & 
z final ${zlo} ${zhi} boundary p p p units box

##################################################

# Set Brownian dynamics

fix 1 all nve
fix 2 all langevin 1.0 1.0 0.5 ${extrude_seed}
fix 3 all balance ${extrude_balancefreq} 1.08 rcb

##################################################

# Dumps/output

variable numbonds equal bonds
compute gyr all gyration
thermo ${extrude_thermofreq}
thermo_style custom step temp epair c_gyr v_numbonds
dump 1 all custom ${extrude_printfreq} ${extrude_trajfile} &
id type xs ys zs ix iy iz

##################################################

# Potentials

bond_style hybrid fene/omp harmonic/omp harmlj
special_bonds fene
bond_coeff 1 fene/omp 30.0 1.6 1.0 1.0
bond_coeff 2 harmonic/omp 200.0 1.1 # Crumpled springs
bond_coeff 3 harmlj 40.0 1.5 1.0

angle_style cosine/omp
angle_coeff 1 ${persist_length}

pair_style lj/cut/omp 1.122462048309373
pair_modify shift yes
pair_coeff * * 1.0 1.0 1.122462048309373

##################################################
" >> $extrude_lam_path

# Create a LAMMPS script for protein-polymer attraction
file=$prot_attract_lam_path
echo \
"# prot-attract.lam
# Protein-chromatin attraction

# Type ${on_act_type} -- active binders
# Type ${on_pcmb_type} -- polycomb binders
# Type ${on_het_type} -- heterochromatin binders" > $file
if [[ $inact_prot == 1 ]]; then
echo "# Type ${on_inact_type} -- inactive/unmarked binders" >> $file
fi

echo "
# Weak active (H3K27ac)" >> $file
for i in $poly_weak_active_types
do
    echo "pair_coeff $i ${on_act_type} ${e_act_act} ${sigma} ${cutoff}" >> $file
done

echo "# Strong active (ATAC)" >> $file
for i in $poly_strong_active_types
do
    echo "pair_coeff $i ${on_act_type} ${e_act_sact} ${sigma} ${cutoff}" >> $file
done

echo "# Polycomb (H3K27me3)" >> $file
for i in $poly_pcmb_types
do
    echo "pair_coeff $i ${on_pcmb_type} ${e_pcmb_pcmb} ${sigma} ${cutoff}" >> $file
done

echo "# Heterochromatin (H3K9me3)" >> $file
for i in $poly_het_types
do
    echo "pair_coeff $i ${on_het_type} ${e_het_het} ${sigma} ${cutoff}" >> $file
done

if [[ $inact_prot == 1 ]]; then
    echo "# Inactive (H3K9me3/H3K27me3/unmarked)" >> $file
    for i in $poly_inact_types
    do
	echo "pair_coeff $i ${on_inact_type} ${e_inact_inact} ${sigma} ${cutoff}" >> $file
    done
fi

# Create a LAMMPS script for (non-specific) chromatin-chromatin attraction
file=$chrom_attract_lam_path
echo \
"# chrom-attract.lam
# Chromatin-chromatin attraction" > $file

if [[ $non_specific == 1 ]]; then
#    echo "# Non-specific chromatin-chromatin attraction" >> $file
    # Determine the possible pairs of interactions
    # If the interaction only applies to beads with no marks (type 1)
    if [[ $no_mark_int == 1 ]]; then
	echo "pair_coeff 1 1 ${e_chrom_chrom} ${sigma} ${cutoff}" >> $file
    else
    # If the interaction applies to all beads except those with K27ac
    # Find the bead types with !K27ac
	for i in $non_acetyl_types
	do
	    for j in $non_acetyl_types
	    do
		if (( $i <= $j )); then
		    echo "pair_coeff $i $j ${e_chrom_chrom} ${sigma} ${cutoff}" >> $file
		fi
	    done
	done
    fi
fi

# Create a LAMMPS script for switching on/off proteins
if [[ $switch == 1 ]]; then # Proteins are switching
    echo \
"# switch.lam
# Protein on/off switching

# Active
group on_act type ${on_act_type}
group off_act type ${off_act_type}
set group on_act type/fraction ${off_act_type} \${actswitchprob} \${switchseed}
set group off_act type/fraction ${on_act_type} \${actswitchprob} \${switchseed}
group on_act delete
group off_act delete

# Polycomb
group on_pcmb type ${on_pcmb_type}
group off_pcmb type ${off_pcmb_type}
set group on_pcmb type/fraction ${off_pcmb_type} \${inactswitchprob} \${switchseed}
set group off_pcmb type/fraction ${on_pcmb_type} \${inactswitchprob} \${switchseed}
group on_pcmb delete
group off_pcmb delete

# Heterochromatin
group on_het type ${on_het_type}
group off_het type ${off_het_type}
set group on_het type/fraction ${off_het_type} \${inactswitchprob} \${switchseed}
set group off_het type/fraction ${on_het_type} \${inactswitchprob} \${switchseed}
group on_het delete
group off_het delete" > $switch_lam_path

if  [[ $inact_prot == 1 ]]; then
    echo "
# Inactive
group on_inact type ${on_inact_type}
group off_inact type ${off_inact_type}
set group on_inact type/fraction ${off_inact_type} \${inactswitchprob} \${switchseed}
set group off_inact type/fraction ${on_inact_type} \${inactswitchprob} \${switchseed}
group on_inact delete
group off_inact delete" >> $switch_lam_path
fi

else # Proteins are not switching
    > $switch_lam_path
fi

####################

# 6. Modify other scripts

# Modify extrusion parameter file
echo "
npolybeads ${npolybeads}
nextruders ${nextruders}
dt ${extrude_dt}
runtime ${extrude_run}
sep_thres ${sep_thres}
extrude_seed ${extrude_prob_seed}
switch_seed ${switch_seed}
extrude_buffer ${extrude_buffer}
extrude_bond_type ${extrude_bond_type}
extrude_init_sep ${extrude_init_sep}
update_time ${update_time}
on_time ${on_time}
off_time ${off_time}
extrude_time ${extrude_time}
act_switch_time ${act_switch_time}
inact_switch_time ${inact_switch_time}
prot_attract_start_time ${prot_attract_start_time}
prot_attract_lammps_file ${prot_attract_lam}
chrom_attract_start_time ${chrom_attract_start_time}
chrom_attract_lammps_file ${chrom_attract_lam}
switch_lammps_file ${switch_lam}
extrude_lammps_file ${extrude_lam}
extrude_pos_file ${extrude_posfile}
extrude_log_file ${extrude_pylog}
lammps_log_file ${extrude_log}
lammps_screen_file ${extrude_screenlog}
ctcf_prob_file ${ctcf_prob_file}
ctcf_map_file ${ctcf_map_file}
restart_file ${extrude_restartfile}
end_file ${extrude_beadfile}" > $extrude_params_path

# Modify run script
file=$run_sh_path
perl -pi -e "s/EXTRUDE_PARAMS/${extrude_params}/g" $file
