#!/bin/bash
# A script to extra crumpled springs in regions where there is a lack of an
# epigenetic mark associated with open chromatin region (e.g., H3K27ac)

# Check arguments
if [[ $# != 6 ]];then
    echo "Usage: add_crumple_springs.sh typesfile typestring bondtype"
    echo "       inlammpsfile outlammpsfile outdir"
    echo "where  typesfile       is the bead types file"
    echo "       typestring      is the bead attribute where there are NO "
    echo "                       crumple springs (e.g. H3K27ac)"
    echo "       bondtype        is the bond type"
    echo "       inlammpsfile    is the orignal LAMMPS bead data file"
    echo "       outlammpsfile   is the updated LAMMPS bead data file"
    echo "       outdir          is the output directory"
    exit 1
fi

typesfile=$1
typestring=$2
bondtype=$3
inlammpsfile=$4
outlammpsfile=$5
outdir=$6

# Check files and directories
if [[ ! -f $typesfile ]]; then
    echo "Error: File ${typesfile} not found"
    exit 1
fi

if [[ ! -f $inlammpsfile ]]; then
    echo "Error: File ${inlammpsfile} not found"
    exit 1
fi

if [[ ! -d $outdir ]]; then
    mkdir -p $outdir
else
    if [[ -f $outlammpsfile ]]; then
	echo "Error: File ${outlammpsfile} already exists"
	exit 1
    fi
fi

# Get the list of bead types that has the specified epigenetic mark
typeslist=$(awk -v type=$typestring '{
if ($1=="#" && $2=="Type") { 
  split($0,args," ")
  for (i in args) {
    if (args[i]==type) {line = line " " args[3]; break}
  }
}} END {print line}' $typesfile)
typeslist=($typeslist)

if [[ ${#typeslist[@]} == 0 ]]; then
    echo "Error: Did not find bead types with $typestring"
    exit 1
fi

echo "Reading file $typesfile ..."
echo "Adding springs between all beads except those with $typestring ..."
echo "(that is bead types ${typeslist[@]})"

# Put an extra next-nearest neighbour spring between any triplet of beads 
# where none has the mark
spring_file="${outdir}/springs.dat"
awk -v btype=$bondtype -v typeslist="${typeslist[*]}" '
BEGIN {
  nbeads=0
  split(typeslist,list," ")
  for (i in list) {types[list[i]]=""}
}{
  if ($1!="#") {
    nbeads++
    if ($2 in types) {bead[$1]=1} else {bead[$1]=0}
  }
} END {
  count=1
  for (i=3;i<=nbeads;i++) {
    if (bead[i-2]==0 && bead[i-1]==0 && bead[i]==0) {
      print count,btype,i-2,i; count++
    }
  }
}' $typesfile > $spring_file

nsprings=$(wc -l $spring_file | awk '{print $1}')

echo "(which means adding $nsprings springs)"

awk -v nsprings=$nsprings -v springfile=$spring_file '
BEGIN{
  readBond=0
  readNewBond=0
  while ((getline<springfile)>0) {newbond[$1]=$2 " " $3 " " $4}
}{
  if ($2=="bonds") {nbonds=$1; $1+=nsprings}
  if ($2=="bond" && $3=="types") {$1++}
  if ($1=="Bonds") {readBond=1}
  if (readBond==1 && readNewBond==1 && $1=="") {
    for (i=1;i<=nsprings;i++) {print nbonds+i,newbond[i]}
  }
  if (readBond==1 && readNewBond==0 && $1=="") {readNewBond=1}
  if ($1=="Angles") {readBond=0}
  print
}' $inlammpsfile > $outlammpsfile
