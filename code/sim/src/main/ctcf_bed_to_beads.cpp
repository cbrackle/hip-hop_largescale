// ctct_bed_to_bead.cpp
// 
// Read bed file with list of motifs and CTCF peak names
// Keep one motif per peak.
// Assign a strand based on highest scoring motif within the peak
// Possibilities are: +
//                    -
//                    .   where the score of a motif on one strand
//                        is only 5% or less stronger than a motif
//                        on the other (percent different threshold).
//
// The bed file must have the following columns
//     chrom start end name peakscore strand motifscore
//
// Also, for the program to work, the bed file must have been sorted
// by name before being parsed to this program

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;
using std::sort;
using std::map;
using std::pair;
using std::stringstream;

struct Bedline {
  string chrom, name, strand;
  long int start, end;
  double peakscore, motifscore;

  Bedline(); // Default constructor
  Bedline(const Bedline&); // Copy constructor
  Bedline(const string&);
  bool operator<(const Bedline&) const; // Comparison for sorting
};

struct Region {
  string chrom;
  long int start, end;
  int bpPerBead, nbp, nbeads;
  Region();
  bool inRegion(const Bedline& bline) const;
  int bpToBead(const long int& bp) const;
  long int pos(const int& bead) const;
  long int beadStart(const int& bead) const;
  long int beadEnd(const int& bead) const;
};

struct Bead {
  vector<pair<double, string> > probList;
  map<string,double> probStrand;
  Bead();
  void computeStrandProbs();

private:
  void findProbs(map<string,int>& count, double accumProb, 
		 size_t index, int state);
};

bool readConfig(const string& filename, Region& region);

int main(int argc, char* argv[]) {  
  if (argc != 4) {
    cout << "Usage: ctcf_bed_to_beads configFile infile outfile" << endl;
    cout << "where  configFile  is the simulation configuration file." << endl;
    cout << "       infile      is the input bed file." << endl;
    cout << "       outfile     is the output bed file." << endl;
    cout << "The input bed file must have the following columns:" << endl;
    cout << "    chrom start end name peakscore strand motifscore" << endl;
    return 1;
  }

  int argi = 0;
  string configFile = string(argv[++argi]);
  string infile = string(argv[++argi]);
  string outfile = string(argv[++argi]);

  ifstream inf; 
  ofstream ouf;

  string line;
  stringstream ss;

  Region region;
  Bedline bedline, nextBedline, outBedline;
  vector<Bedline> bedlines;
  map<int,Bead> beads;

  inf.open(outfile);
  if (inf.good()) {
    cout << "Error: File " << outfile << " already exists. Will not overwrite."
	 << endl;
    inf.close();
    return 1;
  }

  inf.open(infile);
  if (!inf) {
    cout << "Error when trying to open the input bed file " << infile << endl;
    return 1;
  }

  ouf.open(outfile);
  if (!ouf) {
    cout << "Error when trying to open the output bed file " 
	 << outfile << endl;
    return 1;
  }
  
  // Read the configuration file
  if (!readConfig(configFile, region)) {
    cout << "Exiting due to error when reading the config file." << endl;
    return 1;
  }
  
  // Read the bedlines
  while (getline(inf, line)) {
    bedlines.push_back(Bedline(line));
  }
  inf.close();

  // Sort the bedlines by peakscore from highest to lowest
  sort(bedlines.rbegin(), bedlines.rend());
  
  // Assign a probability of occupancy for each peak
  // Assume that the top 10% of CTCFs are always on and the minimum 
  // probability of occupancy is 5%
  int nCTCFs = static_cast<int>(bedlines.size());
  const double minProb = 0.05;
  int maxScore = bedlines[static_cast<int>(nCTCFs*0.1)].peakscore;
  int minScore = bedlines.back().peakscore;
  int width = maxScore-minScore;
  cout << "Max: " << maxScore << endl;
  cout << "Min: " << minScore << endl;
  for (int i = 0; i < nCTCFs; i++) {
    // Only calculate the probability for bedlines within the region
    if (region.inRegion(bedlines[i])) {
      double prob = (1.0-minProb) * (bedlines[i].peakscore-minScore) / 
	width + minProb;
      if (prob > 1.0) prob = 1.0;
      int index = region.bpToBead(bedlines[i].start);
      string strand = bedlines[i].strand;
      if (beads.find(index) == beads.end()) {
	beads[index] = Bead();
      }
      beads[index].probList.push_back(std::make_pair(prob, strand));
    }
  }
  bedlines.clear();

  // Combine the probabilities for each bead
  for (map<int,Bead>::iterator it = beads.begin(); it != beads.end(); it++) {
    it->second.computeStrandProbs();
  }

  // Output the result
  Bead bead;
  for (map<int,Bead>::iterator it = beads.begin(); it != beads.end(); it++) {
    bead = it->second;
    ouf << (it->first+1) << " " << bead.probStrand["+"] << " "
	<< bead.probStrand["-"] << " " << bead.probStrand["."] << "\n";
  }
  ouf.close();
}

bool readConfig(const string& filename, Region& region) {
  ifstream inf;
  stringstream ss;
  string line;

  inf.open(filename);
  if (!inf) {
    cout << "Error opening config file " << filename << endl;
    return false;
  }

  // Get the first four lines on chromosome, start, end, and resolution
  getline(inf, line); ss.str(line); ss >> region.chrom; ss.clear();
  getline(inf, line); ss.str(line); ss >> region.start; ss.clear();
  getline(inf, line); ss.str(line); ss >> region.end; ss.clear();
  getline(inf, line); ss.str(line); ss >> region.bpPerBead; ss.clear();

  // Set up the region of interest in the chromosome
  region.nbp = region.end-region.start;
  if (region.nbp < 1) {
    cout << "Error: incorrect region specification." << endl;
    inf.close();
    return false;
  }

  region.nbeads = static_cast<int>
    (ceil(region.nbp/static_cast<double>(region.bpPerBead)));

  // Output region details
  cout << "Details of region:" << endl;
  cout << region.chrom << ":" << region.start << "-" << region.end << endl;
  cout << "Number of bps: " << region.nbp << endl;
  cout << "bps per bead: " << region.bpPerBead << endl;
  cout << "Number of beads: " << region.nbeads << endl;
  return true;
}

// ############################################################################
// Member functions of Bedline

Bedline::Bedline() {}

Bedline::Bedline(const Bedline& b) :
  chrom(b.chrom), name(b.name), strand(b.strand), start(b.start), end(b.end),
  peakscore(b.peakscore), motifscore(b.motifscore) {}

Bedline::Bedline(const string& line) {
  stringstream ss; 
  ss.str(line);
  ss >> chrom >> start >> end >> name >> peakscore >> strand >> motifscore;
}

bool Bedline::operator<(const Bedline& b) const {
  return peakscore < b.peakscore;
}

// ############################################################################
// Member functions of Region

Region::Region() {}

bool Region::inRegion(const Bedline& bline) const {
  return (bline.chrom == chrom && bline.end > start && bline.start < end);
}

int Region::bpToBead(const long int& bp) const {
  return static_cast<int>(floor(static_cast<double>(bp-start)/
				static_cast<double>(bpPerBead)));
}

long int Region::pos(const int& bead) const {
  return start+bpPerBead*bead+0.5*bpPerBead;
}

long int Region::beadStart(const int& bead) const {
  return start+bpPerBead*bead;
}

long int Region::beadEnd(const int& bead) const {
  // Note that this returns the last bp of the bead + 1 bp
  // (i.e. not inclusive)
  return start+bpPerBead*(bead+1);
}

// ############################################################################
// Member functions of Bead

Bead::Bead() {
  probList = vector<pair<double,string> >();
  probStrand = map<string,double>();
  probStrand["+"] = 0.0;
  probStrand["-"] = 0.0;
  probStrand["."] = 0.0;
}


void Bead::computeStrandProbs() {
  if (probList.size() > 0) {
    map<string,int> count;
    count["+"] = 0;
    count["-"] = 0;
    count["."] = 0;
    probStrand["+"] = 0.0;
    probStrand["-"] = 0.0;
    probStrand["."] = 0.0;
    // First peak being ON
    findProbs(count, 1.0, 0, 1);
    // First peak being OFF
    findProbs(count, 1.0, 0, 0);
  }
}

// Tally up the probabilities of all the combinations recursively
void Bead::findProbs(map<string,int>& count, double accumProb, 
		     size_t index, int state) {
  double currentProb = probList[index].first;
  string strand = probList[index].second;
  if (state == 1) { // Current peak is ON
    accumProb *= currentProb;
    count[strand]++;
  } else { // Current peak is OFF
    accumProb *= (1.0-currentProb);
  }
  if (index >= probList.size()-1) {
    // Reached the last peak
    if (count["."] > 0 || (count["+"] > 0 && count["-"] > 0)) {
      probStrand["."] += accumProb;
    } else if (count["+"] > 0 && count["-"] == 0) {
      probStrand["+"] += accumProb;
    } else if (count["+"] == 0 && count["-"] > 0) {
      probStrand["-"] += accumProb;
    }
  } else {
    // Go to the next peak being ON
    findProbs(count, accumProb, index+1, 1);
    // Go to the next peak being OFF
    findProbs(count, accumProb, index+1, 0);
  }
  if (state == 1) {
    // Need to undo the strand count as the map is passed by reference
    count[strand]--; 
  }
}
