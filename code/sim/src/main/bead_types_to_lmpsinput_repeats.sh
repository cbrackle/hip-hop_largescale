#!/bin/bash
# Script to renumber atom types in a LAMMPS input file

# Check arguments
if [[ $# != 6 ]]; then
    echo " Usage: bead_types_to_lmpsinput.sh beadtype gap infile outfile"
    echo " where  gap       is the size of the 'spacer' region"
    echo "        typesfile is the file listing each bead's type"
#    echo "        fulltypesfile  is the updated listing of each bead's type"
    echo "        infile    is the LAMMPS bead data file from equilibration"
    echo "        outfile   is the LAMMPS bead data file for the main run"
    echo "        indexfile stores the start and end position of each repeat"
    echo "        outdir    is the directory for the main sim LAMMPS file"
    echo " Assumes input has polymer beads as type 1, and"
    echo " protein bead as types > 1"
    echo " Assumes bead ids present in beadlist are polymer"
    echo " beads and all others are proteins"
    exit 1
fi

gap=$1
typesfile=$2
infile=$3
outfile=$4
indexfile=$5
outdir=$6

# Check files and directories
if [[ ! -f $typesfile ]]; then
    echo "Error: File ${typesfile} not found"
    exit 1
fi

if [[ ! -f $infile ]]; then
    echo "Error: File ${infile} not found"
    exit 1
fi

if [[ ! -d $outdir ]]; then
    mkdir -p $outdir
else
    if [[ -e $outfile ]]; then
	echo "Error: File ${outfile} already exists"
	exit 1
    fi
fi

# Make temporary files
types_tmp="${outdir}/types.tmp"
header_tmp="${outdir}/header.tmp"
pos_tmp="${outdir}/pos.tmp"
vel_tmp="${outdir}/vel.tmp"
> $types_tmp
> $header_tmp
> $pos_tmp
> $vel_tmp

echo "Reading types file $typesfile ..."
# Load the types files
# Read the type of each bead and count the total number of beads
awk '{if(NF==2&&$1!="#"){print}}' $typesfile > $types_tmp
Ndna=$(wc -l $types_tmp | awk '{print $1}')

# Get the number of bead types
Ndnatypes=$(awk 'BEGIN{max=0}{if($2>max){max=$2}}END{print max}' $types_tmp)

# Get the number of polymer beads from the LAMMPS file
Npolybeads=$(awk '{if(NF==9&&$3==1){a++}}END{print a}' $infile)

# Get the total number of beads/atoms from the LAMMPS file
# Natoms can be greater than Ndna as there are protein beads as well
Nbeads=$(awk '{if($2=="atoms"){print $1}}' $infile)
Ntotaltypes=$(( $Ndnatypes + 1 ))

# Determine the number of possible repeats and the initial and final gap size
Nrepeats=$(python -c "print ($Npolybeads+$gap)/($Ndna+$gap)")
start_gap=$(python -c "print ($Npolybeads-$Nrepeats*($Ndna+$gap)+$gap)/2")
end_gap=$(python -c "print $Npolybeads-$start_gap-$Nrepeats*($Ndna+$gap)+$gap")

#awk -v Nd=$Ndna -v Nb=$Nbeads -v ptype=$Ntotaltypes '{print}END{
#for (i=Nd+1; i<=Nb; i++){print i, ptype}}' $types_tmp > $types_tmp.2
#mv $types_tmp.2 $types_tmp

# Edit LAMMPS files
echo "Editing lammps inputs with $Ndna polymer atoms"
echo "                           $Nbeads total atoms"
echo "                           $Ndnatypes polymer atom types"
echo "                           $(( $Ndnatypes + 1 )) total atom types"

echo "Converting file $infile to $outfile ..."

# Split infile into three parts
# Header information
awk 'BEGIN{noprint=0}{
if (noprint==0) {print}
if ($1=="Atoms") {noprint=1}
}' $infile > $header_tmp

# Bead position information
awk 'BEGIN{noprint=1}{
if (noprint==0 && $1=="Velocities") {noprint=1}
if (noprint==0 && $0!="") {print}
if ($1=="Atoms")  {noprint=0}
}' $infile > $pos_tmp

# Bead velocity information
awk 'BEGIN{noprint=1}{
if ($1=="Velocities") {noprint=0}
if (noprint==0) {print}
}' $infile > $vel_tmp

# Sort the beads
sort -g $pos_tmp > $pos_tmp.2
mv $pos_tmp.2 $pos_tmp

# Extend the type file to have the same length as the polymer
# Note that type 1 is always set to no signal (for spacer region)
# Start gap
bead=1
awk -v s=$bead -v e=$start_gap \
    'BEGIN {for (i=s;i<=e;i++){print i,1}}' >> $types_tmp.2
bead=$(($bead+$start_gap))
for (( i=0; $i<$Nrepeats; i++ ))
do
    awk -v s=$(($bead-1)) '{$1+=s;print}' $types_tmp >> $types_tmp.2
    bead=$(($bead+$Ndna))
    if [[ $i == $(($Nrepeats-1)) ]]; then break; fi
    awk -v s=$bead -v e=$(($bead+$gap)) \
	'BEGIN {for (i=s;i<e;i++){print i,1}}' >> $types_tmp.2
    bead=$(($bead+$gap))
done
awk -v s=$bead -v e=$(($bead+$end_gap)) \
    'BEGIN {for (i=s;i<e;i++){print i,1}}' >> $types_tmp.2

# Add the protein beads
awk -v Nd=$Npolybeads -v Nb=$Nbeads -v ptype=$Ntotaltypes \
'BEGIN{for (i=Nd+1; i<=Nb; i++){print i, ptype}}' >> $types_tmp.2
mv $types_tmp.2 $types_tmp

# Modify the LAMMPS file with the correct bead types
# The first Ndna beads must be polymer beads
paste $types_tmp $pos_tmp | awk -v Nd=$Npolybeads -v NdT=$Ndnatypes '{
if ($1<=Nd) {$5=$2;$1="";$2="";print}
else {$5=$5+NdT-1;$1="";$2="";print}
}' > $pos_tmp.2
mv $pos_tmp.2 $pos_tmp

# Edit the header of file
awk -v ntypes=$Ntotaltypes '{if($2=="atom"&&$3=="types"){$1=ntypes}; print}' $header_tmp > $header_tmp.2
awk -v ntypes=$Ntotaltypes '{if($0=="Masses"){print "Masses\n"; for(i=1;i<=ntypes;i++){print i,1}; print ""; print "Atoms\n"; exit 1}; print}' $header_tmp.2 > $header_tmp

# Stick them all back together again
cat $header_tmp $pos_tmp > $outfile
echo "" >> $outfile
cat $vel_tmp >> $outfile

# Output the full bead type file (include spacer region)
#awk '{if($1=="#"){print}}' $typesfile > $fulltypesfile
#awk -v Nd=$Npolybeads '{if(NF==9&&$1<=Nd){print $1,$3}}' $outfile >> $fulltypesfile

# Output the start and end index of each repeat
index=$start_gap
for (( i=1; $i <= $Nrepeats; i++ ))
do
    start=$(($index+1))
    end=$(($index+$Ndna))
    echo "$i $start $end" >> $indexfile
    index=$(($index+$Ndna+$gap))
done

# Clean up - remove temporary files
rm $header_tmp*
rm $types_tmp $pos_tmp $vel_tmp
