// peak_motif_strand.cpp
// 
// Read bed file with list of motifs and CTCF peak names
// Keep one motif per peak.
// Assign a strand based on highest scoring motif within the peak
// Possibilities are: +
//                    -
//                    .   where the score of a motif on one strand
//                        is only 5% or less stronger than a motif
//                        on the other (percent different threshold).
//
// The bed file must have the following columns
//     chrom start end name peakscore strand motifscore
//
// Also, for the program to work, the bed file must have been sorted
// by name before being parsed to this program

#define DIFFTHRESH 5 // Percent difference threshold

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;
using std::sort;
using std::istringstream;

struct Bedline {
  string chrom, start, end, name, peakscore, strand;
  double motifscore;
  
  Bedline(); // Default constructor
  Bedline(const Bedline&); // Copy constructor
  Bedline(const string&);
  bool operator<(const Bedline&) const; // Comparison for sorting
};

// Function to find the motif with the highest scoring
void findMotif(vector<Bedline>& bedlines, Bedline* outBedline);

// Function to write a particular bedline
void writeMotif(ofstream& ouf, const Bedline& bedline);

int main(int argc, char* argv[]) {  
  if (argc != 3) {
    cout << "Usage: peak_motif_strand infile outfile" << endl;
    cout << "where  infile   is the input bed file." << endl;
    cout << "       outfile  is the output bed file." << endl;
    cout << "The input bed file must have the following columns:" << endl;
    cout << "    chrom start end name peakscore strand motifscore" << endl;
    cout << "and must be sorted by name first, then by chrom, and " << endl;
    cout << "finally by end before being parsed to this program." << endl;
    return 1;
  }

  int argi = 0;
  string infile = string(argv[++argi]);
  string outfile = string(argv[++argi]);

  ifstream inf; 
  ofstream ouf;

  string line;
  istringstream iss;

  Bedline bedline, nextBedline, outBedline;
  vector<Bedline> bedlines;

  inf.open(outfile);
  if (inf.good()) {
    cout << "Error: File " << outfile << " already exists. Will not overwrite."
	 << endl;
    inf.close();
    return 1;
  }

  inf.open(infile);
  if (!inf) {
    cout << "Error when trying to open the input bed file " << infile << endl;
    return 1;
  }

  ouf.open(outfile);
  if (!ouf) {
    cout << "Error when trying to open the output bed file " 
	 << outfile << endl;
    return 1;
  }

  if (getline(inf, line)) {
    bedline = Bedline(line);
    bedlines.push_back(bedline);
  } else {
    cout << "No lines in the bed file." << endl;
    return 1;
  }

  while (!inf.eof()) {
    while (getline(inf, line)) {
      nextBedline = Bedline(line);
      if (bedline.name == nextBedline.name) {
	bedlines.push_back(nextBedline);
      } else {
	findMotif(bedlines, &outBedline);
	writeMotif(ouf, outBedline);
	bedline = nextBedline;
	bedlines.clear();
	bedlines.push_back(bedline);
      }
    }
  }
  findMotif(bedlines, &outBedline);
  writeMotif(ouf, outBedline);
  inf.close();
  ouf.close();
}

Bedline::Bedline() {}

Bedline::Bedline(const Bedline& b) :
  chrom(b.chrom), start(b.start), end(b.end), name(b.name), 
  peakscore(b.peakscore), strand(b.strand), motifscore(b.motifscore) {}

Bedline::Bedline(const string& line) {
  istringstream iss; 
  iss.str(line);
  iss >> chrom >> start >> end >> name >> peakscore >> strand >> motifscore;
}

bool Bedline::operator<(const Bedline& b) const {
  return motifscore < b.motifscore;
}

void findMotif(vector<Bedline>& bedlines, Bedline* outBedline) {
  // Check if there is only one motif in this peak
  if (bedlines.size() == 1) {
    *outBedline = bedlines[0]; 
    return;
  }

  // Sort the lines by motifscore from highest to lowest
  sort(bedlines.begin(), bedlines.end()); 
  
  bool allSameStrand = true;
  for (size_t i = 0; i < bedlines.size(); i++) {
    for (size_t j = i+1; j < bedlines.size(); j++) {
      if (bedlines[i].strand != bedlines[j].strand) {
	allSameStrand = false;
	break;
      }
    }
  }

  if (allSameStrand) {
    // They are all on the same strand, just return the line with the 
    // highest scoring
    *outBedline = bedlines.back();
    return;
  } else if (bedlines.back().strand == (bedlines.rbegin()+1)->strand) {
    // Top two lines are on the same strand, just return the line with the
    // highest scoring
    *outBedline = bedlines.back();
    return;
  } else {
    double percentDiff;
    double percentThresh = DIFFTHRESH;
    percentDiff = 100*(bedlines.back().motifscore - 
		       (bedlines.rbegin()+1)->motifscore)/
      bedlines.back().motifscore;
    if (percentDiff > percentThresh) {
      // Difference is big, just output the line with the highest scoring
      *outBedline = bedlines.back();
    } else {
      // Difference is small, so say it's on both
      *outBedline = bedlines.back();
      outBedline->strand = ".";
    }
  }
}

void writeMotif(ofstream& ouf, const Bedline& b) {
  ouf << b.chrom << "\t" << b.start << "\t" << b.end << "\t" << b.name << "\t"
      << b.peakscore << "\t" << b.strand << "\t" << b.motifscore << "\n";
}
