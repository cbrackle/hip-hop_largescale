#!/bin/bash
# A script to run the main extrusion simulation of the chromosome with LAMMPS

sh_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [[ $# != 1 ]]; then
    echo "Usage: nprocs"
    exit 1
fi

nprocs=$1

# Run the main simulation
mpirun -np $nprocs ${sh_dir}/programs/extrude.py ${sh_dir}/EXTRUDE_PARAMS 0
