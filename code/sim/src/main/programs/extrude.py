#!/bin/env python3
# extrude.py
# A script which runs LAMMPS coupled with loop extrusion

from mpi4py import MPI
import sys
import random
from array import array
import math
from lammps import lammps

###################

# Classes and functions to handle the parameters and the bond list
class ExtrudeParams(object):
    def __init__(self):
        self.npolybeads = None
        self.nextruders = None
        self.dt = None
        self.runtime = None
        self.sepThres = None
        self.extrudeSeed = None
        self.switchSeed = None
        self.extrudeBuffer = None
        self.extrudeBondType = None
        self.extrudeInitSep = None
        self.updateTime = None
        self.onTime = None
        self.offTime = None
        self.extrudeTime = None
        self.switchTime = None
        self.protAttractStartTime = None
        self.protAttractLAMMPSFile = None
        self.chromAttractStartTime = None
        self.chromAttractLAMMPSFile = None
        self.switchLAMMPSFile = None
        self.extrudeLAMMPSFile = None
        self.CTCFProbFile = None
        self.CTCFMapFile = None
        self.extrudePosFile = None
        self.extrudeLogFile = None
        self.LAMMPSLogFile = None
        self.LAMMPSScreenFile = None
        self.restartFile = None
        self.endFile = None
    
    def readParams(self, paramFile):
        with open(paramFile, 'r') as reader:
            for line in reader:
                if (line.startswith('#') or line == '\n'): continue
                args = line.split()
                if (len(args) < 2):
                    print("Error: not enough arguments (need 2)")
                    sys.exit(1)
                field, value = args[0], args[1]
                if (field == "npolybeads"):
                    self.npolybeads = int(value)                    
                elif (field == "nextruders"):
                    self.nextruders = int(value)
                elif (field == "dt"):
                    self.dt = float(value)
                elif (field == "runtime"):
                    self.runtime = int(value)
                elif (field == "sep_thres"):
                    self.sepThres = float(value)
                elif (field == "extrude_seed"):
                    self.extrudeSeed = int(value)
                elif (field == "switch_seed"):
                    self.switchSeed = int(value)
                elif (field == "extrude_buffer"):
                    self.extrudeBuffer = int(value)
                elif (field == "extrude_bond_type"):
                    self.extrudeBondType = int(value)
                elif (field == "extrude_init_sep"):
                    self.extrudeInitSep = int(value)
                elif (field == "update_time"):
                    self.updateTime = float(value)
                elif (field == "on_time"):
                    self.onTime = float(value)
                elif (field == "off_time"):
                    self.offTime = float(value)
                elif (field == "extrude_time"):
                    self.extrudeTime = float(value)
                elif (field == "switch_time"):
                    self.switchTime = float(value)
                elif (field == "prot_attract_start_time"):
                    self.protAttractStartTime = float(value)
                elif (field == "prot_attract_lammps_file"):
                    self.protAttractLAMMPSFile = value
                elif (field == "chrom_attract_start_time"):
                    self.chromAttractStartTime = float(value)
                elif (field == "chrom_attract_lammps_file"):
                    self.chromAttractLAMMPSFile = value
                elif (field == "switch_lammps_file"):
                    self.switchLAMMPSFile = value
                elif (field == "extrude_lammps_file"):
                    self.extrudeLAMMPSFile = value
                elif (field == "ctcf_prob_file"):
                    self.CTCFProbFile = value
                elif (field == "ctcf_map_file"):
                    self.CTCFMapFile = value
                elif (field == "extrude_pos_file"):
                    self.extrudePosFile = value
                elif (field == "extrude_log_file"):
                    self.extrudeLogFile = value
                elif (field == "lammps_log_file"):
                    self.LAMMPSLogFile = value
                elif (field == "lammps_screen_file"):
                    self.LAMMPSScreenFile = value
                elif (field == "restart_file"):
                    self.restartFile = value
                elif (field == "end_file"):
                    self.endFile = value
                else:
                    print("Error: unknown entry in parameter file: %s" % line)
                    sys.exit(1)
        if (self.npolybeads == None or self.nextruders == None or 
            self.dt == None or self.runtime == None or self.sepThres == None or
            self.extrudeSeed == None or self.switchSeed == None or 
            self.extrudeBuffer == None or self.extrudeBondType == None or
            self.extrudeInitSep == None or self.updateTime == None or 
            self.onTime == None or self.offTime == None or 
            self.extrudeTime == None or self.switchTime == None or
            self.protAttractStartTime == None or 
            self.protAttractLAMMPSFile == None or
            self.chromAttractStartTime == None or 
            self.chromAttractLAMMPSFile == None or
            self.switchLAMMPSFile == None or self.extrudeLAMMPSFile == None or
            self.CTCFProbFile == None or self.CTCFMapFile == None or 
            self.extrudePosFile == None or self.extrudeLogFile == None or 
            self.LAMMPSLogFile == None or self.LAMMPSScreenFile == None or 
            self.restartFile == None or self.endFile == None):
            print("Error: not all arguments specified")
            sys.exit(1)

def deleteBond(commands, occupied, s1, s2, bondType):
    commands.append("group db id %d %d" % (s1, s2))
    commands.append("delete_bonds db bond %d remove special" % bondType)
    commands.append("group db delete")
    occupied[s1] = 0
    occupied[s2] = 0

def addBond(commands, occupied, s1, s2, bondType, thres):
    commands.append("group nb id %d %d" % (s1, s2))
    commands.append("create_bonds many nb nb %d 0.0 %f" % (bondType, thres))
    commands.append("group nb delete")
    occupied[s1] = 1
    occupied[s2] = 1

def idx(index): 
    return 3*index-3

def idy(index):
    return 3*index-2

def idz(index):
    return 3*index-1

def dist(s1, s2, coords, boxsize):
    absdx = abs(coords[idx(s1)]-coords[idx(s2)])
    absdy = abs(coords[idy(s1)]-coords[idy(s2)])
    absdz = abs(coords[idz(s1)]-coords[idz(s2)])
    if (absdx > 0.5*boxsize[0]):
        absdx = boxsize[0]-absdx
    if (absdy > 0.5*boxsize[1]):
        absdy = boxsize[1]-absdy
    if (absdz > 0.5*boxsize[2]):
        absdz = boxsize[2]-absdz
    return math.sqrt(absdx*absdx+absdy*absdy+absdz*absdz)

###################

# Start the main program

# Set up MPI 
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

# Check arguments
args = sys.argv
if (len(args) != 3):
    print("extrude.py paramsFile quiet")
    sys.exit(1)

paramsFile = args[1]    # Parameter file
quiet = bool(int(args[2]))   # Print to screen or not

####################

# Read parameter values from file

params = ExtrudeParams()
if (rank == 0):
    params.readParams(paramsFile)
params = comm.bcast(params, root=0)
        
# Set parameters
npolybeads = params.npolybeads
nextruders = params.nextruders
runtime = params.runtime
dt = params.dt
sepThres = params.sepThres
extrudeSeed = params.extrudeSeed
switchSeed = params.switchSeed
updateTime = params.updateTime
extrudeRate = 1.0/params.extrudeTime
onRate = 1.0/params.onTime
offRate = 1.0/params.offTime
switchRate = 1.0/params.switchTime
extrudeBuffer = params.extrudeBuffer
extrudeBondType = params.extrudeBondType
extrudeInitSep = params.extrudeInitSep
regionStart = extrudeBuffer
regionEnd = npolybeads-extrudeBuffer
protAttractStartTime = params.protAttractStartTime
protAttractLAMMPSFile = params.protAttractLAMMPSFile
chromAttractStartTime = params.chromAttractStartTime
chromAttractLAMMPSFile = params.chromAttractLAMMPSFile
switchLAMMPSFile = params.switchLAMMPSFile
extrudeLAMMPSFile = params.extrudeLAMMPSFile
CTCFProbFile = params.CTCFProbFile
CTCFMapFile = params.CTCFMapFile
extrudePosFile = params.extrudePosFile
extrudeLogFile = params.extrudeLogFile
LAMMPSLogFile = params.LAMMPSLogFile
LAMMPSScreenFile = params.LAMMPSScreenFile
restartFile = params.restartFile
endFile = params.endFile

# Convert time/rate variables in units of timesteps
runtimeStep = int(runtime/dt)
updateTimeStep = int(updateTime/dt)
protAttractStartTimeStep = int(protAttractStartTime/dt)
chromAttractStartTimeStep = int(chromAttractStartTime/dt)
numOfIntervals = int(runtimeStep/updateTimeStep)
extrudeRateStep = extrudeRate*dt
onRateStep = onRate*dt
offRateStep = offRate*dt
switchRateStep = switchRate*dt

stepProb = updateTimeStep*extrudeRateStep
addProb = updateTimeStep*onRateStep
removeProb = updateTimeStep*offRateStep
switchProb = updateTimeStep*switchRateStep
if (stepProb > 1.0): stepProb = 1.0
if (addProb > 1.0): addProb = 1.0
if (removeProb > 1.0): removeProb = 1.0
if (switchProb > 1.0): switchProb = 1.0

# Set up random generator
random.seed(extrudeSeed)

# Arrays for extruders' locations
left = array('i')
right = array('i')
occupied = array('H',[0]*int(npolybeads+1))

# Empty arrays for comparsions
notOccupied = array('H',[0]*(extrudeInitSep+1))
noCTCF = array('h',[0]*(extrudeInitSep+1))

####################

# Read CTCF data

# Store CTCF motif direction (1 for +ve; -1 for -ve; 2 for both)
CTCF = array('h', [0]*int(npolybeads+1))
nCTCFSites = 0
filledCTCF = 0
if (rank == 0):
    CTCFProbs = {}
    with open(CTCFProbFile, 'r') as cf:
        for line in cf:
            data = line.split()
            if (len(data) != 4):
                print("Error: invalid entry in CTCF file")
                sys.exit(1)
            index = int(data[0])
            posProb = float(data[1])
            negProb = float(data[2])
            bothProb = float(data[3])
            CTCFProbs[index] = [posProb,negProb,bothProb]

    nCTCFSites = len(CTCFProbs)            
    for index in CTCFProbs:
        r = random.random()
        posProb = CTCFProbs[index][0]
        negProb = CTCFProbs[index][1]
        bothProb = CTCFProbs[index][2]
        if (r < posProb):
            CTCF[index] = 1
            filledCTCF += 1
        elif (r < posProb+negProb):
            CTCF[index] = -1
            filledCTCF += 1
        elif (r < posProb+negProb+bothProb):
            CTCF[index] = 2
            filledCTCF += 1

    # Output CTCF map
    with open(CTCFMapFile, 'w') as cf:
        for i in range(1,len(CTCF)):
            cf.write("%d %s\n" % (i, CTCF[i]))
    
nCTCFSites = comm.bcast(nCTCFSites, root=0)
filledCTCF = comm.bcast(filledCTCF, root=0)
CTCF = comm.bcast(CTCF, root=0)

####################

# Set up LAMMPS

lmp = lammps(comm=comm, cmdargs=["-screen", LAMMPSScreenFile, 
                                 "-log", LAMMPSLogFile])
lmp.file(extrudeLAMMPSFile)
lmp.command("timestep %f" % dt)

# Get the box size
boxsize = [lmp.extract_global("boxxhi",1)-lmp.extract_global("boxxlo",1),
           lmp.extract_global("boxyhi",1)-lmp.extract_global("boxylo",1),
           lmp.extract_global("boxzhi",1)-lmp.extract_global("boxzlo",1)]

# Get the number of beads and bonds
initialNBonds = lmp.extract_global("nbonds",0)
ntotalbeads = lmp.extract_global("natoms",0)
nprotbeads = ntotalbeads-npolybeads

# Set protein switching variables
lmp.command("variable switchseed equal floor(random(1000,9999999,%d))" %
            switchSeed)
lmp.command("variable switchprob equal %f" % switchProb)

####################

# Output messages about the simulation to be conducted

ostreams = []
if (rank == 0):
    ostreams.append(open(extrudeLogFile,"w"))
    if (not quiet):
        ostreams.append(sys.stdout)

for ostream in ostreams:
    ostream.write("Running loop extrusion simulation ...\n")
    ostream.write("Number of processors: %d\n" % nprocs)
    ostream.write("Number of polymer beads: %d\n" % npolybeads)
    ostream.write("Number of extruders: %d\n" % nextruders)
    ostream.write("Number of switching proteins: %d\n" % nprotbeads)
    ostream.write("Total timesteps: %d\n" % runtimeStep)
    ostream.write("\n")
    ostream.write("Extruders are updated every %d timesteps\n" % 
                  updateTimeStep)
    ostream.write("Extruders advance every %.1f timesteps\n" % 
                  (1.0/extrudeRateStep))
    ostream.write("Extruders detach after %.1f timesteps\n" % 
                  (1.0/offRateStep))
    ostream.write("Extruders detach after travelling %.1f beads\n" %
                  (extrudeRate/offRate))
    ostream.write("Free extruders attach after %.1f timesteps\n" % 
                  (1.0/onRateStep))
    ostream.write("Bridges switch on average every %.1f timesteps\n" %
                  (1.0/switchRateStep))
    ostream.write("\n")
    ostream.write("Probabilities:\n")
    ostream.write("Step prob = %f\n" % stepProb)
    ostream.write("Add prob = %f\n" % addProb)
    ostream.write("Remove prob = %f\n" % removeProb)
    ostream.write("Switch prob = %f (%.1f proteins switch on each attempt)\n" 
                  % (switchProb, switchProb*nprotbeads))
    ostream.write("Extrude seed = %d\n" % extrudeSeed)
    ostream.write("Switch seed = %d\n" % switchSeed)
    ostream.write("%d out of %d CTCF sites have been filled\n" %
                  (filledCTCF, nCTCFSites))
    ostream.write("\n")

####################

# Do extrusion

extrudeWriter = None
if (rank == 0):
    extrudeWriter = open(extrudePosFile, "w")

# Do an initial run without extruders
lmp.command("run %i" % updateTimeStep)

if (len(left) != lmp.extract_global("nbonds",0)-initialNBonds):
    print("Error: lost some bonds")
    sys.exit(1)

# Run subsequent steps, updating according to stepProb
for step in range(1,numOfIntervals):
    
    # Output timestep info
    if (rank == 0 and not quiet):
        sys.stdout.write("Running step %d (%.2f done)\n" % 
                         (step*updateTimeStep,
                          100.0*step/float(numOfIntervals)))
        sys.stdout.flush()
    
    # Switch on protein attraction after some time steps
    if (step*updateTimeStep == protAttractStartTimeStep):
        lmp.file(protAttractLAMMPSFile)

    # Switch on chromatin attraction after some time steps
    if (step*updateTimeStep == chromAttractStartTimeStep):
        lmp.file(chromAttractLAMMPSFile)
    
    # Get the bead coordinates from LAMMPS
    coords = lmp.gather_atoms("x",1,3)

    # Output extruder positions
    if (rank == 0):
        extrudeWriter.write("Extruders: %d\n" % (len(left)))
        extrudeWriter.write("Timestep: %d\n" % (step*updateTimeStep))
        for l, r in zip(left,right):
            extrudeWriter.write("%d %d\n" % (l, r))
    
    countCommand = 0
    commands = []
    if (rank == 0):
        # Remove extruders
        for i in range(len(left)):
            if (random.random() < removeProb):
                deleteBond(commands, occupied, left[i], right[i], 
                           extrudeBondType)
                left[i] = -1
                right[i] = -1
        left = array('i',[value for value in left if value != -1])
        right = array('i', [value for value in right if value != -1])
        
        # Update extruders
        for i in range(len(left)):
            prevLeft = left[i]
            prevRight = right[i]
            ldown = left[i]-1
            rup = right[i]+1
            if (ldown > 0 and occupied[ldown] == 0 and CTCF[ldown] != 1 and 
                CTCF[ldown] != 2 and random.random() < stepProb):
                left[i] = ldown
            if (rup <= npolybeads and occupied[rup] == 0 and 
                CTCF[rup] != -1 and CTCF[rup] != 2 and 
                random.random() < stepProb):
                right[i] = rup
            if (prevLeft != left[i] or prevRight != right[i]):
                sep = dist(left[i], right[i], coords, boxsize)
                if (sep > sepThres):
                    print("Error: bond cannot be made."
                          "Separation is too large: d = %.2f" % sep)
                    sys.exit(1)
                deleteBond(commands, occupied, prevLeft, prevRight, 
                           extrudeBondType)
                addBond(commands, occupied, left[i], right[i], 
                        extrudeBondType, sepThres)
    
        # Add extruders
        for i in range(nextruders-len(left)):
            if (random.random() < addProb):
                sleft = random.randrange(regionStart,regionEnd-extrudeInitSep)
                sright = sleft+extrudeInitSep
                if (occupied[sleft:sright+1] == notOccupied and
                    CTCF[sleft:sright+1] == noCTCF):
                    left.append(sleft)
                    right.append(sright)
                    addBond(commands, occupied, sleft, sright, 
                            extrudeBondType, sepThres)
    
    # Broadcast updated extruders' positions and LAMMPS commands
    left = comm.bcast(left, root=0)
    right = comm.bcast(right, root=0)
    comm.Bcast(occupied, root=0)
    commands = comm.bcast(commands, root=0)

    if (len(commands) > 0):
        lmp.commands_list(commands)
        countCommand += len(commands)

    # Check if there are any errors 
    if (len(left) != lmp.extract_global("nbonds",0)-initialNBonds):
        print("Error: lost some bonds")
        sys.exit(1)

    if (len(left) > nextruders):
        print("Error: too many extruders")
        sys.exit(1)

    # Make sure all procs have updated the bond list before the next run
    comm.Barrier()

    # Do switching
    lmp.file(switchLAMMPSFile)

    # Do the run
    lmp.command("run %d post no" % updateTimeStep)

# Run a final timestep with post set to yes to calculate timing stats
lmp.command("run 1")

# Remove all the extruder bonds before writing restart file
commands = []
commands.append("group db id <> 1 %d" % npolybeads)
commands.append("delete_bonds db bond %d remove special" % extrudeBondType)
commands.append("group db delete")
lmp.commands_list(commands)

# Exit main loop
if (rank == 0 and not quiet):
    print("Done")

if (rank == 0):
    for ostream in ostreams:
        ostream.close()
    extrudeWriter.close()

# Write restart file and final output file
lmp.command("write_restart %s" % restartFile)
lmp.command("write_data %s nocoeff" % endFile)
lmp.close()

####################

# Clean up

MPI.Finalize()
