#!/bin/env python3
# extrude.py
# A script which runs LAMMPS coupled with loop extrusion

import sys
import random
from array import array
import math
from lammps import lammps

###################

# Classes and functions to handle the parameters and the bond list
class ExtrudeParams(object):
    def __init__(self):
        self.npolybeads = None
        self.nextruders = None
        self.dt = None
        self.runtime = None
        self.sepThres = None
        self.extrudeSeed = None
        self.switchSeed = None
        self.extrudeBuffer = None
        self.extrudeBondType = None
        self.extrudeInitSep = None
        self.updateTime = None
        self.onTime = None
        self.offTime = None
        self.extrudeTime = None
        self.switchTime = None
        self.protAttractStartTime = None
        self.protAttractLAMMPSFile = None
        self.chromAttractStartTime = None
        self.chromAttractLAMMPSFile = None
        self.switchLAMMPSFile = None
        self.extrudeLAMMPSFile = None
        self.CTCFProbFile = None
        self.CTCFMapFile = None
        self.extrudePosFile = None
        self.extrudeLogFile = None
        self.LAMMPSLogFile = None
        self.LAMMPSScreenFile = None
        self.restartFile = None
        self.endFile = None
    
    def readParams(self, paramFile):
        with open(paramFile, 'r') as reader:
            for line in reader:
                if (line.startswith('#') or line == '\n'): continue
                args = line.split()
                if (len(args) < 2):
                    print("Error: not enough arguments (need 2)")
                    sys.exit(1)
                field, value = args[0], args[1]
                if (field == "npolybeads"):
                    self.npolybeads = int(value)                    
                elif (field == "nextruders"):
                    self.nextruders = int(value)
                elif (field == "dt"):
                    self.dt = float(value)
                elif (field == "runtime"):
                    self.runtime = int(value)
                elif (field == "sep_thres"):
                    self.sepThres = float(value)
                elif (field == "extrude_seed"):
                    self.extrudeSeed = int(value)
                elif (field == "switch_seed"):
                    self.switchSeed = int(value)
                elif (field == "extrude_buffer"):
                    self.extrudeBuffer = int(value)
                elif (field == "extrude_bond_type"):
                    self.extrudeBondType = int(value)
                elif (field == "extrude_init_sep"):
                    self.extrudeInitSep = int(value)
                elif (field == "update_time"):
                    self.updateTime = float(value)
                elif (field == "on_time"):
                    self.onTime = float(value)
                elif (field == "off_time"):
                    self.offTime = float(value)
                elif (field == "extrude_time"):
                    self.extrudeTime = float(value)
                elif (field == "switch_time"):
                    self.switchTime = float(value)
                elif (field == "prot_attract_start_time"):
                    self.protAttractStartTime = float(value)
                elif (field == "prot_attract_lammps_file"):
                    self.protAttractLAMMPSFile = value
                elif (field == "chrom_attract_start_time"):
                    self.chromAttractStartTime = float(value)
                elif (field == "chrom_attract_lammps_file"):
                    self.chromAttractLAMMPSFile = value
                elif (field == "switch_lammps_file"):
                    self.switchLAMMPSFile = value
                elif (field == "extrude_lammps_file"):
                    self.extrudeLAMMPSFile = value
                elif (field == "ctcf_prob_file"):
                    self.CTCFProbFile = value
                elif (field == "ctcf_map_file"):
                    self.CTCFMapFile = value
                elif (field == "extrude_pos_file"):
                    self.extrudePosFile = value
                elif (field == "extrude_log_file"):
                    self.extrudeLogFile = value
                elif (field == "lammps_log_file"):
                    self.LAMMPSLogFile = value
                elif (field == "lammps_screen_file"):
                    self.LAMMPSScreenFile = value
                elif (field == "restart_file"):
                    self.restartFile = value
                elif (field == "end_file"):
                    self.endFile = value
                else:
                    print("Error: unknown entry in parameter file: %s" % line)
                    sys.exit(1)
        if (self.npolybeads == None or self.nextruders == None or 
            self.dt == None or self.runtime == None or self.sepThres == None or
            self.extrudeSeed == None or self.switchSeed == None or 
            self.extrudeBuffer == None or self.extrudeBondType == None or
            self.extrudeInitSep == None or self.updateTime == None or 
            self.onTime == None or self.offTime == None or 
            self.extrudeTime == None or self.switchTime == None or
            self.protAttractStartTime == None or 
            self.protAttractLAMMPSFile == None or
            self.chromAttractStartTime == None or 
            self.chromAttractLAMMPSFile == None or
            self.switchLAMMPSFile == None or self.extrudeLAMMPSFile == None or
            self.CTCFProbFile == None or self.CTCFMapFile == None or 
            self.extrudePosFile == None or self.extrudeLogFile == None or 
            self.LAMMPSLogFile == None or self.LAMMPSScreenFile == None or 
            self.restartFile == None or self.endFile == None):
            print("Error: not all arguments specified")
            sys.exit(1)

def deleteBond(commands, occupied, s1, s2, bondType):
    occupied[s1] = 0
    occupied[s2] = 0

def addBond(commands, occupied, s1, s2, bondType, thres):
    occupied[s1] = 1
    occupied[s2] = 1

###################

# Start the main program

# Check arguments
args = sys.argv
if (len(args) != 3):
    print("extrude.py paramsFile quiet")
    sys.exit(1)

paramsFile = args[1]    # Parameter file
quiet = bool(int(args[2]))   # Print to screen or not

####################

# Read parameter values from file

params = ExtrudeParams()
params.readParams(paramsFile)
        
# Set parameters
npolybeads = params.npolybeads
nextruders = params.nextruders
runtime = params.runtime
dt = params.dt
sepThres = params.sepThres
extrudeSeed = params.extrudeSeed
switchSeed = params.switchSeed
updateTime = params.updateTime
extrudeRate = 1.0/params.extrudeTime
onRate = 1.0/params.onTime
offRate = 1.0/params.offTime
switchRate = 1.0/params.switchTime
extrudeBuffer = params.extrudeBuffer
extrudeBondType = params.extrudeBondType
extrudeInitSep = params.extrudeInitSep
regionStart = extrudeBuffer
regionEnd = npolybeads-extrudeBuffer
protAttractStartTime = params.protAttractStartTime
protAttractLAMMPSFile = params.protAttractLAMMPSFile
chromAttractStartTime = params.chromAttractStartTime
chromAttractLAMMPSFile = params.chromAttractLAMMPSFile
switchLAMMPSFile = params.switchLAMMPSFile
extrudeLAMMPSFile = params.extrudeLAMMPSFile
CTCFProbFile = params.CTCFProbFile
CTCFMapFile = params.CTCFMapFile
extrudePosFile = params.extrudePosFile
extrudeLogFile = params.extrudeLogFile
LAMMPSLogFile = params.LAMMPSLogFile
LAMMPSScreenFile = params.LAMMPSScreenFile
restartFile = params.restartFile
endFile = params.endFile

# Convert time/rate variables in units of timesteps
runtimeStep = int(runtime/dt)
updateTimeStep = int(updateTime/dt)
protAttractStartTimeStep = int(protAttractStartTime/dt)
chromAttractStartTimeStep = int(chromAttractStartTime/dt)
numOfIntervals = int(runtimeStep/updateTimeStep)
extrudeRateStep = extrudeRate*dt
onRateStep = onRate*dt
offRateStep = offRate*dt
switchRateStep = switchRate*dt

stepProb = updateTimeStep*extrudeRateStep
addProb = updateTimeStep*onRateStep
removeProb = updateTimeStep*offRateStep
switchProb = updateTimeStep*switchRateStep
if (stepProb > 1.0): stepProb = 1.0
if (addProb > 1.0): addProb = 1.0
if (removeProb > 1.0): removeProb = 1.0
if (switchProb > 1.0): switchProb = 1.0

# Set up random generator
random.seed(extrudeSeed)

# Arrays for extruders' locations
left = array('i')
right = array('i')
occupied = array('H',[0]*int(npolybeads+1))

# Empty arrays for comparsions
notOccupied = array('H',[0]*(extrudeInitSep+1))
noCTCF = array('h',[0]*(extrudeInitSep+1))

####################

# Read CTCF data

# Store CTCF motif direction (1 for +ve; -1 for -ve; 2 for both)
CTCF = array('h', [0]*int(npolybeads+1))
nCTCFSites = 0
filledCTCF = 0
if (rank == 0):
    CTCFProbs = {}
    with open(CTCFProbFile, 'r') as cf:
        for line in cf:
            data = line.split()
            if (len(data) != 4):
                print("Error: invalid entry in CTCF file")
                sys.exit(1)
            index = int(data[0])
            posProb = float(data[1])
            negProb = float(data[2])
            bothProb = float(data[3])
            CTCFProbs[index] = [posProb,negProb,bothProb]

    nCTCFSites = len(CTCFProbs)            
    for index in CTCFProbs:
        r = random.random()
        posProb = CTCFProbs[index][0]
        negProb = CTCFProbs[index][1]
        bothProb = CTCFProbs[index][2]
        if (r < posProb):
            CTCF[index] = 1
            filledCTCF += 1
        elif (r < posProb+negProb):
            CTCF[index] = -1
            filledCTCF += 1
        elif (r < posProb+negProb+bothProb):
            CTCF[index] = 2
            filledCTCF += 1

    # Output CTCF map
    with open(CTCFMapFile, 'w') as cf:
        for i in range(1,len(CTCF)):
            cf.write("%d %s\n" % (i, CTCF[i]))
    
####################

# Output messages about the simulation to be conducted

ostreams = []
if (rank == 0):
    ostreams.append(open(extrudeLogFile,"w"))
    if (not quiet):
        ostreams.append(sys.stdout)

for ostream in ostreams:
    ostream.write("Running loop extrusion simulation ...\n")
    ostream.write("Number of processors: %d\n" % nprocs)
    ostream.write("Number of extruders: %d\n" % nextruders)
    ostream.write("Total timesteps: %d\n" % runtimeStep)
    ostream.write("\n")
    ostream.write("Extruders are updated every %d timesteps\n" % 
                  updateTimeStep)
    ostream.write("Extruders advance every %.1f timesteps\n" % 
                  (1.0/extrudeRateStep))
    ostream.write("Extruders detach after %.1f timesteps\n" % 
                  (1.0/offRateStep))
    ostream.write("Extruders detach after travelling %.1f beads\n" %
                  (extrudeRate/offRate))
    ostream.write("Free extruders attach after %.1f timesteps\n" % 
                  (1.0/onRateStep))
    ostream.write("Bridges switch on average every %.1f timesteps\n" %
                  (1.0/switchRateStep))
    ostream.write("\n")
    ostream.write("Probabilities:\n")
    ostream.write("Step prob = %f\n" % stepProb)
    ostream.write("Add prob = %f\n" % addProb)
    ostream.write("Remove prob = %f\n" % removeProb)
    ostream.write("Extrude seed = %d\n" % extrudeSeed)
    ostream.write("Switch seed = %d\n" % switchSeed)
    ostream.write("%d out of %d CTCF sites have been filled\n" %
                  (filledCTCF, nCTCFSites))
    ostream.write("\n")

####################

# Do extrusion

extrudeWriter = open(extrudePosFile, "w")

# Run subsequent steps, updating according to stepProb
for step in range(1,numOfIntervals):
    
    # Output timestep info
    if (not quiet):
        sys.stdout.write("Running step %d (%.2f done)\n" % 
                         (step*updateTimeStep,
                          100.0*step/float(numOfIntervals)))
        sys.stdout.flush()
    
    # Output extruder positions
    extrudeWriter.write("Extruders: %d\n" % (len(left)))
    extrudeWriter.write("Timestep: %d\n" % (step*updateTimeStep))
    for l, r in zip(left,right):
        extrudeWriter.write("%d %d\n" % (l, r))
    
    countCommand = 0
    commands = []
    
    # Remove extruders
    for i in range(len(left)):
        if (random.random() < removeProb):
            deleteBond(commands, occupied, left[i], right[i], 
                       extrudeBondType)
            left[i] = -1
            right[i] = -1
    left = array('i',[value for value in left if value != -1])
    right = array('i', [value for value in right if value != -1])
        
    # Update extruders
    for i in range(len(left)):
        prevLeft = left[i]
        prevRight = right[i]
        ldown = left[i]-1
        rup = right[i]+1
        if (ldown > 0 and occupied[ldown] == 0 and CTCF[ldown] != 1 and 
            CTCF[ldown] != 2 and random.random() < stepProb):
            left[i] = ldown
        if (rup <= npolybeads and occupied[rup] == 0 and 
            CTCF[rup] != -1 and CTCF[rup] != 2 and 
            random.random() < stepProb):
            right[i] = rup
        if (prevLeft != left[i] or prevRight != right[i]):
            deleteBond(commands, occupied, prevLeft, prevRight, 
                       extrudeBondType)
            addBond(commands, occupied, left[i], right[i], 
                    extrudeBondType, sepThres)
    
    # Add extruders
    for i in range(nextruders-len(left)):
        if (random.random() < addProb):
            sleft = random.randrange(regionStart,regionEnd-extrudeInitSep)
            sright = sleft+extrudeInitSep
            if (occupied[sleft:sright+1] == notOccupied and
                CTCF[sleft:sright+1] == noCTCF):
                left.append(sleft)
                right.append(sright)
                addBond(commands, occupied, sleft, sright, 
                        extrudeBondType, sepThres)
    
    if (len(left) > nextruders):
        print("Error: too many extruders")
        sys.exit(1)

# Exit main loop
if (not quiet):
    print("Done")

for ostream in ostreams:
    ostream.close()
extrudeWriter.close()
