// data_to_beads.cpp
//
// Program which reads in data sets from bed files, and generates a
// list of beads with different types, depending on the overlaps from
// the bed files.

#define COVERFRACTION 0.25 // Feature must cover at least this much of 
                           // subsequent beads to be counted as covering
                           // more than one bead
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <vector>
#include <map>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::map;
using std::pair;

struct Bed {
  string name, filename;
  Bed(string n, string fn) : name(n), filename(fn) {}
};

struct Bedline {
  string chrom;
  long int start, end, mid, width;
  
  Bedline();
  Bedline(const Bedline& bline);
  Bedline(const string& line);
};

struct Region {
  std::string chrom;
  long int start, end;
  int bpPerBead, nbp, nbeads;
  double coverfrac;
  map<string,int> typeList;
  map<string,string> typeDescription;

  Region(double coverfrac);
  bool inRegion(const Bedline& bline) const;
  int bpToBead(const long int& bp) const;
  long int pos(const int& bead) const;
  long int beadStart(const int& bead) const;
  long int beadEnd(const int& bead) const;
  vector<int> overlapBeads(const Bedline& bline) const;
  void getTypes(vector<int>& beadTypes, 
		const map<string,vector<int> >& beads);
  bool outputBeadList(const map<string,vector<int> >& beads, 
		      const string& outfile);
  bool outputTypeList(const vector<int>& types, const string& outfile);

};

void help();
bool readConfig(const string& filename, Region& reg, map<string,Bed>& beds);
bool loadBed(const Region& reg, const Bed& bed, vector<int>& beads);
int overlap(long int start1, long int end1, long int start2, long int end2);

int main(int argc, char* argv[]) {
  Region region(COVERFRACTION);   // Region of interest in the chromosome
  map<string,Bed> beds;           // Bed files
  map<string,vector<int> > beads; // Store binarized signal {0,1} for each bead
                                  // for each bed file
  vector<int> beadTypes;          // Store the type of each bead
  
  if (argc != 3) {
    help();
    return 1;
  }
  int argi = 0;
  const string configFile = string(argv[++argi]);
  const string outDir = string(argv[++argi]);
  
  const string beadListFile = outDir + "/bead_list.dat";
  const string beadTypeFile = outDir + "/bead_type.dat";

  // Read the configuration file
  if (!readConfig(configFile, region, beds)) {
    cout << "Exiting due to error when reading the config file." << endl;
    return 1;
  }

  // Parse bed files and set bead types
  cout << "Parsing bed files ..." << endl;
  for (map<string,Bed>::iterator it = beds.begin(); it != beds.end(); it++) {
    cout << "Parsing bed file for " << it->first << " ..." << endl;
    beads[it->second.name] = vector<int>(region.nbeads, 0);
    loadBed(region, it->second, beads[it->second.name]);
  }

  // Output bead list
  cout << "Outputing bead list ..." << endl;
  if (!region.outputBeadList(beads, beadListFile)) {
    cout << "Exiting due to output bead list error." << endl;
    return 1;
  }

  // Identify bead types
  region.getTypes(beadTypes, beads);

  // Output bead types
  if (!region.outputTypeList(beadTypes, beadTypeFile)) {
    cout << "Exiting due to output type list error." << endl;
    return 1;
  }
  
  cout << "Bead lists generated successfully." << endl;
}

void help() {
  cout << "Usage : data_to_beads configFile outDir\n";
  cout << "outDir is the directory where output files are written, and\n";
  cout << "configFile has the following lines (without comments):\n\n";
  cout << "  chr5                      # Chromosome of region\n";
  cout << "  28750000                  # Start of region (bp)\n";
  cout << "  29750000                  # End of region (bp)\n";
  cout << "  1000                      # Number of bp per bead\n";
  cout << "  name path/to/bedfile.bed  # input data\n\n";
  cout << "  where multiple data sets with unique names can be listed.\n\n";
  cout << "Notes:\n";
  cout << "  * Bed files start counting from 0 and end is not inclusive.\n";
  cout << "  * Internally beads are counted from 0, but from 1 when output.\n";
  cout << "  * Features assumes to be in at lead one bead; to be said to\n";
  cout << "    overlap multiple beads, they must cover at least some\n";
  cout << "    fraction of the subsequent beads.\n" << endl;
}

bool readConfig(const string& filename, Region& reg, map<string,Bed>& beds) {
  ifstream inf, testinf;
  stringstream ss;
  string line, bedname, bedfile;

  inf.open(filename);
  if (!inf) {
    cout << "Error opening config file " << filename << endl;
    return false;
  }

  // Get the first four lines on chromosome, start, end, and resolution
  getline(inf, line); ss.str(line); ss >> reg.chrom; ss.clear();
  getline(inf, line); ss.str(line); ss >> reg.start; ss.clear();
  getline(inf, line); ss.str(line); ss >> reg.end; ss.clear();
  getline(inf, line); ss.str(line); ss >> reg.bpPerBead; ss.clear();

  // Set up the region of interest in the chromosome
  reg.nbp = reg.end-reg.start;
  if (reg.nbp < 1) {
    cout << "Error: incorrect region specification." << endl;
    inf.close();
    return false;
  }

  reg.nbeads = static_cast<int>
    (ceil(reg.nbp/static_cast<double>(reg.bpPerBead)));

  // Output region details
  cout << "Setting up region ..." << endl;
  cout << reg.chrom << ":" << reg.start << "-" << reg.end << endl;
  
  if (reg.nbeads*reg.bpPerBead+reg.start > reg.end) {
    cout << "Adjusting region to keep whole number of beads:" << endl;
    reg.end = reg.nbeads*reg.bpPerBead+reg.start;
    reg.nbp = reg.end-reg.start;
    cout << "New region: " << reg.chrom << ":" << reg.start << "-"
	 << reg.end << endl;
    cout << "Length = " << reg.nbp << "bp. Giving " << reg.nbeads 
	 << " beads at " << reg.bpPerBead << " bp per bead." << endl;
  }

  // Read names and bed files
  cout << "Checking bed files ..." << endl;
  while (getline(inf, line)) {
    ss.str(line); ss >> bedname >> bedfile; ss.clear();
    cout << "Data set " << bedname << " in file " << bedfile << " ... ";
    testinf.open(bedfile);
    if (testinf) {
      testinf.close();
      if (beds.insert(pair<string,Bed>(bedname,Bed(bedname,bedfile))).second) {
	cout << "OK." << endl;
      } else {
	cout << "Error: attempting to load multiple bed files with the "
	     << "same name." << endl;
	inf.close();
	return false;
      }
    } else {
      testinf.close();
      cout << "FAIL." << endl;
      return false;
    }
  }
  inf.close();
  
  if (beds.size() == 0) {
    cout << "Error: no bed files specified." << endl;
    return false;
  }
  return true;
}

bool loadBed(const Region& reg, const Bed& bed, vector<int>& beads) {
  ifstream inf;
  string line;
  Bedline bline;
  vector<int> beadList;

  inf.open(bed.filename);
  while (getline(inf, line)) {
    bline = Bedline(line);
    if (reg.inRegion(bline)) {
      if (bline.width < (1+reg.coverfrac)*reg.bpPerBead) {
	// Site only covers one bead
	beads[reg.bpToBead(bline.mid)] = 1;
      } else {
	beadList = reg.overlapBeads(bline);
	for (size_t i = 0; i < beadList.size(); i++) {
	  beads[beadList[i]] = 1;
	}
      }
    }
  }
  inf.close();
  return 1;
}

int overlap(long int start1, long int end1, long int start2, long int end2) {
  // Return the number of bp by which the two regions overlap
  if (start1 >= start2 && end1 <= end2) {
    return end1-start1;
  } else if (start2 >= start1 && end2 <= end1) {
    return end2-start2;
  } else if (end1 > start2 && end1 < end2) {
    return end1-start2;
  } else if (start1 > start2 && start1 < end2) {
    return end2-start1;
  } else {
    return 0;
  }
}

// ############################################################################
// Member functions of Bedline

Bedline::Bedline() {}

Bedline::Bedline(const Bedline& b) :
  chrom(b.chrom), start(b.start), end(b.end), mid(b.mid), width(b.width) {}

Bedline::Bedline(const string& line) {
  stringstream ss;
  ss.str(line);
  ss >> chrom >> start >> end;
  ss.clear();
  width = end-start;
  mid = static_cast<long int>(floor(0.5*static_cast<double>(start+end)));
}

// ############################################################################
// Member functions of Region

Region::Region(double cf) : coverfrac(cf) {}

bool Region::inRegion(const Bedline& bline) const {
  return (bline.chrom == chrom && bline.end > start && bline.start < end);
}

int Region::bpToBead(const long int& bp) const {
  return static_cast<int>(floor(static_cast<double>(bp-start)/
				static_cast<double>(bpPerBead)));
}

long int Region::pos(const int& bead) const {
  return start+bpPerBead*bead+0.5*bpPerBead;
}

long int Region::beadStart(const int& bead) const {
  return start+bpPerBead*bead;
}

long int Region::beadEnd(const int& bead) const {
  // Note that this returns the last bp of the bead + 1 bp
  // (i.e. not inclusive)
  return start+bpPerBead*(bead+1);
}

vector<int> Region::overlapBeads(const Bedline& bline) const {
  // Return a list of overlapping beads
  int first = bpToBead(bline.start);
  int last = bpToBead(bline.end);
  vector<int> list = vector<int>();
  if (last < 0 || first >= nbeads) return list;
  if (last >= nbeads) last = nbeads-1;
  if (first < 0) first = 0;
  for (int i = first; i <= last; i++) {    
    if (overlap(beadStart(i), beadEnd(i), bline.start, bline.end) > 
	coverfrac*bpPerBead) {
      list.push_back(i);
    }
  }
  return list;
}

void Region::getTypes(vector<int>& beadTypes, 
		      const map<string,vector<int> >& beads) {
  // Identify the bead types and make a list of which type each bead is
  stringstream typestr, descript;
  string type;
  int typecount = 1;

  typeList.clear();
  typestr.clear();
  descript.clear();

  beadTypes = vector<int>(nbeads, 0);

  map<string,vector<int> >::const_iterator it;
  
  // Set type 0 (No signal from all bed data)
  for (it = beads.begin(); it != beads.end(); it++) {
    typestr << "0";
    descript << "!" << it->first << " ";
  }
  type = typestr.str();
  typeList[type] = typecount++;
  typeDescription[type] = descript.str();
  
  // Go through beads finding the possible types and assigning a type 
  // to each bead
  for (int i = 0; i < nbeads; i++) {
    typestr.str(""); typestr.clear();
    descript.str(""); descript.clear();
    for (it = beads.begin(); it != beads.end(); it++) {
      typestr << it->second[i];
      if (it->second[i] == 0) descript << "!";
      descript << it->first << " ";
    }
    type = typestr.str();
    if (typeList.find(type) == typeList.end()) {
      // Found a new type
      typeList[type] = typecount++;
      typeDescription[type] = descript.str();
    }
    beadTypes[i] = typeList[type];
  }
}

bool Region::outputBeadList(const map<string,vector<int> >& beads, 
			    const string& outfile) {
  // Output a list of properties for each bead
  ifstream testinf;
  ofstream ouf;

  map<string,vector<int> >::const_iterator it;

  cout << "Writing file " << outfile << " ..." << endl;
  
  // Test if outfile already exists
  testinf.open(outfile);
  if (testinf) {
    cout << "Error: " << outfile << " already exists." << endl;
    testinf.close();
    return false;
  } else {
    testinf.close();
  }

  // Write file
  ouf.open(outfile);
  ouf << "# List of bead properties for region " 
      << chrom << ":" << start << "-" << end << "\n";
  ouf << "# Bead id, bp midpoint";
  for (it = beads.begin(); it != beads.end(); it++) {
    ouf << ", " << it->first;
  }
  ouf << "\n";
  for (int i = 0; i < nbeads; i++) {
    ouf << i+1 << " " << pos(i);
    for (it = beads.begin(); it != beads.end(); it++) {
      ouf << " " << it->second[i];
    }
    ouf << "\n";
  }
  ouf.close();
  return 1;
}

bool Region::outputTypeList(const vector<int>& beadTypes,
			    const string& outfile) {
  // Output a list of each bead type
  ifstream testinf;
  ofstream ouf;

  cout << "Writing file " << outfile << "..." << endl;
  
  // Test if outfile already exists
  testinf.open(outfile);
  if (testinf) {
    cout << "Error: " << outfile << " already exists." << endl;
    testinf.close();
    return 0;
  } else {
    testinf.close();
  }

  // Write file
  ouf.open(outfile);
  ouf << "# List of beads with " << typeList.size() << " different types.\n";
  for (map<string,int>::const_iterator it = typeList.begin(); 
       it != typeList.end(); it++) {
    ouf << "# Type " << it->second << " : " 
	<< typeDescription[it->first] << "\n";
  }
  ouf << "#\n# Bead id, type\n";
  for (int i = 0; i < nbeads; i++) {
    ouf << i+1 << " " << beadTypes[i] << "\n";
  }
  ouf.close();
  return 1;
}
