/* ctcf_distrb.cpp
 * A program which generates the distribution of assigned ctcf sites
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

int main(int argc, char* argv[]) {
  if (argc < 7) {
    cout << "Usage: ctcf_distrb startBp endBp bpPerBead useGenomeCoords "
	 << "outFile ctcfMapFile1 [ctcfMapFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBead = stol(string(argv[++argi]), nullptr, 10);
  bool useGenomeCoords = 
    static_cast<bool>(stoi(string(argv[++argi]), nullptr, 10));
  string outFile (argv[++argi]);

  int nbeads = static_cast<int>(ceil(static_cast<double>(endBp-startBp)/
				     static_cast<double>(bpPerBead)));
  
  int nsamples = argc-(++argi);
  
  ifstream reader;
  string ctcfMapFile;
  string line;
  stringstream ss;
  int index, ctcfVal;
  vector<double> ctcfForFreq (nbeads, 0);
  vector<double> ctcfBackFreq (nbeads, 0);

  for (int i = argi; i < argc; i++) {
    ctcfMapFile = string(argv[i]);
    reader.open(ctcfMapFile);

    if (!reader) {
      cout << "Error: cannot open the file " << ctcfMapFile << endl;
      return 1;
    }
       
    for (int j = 0; j < nbeads; j++) {
      getline(reader, line);
      ss.str(line);
      ss >> index >> ctcfVal;
      ss.clear();
      if (ctcfVal == 1 || ctcfVal == 2) {
	ctcfForFreq[index-1] += 1.0;
      } else if (ctcfVal == -1 || ctcfVal == 2) {
	ctcfBackFreq[index-1] += 1.0;
      }
    }
    reader.close();
  }

  // Normalise
  for (int i = 0; i < nbeads; i++) {
    ctcfForFreq[i] /= static_cast<double>(nsamples);
    ctcfBackFreq[i] /= static_cast<double>(nsamples);
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nbeads; i++) {
    writer << (useGenomeCoords?(long)(startBp+(i+0.5)*bpPerBead):i) << " " 
	   << ctcfForFreq[i] << " " << ctcfBackFreq[i] << "\n";
  }
  writer.close();
}
