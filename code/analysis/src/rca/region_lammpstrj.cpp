/* region_lammpstrj.cpp
 * A program to create a separate lammpstrj file for each sampled frame in
 * each configuration file
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

int main(int argc, char* argv[]){
  // Read input arguments
  if (argc != 8){
    cout << "Usage: region_lammpstrj startBead endBead startTime endTime "
	 << "timeInc posFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  BeadStream bstream;
  BeadList blist;
  ofstream writer;
  double boxSize[3], boxLo[3], boxHi[3];

  if (!bstream.open(posFile, blist)) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  
  for (int i = 0; i < 3; i++) {
    boxSize[i] = bstream.getBoxSize(i);
    boxLo[i] = bstream.getBoxLo(i);
    boxHi[i] = bstream.getBoxHi(i);
  }
  
  for (long time = startTime; time <= endTime; time+=timeInc) {
    if (!bstream.moveTo(time)) break;
    // Writer header (only works for periodic boundaries)
    writer << "ITEM: TIMESTEP\n";
    writer << bstream.getTime() << "\n";
    writer << "ITEM: NUMBER OF ATOMS\n";
    writer << endBead-startBead+1 << "\n";
    writer << "ITEM: BOX BOUNDS pp pp pp\n";
    writer << std::setprecision(16) << std::scientific;
    writer << boxLo[0] << " " << boxHi[0] << "\n";
    writer << boxLo[1] << " " << boxHi[1] << "\n";
    writer << boxLo[2] << " " << boxHi[2] << "\n";
    writer.unsetf(std::ios_base::floatfield);
    writer << "ITEM: ATOMS id type xs ys zs ix iy iz\n";
    writer << std::setprecision(6);
    
    // Write the position of each bead
    for (int i = startBead; i <= endBead; i++) {
      writer << i-startBead+1 << " " 
	     << blist.getType(i) << " "
	     << (blist.getPosition(i,0)-boxLo[0])/boxSize[0] << " "
	     << (blist.getPosition(i,1)-boxLo[1])/boxSize[1] << " "
	     << (blist.getPosition(i,2)-boxLo[2])/boxSize[2] << " "
	     << blist.getBoundCount(i,0) << " "
	     << blist.getBoundCount(i,1) << " "
	     << blist.getBoundCount(i,2) << "\n";
    }
    writer.unsetf(std::ios_base::floatfield);
  }
  writer.close();
  bstream.close();
}
