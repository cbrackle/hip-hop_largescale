# cluster.py
# Agglomerative clustering of Zernike descriptor data

import sys
import numpy as np
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
#from sklearn.datasets import load_iris
#from sklearn.cluster import AgglomerativeClustering

args = sys.argv

if (len(args) != 3):
    print("Usage: cluster_configs_dist_matrix.py nconfigs matrix_file")
    sys.exit(1)

nconfigs = int(args.pop(1))
matrix_file = args.pop(1)

dist_mat = np.zeros((nconfigs,nconfigs),dtype=float)

# Load the data
with open(matrix_file,'r') as reader:
    for line in reader:
        data = line.split()
        i = int(data[0])
        j = int(data[1])
        val = float(data[2])
        dist_mat[i,j] = val
        dist_mat[j,i] = val

dist_mat = squareform(dist_mat)
print(dist_mat)
"""
def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack([model.children_, model.distances_,
                                      counts]).astype(float)
    for i in linkage_matrix:
        print(i)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)
"""
#dist_mode = "precomputed"
#link_mode = "complete"
#link_mode = "ward"
link_mode = "average"
#clustering = AgglomerativeClustering(distance_threshold=0,linkage=link_mode,affinity=dist_mode,n_clusters=None)
#cluster = clustering.fit_predict(dist_mat)

link_matrix = linkage(dist_mat,method=link_mode)
#for i in link_matrix:
#    print(i)

#plot_dendrogram(clustering, truncate_mode='level',p=3)
#plot_dendrogram(clustering,count_sort=True)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
dendrogram(link_matrix, count_sort=True)
ax.tick_params(axis='x', which='major', labelsize=14)
ax.tick_params(axis='y', which='major', labelsize=14)
plt.show()
