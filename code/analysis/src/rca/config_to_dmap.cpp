// config_to_dmap.cpp
// A program which computes the pairwise distance map of a configuration

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

// Helper functions
double distance(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: config_to_dmap startBead endBead time fullMap "
	 << "posFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  long time = stol(string(argv[++argi]), nullptr, 10);
  bool fullMap = static_cast<bool>(stoi(string(argv[++argi]), nullptr, 10));
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Check the bead range is valid
  // Include both start and end bead
  if (endBead < startBead) {
    cout << "Error: end bead index is smaller than start bead index" << endl;
    return 1;
  }
  
  BeadStream reader;
  BeadList blist;
  if (!reader.open(posFile, blist)) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }
  if (time != -1 && !reader.moveTo(time)) {
    cout << "Error: cannot read the frame at time " << time << endl;
    return 1;
  } else if (!reader.nextFrame()) {
    cout << "Error: no frames available to read" << endl;
    return 1;
  }
  reader.close();
  
  // Compute and output distances
  double xi[3], xj[3];
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  if (!fullMap) {
    for (int i = startBead; i <= endBead; i++) {
      for (int k = 0; k < 3; k++) {
	xi[k] = blist.getUnwrappedPosition(i,k);
      }
      for (int j = startBead; j < i; j++) {
	for (int k = 0; k < 3; k++) {
	  xj[k] = blist.getUnwrappedPosition(j,k);
	}
	writer << i << " " << j << " " << distance(xi,xj) << "\n";
      }
    }
  } else {
    for (int i = startBead; i <= endBead; i++) {
      for (int k = 0; k < 3; k++) {
	xi[k] = blist.getUnwrappedPosition(i,k);
      }
      for (int j = startBead; j <= endBead; j++) {
	for (int k = 0; k < 3; k++) {
	  xj[k] = blist.getUnwrappedPosition(j,k);
	}
	writer << i << " " << j << " " << distance(xi,xj) << "\n";
      }
      writer << "\n";
    }
  }
  writer.close();
}

double distance(double x1[3], double x2[3]) {
  double sum = 0.0;
  double diff;
  for (int i = 0; i < 3; i++) {
    diff = x1[i]-x2[i];
    sum += diff*diff;
  }
  return sqrt(sum);
}
