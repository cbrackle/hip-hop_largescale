// euclid_dist_matrix_cmplx.cpp
// A program which computes the euclidean distance between samples
// based on the number of features in each sample

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;

// Helper functions
double distance(int n, double** x1, double** x2);

int main(int argc, char* argv[]) {
  if (argc < 6) {
    cout << "Usage: euclid_dist_matrix_cmplx nfeatures column outFile " 
	 << "dataFile1 dataFile2 [dataFiles ...]" << endl;
    return 1;
  }

  int argi = 0;
  int nfeatures = stoi(string(argv[++argi]), nullptr, 10);
  int column = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);
  
  int nsamples = argc-(++argi);
  double*** data = create3DArray<double>(nsamples,nfeatures,2);

  string file, line, str;
  ifstream reader;
  stringstream ss;
  size_t start, end;
  double re, im;
  int index;
  for (int i = 0; i < nsamples; i++) {
    file = argv[argi+i];
    // Determine the run number
    start = file.find_last_of("run_");
    end = file.find_last_of(".");
    index = stoi(file.substr(start+1,end-(start+1)), nullptr, 10)-1;
    cout << "Reading " << file << endl;
    reader.open(file);
    if (!reader) {
      cout << "Error: cannot open the file " << file << endl;
    }
    for (int j = 0; j < nfeatures; j++) {
      getline(reader,line);
      ss.str(line);
      for (int k = 0; k < column; k++) {
	ss >> str;
      }
      ss >> re >> im;
      ss.clear();
      data[index][j][0] = re;
      data[index][j][1] = im;
      cout << index << " " << j << " " << re << " " << im << endl;
    }
    reader.close();
  }

  // Compute and output the distance between samples
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nsamples; i++) {
    for (int j = 0; j < i; j++) {
      writer << i << " " << j << " " 
	     << std::setprecision(10) << std::scientific
	     << distance(nfeatures, data[i], data[j]) << "\n";
      writer.unsetf(std::ios_base::floatfield);
    }
  }
  writer.close();

  // Clean up
  deleteArray(data);
}

double distance(int n, double** x1, double** x2) {
  double sum = 0.0;
  double diff;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < 2; j++) {
      diff = x1[i][j]-x2[i][j];
      sum += diff*diff;
    }
  }
  return sqrt(sum);
}
