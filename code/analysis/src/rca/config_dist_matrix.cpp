// config_dist_matrix.cpp
// A program which computes a distance matrix for some polymer configurations

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

// Helper functions
void loadMap(int nbeads, int startBead, long time, const string& file, 
	     double* dmap);
double configDistance(int mapSize, double cutoff, 
		      double* dmap1, double* dmap2);
double distance(double* x1, double* x2);
int toMapIndex(int i, int j);

int main(int argc, char* argv[]) {
  if (argc < 9) {
    cout << "Usage: config_dist_matrix startBead endBead cutoff time "
	 << "blocksize outFile configFile1 configFile2 [configFiles ...]" 
	 << endl;
    return 1;
  }

  int argi = 0;
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  double cutoff = stod(string(argv[++argi]), nullptr);
  long time = stol(string(argv[++argi]), nullptr, 10);
  int blocksize = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);

  // Check the bead range is valid
  if (endBead < startBead) {
    cout << "Error: end bead index is smaller than start bead index" << endl;
    return 1;
  }
  int nbeads = endBead-startBead+1; // Include both start and end bead
  int nconfigs = argc-(++argi);

  int mapSize = nbeads*(nbeads-1)/2;
  int matSize = nconfigs*(nconfigs-1)/2;
  double** imaps = create2DDoubleArray(blocksize,mapSize);
  double** jmaps = create2DDoubleArray(blocksize,mapSize);
  double* distMat = create1DDoubleArray(matSize);
  
  // Compare each pair of configurations
  vector<string> configFiles (nconfigs);
  for (int i = 0; i < nconfigs; i++) {
    configFiles[i] = argv[i+argi];
  }

  // Read configurations in blocks (to save time)
  int nblocks = static_cast<int>
    (ceil(nconfigs/static_cast<double>(blocksize)));
  cout << nblocks << endl;
  for (int i = 0; i < nblocks; i++) {
    cout << "Doing block i = " << i << endl;
    for (int n = 0; n < blocksize; n++) {
      int ii = i*blocksize+n;
      if (ii >= nconfigs) break;
      loadMap(nbeads, startBead, time, configFiles[ii], imaps[n]);
    }
    for (int j = 0; j <= i; j++) {
      for (int n = 0; n < blocksize; n++) {
	int jj = j*blocksize+n;
	if (jj >= nconfigs) break;
	loadMap(nbeads, startBead, time, configFiles[jj], jmaps[n]);
      }
      for (int k = 0; k < blocksize; k++) {
	int kk = i*blocksize+k;
	if (kk >= nconfigs) break;
	for (int l = 0; l < blocksize; l++) {
	  int ll = j*blocksize+l;
	  if (ll >= kk || ll >= nconfigs) break;
	  distMat[toMapIndex(kk,ll)] = 
	    configDistance(mapSize, cutoff, imaps[k], jmaps[l]);
	}
      }
    }
  }
  
  // Output the distance matrix (in triangular form)
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  int n = 0;
  for (int i = 0; i < nconfigs; i++) {
    for (int j = 0; j < i; j++) {
      writer << i << " " << j << " " << distMat[n] << "\n";
      n++;
    }
  }
  writer.close();

  // Clean up
  deleteArray(imaps);
  deleteArray(jmaps);
  deleteArray(distMat);
}

void loadMap(int nbeads, int startBead, long time, const string& file, 
	     double* dmap) {
  BeadStream reader;
  BeadList blist;
  reader.open(file, blist);
  if (!reader.isOpen()) {
    cout << "Error: cannot open position file " << file << endl;
    exit(1);
  }
  if (time != -1 && !reader.moveTo(time)) {
    cout << "Error: cannot read the frame at time " << time << endl;
    exit(1);
  } else if (!reader.nextFrame()) {
    cout << "Error: no frames available to read" << endl;
    exit(1);
  }
  reader.close();
  
  // Compute distances
  int n = 0;
  double xi[3], xj[3];
  for (int i = 0; i < nbeads; i++) {
    for (int k = 0; k < 3; k++) {
      xi[k] = blist.getUnwrappedPosition(i+startBead,k);
    }
    for (int j = 0; j < i; j++) {
      for (int k = 0; k < 3; k++) {
	xj[k] = blist.getUnwrappedPosition(j+startBead,k);
      }
      dmap[n] = distance(xi,xj);
      n++;
    }
  }
}

double configDistance(int mapSize, double cutoff, 
		      double* dmap1, double* dmap2) {
  double dist = 0.0;
  double diff;
  double r1, r2;
  for (int i = 0; i < mapSize; i++) {
    r1 = dmap1[i];
    r2 = dmap2[i];
    if ((r1 <= cutoff) != (r2 <= cutoff)) {
      diff = r1-r2;
      dist += diff*diff;
    }
  }
  return dist/static_cast<double>(mapSize);
}

double distance(double* x1, double* x2) {
  double sum = 0.0;
  double diff;
  for (int i = 0; i < 3; i++) {
    diff = x1[i]-x2[i];
    sum += diff*diff;
  }
  return sqrt(sum);
}

inline int toMapIndex(int i, int j) {
  return (i-1)*i/2+j;
}
