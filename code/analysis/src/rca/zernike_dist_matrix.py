# zernike_dist_matrix.py
# Agglomerative clustering of Zernike descriptor data

import sys
import numpy as np

args = sys.argv

if (len(args) < 2):
    print("Usage: zernike_dist_matrix.py order out_file descriptor_files")
    sys.exit(1)

order = int(args.pop(1))
out_file = args.pop(1)

npoints = 0
for n in range(order+1): # Need to include zeroth order descriptor
    for l in range(n%2,n+1,2):
        npoints += 1

nconfigs = len(args)-1
descriptors = np.zeros((nconfigs,npoints),dtype=float)

# Load the data
data_files_tmp = args[1:]

# Sort the data files by run
data_files = ['' for i in range(len(data_files_tmp))]
for df in data_files_tmp:
    # Pick out the run number (assume the last parameter is the run number)
    index = int(df[len(df)-df[::-1].index("_"):len(df)-df[::-1].index(".")-1])
    data_files[index-1] = df

for i,df in enumerate(data_files):
    with open(df,'r') as reader:
        for j,line in enumerate(reader):
            data = line.split()
            descriptors[i,j] = float(data[2])

# Compute the Euclidean distance between configurations
with open(out_file,'w') as writer:
    for i in range(nconfigs):
        for j in range(i):
            total = 0.0
            for k in range(npoints):
                diff = descriptors[i,k]-descriptors[j,k]
                total += diff*diff
            writer.write("{:d} {:d} {:.5f}\n".format(i,j,total))
