// dist_contact.cpp
// A program which computes the distance/dissimilarity betweeen two 
// configurations based on the pairwise distances of bead pairs that are
// interacting in one configuration (i.e. within a contact threshold) and
// not interacting in the other configuration. This method is considered in
// the paper Brackley et. al Genome Biol. 2016

#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include "array.hpp"
#include "mat.hpp"

using std::cout;
using std::endl;
using std::string;
using std::vector;

using Mat = mat::SymMat<double>;
using MatStream = mat::SymMatStream<double>;

int main(int argc, char* argv[]) {
  if (argc < 6) {
    cout << "Usage: dist_contact nbeads thres outFile "
	 << "dmapFile1 [dmapFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  double thres = stod(string(argv[++argi]), nullptr);
  string outFile (argv[++argi]);
  int nsamples = argc-(++argi);

  vector<Mat> dmaps (nsamples, Mat(nbeads));
  Mat distMat (nsamples);
  MatStream stream;

  string dmapFile;
  for (int i = 0; i < nsamples; i++) {
    dmapFile = string(argv[i+argi]);
    if (!stream.read(dmapFile, dmaps[i])) {
      cout << "Error: cannot open the file " << dmapFile << endl;
    }
  }

  int count;
  double di, dj, dij, diff;
  for (int i = 0; i < nsamples; i++) {
    for (int j = 0; j < i; j++) {
      diff = 0.0;
      count = 0;
      for (int k = 0; k < nbeads; k++) {
	for (int l = 0; l < k; l++) {
	  di = dmaps[i](k,l);
	  dj = dmaps[j](k,l);
	  if ((di < thres) ^ (dj < thres)) {
	    dij = di-dj;
	    diff += dij*dij;
	    count++;
	  }
	}
      }
      distMat(i,j) = diff/static_cast<double>(count);
    }
  }
  
  if (!stream.dump(outFile, distMat, false)) { // Don't output the diagonal
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
}
