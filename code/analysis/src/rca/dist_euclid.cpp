// dist_euclid.cpp
// A porgram which computes the distance/dissimilarity between two 
// configurations based on the euclidea distance of the differences in
// all the pairwise distances of bead pairs in the two configurations

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;

// Helper functions
double distance(int n, double* x1, double* x2);

int main(int argc, char* argv[]) {
  if (argc < 6) {
    cout << "Usage: dist_euclid nfeatures column outFile " 
	 << "dataFile1 dataFile2 [dataFiles ...]" << endl;
    return 1;
  }

  int argi = 0;
  int nfeatures = stoi(string(argv[++argi]), nullptr, 10);
  int column = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);
  
  int nsamples = argc-(++argi);
  double** data = create2DArray<double>(nsamples,nfeatures);

  string file, line, str;
  ifstream reader;
  stringstream ss;
  size_t start, end;
  int index;
  double val;
  for (int i = 0; i < nsamples; i++) {
    file = argv[argi+i];
    // Determine the run number
    start = file.find_last_of("run_");
    end = file.find_last_of(".");
    index = stoi(file.substr(start+1,end-(start+1)), nullptr, 10)-1;
    reader.open(file);
    if (!reader) {
      cout << "Error: cannot open the file " << file << endl;
    }
    for (int j = 0; j < nfeatures; j++) {
      getline(reader,line);
      ss.str(line);
      for (int k = 0; k < column; k++) {
	ss >> str;
      }
      ss >> val;
      ss.clear();
      data[index][j] = val;
    }
    reader.close();
  }

  // Compute and output the distance between samples
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nsamples; i++) {
    for (int j = 0; j < i; j++) {
      writer << i << " " << j << " " 
	     << distance(nfeatures, data[i], data[j]) << "\n";
    }
  }
  writer.close();

  // Clean up
  deleteArray(data);
}

double distance(int n, double* x1, double* x2) {
  double sum = 0.0;
  double diff;
  for (int i = 0; i < n; i++) {
    diff = x1[i]-x2[i];
    sum += diff*diff;
  }
  return sqrt(sum);
}
