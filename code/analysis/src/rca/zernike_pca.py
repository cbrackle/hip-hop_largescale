# zernike_pca.py
# Principal component analysis of Zernike descriptor data

import sys
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

args = sys.argv

if (len(args) < 4):
    print("Usage: zernike_pca.py order out_file",\
          "descript_file1 descript_file2 [descript_files...]")
    sys.exit(1)

order = int(args.pop(1))
out_file = args.pop(1)

npoints = 0
for n in range(order+1): # Need to include zeroth order descriptor
    for l in range(n%2,n+1,2):
        npoints += 1

nconfigs = len(args)-1
descriptors = np.zeros((nconfigs,npoints),dtype=float)

# Load the data
data_files = args[1:]
for i,df in enumerate(data_files):
    with open(df,'r') as reader:
        # Pick out the run number (assume it is the last parameter)
        index = int(df[len(df)-df[::-1].index("_"):
                       len(df)-df[::-1].index(".")-1])-1
        for j,line in enumerate(reader):
            data = line.split()
            descriptors[index,j] = float(data[2])

# Do principal component analysis
# Need to rescale the data first
x = StandardScaler().fit_transform(descriptors)
# Do PCA
ncomps = 100
pca = PCA(n_components=ncomps)
pca_mat = pca.fit_transform(x)
#print(pca_mat)
print(pca.explained_variance_ratio_)
print(sum(pca.explained_variance_ratio_))

# Compute distance matrix
with open(out_file,'w') as writer:
    for i in range(nconfigs):
        for j in range(i):
            total = 0.0
            for k in range(ncomps):
                diff = pca_mat[i,k]-pca_mat[j,k]
                total += diff*diff
            dist = total**0.5
            writer.write("{:d} {:d} {:.10e}\n".format(i,j,dist))
        
