// zernike.cpp
// Compute the Zernike moments for a voxelised polymer configuration

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include "zernike_descriptor.hpp"
#include "zernike_moments.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: zernike dim minPoint voxelSize order "
	 << "voxelFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int dim = stoi(string(argv[++argi]), nullptr, 10); // Assume a square box
  double minPoint = stod(string(argv[++argi]), nullptr);
  double voxelSize = stod(string(argv[++argi]), nullptr);
  int order = stoi(string(argv[++argi]), nullptr, 10);
  string voxelFile (argv[++argi]);
  string outFile (argv[++argi]);

  typedef long double Double;
  //typedef __float128 Double;

  // Read the voxel data
  ifstream reader;
  reader.open(voxelFile);
  if (!reader) {
    cout << "Error: cannot open the file " << voxelFile << endl;
    return 1;
  }

  stringstream ss;
  string line;
  int x, y, z;
  double val;
  Double* voxels = new Double [dim*dim*dim];
  while (getline(reader, line)) {
    if (line.size() == 0 || line[0] == '#') continue;
    ss.str(line);
    ss >> x >> y >> z >> val;
    ss.clear();
    if (x >= 0 && x < dim && y >= 0 && y < dim && z >= 0 && z < dim) {
      voxels[(z*dim+y)*dim+x] = val;
    }
  }
  reader.close();
  
  //double boxsize = 2.0/sqrt(3.0);
  //double min = -boxsize*0.5;
  //double scale = boxsize/dim;
  
  ZernikeDescriptor<Double,Double> zd (voxels, dim, order, 
				       minPoint, voxelSize);
  
  ofstream writer;
  writer.open(outFile);
  for (int n = 0; n <= order; n++) {
    for (int l = n%2; l <= n; l+=2) {
      writer << n << " " << l << " " << zd.getInvariants(n,l) << endl;
    }
  }
  writer.close();
  
  // Clean up
  delete[] voxels;
}
