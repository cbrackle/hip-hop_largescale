// shape.cpp
// A program which computes shape parameters of a configuration

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <armadillo>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using namespace arma;

// Helper functions
double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 6) {
    cout << "Usage: gyration startBead endBead time posFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  long time = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Check the bead range is valid
  // Include both start and end bead
  if (endBead < startBead) {
    cout << "Error: end bead index is smaller than start bead index" << endl;
    return 1;
  }

  BeadStream reader;
  BeadList blist;
  if (!reader.open(posFile, blist)) {
    cout << "Error: cannot opne the file " << posFile << endl;
    return 1;
  }
  if (time != -1 && !reader.moveTo(time)) {
    cout << "Error: cannot read the frame at time " << time << endl;
    return 1;
  } else if (!reader.nextFrame()) {
    cout << "Error: no frames available to read" << endl;
    return 1;
  }
  reader.close();

  // Compute the radius of gyration
  int nbeads = endBead-startBead+1;
  double** pos = create2DArray<double>(nbeads,3);
  double cm[3] = {0.0,0.0,0.0};
  mat gyr (3,3,fill::zeros);
  double dx, dy, dz;
  for (int i = 0; i < nbeads; i++) {
    pos[i][0] = blist.getUnwrappedPosition(i+startBead,0);
    pos[i][1] = blist.getUnwrappedPosition(i+startBead,1);
    pos[i][2] = blist.getUnwrappedPosition(i+startBead,2);
    cm[0] += pos[i][0];
    cm[1] += pos[i][1];
    cm[2] += pos[i][2];
  }
  cm[0] /= static_cast<double>(nbeads);
  cm[1] /= static_cast<double>(nbeads);
  cm[2] /= static_cast<double>(nbeads);

  for (int i = 0; i < nbeads; i++) {
    dx = pos[i][0]-cm[0];
    dy = pos[i][1]-cm[1];
    dz = pos[i][2]-cm[2];
    gyr(0,0) += dx*dx;
    gyr(0,1) += dx*dy;
    gyr(0,2) += dx*dz;
    gyr(1,1) += dy*dy;
    gyr(1,2) += dy*dz;
    gyr(2,2) += dz*dz;
  }
  gyr /= static_cast<double>(nbeads);
  gyr(1,0) = gyr(0,1);
  gyr(2,0) = gyr(0,2);
  gyr(2,1) = gyr(1,2);

  // Find eigenvalues
  vec eigval; // Eigenvalues are stored in ascending order
  mat eigvec;
  eig_sym(eigval, eigvec, gyr);
  double g1 = eigval(0);
  double g2 = eigval(1);
  double g3 = eigval(2);
  double rg2 = g1+g2+g3;
  double b = g3-0.5*(g1+g2);
  double c = g2-g1;
  double k2 = 1.5*(g1*g1+g2*g2+g3*g3)/(rg2*rg2)-0.5;

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  writer << g1 << " " << g2 << " " << g3 << " " << rg2 << " " << b << " " 
	 << c << " " << k2 << endl;
  writer.close();

  // Clean up
  deleteArray(pos);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
