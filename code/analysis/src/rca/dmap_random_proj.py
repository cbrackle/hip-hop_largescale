#!/bin/env python3
# dmap_random_proj.py
# A program which projects the pairwise distance map into a smaller space
# using a random projection algorithm

import sys

# Check python version
if (sys.version_info < (3,)):
    print("This code must be run with Python 3")
    sys.exit(1)

import numpy as np
from sklearn.random_projection import johnson_lindenstrauss_min_dim
from sklearn.random_projection import GaussianRandomProjection
from sklearn.random_projection import SparseRandomProjection

args = sys.argv
if (len(args) != 7):
    print("Usage: dmap_random_proj.py start_bead end_bead ndim seed",
          "dmap_file out_file")
    sys.exit(1)
    
start_bead = int(args.pop(1))
end_bead = int(args.pop(1))
ndim = int(args.pop(1))
seed = int(args.pop(1))
dmap_file = args.pop(1)
out_file = args.pop(1)

if (end_bead < start_bead):
    print("Error: end bead index is smaller than start bead index")
    sys.exit(1)

nbeads = end_bead-start_bead+1 # Include both start and end bead
map_size = int(nbeads*(nbeads-1)/2)

# Load the distance map
dmap = np.zeros((1,map_size),dtype=float)
with open(dmap_file,'r') as reader:
    for i,line in enumerate(reader): # Assume data are sorted
        data = line.split()
        dmap[0][i] = float(data[2])

# Find the number of dimensions to reduce to
#ndim = johnson_lindenstrauss_min_dim(n_samples=map_size, eps=0.1)
print("Number of dimensions in projected space:",ndim)

# Do the projection
rng = np.random.RandomState(seed)
transformer = GaussianRandomProjection(n_components=ndim,random_state=rng)
#transformer = SparseRandomProjection(n_components=ndim,random_state=rng)
dmap_new = transformer.fit_transform(dmap)
#print(transformer.components_)

# Output the reduced map
with open(out_file,'w') as writer:
    for comp in dmap_new[0]:
        writer.write("{:.10e}\n".format(comp))
