// zernike_dist_matrix.cpp
// Compute the distance matrix of the zernike descriptors from 
// different configurations

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;

double zernikeDistance(int nzdscripts, double* zd1, double* zd2);

int main(int argc, char* argv[]) {
  if (argc < 5) {
    cout << "Usage: zernike_dist_matrix order outFile "
	 << "descriptFile1 descriptFile2 [descriptFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  int order = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);
  int nconfigs = argc-(++argi);

  // Determine the actual number of descriptors
  int nzdscripts = 0;
  for (int n = 0; n <= order; n++) {
    for (int l = n%2; l <= n; l+=2) {
      nzdscripts++;
    }
  }
  
  double** zdscripts = create2DDoubleArray(nconfigs,nzdscripts);
  
  string file;
  size_t start, end;
  int index, n, l;
  double zd;
  ifstream reader;
  
  for (int i = 0; i < nconfigs; i++) {
    file = argv[argi+i];
    // Determine the run number
    start = file.find_last_of("run_");
    end = file.find_last_of(".");
    index = stoi(file.substr(start+1,end-(start+1)), nullptr, 10)-1;
    reader.open(file);
    if (!reader) {
      cout << "Error: cannot open the file " << file << endl;
      return 1;
    }
    // Get the descriptors (assume they are sorted)
    for (int j = 0; j < nzdscripts; j++) {
      reader >> n >> l >> zd;
      cout << j << " " << n << " " << l << " " << zd << endl;
      zdscripts[index][j] = zd;
    }
    reader.close();
  }

  // Compute and output the distances
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  for (int i = 0; i < nconfigs; i++) {
    for (int j = 0; j < i; j++) {
      writer << i << " " << j << " ";
      writer << std::setprecision(10) << std::scientific;
      writer << zernikeDistance(nzdscripts,zdscripts[i],zdscripts[j]) << "\n";
      writer.unsetf(std::ios_base::floatfield);
    }
  }

  // Clean up
  deleteArray(zdscripts);
}


double zernikeDistance(int nzdscripts, double* zd1, double* zd2) {
  double sum = 0.0;
  double diff;
  for (int i = 0; i < nzdscripts; i++) {
    diff = zd1[i]-zd2[i];
    sum += diff*diff;
  }
  return sqrt(sum);
}
