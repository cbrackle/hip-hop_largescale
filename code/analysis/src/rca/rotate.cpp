// rotate.cpp
// A program which rotates a polymer configuration based on a randomly
// chosen axis and by a random angle about the centre of mass of the polymer

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <random>
#include <cmath>
#include <armadillo> // Need this for matrix algebra
#include "array.hpp"
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using namespace arma;

mat rotationMat(double theta, double phi, double psi);

int main(int argc, char* argv[]) {
  if (argc != 8) {
    cout << "Usage: rotate npolybeads time theta phi psi posFile outFile" 
	 << endl;
    return 1;
  }

  int argi = 0;
  int npolybeads = stoi(string(argv[++argi]), nullptr, 10);
  long time = stol(string(argv[++argi]), nullptr, 10);
  double theta = stod(string(argv[++argi]), nullptr);
  double phi = stod(string(argv[++argi]), nullptr);
  double psi = stod(string(argv[++argi]), nullptr);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read position data
  BeadStream reader;
  BeadList blist;
  reader.open(posFile, blist);
  if (!reader.isOpen()) {
    cout << "Error: cannot open position file " << posFile << endl;
    return 1;
  }
  if (time != -1 && !reader.moveTo(time)) {
    cout << "Error: cannot read the frame at time " << time << endl;
    return 1;
  } else if (!reader.nextFrame()) {
    cout << "Error: no frames available to read" << endl;
    return 1;
  }
  // Get box info
  double boxLo[3], boxHi[3], boxSize[3];
  int nbeads= reader.getNumOfBeads();
  time = reader.getTime();
  for (int i = 0; i < 3; i++) {
    boxLo[i] = reader.getBoxLo(i);
    boxHi[i] = reader.getBoxHi(i);
    boxSize[i] = reader.getBoxSize(i);
  }
  reader.close();
  
  // Find centre of mass
  double** pos = create2DDoubleArray(nbeads,3);
  double cm[3] = {0.0, 0.0, 0.0};
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < 3; j++) {
      pos[i][j] = blist.getUnwrappedPosition(i,j);
      if (i < npolybeads) cm[j] += pos[i][j];
    }
  }
  for (int i = 0; i < 3; i++) {
    cm[i] /= static_cast<double>(npolybeads);
  }
  
  // Perform an active rotation (in centre of mass frame)
  mat rotate = rotationMat(theta,phi,psi);
  vec pt(3);
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < 3; j++) {
      pt.at(j) = pos[i][j]-cm[j];
    }
    pt = rotate * pt;
    for (int j = 0; j < 3; j++) {
      pos[i][j] = pt.at(j)+cm[j];
    }
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  // Write the header (assume periodic boundary conditions)
  writer << "ITEM: TIMESTEP\n";
  writer << time << "\n";
  writer << "ITEM: NUMBER OF ATOMS\n";
  writer << nbeads << "\n";
  writer << "ITEM: BOX BOUNDS pp pp pp\n";
  for (int i = 0; i < 3; i++) {
    writer << boxLo[i] << " " << boxHi[i] << "\n";
  }
  writer << "ITEM: ATOMS id type xs ys zs ix iy iz \n";

  // Write the rotated bead positions
  int bound[3];
  double x[3];
  for (int i = 0; i < nbeads; i++) {
    writer << (i+1) << " " << blist.getType(i) << " ";
    // Compute the scaled coordinates
    for (int j = 0; j < 3; j++) {
      bound[j] = static_cast<int>(floor((pos[i][j]-boxLo[j])/boxSize[j]));
      x[j] = ((pos[i][j]-boxLo[j])-boxSize[j]*bound[j])/boxSize[j];
    }
    writer << x[0] << " " << x[1] << " " << x[2] << " ";
    writer << bound[0] << " " << bound[1] << " " << bound[2] << "\n";
  }
  writer.close();
  
  // Clean up
  deleteArray(pos);
}

mat rotationMat(double theta, double phi, double psi) {
  // Construct the (active) rotation matrix based on the formula
  // R_{ij} = delta_{ij}\cos\psi + (1-\cos\psi)n_{i}n_{j} - 
  //          \epsilon_{ijk}n_{k}\sin\psi
  // where n is the axis of rotation and \psi is the rotation angle
  double ct = cos(theta);
  double st = sin(theta);
  double cphi = cos(phi);
  double sphi = sin(phi);
  double cpsi = cos(psi);
  double spsi = sin(psi);

  // Create the rotation axis
  vec n(3);
  n.at(0) = st*cphi;
  n.at(1) = st*sphi;
  n.at(2) = ct;

  // Create the rotation matrix
  mat rotation(3,3);
  rotation.at(0,0) = cpsi + (1-cpsi)*n(0)*n(0);
  rotation.at(0,1) = (1-cpsi)*n(0)*n(1)-n(2)*spsi;
  rotation.at(0,2) = (1-cpsi)*n(0)*n(2)+n(1)*spsi;
  rotation.at(1,0) = (1-cpsi)*n(1)*n(0)+n(2)*spsi;
  rotation.at(1,1) = cpsi + (1-cpsi)*n(1)*n(1);
  rotation.at(1,2) = (1-cpsi)*n(1)*n(2)-n(0)*spsi;
  rotation.at(2,0) = (1-cpsi)*n(2)*n(0)-n(1)*spsi;
  rotation.at(2,1) = (1-cpsi)*n(2)*n(1)+n(0)*spsi;
  rotation.at(2,2) = cpsi + (1-cpsi)*n(2)*n(2);
  
  return rotation;
}
