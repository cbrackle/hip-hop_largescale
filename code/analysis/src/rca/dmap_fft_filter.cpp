// dmap_fft_filter.cpp
// A program which Fourier transforms a distance map and remove high
// frequency modes beyond a certain radius in k space

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include <fftw3.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: dmap_fft_filter startBead endBead radiusFactor "
	 << "dmapFile fftFile outMapFile" << endl;
    return 1;
  }

  int argi = 0;
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  double radiusFactor = stod(string(argv[++argi]), nullptr);
  string dmapFile (argv[++argi]);
  string fftFile (argv[++argi]);
  string outMapFile (argv[++argi]);

  // Check the bead range is valid
  if (endBead < startBead) {
    cout << "Error: end bead index is smaller than start bead index" << endl;
    return 1;
  }
  
  int nbeads = endBead-startBead+1; // Include both start and end bead
  int nbeadsSq = nbeads*nbeads;
  double** distMap = create2DArray<double>(nbeads,nbeads);
  
  ifstream reader;
  reader.open(dmapFile);
  if (!reader) {
    cout << "Error: cannot open the file " << dmapFile << endl;
    return 1;
  }
  string line;
  stringstream ss;
  int x, y;
  double val;
  while (getline(reader, line)) {
    ss.str(line);
    ss >> x >> y >> val;
    ss.clear();
    x -= startBead;
    y -= startBead;
    if (x < 0 || x >= nbeads || y < 0 || y >= nbeads) {
      cout << "Warning: ignoring out of bound entry:\n" << line << endl;
    }
    distMap[x][y] = val;
    distMap[y][x] = val;
  }
  reader.close();
  
  // Notice fftw_complex is just a typedef of double*[2]
  double*** fftMap = create3DArray<double>(nbeads,nbeads,2);
  double*** ifftMap = create3DArray<double>(nbeads,nbeads,2);
  double*** fftIn = create3DArray<double>(nbeads,nbeads,2);
  double*** fftOut = create3DArray<double>(nbeads,nbeads,2);

  // Create FFTW plans
  fftw_complex* in = (fftw_complex*) &fftIn[0][0][0];
  fftw_complex* out = (fftw_complex*) &fftOut[0][0][0];
  fftw_plan forwardPlan = 
    fftw_plan_dft_2d(nbeads, nbeads, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_plan backwardPlan = 
    fftw_plan_dft_2d(nbeads, nbeads, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
  if (!forwardPlan || !backwardPlan) {
    cout << "Error: cannot create FFT plans" << endl;
    return 1;
  }

  // Copy the original data to the in array
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < nbeads; j++) {
      fftIn[i][j][0] = distMap[i][j];
      fftIn[i][j][1] = 0.0;
    }
  }

  // Do the forward transform
  fftw_execute(forwardPlan);

  // Copy the transformed data to the fft array
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < nbeads; j++) {
      fftMap[i][j][0] = fftOut[i][j][0];
      fftMap[i][j][1] = fftOut[i][j][1];
    }
  }

  // Output the complex amplitudes of the frequencies with high frequencies
  // filtered out
  ofstream writer;
  writer.open(fftFile);
  if (!writer) {
    cout << "Error: cannot open the file " << fftFile << endl;
    return 1;
  }
  double kr = nbeads/radiusFactor;
  double kx0 = nbeads/2.0;
  double ky0 = nbeads/2.0;
  double kx = 0.0;
  double ky = 0.0;
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < nbeads; j++) {
      kx = static_cast<double>(i);
      ky = static_cast<double>(j);
      if (i > kx0) kx = i-nbeads;
      if (j > ky0) ky = j-nbeads;
      //if ((kx*kx+ky*ky) < kr*kr) {
      if (fabs(kx) <= kr/2.0 && fabs(ky) <= kr/2.0) {
	// Only output the top right quadrant as the input data is real
	// so we can exploit the property c_{n,-m} = c_{n,m}*
	// Also since the map is symmetric, c_{n,m} = c_{m,n}
	if (kx >= 0 && ky >= 0 && kx >= ky) {
	  writer << kx << " " << ky << " " 
		 << std::setprecision(10) << std::scientific 
		 << fftMap[i][j][0] << " " << fftMap[i][j][1] << "\n";
	  writer.unsetf(std::ios_base::floatfield);
	}
      } else {
	fftMap[i][j][0] = 0.0;
	fftMap[i][j][1] = 0.0;
      }
    }
  }
  writer.close();

  // Copy the fft data to the in array
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < nbeads; j++) {
      fftIn[i][j][0] = fftMap[i][j][0];
      fftIn[i][j][1] = fftMap[i][j][1];
    }
  }
  
  // Do the backward transform
  fftw_execute(backwardPlan);

  // Copy the inverse transformed data to the ifft array
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < nbeads; j++) {
      ifftMap[i][j][0] = fftOut[i][j][0] / static_cast<double>(nbeadsSq);
      ifftMap[i][j][1] = fftOut[i][j][1] / static_cast<double>(nbeadsSq);
    }
  }

  // Output the original, fft and ifft data
  writer.open(outMapFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outMapFile << endl;
    return 1;
  }
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < nbeads; j++) {
      writer << i << " " << j << " " << distMap[i][j] << " " 
	     << fftMap[i][j][0] << " " << fftMap[i][j][1] << " "
	     << ifftMap[i][j][0] << " " << ifftMap[i][j][1] << "\n";
    }
    writer << "\n";
  }
  
  // Clean up resources
  deleteArray(distMap);
  deleteArray(fftMap);
  deleteArray(ifftMap);
  deleteArray(fftIn);
  deleteArray(fftOut);
}
