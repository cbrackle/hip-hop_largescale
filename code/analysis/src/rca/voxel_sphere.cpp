// voxel_sphere.cpp
// A program which converts a polymer configuration into a 3D binarized
// voxel representation. Each bead is represented as a hard sphere

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>
#include "array.hpp"
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

int main(int argc, char* argv[]) {
  if (argc != 9) {
    cout << "Usage: voxel_sphere nbeads nvoxels beadSize boxSize scale time "
	 << "posFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  int nvoxels = stoi(string(argv[++argi]), nullptr, 10);
  double beadSize = stod(string(argv[++argi]), nullptr);
  double boxSize = stod(string(argv[++argi]), nullptr);
  double scale = stod(string(argv[++argi]), nullptr);
  long time = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read position data
  BeadStream reader;
  BeadList blist;
  reader.open(posFile, blist);
  if (!reader.isOpen()) {
    cout << "Error: cannot open position file " << posFile << endl;
    return 1;
  }
  if (time != -1 && !reader.moveTo(time)) {
    cout << "Error: cannot read the frame at time " << time << endl;
    return 1;
  } else if (!reader.nextFrame()) {
    cout << "Error: no frames available to read" << endl;
    return 1;
  }
  
  // Find centre of mass
  double** pos = create2DDoubleArray(nbeads,3);
  double cm[3] = {0.0, 0.0, 0.0};
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < 3; j++) {
      pos[i][j] = blist.getUnwrappedPosition(i,j);
      cm[j] += pos[i][j];
    }
  }
  for (int i = 0; i < 3; i++) {
    cm[i] /= static_cast<double>(nbeads);
  }
  
  reader.close();
  
  // Translate to centre of mass frame and rescale
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < 3; j++) {
      pos[i][j] -= cm[j];
      pos[i][j] *= scale;
    }
  }

  beadSize *= scale; // Scale the bead size as well
  double beadRadius = beadSize*0.5;
  double voxelSize = boxSize / static_cast<double>(nvoxels);
  double cutoff = beadRadius*5.0; // This is the cutoff radius
  int domainSize = static_cast<int>(ceil(cutoff*2.0/voxelSize));
  double*** voxels = create3DDoubleArray(nvoxels,nvoxels,nvoxels);
  double boxLo = -boxSize*0.5;
  int index[3];
  int lo[3], hi[3];
  double dx, dy, dz, dr;
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < 3; j++) {
      index[j] = static_cast<int>(floor((pos[i][j]-boxLo)/voxelSize));
      lo[j] = index[j]-domainSize/2;
      hi[j] = lo[j]+domainSize;
      // Check boundary
      if (lo[j] < 0) lo[j] = 0;
      if (hi[j] > nvoxels) hi[j] = nvoxels;
    }
    for (int x = lo[0]; x < hi[0]; x++) {
      dx = (x+0.5)*voxelSize-(pos[i][0]-boxLo);
      for (int y = lo[1]; y < hi[1]; y++) {
	dy = (y+0.5)*voxelSize-(pos[i][1]-boxLo);
	for (int z = lo[2]; z < hi[2]; z++) {
	  dz = (z+0.5)*voxelSize-(pos[i][2]-boxLo);
	  dr = sqrt(dx*dx+dy*dy+dz*dz);
	  if (dr < beadRadius) {
	    voxels[x][y][z] = 1.0;
	  }
	}
      }
    }
  }

  // Output the voxel representation
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nvoxels; i++) {
    for (int j = 0; j < nvoxels; j++) {
      for (int k = 0; k < nvoxels; k++) {
	writer << i << " " << j << " " << k << " " << voxels[i][j][k] << "\n";
      }
      writer << "\n";
    }
    writer << "\n";
  }
  writer.close();
  
  // Clean up
  deleteArray(pos);
  deleteArray(voxels);
}
