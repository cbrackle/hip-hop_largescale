// size_maxdist.cpp
// A program which outputs a list of the size of individual
// polymer configuration 

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>
#include "array.hpp"
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

double distance(double* r1, double* r2);

int main(int argc, char* argv[]) {
  if (argc < 8) {
    cout << "Usage: size_maxdist startBead endBead startTime endTime timeInc "
	 << "outfile posFile1 [posFiles ...]" << endl;
    return 1;
  }

  int argi = 0;
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);

  // Check the bead range is valid
  if (endBead < startBead) {
    cout << "Error: end bead index smaller than start bead index" << endl;
    return 1;
  }
  int nbeads = endBead-startBead+1; // Include both start and end bead

  double** pos = create2DDoubleArray(nbeads, 3);
  
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  writer << std::setprecision(5) << std::scientific;

  // Read position data
  BeadStream reader;
  BeadList blist;
  long time;
  string posFile;
  for (int n = ++argi; n < argc; n++) {
    posFile = string(argv[n]);
    reader.open(posFile, blist);
    if (!reader.isOpen()) {
      cout << "Error: cannot open position file " << posFile << endl;
      return 1;
    }
    while (reader.nextFrame()) {
      time = reader.getTime();
      if (endTime != -1 && time > endTime) break;  
      if (startTime != -1 && time < startTime) continue;
      if (timeInc != -1 && time % timeInc != 0) continue;

      // Compute the centre of mass of the polymer configuration
      double cm[3] = {0.0,0.0,0.0};
      for (int i = 0; i < nbeads; i++) {
	for (int j = 0; j < 3; j++) {
	  pos[i][j] = blist.getUnwrappedPosition(i+startBead,j);
	  cm[j] += pos[i][j];
	}
      }
      for (int i = 0; i < 3; i++) {
	cm[i] /= static_cast<double>(nbeads);
      }
      // Find the maximum radial distance from the cm
      double max = 0.0, d;
      for (int i = 0; i < nbeads; i++) {
	d = distance(pos[i],cm);
	if (d > max) {
	  max = d;
	}
      }
      writer << max << "\n";
    }
    reader.close();
  }
  writer.close();
  
  // Clean up
  deleteArray(pos);
}

double distance(double* r1, double* r2) {
  double distSq = 0.0; 
  double diff;
  for (int i = 0; i < 3; i++) {
    diff = r1[i]-r2[i];
    distSq += diff*diff;
  }
  return sqrt(distSq);
}
