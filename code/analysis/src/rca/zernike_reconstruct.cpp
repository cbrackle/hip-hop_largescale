// zernike_reconstruct.cpp
// Compute the zernike descriptors and reconstruct the configuration

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include "zernike_descriptor.hpp"
#include "zernike_moments.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: zernike_reconstruct dim minPoint voxelSize order "
	 << "voxelFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int dim = stoi(string(argv[++argi]), nullptr, 10); // Assume a square box
  double minPoint = stod(string(argv[++argi]), nullptr);
  double voxelSize = stod(string(argv[++argi]), nullptr);
  int order = stoi(string(argv[++argi]), nullptr, 10);
  string voxelFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the voxel data
  ifstream reader;
  reader.open(voxelFile);
  if (!reader) {
    cout << "Error: cannot open the file " << voxelFile << endl;
    return 1;
  }

  stringstream ss;
  string line;
  int x, y, z;
  double val;
  double* voxels = new double [dim*dim*dim];
  while (getline(reader, line)) {
    if (line.size() == 0 || line[0] == '#') continue;
    ss.str(line);
    ss >> x >> y >> z >> val;
    ss.clear();
    if (x >= 0 && x < dim && y >= 0 && y < dim && z >= 0 && z < dim) {
      voxels[(z*dim+y)*dim+x] = val;
    }
  }
  reader.close();
    
  ZernikeDescriptor<double,double> zd (voxels, dim, order, 
				       minPoint, voxelSize);
  ZernikeDescriptor<double,double>::ComplexT3D grid;
  grid.resize(dim);
  for (int i = 0; i < dim; i++) {
    grid[i].resize(dim);
    for (int j = 0; j < dim; j++) {
      grid[i][j].resize(dim);
    }
  }

  zd.reconstruct(grid, 0, order, 0, order, minPoint, voxelSize);

  ofstream writer;
  writer.open(outFile);
  for (int i = 0; i < dim; i++) {
    for (int j = 0; j < dim; j++) {
      for (int k = 0; k < dim; k++) {
	writer << i << " " << j << " " << k << " " 
	       << grid[i][j][k].real() << "\n";
      }
      writer << "\n";
    }
    writer << "\n";
  }
  writer.close();
  
  // Clean up
  delete[] voxels;
}
