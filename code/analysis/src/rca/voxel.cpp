// voxel.cpp
// A program which converts a polymer configuration into a 3D binarized
// voxel representation

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cmath>
#include <set>
#include <tuple>
#include "array.hpp"
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::set;
using std::tuple;

tuple<int,int,int> posToIndex(double* x, double boxSize, double voxelSize);

int main(int argc, char* argv[]) {
  if (argc != 9) {
    cout << "Usage: voxel nbeads pointsPerBead nvoxels boxSize scale time "
	 << "posFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  int pointsPerBead = stoi(string(argv[++argi]), nullptr, 10);
  int nvoxels = stoi(string(argv[++argi]), nullptr, 10);
  double boxSize = stod(string(argv[++argi]), nullptr);
  double scale = stod(string(argv[++argi]), nullptr);
  long time = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read position data
  BeadStream reader;
  BeadList blist;
  reader.open(posFile, blist);
  if (!reader.isOpen()) {
    cout << "Error: cannot open position file " << posFile << endl;
    return 1;
  }
  if (time != -1 && !reader.moveTo(time)) {
    cout << "Error: cannot read the frame at time " << time << endl;
    return 1;
  } else if (!reader.nextFrame()) {
    cout << "Error: no frames available to read" << endl;
    return 1;
  }
  
  int npoints = (nbeads-1)*pointsPerBead+1;
  
  // Find centre of mass
  double** pos = create2DDoubleArray(npoints,3);
  double cm[3] = {0.0, 0.0, 0.0};
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < 3; j++) {
      pos[i*pointsPerBead][j] = blist.getUnwrappedPosition(i,j);
      cm[j] += pos[i*pointsPerBead][j];
    }
  }
  for (int i = 0; i < 3; i++) {
    cm[i] /= static_cast<double>(nbeads);
  }
  
  reader.close();
  
  // Translate to centre of mass frame and rescale
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < 3; j++) {
      pos[i*pointsPerBead][j] -= cm[j];
      pos[i*pointsPerBead][j] *= scale;
    }
  }

  // Interpolate (linearly) and add the extra points
  double weight[pointsPerBead];
  for (int i = 0; i < pointsPerBead; i++) {
    weight[i] = static_cast<double>(i)/static_cast<double>(pointsPerBead);
  }
  for (int i = 0; i < nbeads-1; i++) {
    for (int j = 1; j < pointsPerBead; j++) {
      for (int k = 0; k < 3; k++) {
	pos[i*pointsPerBead+j][k] = (1.0-weight[j])*pos[i*pointsPerBead][k] + 
	  weight[j]*pos[(i+1)*pointsPerBead][k];
      }
    }
  }

  // Set up the voxel representation
  set<tuple<int,int,int> > unityVoxels;
  double voxelSize = boxSize / static_cast<double>(nvoxels);
  for (int i = 0; i < npoints; i++) {
    unityVoxels.insert(posToIndex(pos[i], boxSize, voxelSize));
  }

  // Output the voxel representation
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  tuple<int,int,int> index;
  for (int i = 0; i < nvoxels; i++) {
    for (int j = 0; j < nvoxels; j++) {
      for (int k = 0; k < nvoxels; k++) {
	index = std::make_tuple(i,j,k);
	if (unityVoxels.find(index) == unityVoxels.end()) {
	  writer << i << " " << j << " " << k << " " << 0 << "\n";
	} else {
	  writer << i << " " << j << " " << k << " " << 1 << "\n";
	}
      }
      writer << "\n";
    }
    writer << "\n";
  }
  writer.close();
  
  // Clean up
  deleteArray(pos);
}

tuple<int,int,int> posToIndex(double* x, double boxSize, double voxelSize) {
  int index[3];
  double boxLo = -boxSize*0.5;
  for (int i = 0; i < 3; i++) {
    index[i] = static_cast<int>(floor(((x[i]-boxLo)/voxelSize)));
  }
  return std::make_tuple(index[0],index[1],index[2]);
}
