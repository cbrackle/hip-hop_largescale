// dist_beads.cpp
// A program which computes the distance/dissimilarity betweeen two 
// configurations based on a comparison of binary contacts between a
// specific set of beads 

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <set>
#include <cmath>
#include "array.hpp"
#include "mat.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::stringstream;
using std::string;
using std::vector;
using std::set;

using Mat = mat::SymMat<double>;
using MatStream = mat::SymMatStream<double>;

int main(int argc, char* argv[]) {
  if (argc < 6) {
    cout << "Usage: dist_beads nbeads beadFile outFile "
	 << "dmapFile1 [dmapFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string outFile (argv[++argi]);
  int nsamples = argc-(++argi);

  // Read the list of beads
  ifstream reader;
  reader.open(beadFile);
  if (!reader) {
    cout << "Error: cannot open the file " << beadFile << endl;
    return 1;
  }

  set<int> beadset;
  stringstream ss;
  string line;
  int index;
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    beadset.insert(index-1); // Convert to zero-based
  }
  reader.close();
  vector<int> beads (beadset.begin(), beadset.end());
  int nselected = static_cast<int>(beads.size());
  beadset.clear();

  vector<Mat> dmaps (nsamples, Mat(nbeads));
  Mat distMat (nsamples);
  MatStream stream;

  string dmapFile;
  for (int i = 0; i < nsamples; i++) {
    dmapFile = string(argv[i+argi]);
    if (!stream.read(dmapFile, dmaps[i])) {
      cout << "Error: cannot open the file " << dmapFile << endl;
      return 1;
    }
  }

  double di, dj, dij, diff;
  for (int i = 0; i < nsamples; i++) {
    for (int j = 0; j < i; j++) {
      diff = 0;
      for (int k = 0; k < nselected; k++) {
	for (int l = 0; l < k; l++) {
	  di = dmaps[i](beads[k],beads[l]);
	  dj = dmaps[j](beads[k],beads[l]);
	  dij = di-dj;
	  diff += dij*dij;
	}
      }
      distMat(i,j) = sqrt(diff);
    }
  }
  
  if (!stream.dump(outFile, distMat, false)) { // Don't output the diagonal
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
}
