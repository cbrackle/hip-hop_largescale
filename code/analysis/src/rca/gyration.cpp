// gyration.cpp
// A program whcih computes the radius of gyration of a configuration

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

// Helper functions
double distSq(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 6) {
    cout << "Usage: gyration startBead endBead time posFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  long time = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Check the bead range is valid
  // Include both start and end bead
  if (endBead < startBead) {
    cout << "Error: end bead index is smaller than start bead index" << endl;
    return 1;
  }

  BeadStream reader;
  BeadList blist;
  if (!reader.open(posFile, blist)) {
    cout << "Error: cannot opne the file " << posFile << endl;
    return 1;
  }
  if (time != -1 && !reader.moveTo(time)) {
    cout << "Error: cannot read the frame at time " << time << endl;
    return 1;
  } else if (!reader.nextFrame()) {
    cout << "Error: no frames available to read" << endl;
    return 1;
  }
  reader.close();

  // Compute the radius of gyration
  int nbeads = endBead-startBead+1;
  double** pos = create2DArray<double>(nbeads,3);
  double cm[3] = {0.0,0.0,0.0};
  for (int i = 0; i < nbeads; i++) {
    pos[i][0] = blist.getUnwrappedPosition(i+startBead,0);
    pos[i][1] = blist.getUnwrappedPosition(i+startBead,1);
    pos[i][2] = blist.getUnwrappedPosition(i+startBead,2);
    cm[0] += pos[i][0];
    cm[1] += pos[i][1];
    cm[2] += pos[i][2];
  }
  cm[0] /= static_cast<double>(nbeads);
  cm[1] /= static_cast<double>(nbeads);
  cm[2] /= static_cast<double>(nbeads);
  double gyr = 0.0;
  for (int i = 0; i < nbeads; i++) {
    gyr += distSq(pos[i],cm);
  }
  gyr /= static_cast<double>(nbeads);
  gyr = sqrt(gyr);

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  writer << gyr << "\n";
  writer.close();

  // Clean up
  deleteArray(pos);
}

double distSq(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sum;
}
