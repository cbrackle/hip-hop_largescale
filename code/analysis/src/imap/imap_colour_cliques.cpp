// imap_colour_cliques.cpp
// A program which colours the interaction matrix by the observed cliques
// within the matrix. Interactions which are not part of any cliques are
// labelled as 0

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::map;

int main(int argc, char* argv[]) {
  if (argc < 4) {
    cout << "Usage: imap_colour_cliques minSize outFile cliqueFile1 "
	 << "[cliqueFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  // Minimum size to be considered as a cluster
  int minSize = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);

  // Read the cliques
  map<int,int> cliqueIndex;
  vector<int> cliqueSize;
  cliqueSize.push_back(0);

  ifstream reader;
  stringstream ss;
  string line;
  string token;
  int ncliques = 1;

  int nfiles = argc-(++argi);
  for (int n = 0; n < nfiles; n++) {
    string cliqueFile (argv[n+argi]);
    reader.open(cliqueFile);
    if (!reader) {
      cout << "Error: cannot open the file " << cliqueFile << endl;
      return 1;
    }
    vector<int> clique;
    while (getline(reader,line)) {
      ss.str(line);
      clique.clear();
      while (getline(ss,token,' ')) {
	clique.push_back(stoi(token, nullptr, 10));
      }
      ss.clear();
      if (static_cast<int>(clique.size()) >= minSize) {
	for (size_t i = 0; i < clique.size(); i++) {
	  cliqueIndex[clique[i]] = ncliques;
	}
	cliqueSize.push_back(clique.size());
	ncliques++; 
      } else {
	for (size_t i = 0; i < clique.size(); i++) {
	  cliqueIndex[clique[i]] = 0;
	}
      }
    }
    reader.close();
  }
  
  int nbeads = static_cast<int>(cliqueIndex.size());
  vector<int> beads (nbeads);
  int n = 0;
  for (map<int,int>::iterator it = cliqueIndex.begin();
       it != cliqueIndex.end(); it++) {
    beads[n] = it->first;
    n++;
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  int cliquei, cliquej;
  for (int i = 0; i < nbeads; i++) {
    cliquei = cliqueIndex[beads[i]];
    for (int j = 0; j < nbeads; j++) {
      cliquej = cliqueIndex[beads[j]];
      writer << i << " " << j << " " << beads[i] << " " << beads[j] << " "
	     << (cliquei == cliquej ? cliquei : 0) <<  " " 
	     << (cliquei == cliquej ? cliqueSize[cliquei] : 0) << "\n";
    }
    writer << "\n";
  }
  writer.close();
}
