// imap_colour_by_cluster.cpp
// A program which colours the interaction network by the clusters found

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include "bead_map.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::map;

using IMap = BeadMap<int>;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: imap_colour_by_cluster minSize freqThres "
	 << "beadFile imapFile clusterFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  // Minimum size to be considered as a cluster
  int minSize = stoi(string(argv[++argi]), nullptr, 10);
  int freqThres = stoi(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string imapFile (argv[++argi]);
  string clusterFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the interaction matrix
  IMap imap;
  if (!imap.load(beadFile,imapFile)) {
    cout << "Error: cannot load the matrix data" << endl;
    return 1;
  }
  const int nbeads = static_cast<int>(imap.size());
  
  // Read the interaction clusters
  map<int,int> beadGroup;
  vector<int> clusterSize;
  clusterSize.push_back(0);
  
  ifstream reader;
  reader.open(clusterFile);
  if (!reader) {
    cout << "Error: cannot open the file " << clusterFile << endl;
    return 1;
  }
  
  stringstream ss;
  string line;
  string token;
  int group = 1;
  vector<int> cluster;
  while (getline(reader,line)) {
    ss.str(line);
    cluster.clear();
    while (getline(ss,token,' ')) {
      cluster.push_back(stoi(token, nullptr, 10));
    }
    ss.clear();
    if (static_cast<int>(cluster.size()) >= minSize) {
      for (size_t i = 0; i < cluster.size(); i++) {
	beadGroup[cluster[i]] = group;
      }
      clusterSize.push_back(cluster.size());
      group++; 
    } else {
      for (size_t i = 0; i < cluster.size(); i++) {
	beadGroup[cluster[i]] = 0;
      }
    }
  }
  
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  int groupi, groupj;
  for (int i = 0; i < nbeads; i++) {
    groupi = beadGroup[imap.getBead(i)];
    for (int j = 0; j < nbeads; j++) {
      groupj = beadGroup[imap.getBead(j)];
      writer << i << " " << j << " " 
	     << imap.getBead(i) << " " << imap.getBead(j) << " ";
      if (groupi == groupj && imap(i,j) >= freqThres) {
	writer << groupi << " " << clusterSize[groupi] << "\n";
      } else {
	writer << 0 << " " << 0 << "\n";
      }
    }
    writer << "\n";
  }
  writer.close();
}
