# imap_prob.py
# A program which computes the p-value of the interaction matrix

import sys
import scipy.stats as stats
import numpy as np

args = sys.argv

if (len(args) != 5):
    print "Usage: imap_prob.py nbins prob_file imap_file out_file"
    sys.exit(1)

nbins = int(args.pop(1))
prob_file = args.pop(1)
imap_file = args.pop(1)
out_file = args.pop(1)

# Read the probability of contacts
print "Reading probability of contacts ..."
prob = [0.0 for i in xrange(nbins)]
count = [0.0 for i in xrange(nbins)]
with open(prob_file, 'r') as reader:
    for line in reader:
        if (len(line) == 0 or line[0] == '#'): continue
        data = line.split()
        index = int(data[0])
        p = float(data[1])
        n = float(data[2])
        prob[index] = p
        count[index] = n

# Read the interaction matrix
print "Reading interaction matrix ..."
with open(imap_file, 'r') as reader:
    with open(out_file, 'w') as writer:
        for line in reader:
            if (len(line) == 0 or line[0] == '#'): continue
            data = line.split()
            i = int(data[0])
            j = int(data[1])
            freq = int(data[2])
            sep = abs(i-j)
            sep_prob = prob[sep]
            sep_count = count[sep]
            p_value = stats.binom.sf(freq,sep_count,sep_prob)
            writer.write("{:d} {:d} {:d} {:.10e}\n".format(
                i,j,freq,p_value))
