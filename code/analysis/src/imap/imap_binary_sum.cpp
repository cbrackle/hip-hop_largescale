// imap_binary_sum.cpp
// A program which computes the interaction matrices and convert them into
// binary form based on a threshold (i.e., 0 for no contact, 1 for contact).
// The program then sums these matrices and outputs the result

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::pair;
using std::map;

using IMap = map<pair<int,int>,int>;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc < 9) {
    cout << "Usage: imap_binary_sum cutoff freqThres startTime endTime "
	 << "timeInc beadFile outFile posFile1 [posFiles ...]" << endl;
    return 1;
  }

  int argi = 0;
  double cutoff = stod(string(argv[++argi]), nullptr);
  int freqThres = stoi(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]); // Bead index is one-based and sorted
  string outFile (argv[++argi]);

  // Read the beads
  ifstream reader;
  reader.open(beadFile);
  if (!reader) {
    cout << "Error: cannot open the file " << beadFile << endl;
    return 1;
  }

  string line;
  stringstream ss;
  int index;
  vector<int> beads;
  
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    beads.push_back(index-1); // Convert to zero-based
    ss.clear();
  }
  reader.close();
  int nbeads = static_cast<int>(beads.size());
  
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif

  double*** pos = create3DArray<double>(nthreads,nbeads,3);
  vector<IMap> imap (nthreads);
  vector<IMap> clusterCount (nthreads);
  vector<BeadStream> bstreams (nthreads);
  vector<BeadList> blists (nthreads);
  
  // Read the position file and compute the interaction matrix
  int nfiles = argc-(++argi);
  int error = 0;
  
#pragma omp parallel default(none),\
  shared(imap, clusterCount, pos, bstreams, blists, nfiles, argi, argv),\
  shared(startTime, endTime, timeInc, cout, nbeads, beads, cutoff, freqThres),\
  reduction(+:error)
  {

#ifdef _OPENMP
    int id = omp_get_thread_num();
#else
    int id = 0;
#endif
    
#pragma omp for schedule(static)
    for (int n = 0; n < nfiles; n++) {
      string posFile = string(argv[n+argi]);
      cout << "Working on file " << posFile << endl;
      if (!bstreams[id].open(posFile, blists[id])) {
	cout << "Error: cannot open the file " << posFile << endl;
	error++;
	continue;
      }

      // Reset the interaction matrix
      for (int i = 0; i < nbeads; i++) {
	for (int j = 0; j <= i; j++) {
	  imap[id][std::make_pair(beads[i],beads[j])] = 0;
	}
      }

      for (long time = startTime; time <= endTime; time += timeInc) {
	if (bstreams[id].moveTo(time)) {
	  for (int i = 0; i < nbeads; i++) {
	    pos[id][i][0] = blists[id].getUnwrappedPosition(beads[i],0);
	    pos[id][i][1] = blists[id].getUnwrappedPosition(beads[i],1);
	    pos[id][i][2] = blists[id].getUnwrappedPosition(beads[i],2);
	  }
	  for (int i = 0; i < nbeads; i++) {
	    for (int j = 0; j <= i; j++) {
	      if (dist(pos[id][i], pos[id][j]) < cutoff) {
		imap[id][std::make_pair(beads[i],beads[j])]++;
	      }
	    }
	  }
	}
      }

      // Update clusters
      pair<int,int> coords;
      for (int i = 0; i < nbeads; i++) {
	for (int j = 0; j <= i; j++) {
	  coords = std::make_pair(beads[i],beads[j]);
	  if (imap[id][coords] >= freqThres) {
	    clusterCount[id][coords]++;
	  }
	}
      }
      bstreams[id].close();
    }
  } // Close parallel region

  if (error > 0) {
    cout << "There was an error while reading files - stop proceeding." 
	 << endl;
    return 1;
  }

  // Combine results
  pair<int,int> coords;
  for (int i = 1; i < nthreads; i++) {
    for (int j = 0; j < nbeads; j++) {
      for (int k = 0; k <= j; k++) {
	coords = std::make_pair(beads[j],beads[k]);
	clusterCount[0][coords] += clusterCount[i][coords];
      }
    }
  }

  // Output results
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j <= i; j++) {
      writer << beads[i] << " " << beads[j] << " "
	     << clusterCount[0][std::make_pair(beads[i],beads[j])] << "\n";
    }
  }
  writer.close();

  // Clean up
  deleteArray(pos);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
