// imap_binarise.cpp
// A program which binarises an interaction matrix (imap) based on a
// threshold. Pairs interacting less often than the threshold are set to 0
// and 1 otherwise

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;


int main(int argc, char* argv[]) {
  if (argc != 4) {
    cout << "Usage: imap_cluster thres imapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  int thres = stoi(string(argv[++argi]), nullptr, 10);
  string imapFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the interaction matrix
  ifstream reader;
  reader.open(imapFile);
  if (!reader) {
    cout << "Error: cannot open the file " << imapFile << endl;
    return 1;
  }
  
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  
  string line;
  stringstream ss;
  int beadi, beadj, freq;
  while (getline(reader, line)) {
    ss.str(line);
    ss >> beadi >> beadj >> freq; // beadi, beadj are zero-based
    ss.clear();
    writer << beadi << " " << beadj << " " 
	   << (freq < thres ? 0 : 1) << "\n"; 
  }
  reader.close();
  writer.close();
}
