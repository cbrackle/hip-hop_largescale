// imap_cluster_stats.cpp
// A program which finds all subgraphs/subclusters in a given 
// interaction network (imap) and computes their statistics as a
// function of the threshold on frequency of contact
// See also imap_cluster.cpp

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <omp.h>
#include "bead_map.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;
using std::set;

using IMap = BeadMap<int>;

struct Cluster {
  vector<int> list;
  vector<int> clusterStartIndex;
  vector<int> clusterSize;
  int nclusters;
  double avgClusterSize;
  double medianClusterSize;
  double maxClusterSize;
  double avgMaxSep;

  Cluster(int nbeads);
  void findCluster(int thres, int minSize, const IMap& imap);
  void reset();
};

struct ClusterStats {
  int nclusters;
  double avgClusterSize;
  double medianClusterSize;
  double maxClusterSize;
  double avgMaxSep;
};

int main(int argc, char* argv[]) {
  if (argc != 8) {
    cout << "Usage: imap_cluster minSize thresStart thresEnd thresInc "
	 << "beadFile imapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  int minSize = stoi(string(argv[++argi]), nullptr, 10);
  int thresStart = stoi(string(argv[++argi]), nullptr, 10);
  int thresEnd = stoi(string(argv[++argi]), nullptr, 10);
  int thresInc = stoi(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string imapFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the interaction matrix
  IMap imap;
  if (!imap.load(beadFile,imapFile)) {
    cout << "Error: cannot load the matrix data" << endl;
    return 1;
  }
  
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
  vector<Cluster> clusters (nthreads, 
			    Cluster(static_cast<int>(imap.size())));
  int nbins = (thresEnd-thresStart)/thresInc+1;
  vector<ClusterStats> stats (nbins);
  
#pragma omp parallel default(none), \
  shared(minSize, imap, stats, thresStart, thresEnd, thresInc, clusters)
  {
#ifdef _OPENMP
    int id = omp_get_thread_num();
#else
    int id = 0;
#endif
#pragma omp for schedule(static)
    for (int thres = thresStart; thres <= thresEnd; thres += thresInc) {
      clusters[id].reset();
      clusters[id].findCluster(thres, minSize, imap);
      int bin = (thres-thresStart)/thresInc;
      stats[bin].nclusters = clusters[id].nclusters;
      stats[bin].avgClusterSize = clusters[id].avgClusterSize;
      stats[bin].medianClusterSize = clusters[id].medianClusterSize;
      stats[bin].maxClusterSize = clusters[id].maxClusterSize;
      stats[bin].avgMaxSep = clusters[id].avgMaxSep;
    }
  }

  // Output statistics
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nbins; i++) {
    writer << thresStart+(i*thresInc) << " "
	   << stats[i].nclusters << " "
	   << stats[i].avgClusterSize << " " 
	   << stats[i].medianClusterSize << " "
	   << stats[i].maxClusterSize << " "
	   << stats[i].avgMaxSep << "\n";
  }
  writer.close();
}

// Member functions of Cluster
Cluster::Cluster(int nbeads) {
  list = vector<int>(nbeads,0);
  reset();
}

void Cluster::reset() { 
  for (size_t i = 0; i < list.size(); i++) {
    list[i] = i;
  }
  clusterStartIndex.clear();
  clusterSize.clear();
  nclusters = 0;
  maxClusterSize = 0.0;
  avgClusterSize = 0.0;
  medianClusterSize = 0.0;
  avgMaxSep = 0.0;
}

void Cluster::findCluster(int thres, int minSize, const IMap& imap) {
  // Find clusters
  const int nbeads = static_cast<int>(imap.size());
  int temp;
  int beadsInCluster = 0;
  for (int i = 0; i < nbeads; i++) {
    if (i == list[i]) {
      int j = i;
      beadsInCluster++;
      clusterStartIndex.push_back(i);
      do {
	for (int k = i+1; k < nbeads; k++) {
	  if (k != list[k]) continue;
	  if (imap(j,k) >= thres) {
	    temp = list[j];
	    list[j] = list[k];
	    list[k] = temp;
	    beadsInCluster++;
	  }
	}
	j = list[j];
      } while (j != i);
      clusterSize.push_back(beadsInCluster);
      beadsInCluster = 0;
    }
  }

  // Compute statistics
  // Statistics on cluster sizes
  nclusters = static_cast<int>(clusterSize.size());
  maxClusterSize = 0.0;
  avgClusterSize = 0.0;
  vector<int> sizes;
  for (size_t i = 0; i < clusterSize.size(); i++) {
    if (clusterSize[i] >= minSize) {
      sizes.push_back(clusterSize[i]);
      avgClusterSize += clusterSize[i];
      if (clusterSize[i] > maxClusterSize) {
	maxClusterSize = clusterSize[i];
      }
    }
  }
  int count = sizes.size();
  avgClusterSize /= static_cast<double>(count);
  std::sort(sizes.begin(), sizes.end());
  medianClusterSize = sizes[sizes.size()/2];
  
  // Max separation
  vector<int> sublist;
  int startIndex, index;
  avgMaxSep = 0.0;
  for (size_t i = 0; i < clusterStartIndex.size(); i++) {
    if (clusterSize[i] < minSize) continue;
    sublist.clear();
    startIndex = clusterStartIndex[i];
    sublist.push_back(imap.getBead(startIndex));
    index = list[startIndex];
    while (index != startIndex) {
      sublist.push_back(imap.getBead(index));
      index = list[index];
    }
    // Find max separation
    int sep, maxSep = 0;
    for (size_t j = 0; j < sublist.size(); j++) {
      for (size_t k = j+1; k < sublist.size(); k++) {
	sep = abs(imap.getBead(j)-imap.getBead(k));
	if (sep > maxSep) maxSep = sep;
      }
    }
    avgMaxSep += maxSep;
  }
  avgMaxSep /= static_cast<double>(count);
}
