# imap_filter_by_prob.py
# A program which filters the interaction matrix by probabilty of contacts

import sys
import scipy.stats as stats
import numpy as np

args = sys.argv

if (len(args) != 8):
    print "Usage: imap_filter_by_prob.py nbins sep_min sep_max",\
        "fdr prob_file imap_file out_file"
    sys.exit(1)

nbins = int(args.pop(1))
sep_min = int(args.pop(1))
sep_max = int(args.pop(1))
fdr = float(args.pop(1)) # False discovery rate
prob_file = args.pop(1)
imap_file = args.pop(1)
out_file = args.pop(1)

# Read the probability of contacts
print "Reading probability of contacts ..."
prob = [0.0 for i in xrange(nbins)]
count = [0.0 for i in xrange(nbins)]
with open(prob_file, 'r') as reader:
    for line in reader:
        if (len(line) == 0 or line[0] == '#'): continue
        data = line.split()
        index = int(data[0])
        p = float(data[1])
        n = float(data[2])
        prob[index] = p
        count[index] = n

# Read the interaction matrix
print "Reading interaction matrix ..."
imap = [{} for i in xrange(sep_min,sep_max+1)]
with open(imap_file, 'r') as reader:
    for line in reader:
        if (len(line) == 0 or line[0] == '#'): continue
        data = line.split()
        i = int(data[0])
        j = int(data[1])
        freq = int(data[2])
        sep = abs(i-j)
        if (sep < sep_min or sep > sep_max): continue
        sep_prob = prob[sep]
        sep_count = count[sep]
        p_value = stats.binom.sf(freq,sep_count,sep_prob)
        #print i,j,freq,p_value
        imap[sep-sep_min][(i,j)] = (freq,p_value)

# Sort by p-value and output those below the false discovery rate (fdr)
with open(out_file, 'w') as writer:
    for s, pairs in enumerate(imap):
        print "Doing",s+sep_min
        sorted_pairs = sorted(pairs.items(), key=lambda kv : kv[1][1], 
                              reverse=True)
        n = float(len(sorted_pairs))
        found_largest = False
        for i,pair in enumerate(sorted_pairs):
            rank_value = (i+1)*fdr/n
            p_value = pair[1][1]
            if (not found_largest and p_value < rank_value):
                found_largest = True
            if (found_largest):
                writer.write("{:d} {:d} {:d}\n".format(
                    pair[0][0], pair[0][1], pair[1][0]))
