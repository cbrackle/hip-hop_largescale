// imap.cpp
// A program which computes the interaction matrix/network between
// a list of specified beads

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <set>
#include <cmath>
#include <omp.h>
#include "array.hpp"
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "bead_map.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::pair;
using std::map;
using std::set;

using BMap = BeadMap<int>;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc < 8) {
    cout << "Usage: imap thres startTime endTime timeInc "
	 << "beadFile outFile posFile1 [posFiles ...]" << endl;
    return 1;
  }

  int argi = 0;
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]); // Bead index is one-based and sorted
  string outFile (argv[++argi]);

  // Read the beads
  ifstream reader;
  reader.open(beadFile);
  if (!reader) {
    cout << "Error: cannot open the file " << beadFile << endl;
    return 1;
  }

  string line;
  stringstream ss;
  int index;
  set<int> beadset;
  while (getline(reader,line)) {
    ss.str(line);
    ss >> index;
    beadset.insert(index-1); // Convert to zero-based
    ss.clear();
  }
  reader.close();
  int nbeads = static_cast<int>(beadset.size());
  vector<int> beads (beadset.begin(), beadset.end());
  beadset.clear();
  
  // Initialise the interaction matrix
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
  
  vector<BMap> bmaps (nthreads, BMap(nbeads,beads));
  
  // Read the position file and compute the interaction matrix
  double*** pos = create3DArray<double>(nthreads,nbeads,3);
  vector<BeadStream> bstreams (nthreads);
  vector<BeadList> blists (nthreads);
  int error = 0;  
  int nfiles = argc-(++argi);

#pragma omp parallel default(none),\
  shared(bstreams, blists, startTime, endTime, timeInc, cout),\
  shared(pos, beads, bmaps, nbeads, nfiles, argv, argi, thres),\
  reduction(+:error)
  {

#ifdef _OPENMP
    int id = omp_get_thread_num();
#else
    int id = 0;
#endif

#pragma omp for schedule(static)
    for (int m = 0; m < nfiles; m++) {
      string posFile = string(argv[m+argi]);
      cout << "Working on file " << posFile << endl;
      if (!bstreams[id].open(posFile, blists[id])) {
	cout << "Error: cannot open the file " << posFile << endl;
	error++;
	continue;
      }
      for (long time = startTime; time <= endTime; time += timeInc) {
	if (bstreams[id].moveTo(time)) {
	  for (int i = 0; i < nbeads; i++) {
	    pos[id][i][0] = blists[id].getUnwrappedPosition(beads[i],0);
	    pos[id][i][1] = blists[id].getUnwrappedPosition(beads[i],1);
	    pos[id][i][2] = blists[id].getUnwrappedPosition(beads[i],2);
	  }
	  
	  for (int i = 0; i < nbeads; i++) {
	    for (int j = 0; j <= i; j++) {
	      if (dist(pos[id][i], pos[id][j]) < thres) {
		bmaps[id](i,j)++;
	      }
	    }
	  }
	}
      }
      bstreams[id].close();
    }
  } // Close parallel region
  
  if (error > 0) {
    cout << "There were errors while reading files - stop proceeding" << endl;
    deleteArray(pos);
    return 1;
  }

  // Combine matrices
  for (int i = 1; i < nthreads; i++) {
    for (int j = 0; j < nbeads; j++) {
      for (int k = 0; k <= j; k++) {
	bmaps[0](j,k) += bmaps[i](j,k);
      }
    }
  }

  // Output results
  bmaps[0].dump(outFile);

  // Clean up
  deleteArray(pos);  
}
  

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
