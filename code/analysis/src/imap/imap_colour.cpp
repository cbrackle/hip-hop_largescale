// imap_colour.cpp
// A program which colours the interaction matrix by the observed clusters
// with the matrix. Interactions which are not part of any cluster/domain are
// labelled as 0

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::map;

int main(int argc, char* argv[]) {
  if (argc != 4) {
    cout << "Usage: imap_colour minSize clusterFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  // Minimum size to be considered as a cluster
  int minSize = stoi(string(argv[++argi]), nullptr, 10);
  string clusterFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the interaction clusters
  map<int,int> beadGroup;
  vector<int> clusterSize;
  clusterSize.push_back(0);
  
  ifstream reader;
  reader.open(clusterFile);
  if (!reader) {
    cout << "Error: cannot open the file " << clusterFile << endl;
    return 1;
  }

  stringstream ss;
  string line;
  string token;
  int group = 1;
  vector<int> cluster;
  while (getline(reader,line)) {
    ss.str(line);
    cluster.clear();
    while (getline(ss,token,' ')) {
      cluster.push_back(stoi(token, nullptr, 10));
    }
    ss.clear();
    if (static_cast<int>(cluster.size()) >= minSize) {
      cout << group << endl;
      for (size_t i = 0; i < cluster.size(); i++) {
	beadGroup[cluster[i]] = group;
      }
      clusterSize.push_back(cluster.size());
      group++; 
    } else {
      for (size_t i = 0; i < cluster.size(); i++) {
	beadGroup[cluster[i]] = 0;
      }
    }
  }

  int nbeads = static_cast<int>(beadGroup.size());
  vector<int> beads (nbeads);
  int n = 0;
  for (map<int,int>::iterator it = beadGroup.begin();
       it != beadGroup.end(); it++) {
    beads[n] = it->first;
    n++;
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  int groupi, groupj;
  for (int i = 0; i < nbeads; i++) {
    groupi = beadGroup[beads[i]];
    for (int j = 0; j < nbeads; j++) {
      groupj = beadGroup[beads[j]];
      writer << i << " " << j << " " << beads[i] << " " << beads[j] << " "
	     << (groupi == groupj ? groupi : 0) <<  " " 
	     << (groupi == groupj ? clusterSize[groupi] : 0) << "\n";
    }
    writer << "\n";
  }
  writer.close();
}
