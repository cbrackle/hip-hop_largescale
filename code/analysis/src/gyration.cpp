// gyration.cpp
// A program which computes the radius of gyration of a polymer chain
// as a function of its contour length

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <omp.h>
#include "array.hpp"
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

double distanceSq(double* r1, double* r2);

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: gyration nbeads startTime endTime timeInc "
	 << "posfile outfile" << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  int startTime = stoi(string(argv[++argi]), nullptr, 10);
  int endTime = stoi(string(argv[++argi]), nullptr, 10);
  int timeInc = stoi(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  int nbins = ((endTime-startTime)/timeInc)+1;
  double*** pos = create3DDoubleArray(nbins, nbeads, 3);
  
  // Read position data
  BeadStream reader;
  BeadList beadData;
  long time;
  int ibin;
  reader.open(posFile, beadData);
  if (!reader.isOpen()) {
    cout << "Error: cannot open position file " << posFile << endl;
    return 1;
  }
  while (reader.nextFrame()) {
    time = reader.getTime();
    if (time < startTime) {
      continue;
    } else if (time > endTime) {
      break;
    } else if (time >= startTime && time <= endTime) {
      ibin = (time-startTime)/timeInc;
      for (int i = 0; i < nbeads; i++) {
	for (int j = 0; j < 3; j++) {
	  pos[ibin][i][j] = beadData.getUnwrappedPosition(i,j);
	}
      }
    }
  }
  reader.close();
  
  // Compute the radius of gyration
  int i, j, k, sep;
  int* totalCount = create1DIntArray(nbeads);
  double* gyrAvg = create1DDoubleArray(nbeads);
  
#pragma omp parallel default(none) \
  shared(nbins, nbeads, pos, totalCount, gyrAvg) \
  private(ibin, i, j, k, sep)
  {
    // Initialise arrays
    int* count = create1DIntArray(nbeads);
    double* gyr = create1DDoubleArray(nbeads);
    double cm[3] = {0.0,0.0,0.0};
    int len;
    double r2;
#pragma omp for schedule(static)
    for (ibin = 0; ibin < nbins; ibin++) {
      for (i = 0; i < nbeads; i++) {
	for (j = 0; j <= i; j++) {
	  cm[0] = 0.0;
	  cm[1] = 0.0;
	  cm[2] = 0.0;
	  r2 = 0.0;
	  // Compute the CM of the segment from bead j to bead i
	  for (k = j; k <= i; k++) {
	    cm[0] += pos[ibin][k][0];
	    cm[1] += pos[ibin][k][1];
	    cm[2] += pos[ibin][k][2];
	  }
	  len = (i-j)+1;
	  cm[0] /= static_cast<double>(len);
	  cm[1] /= static_cast<double>(len);
	  cm[2] /= static_cast<double>(len);
	  // Compute the difference between positions of bead k and CM
	  for (k = j; k <= i; k++) {
	    r2 += distanceSq(pos[ibin][k], cm);
	  }
	  sep = abs(i-j);
	  gyr[sep] += (r2/static_cast<double>(len));
	  count[sep]++;
	}
      }
    }
    
    for (i = 0; i < nbeads; i++) {
#pragma omp atomic
      gyrAvg[i] += gyr[i];
#pragma omp atomic
      totalCount[i] += count[i];
    }

    // Free resources
    deleteArray(count);
    deleteArray(gyr);
  }
  
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open output file " << outFile << endl;
    return 1;
  }

  writer << std::setprecision(5) << std::fixed;
  for (i = 0; i < nbeads-1; i++) {
    gyrAvg[i] /= static_cast<double>(totalCount[i]);
    writer << i << " " << gyrAvg[i] << "\n";
  }
  writer.close();

  // Clean up
  deleteArray(totalCount);
  deleteArray(gyrAvg);
}

double distanceSq(double* r1, double* r2) {
  double distSq = 0.0; 
  double diff;
  for (int i = 0; i < 3; i++) {
    diff = r1[i]-r2[i];
    distSq += diff*diff;
  }
  return distSq;
}
