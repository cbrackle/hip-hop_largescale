/* cmap_corr_bead.cpp
 * A program which computes the Pearson correlation coefficient for each bin
 * between two contact maps
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

double correlation(const vector<double>& x, const vector<double>& y);

int main(int argc, char* argv[]) {
  if (argc != 8) {
    cout << "Usage: cmap_prob startBp endBp bpPerBin useGenomeCoords "
	 << "cmapFile1 cmapFile2 outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile1 (argv[++argi]);
  string cmapFile2 (argv[++argi]);
  string outFile (argv[++argi]);

  CMap cmap1 (startBp, endBp, bpPerBin, useGenomeCoords);
  CMap cmap2 (startBp, endBp, bpPerBin, useGenomeCoords);

  cout << "Reading the first map ..." << endl;
  if (!cmap1.read(cmapFile1)) {
    cout << "Error: cannot open the file " << cmapFile1 << endl;
    return 1;
  }

  cout << "Reading the second map ..." << endl;
  if (!cmap2.read(cmapFile2)) {
    cout << "Error: cannot open the file " << cmapFile2 << endl;
    return 1;
  }

  // Compute correlation for each bin
  cout << "Computing the correlation ..." << endl;
  uint nbins = cmap1.size();
  vector<double> corr (nbins, 0.0);
  vector<double> x (nbins, 0.0);
  vector<double> y (nbins, 0.0);
  for (uint i = 0; i < nbins; i++) {
    cout << "Doing bin " << i << endl;
    for (uint j = 0; j < nbins; j++) {
      x[j] = cmap1.at(i,j);
      y[j] = cmap2.at(i,j);
    }
    corr[i] = correlation(x,y);
  }
  
  // Output
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (uint i = 0; i < nbins; i++) {
    writer << (useGenomeCoords ? cmap1.binToBp(i) : i) << " " 
	   << corr[i] << "\n";
  }
  writer.close();
}


double correlation(const vector<double>& x, const vector<double>& y) {
  double xsum = 0.0;
  double ysum = 0.0;
  double xsqsum = 0.0;
  double ysqsum = 0.0;
  double xysum = 0.0;
  size_t n = x.size();
  for (size_t i = 0; i < n; i++) {
    xsum += x[i];
    ysum += y[i];
    xsqsum += x[i]*x[i];
    ysqsum += y[i]*y[i];
    xysum += x[i]*y[i];
  }
  return (n*xysum-xsum*ysum) / 
    (sqrt(n*xsqsum-xsum*xsum)*sqrt(n*ysqsum-ysum*ysum));
}
