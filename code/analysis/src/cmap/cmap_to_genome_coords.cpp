/* cmap_to_genome_coords.cpp
 * A program which converts contact maps in bin index to genome coordinates
 */

#include <iostream>
#include <string>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::string;

using CMap = ContactMap<unsigned int,double>;

int main(int argc, char* argv[]) {
  if (argc != 6) {
    cout << "Usage: cmap_to_genome_coords startBp endBp bpPerBin cmapFile "
	 << "outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  string cmapFile (argv[++argi]);
  string outFile (argv[++argi]);

  CMap cmap (startBp, endBp, bpPerBin, false);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  }
  cmap.enableGenomeCoords(true);
  cmap.dumpToGnuplot(outFile);
}
