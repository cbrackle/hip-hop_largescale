// cmap_combine.cpp
// A program which merges two contact maps. The first map is outputted in the
// upper triangle and the second map in the lower triangle

#include <iostream>
#include <string>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::string;

using CMap = ContactMap<unsigned int,double>;

int main(int argc, char* argv[]) {
  if (argc != 8) {
    cout << "Usage: cmap_combine startBp endBp bpPerBin useGenomeCoords "
	 << "cmapFile1 cmapFile2 outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile1 (argv[++argi]);
  string cmapFile2 (argv[++argi]);
  string outFile (argv[++argi]);

  CMap cmap1 (startBp, endBp, bpPerBin, useGenomeCoords);
  CMap cmap2 (startBp, endBp, bpPerBin, useGenomeCoords);
  
  if (!cmap1.read(cmapFile1)) {
    cout << "Error: cannot open the file " << cmapFile1 << endl;
    return 1;
  }
  if (!cmap2.read(cmapFile2)) {
    cout << "Error: cannot open the file " << cmapFile2 << endl;
    return 1;
  }
  
  CMap::dumpToGnuplot(cmap1, cmap2, outFile);
}
