/* cmap_corr_diag.cpp
 * A program which computes the Pearson correlation coefficient
 * between two contact maps for each diagonal band
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

double correlation(const vector<double>& x, const vector<double>& y);

int main(int argc, char* argv[]) {
  if (argc != 11) {
    cout << "Usage: cmap_diag mapStartBp mapEndBp bpPerBin startBp endBp "
	 << "startBand endBand useGenomeCoords cmapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long mapStartBp = stol(string(argv[++argi]), nullptr, 10);
  long mapEndBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  uint startBand = stoul(string(argv[++argi]), nullptr, 10);
  uint endBand = stoul(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile (argv[++argi]);
  string outFile (argv[++argi]);

  cout << "Reading the first map ..." << endl;
  CMap cmap (mapStartBp, mapEndBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  } 
  
  uint startBin = cmap.bpToBin(startBp);
  uint endBin = cmap.bpToBin(endBp);
  uint maxBins = cmap.size();
  if (startBin < 0) startBin = 0;
  if (endBin >= maxBins) endBin = maxBins-1;
  if (startBand < 0) startBand = 0;
  if (endBand >= maxBins) endBand = maxBins-1;
  cout << startBin << " " << endBin << " " << startBand << " " 
       << endBand << endl;
  
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  
  // Compute correlation for each bin
  //int nbins = endBin-startBin+1;
  int count = 0;
  double reads = 0.0;
  for (uint i = startBin; i <= endBin; i++) {
    count = 0;
    reads = 0.0;
    for (uint j = i+startBand; j <= i+endBand && j < maxBins; j++) {
      cout << i << " " << j << endl;
      reads += cmap.at(i,j);
      count++;
    }
    if (count > 0) {
      writer << (useGenomeCoords?mapStartBp+i*bpPerBin:i) << " "
	     << reads/static_cast<double>(count) << endl;
    }
  }
  writer.close();
}


double correlation(const vector<double>& x, const vector<double>& y) {
  double xsum = 0.0;
  double ysum = 0.0;
  double xsqsum = 0.0;
  double ysqsum = 0.0;
  double xysum = 0.0;
  size_t n = x.size();
  for (size_t i = 0; i < n; i++) {
    xsum += x[i];
    ysum += y[i];
    xsqsum += x[i]*x[i];
    ysqsum += y[i]*y[i];
    xysum += x[i]*y[i];
  }
  return (n*xysum-xsum*ysum) / 
    (sqrt(n*xsqsum-xsum*xsum)*sqrt(n*ysqsum-ysum*ysum));
}
