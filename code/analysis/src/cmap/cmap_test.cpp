/* cmap_test.cpp
 * A program which retrieves a diagonal band of the contact map
 */

#include <iostream>
#include <string>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::string;

using CMap = ContactMap<unsigned int, double>;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: cmap_test startBp endBp bpPerBin "
	 << "useGenomeCoords mapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string mapFile (argv[++argi]);
  string outFile (argv[++argi]);

  cout << "Reading contact map ..." << endl;
  CMap cmap(startBp, endBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(mapFile)) {
    cout << "Error: cannot load the contact map" << endl;
    return 1;
  }
  
  cmap.dumpToGnuplot(outFile);
}
