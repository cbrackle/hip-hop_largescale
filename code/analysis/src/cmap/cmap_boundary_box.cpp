/* cmap_tad_boundary.cpp
 * A program which identifies TAD boundaries in a contact map
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

int main(int argc, char* argv[]) {
  if (argc != 8) {
    cout << "Usage: cmap_boundary_box startBp endBp bpPerBin boxBp "
	 << "useGenomeCoords cmapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  long boxBp = stoi(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile (argv[++argi]);
  string outFile (argv[++argi]);

  CMap cmap (startBp, endBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  }
  
  // Use the sliding box algorithm to find TAD boundaries
  uint nbins = cmap.size();
  uint boxSize = cmap.bpToBin(startBp+boxBp);
  vector<double> signal (nbins, 0.0);
  double sum = 0.0;
  int width;
  for (uint i = 1; i < nbins; i++) {
    sum = 0.0;
    if (i < boxSize) {
      width = i;
    } else if (i > nbins-boxSize) {
      width = nbins-i;
    } else {
      width = boxSize;
    }
    for (uint j = i-width; j < i; j++) {
      for (uint k = i; k < i+width; k++) {
	sum += cmap.at(j,k);
      }
    }
    sum /= (width*width);
    signal[i] = sum;
  }
  
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  
  for (uint i = 1; i < nbins; i++) {
    writer << (useGenomeCoords ? cmap.binToBp(i) : i) << " "
	   << signal[i] << "\n";
  }
  writer.close();
}
