/* cmap_to_pmap.cpp
 * A program which converts contact maps in bin index to genome coordinates
 */

#include <iostream>
#include <string>
#include <cmath>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::string;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: cmap_to_pmap startBp endBp bpPerBin useGenomeCoords "
	 << "cmapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile (argv[++argi]);
  string outFile (argv[++argi]);

  CMap cmap (startBp, endBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  }
  
  // Find the average of the maximum of each column/row
  double sum = 0.0, colMax  = 0.0, val;
  int count = 0;
  uint nbins = cmap.size();
  for (uint i = 0; i < nbins; i++) {
    colMax = 0.0;
    for (uint j = 0; j < nbins; j++) {
      val = cmap.at(i,j);
      if (val > colMax) colMax = val;
    }
    // Only count columns/rows with data
    if (fabs(colMax) > 0.0) {
      sum += colMax;
      count++;
    }
  }
  
  // Normalise the map
  double norm = sum/static_cast<double>(count);
  cmap /= norm;
  
  cmap.dumpToGnuplot(outFile);
}
