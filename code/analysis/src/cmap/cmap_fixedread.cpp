// cmap_fixedread.cpp
// A program which creates a contact map from the simulation trajectories 
// based on a threshold value

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <random>
#include <cmath>
#include <omp.h>
#include <set>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "contact.hpp"
#include "contact_kernel.hpp"

using std::cout;
using std::endl;
using std::stringstream;
using std::string;
using std::vector;
using std::set;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc < 20) {
    cout << "Usage: cmap_fixed_read_stochastic npolybeads beadsPerBin startBp "
	 << "bpPerBead nreads multiplier range seed thres startTime endTime "
	 << "timeInc norm kernelFunc kernelArgs useGenomeCoords useHeader "
	 << "outFile posFile1 [posFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  int npolybeads = stoi(string(argv[++argi]), nullptr, 10);
  int beadsPerBin = stoi(string(argv[++argi]), nullptr, 10);
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBead = stol(string(argv[++argi]), nullptr, 10);
  long nreads = stol(string(argv[++argi]), nullptr, 10);
  int multiplier = stoi(string(argv[++argi]), nullptr, 10);
  int range = stoi(string(argv[++argi]), nullptr, 10);
  unsigned int seed = stoul(string(argv[++argi]), nullptr, 10);
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  int norm = stoi(string(argv[++argi]), nullptr, 10);
  string kernelFunc (argv[++argi]);
  string kernelArgs (argv[++argi]);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  int useHeader = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);

  ContactKernel kernel (kernelFunc, kernelArgs);
  
#ifdef _OPENMP  
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
  
  BeadStream bstream;
  BeadList blist;
  int nsamples = 0;
  uint nbins = static_cast<uint>(ceil(static_cast<double>(npolybeads)/
				      static_cast<double>(beadsPerBin)));
  long binsize = bpPerBead*beadsPerBin;
  vector<CMap> cmaps (nthreads, 
		      CMap(startBp, binsize, nbins, useGenomeCoords));
  vector<std::mt19937> randGen;
  for (int i = 0; i < nthreads; i++) {
    randGen.push_back(std::mt19937(seed+i));
  }
  std::uniform_int_distribution<int> randInt(0,npolybeads-1);
  std::uniform_real_distribution<double> randDouble(0.0,1.0);

  // Determine the number of attempts in each frame to get the desired
  // number of reads and find the total number of frames to sample
  int nfiles = argc-(++argi);
  vector<string> posFiles (nfiles);
  for (int i = 0; i < nfiles; i++) {
    posFiles[i] = string(argv[argi+i]);
  }
  
  vector<BeadStream> bstreams (nthreads);
  vector<BeadList> blists (nthreads);
  int error = 0;
#pragma omp parallel default(none),\
  shared(bstreams, blists, posFiles, nfiles, startTime, endTime, timeInc),\
  shared(cout), reduction(+:error), reduction(+:nsamples)
  {
#ifdef _OPENMP
    int id = omp_get_thread_num();
#else
    int id = 0;
#endif
#pragma omp for schedule(static)
    for (int i = 0; i < nfiles; i++) {
      cout << "Checking file " << posFiles[i] << endl;
      if (!bstreams[id].open(posFiles[i], blists[id])) {
	cout << "Error: cannot open the file " << posFiles[i] << endl;
	error++;
      }
      for (long time = startTime; time <= endTime; time += timeInc) {
	if (bstreams[id].moveTo(time)) {
	  nsamples++;
	}
      }
    }
  }
  if (error > 0) {
    cout << "There are errors when checking files - stop proceeding" << endl;
    return 1;
  }

  // Compute the required number of successful attempts per frame per thread
  nreads *= multiplier;
  long nreadsPerFrame = 
    static_cast<long>(floor(static_cast<double>(nreads)/
			    static_cast<double>(nsamples)));
  long nreadsPerThread = 
    static_cast<long>(floor(static_cast<double>(nreadsPerFrame)/
			    static_cast<double>(nthreads)));
  long remainder = nreads-nreadsPerThread*nthreads*nsamples;
  vector<int> vec (nthreads, 0);
  vector<vector<int> > nextras (nsamples, vec);
  std::mt19937 mt (seed+nthreads);
  std::uniform_int_distribution<int> randSamples(0,nsamples-1);
  std::uniform_int_distribution<int> randThreads(0,nthreads-1);
  for (long i = 0; i < remainder; i++) {
    int j = randSamples(mt);
    int k = randThreads(mt);
    nextras[j][k]++;
  }
  
  cout << "Number of reads to do: " << nreads << endl;
  cout << "Number of frames to sample: " << nsamples << endl;
  cout << "Number of reads per frame: " << nreadsPerFrame << endl;
  cout << "Number of reads per frame per thread: " << nreadsPerThread << endl;
  
  int sample = 0;
  for (int n = 0; n < nfiles; n++) {
    bstream.open(posFiles[n], blist);
    for (long time = startTime; time <= endTime; time += timeInc) {
      if (!bstream.moveTo(time)) continue;
      cout << "Doing t = " << time << endl;
#pragma omp parallel default(none),\
  shared(blist, cmaps, npolybeads, beadsPerBin, sample, range, thres),\
  shared(randInt, randDouble, randGen, nextras, nreadsPerThread, kernel)
      {
#ifdef _OPENMP
	int id = omp_get_thread_num();
#else
	int id = 0;
#endif
	int i, j;
	double xi[3], xj[3];
	int naccepted = 0;
	int nsuccess = nreadsPerThread+nextras[sample][id];
	while (naccepted < nsuccess) {
	  i = randInt(randGen[id]);
	  j = randInt(randGen[id]);
	  if (abs(i-j) <= range) {
	    xi[0] = blist.getUnwrappedPosition(i,0);
	    xi[1] = blist.getUnwrappedPosition(i,1);
	    xi[2] = blist.getUnwrappedPosition(i,2);
	    xj[0] = blist.getUnwrappedPosition(j,0);
	    xj[1] = blist.getUnwrappedPosition(j,1);
	    xj[2] = blist.getUnwrappedPosition(j,2);
	    if (randDouble(randGen[id]) < kernel.prob(dist(xi,xj),thres)) {
	      cmaps[id](i/beadsPerBin,j/beadsPerBin) += 1.0;
	      naccepted++;
	    }
	  }
	}
      }
      sample++;
    }
    bstream.close();
  }

  // Combine the maps and clean up
  for (int i = 1; i < nthreads; i++) {
    cmaps[0] += cmaps[i];
    cmaps[i].clear();
  }

  // Normalise the map by the multiplier factor
  if (norm) {
    cmaps[0] /= static_cast<double>(multiplier);
  }

  // Store the command, the seed and the number of threads used 
  // for generating the contact map
  if (useHeader) {
    stringstream ss;
    ss << "# ";
    for (int i = 0; i < argc-1; i++) {
      ss << argv[i] << " ";
    }
    ss << argv[argc-1] << "\n";
    ss << "# Number of threads: " << nthreads << "\n";
    ss << "# Seed: " << seed << "\n";
    ss << "# Probability kernel: " << kernelFunc << " " << kernelArgs << "\n";
    cmaps[0].setHeader(ss.str());
    cmaps[0].enableHeader(true);
  }

  // Output the results
  cmaps[0].dumpToGnuplot(outFile);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
