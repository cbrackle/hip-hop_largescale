// cmap_apa.cpp
// A program which performs the aggregrate peak analysis (APA) by summing the
// pixel values of submatrices centred at loops

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <set>
#include <utility>
#include "array.hpp"
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::set;
using std::pair;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

struct CornerAPA {
  double mean;
  double stdev;
  double ratio;
  double zscore;
};

CornerAPA getCornerAPA(int istart, int iend, int jstart, int jend, 
		       int boxsize, double** apa);

int main(int argc, char* argv[]) {
  if (argc != 13) {
    cout << "Usage: cmap_apa width cornerWidth minSep maxSep startBp endBp "
	 << "bpPerBin norm loopFile cmapFile apaStatsFile apaImageFile" 
	 << endl;
    return 1;
  }

  int argi = 0;
  int width = stoi(string(argv[++argi]), nullptr, 10);
  int cornerWidth = stoi(string(argv[++argi]), nullptr, 10);
  int minSep = stoi(string(argv[++argi]), nullptr, 10);
  int maxSep = stoi(string(argv[++argi]), nullptr, 10);
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  int norm = stoi(string(argv[++argi]), nullptr, 10);
  string loopFile (argv[++argi]);
  string cmapFile (argv[++argi]);
  string apaStatsFile (argv[++argi]);
  string apaImageFile (argv[++argi]);

  // Read the contact map
  CMap cmap (startBp, endBp, bpPerBin, true);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  }
  int nbins = static_cast<int>(cmap.size());

  // Read the loop list
  ifstream reader;
  reader.open(loopFile);
  if (!reader) {
    cout << "Error: cannot open the file " << loopFile << endl;
    return 1;
  }
  
  string line;
  stringstream ss;
  int x, y, sep;
  set<pair<int,int> > loops;
  while (getline(reader,line)) {
    ss.str(line);
    ss >> x >> y;
    ss.clear();
    sep = abs(x-y);
    if (sep >= minSep && sep <= maxSep && 
	x-width >= 0 && x+width < nbins &&
	y-width >= 0 && y+width < nbins) {
      loops.insert(x < y ? std::make_pair(x,y) : std::make_pair(y,x));
    }
  }
  reader.close();
  
  // Compute the aggregate peak score
  int boxsize = width*2+1;
  int x0, y0;
  double** apa = create2DArray<double>(boxsize,boxsize);
  for (set<pair<int,int> >::iterator it = loops.begin(); 
       it != loops.end(); it++) {
    x0 = it->first;
    y0 = it->second;
    for (int i = 0; i < boxsize; i++) {
      x = x0-width+i;
      for (int j = 0; j < boxsize; j++) {
	y = y0-width+j;
	// Use the "image" coordinate system like the APA program in Juicer
	apa[j][boxsize-(i+1)] += cmap.at(x,y);
      }
    }
  }

  // Compute apa statistics - find the ratio of the score of the middle pixel 
  // to the average score of pixels within each corner box
  int rstart = boxsize-cornerWidth;
  CornerAPA ll = getCornerAPA(0, cornerWidth, 0, cornerWidth, boxsize, apa);
  CornerAPA lr = getCornerAPA(rstart, boxsize, 0, cornerWidth, boxsize, apa);
  CornerAPA ul = getCornerAPA(0, cornerWidth, rstart, boxsize, boxsize, apa);
  CornerAPA ur = getCornerAPA(rstart, boxsize, rstart, boxsize, boxsize, apa);

  // Find the ratio of the score of the middle pixel to the average score of
  // the remaining pixels 
  double avg = 0.0;
  for (int i = 0; i < boxsize; i++) {
    for (int j = 0; j < boxsize; j++) {
      avg += apa[i][j];
    }
  }
  int centre = (boxsize-1)/2;
  avg -= apa[centre][centre];
  avg /= static_cast<double>(boxsize*boxsize-1);
  double p2m = apa[centre][centre] / avg; 
  
  // Normalise the apa scores
  if (norm) {
    double nloops = static_cast<double>(loops.size());
    for (int i = 0; i < boxsize; i++) {
      for (int j = 0; j < boxsize; j++) {
	apa[i][j] /= nloops;
      }
    }
  }

  // Output the apa results
  ofstream writer;
  writer.open(apaImageFile);
  if (!writer) {
    cout << "Error: cannot open the file " << apaImageFile << endl;
    return 1;
  }
  for (int i = 0; i < boxsize; i++) {
    for (int j = 0; j < boxsize; j++) {
      writer << i << " " << j << " " << apa[i][j] << "\n";
    }
    writer << "\n";
  }
  writer.close();

  writer.open(apaStatsFile);
  if (!writer) {
    cout << "Error: cannot open the file " << apaStatsFile << endl;
    return 1;
  }
  writer << "Loops\t" << loops.size() << "\n";
  writer << "P2M\t" << p2m << "\n";
  writer << "P2UL\t" << ul.ratio << "\n";
  writer << "P2UR\t" << ur.ratio << "\n";
  writer << "P2LL\t" << ll.ratio << "\n";
  writer << "P2LR\t" << lr.ratio << "\n";
  writer << "ZscoreLL\t" << ll.zscore << "\n";
  writer.close();

  // Clean up
  deleteArray(apa);
}

CornerAPA getCornerAPA(int istart, int iend, int jstart, int jend, 
		       int boxsize, double** apa) {
  double avg = 0.0, avgSq = 0.0;
  for (int i = istart; i < iend; i++) {
    for (int j = jstart; j < jend; j++) {
      avg += apa[i][j];
      avgSq += apa[i][j]*apa[i][j];
    }
  }
  int npixels = (iend-istart)*(jend-jstart);
  avg /= static_cast<double>(npixels);
  avgSq /= static_cast<double>(npixels);

  CornerAPA capa;
  int c = (boxsize-1)/2;
  double centre = apa[c][c];
  capa.mean = avg;
  // Use un-biased sample variance
  capa.stdev = npixels > 1 ? sqrt(npixels/static_cast<double>(npixels-1)*
				  (avgSq-avg*avg)) : 0.0;
  capa.ratio = centre/avg;
  capa.zscore = (centre-capa.mean)/capa.stdev;
  return capa;
}
