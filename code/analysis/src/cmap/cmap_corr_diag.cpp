/* cmap_corr_diag.cpp
 * A program which computes the Pearson correlation coefficient
 * between two contact maps for each diagonal band
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

double correlation(const vector<double>& x, const vector<double>& y);

int main(int argc, char* argv[]) {
  if (argc != 11) {
    cout << "Usage: cmap_corr_diag mapStartBp mapEndBp bpPerBin startBp endBp "
	 << "bandWidth useGenomeCoords cmapFile1 cmapFile2 outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long mapStartBp = stol(string(argv[++argi]), nullptr, 10);
  long mapEndBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  uint bandWidth = stoul(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile1 (argv[++argi]);
  string cmapFile2 (argv[++argi]);
  string outFile (argv[++argi]);

  CMap cmap1 (mapStartBp, mapEndBp, bpPerBin, useGenomeCoords);
  CMap cmap2 (mapStartBp, mapEndBp, bpPerBin, useGenomeCoords);

  cout << "Reading the first map ..." << endl;
  if (!cmap1.read(cmapFile1)) {
    cout << "Error: cannot open the file " << cmapFile1 << endl;
    return 1;
  }

  cout << "Reading the second map ..." << endl;
  if (!cmap2.read(cmapFile2)) {
    cout << "Error: cannot open the file " << cmapFile2 << endl;
    return 1;
  }

  uint startBin = cmap1.bpToBin(startBp);
  uint endBin = cmap1.bpToBin(endBp);
  uint maxBins = cmap1.size();
  if (startBin < 0) startBin = 0;
  if (endBin >= maxBins) endBin = maxBins-1;
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  
  // Compute correlation for each bin
  cout << "Computing the correlation ..." << endl;
  uint nbins = endBin-startBin+1;//map1->getNumOfBins();
  vector<double> x;
  vector<double> y;
  double corr;
  for (uint n = 0; n < nbins; n+=bandWidth) {
    x.clear();
    y.clear();
    for (uint i = startBin; i <= endBin; i++) {
      uint j = i+n+1;
      if (j > endBin) break;
      for (uint k = 0; k < bandWidth; k++) {
	if (j+k > endBin) break;
	x.push_back(cmap1.at(i,j+k));
	y.push_back(cmap2.at(i,j+k));
      }
    }
    corr = correlation(x,y);
    writer << (useGenomeCoords?n*bpPerBin:n) << " " << corr << "\n";
  }
  writer.close();
}


double correlation(const vector<double>& x, const vector<double>& y) {
  double xsum = 0.0;
  double ysum = 0.0;
  double xsqsum = 0.0;
  double ysqsum = 0.0;
  double xysum = 0.0;
  size_t n = x.size();
  for (size_t i = 0; i < n; i++) {
    xsum += x[i];
    ysum += y[i];
    xsqsum += x[i]*x[i];
    ysqsum += y[i]*y[i];
    xysum += x[i]*y[i];
  }
  return (n*xysum-xsum*ysum) / 
    (sqrt(n*xsqsum-xsum*xsum)*sqrt(n*ysqsum-ysum*ysum));
}
