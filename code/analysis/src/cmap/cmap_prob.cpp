/* cmap_prob.cpp
 * A program which computes the contact prob as a function of the linear
 * separation distance between two genomic loci
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

int main(int argc, char* argv[]) {
  if (argc != 6) {
    cout << "Usage: cmap_prob startBp endBp bpPerBin useGenomeCoords cmapFile "
	 << "outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile (argv[++argi]);
  string outFile (argv[++argi]);
  
  CMap cmap (startBp, endBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  }
  
  uint nbins = cmap.size();

  // Compute the contact probability
  vector<double> prob (nbins, 0.0);
  uint sep;
  for (uint i = 0; i < nbins; i++) {
    for (uint j = 0; j <= i; j++) {
      sep = abs(i-j);
      prob[sep] += cmap.at(i,j);
    }
  }

  // Normalise
  double sum = 0.0;
  for (uint i = 0; i < nbins; i++) {
    prob[i] /= static_cast<double>(nbins-i);
    sum += prob[i];
  }
  
  // Convert to a probability and output
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (uint i = 0; i < nbins; i++) {
    prob[i] /= sum;
    writer << (useGenomeCoords ? cmap.binToBp(i) : i) << " " 
	   << prob[i] << "\n";
  }
  writer.close();
}
