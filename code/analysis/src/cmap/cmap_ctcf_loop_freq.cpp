// cmap_ctcf_loop_freq.cpp
// A program which computes the contact frequency of CTCF loops

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <utility>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::map;
using std::pair;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

int main(int argc, char* argv[]) {
  if (argc != 9) {
    cout << "Usage: cmap_ctcf_loop_freq minSep maxSep startBp endBp "
	 << "bpPerBin ctcfFile cmapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  int minSep = stoi(string(argv[++argi]), nullptr, 10);
  int maxSep = stoi(string(argv[++argi]), nullptr, 10);
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stoi(string(argv[++argi]), nullptr, 10);
  string ctcfFile (argv[++argi]);
  string cmapFile (argv[++argi]);
  string outFile (argv[++argi]);
  
  // Read CTCF file
  ifstream reader;
  reader.open(ctcfFile);
  if (!reader) {
    cout << "Error: cannot open the file " << ctcfFile << endl;
    return 1;
  }

  string line;
  stringstream ss;
  int index, dir;
  vector<pair<int,int> > ctcfDir;
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index >> dir;
    ctcfDir.push_back(std::make_pair(index,dir));
    ss.clear();
  }
  reader.close();

  int nCTCFs = static_cast<int>(ctcfDir.size());  

  map<pair<int,int>,double> ctcfScore;
  int ibin, jbin, idir, jdir, ijsep;
  // Find all convergent CTCF pairs within the range [minSep:maxSep]
  for (int i = 0; i < nCTCFs; i++) {
    ibin = ctcfDir[i].first;
    idir = ctcfDir[i].second;
    for (int j = 0; j < i; j++) {
      jbin = ctcfDir[j].first;
      jdir = ctcfDir[j].second;
      ijsep = abs(ibin-jbin);
      if (ijsep >= minSep && ijsep <= maxSep && 
	  (ibin < jbin ? idir >= 0 && jdir <= 0 : idir <= 0 && jdir >= 0)) {
	ctcfScore[std::make_pair(ibin,jbin)] = 0.0;
      }
    }
  }
  ctcfDir.clear();

  // Read contact map
  CMap cmap (startBp, endBp, bpPerBin, true);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  }

  // Find expected (average) count for each genomic separation
  uint nbins = cmap.size();
  vector<double> avgCount (nbins, 0.0);
  uint sep;
  for (uint i = 0; i < nbins; i++) {
    for (uint j = 0; j <= i; j++) {
      sep = i-j;
      avgCount[sep] += cmap.at(i,j);
    }
  }
  for (uint i = 0; i < nbins; i++) {
    avgCount[i] /= static_cast<double>(nbins-i);
  }

  // Compute the observed/expected value for each CTCF loop
  for (map<pair<int,int>,double>::iterator it = ctcfScore.begin(); 
       it != ctcfScore.end(); it++) {
    ibin = it->first.first;
    jbin = it->first.second;
    ijsep = abs(ibin-jbin);
    it->second = cmap.at(ibin,jbin) / avgCount[ijsep];
  }

  // Output the results
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  
  for (map<pair<int,int>,double>::iterator it = ctcfScore.begin();
       it != ctcfScore.end(); it++) {
    writer << it->first.first << " " << it->first.second << " "
	   << it->second << "\n";
  }
  writer.close();
}
