// cmap_submatrix.cpp
// Extract a subsection fo a contact matrix
// Assume that there is a full matrix

#include <iostream>
#include <string>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::string;

using CMap = ContactMap<unsigned int,double>;

int main(int argc, char* argv[]) {
  if (argc != 9) {
    cout << "Usage: cmap_submatrix inStartBp inEndBp outStartBp outEndBp "
	 << "bpPerBin useGenomeCoords inMapFile outMapFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long inStartBp = stol(string(argv[++argi]), nullptr, 10);
  long inEndBp = stol(string(argv[++argi]), nullptr, 10);
  long outStartBp = stol(string(argv[++argi]), nullptr, 10);
  long outEndBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string inMapFile (argv[++argi]);
  string outMapFile (argv[++argi]);
  
  CMap cmap (inStartBp, inEndBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(inMapFile)) {
    cout << "Error: cannot open the file " << inMapFile << endl;
    return 1;
  }
  cmap.dumpToGnuplot(outStartBp, outEndBp, outMapFile);
}
