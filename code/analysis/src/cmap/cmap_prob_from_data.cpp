// cmap_prob_from_data.cpp
// A program which computes the probability of contact as a function of
// linear/genomic separation directly from the position data

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

double dist(double x1[3], double x2[3]);
int mapToBin(int sep, int scale);

int main(int argc, char* argv[]) {
  if (argc < 11) {
    cout << "Usage: cmap_prob_from_data npolybeads beadsPerBin "
	 << "bpPerBead thres startTime endTime timeInc useGenomicCoords "
	 << "outFile posFile1 [posFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  int npolybeads = stoi(string(argv[++argi]), nullptr, 10);
  int beadsPerBin = stoi(string(argv[++argi]), nullptr, 10);
  long bpPerBead = stol(string(argv[++argi]), nullptr, 10);
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  int useGenomicCoords = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);
  
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
  
  BeadStream reader;
  BeadList blist;
  long nsamples = 0L;
  int nbins = static_cast<int>(ceil(static_cast<double>(npolybeads)/
				   static_cast<double>(beadsPerBin)));
  long binsize = bpPerBead*beadsPerBin;
  
  long** contacts = create2DArray<long>(nthreads, nbins);

  string posFile;
  for (int n = ++argi; n < argc; n++) {
    posFile = string(argv[n]);
    if (!reader.open(posFile, blist)) {
      cout << "Error: cannot open the file " << posFile << endl;
      return 1;
    }
    for (long time = startTime; time <= endTime; time += timeInc) {
      if (reader.moveTo(time)) {
	cout << "Doing t = " << time << endl;
	nsamples++;
#pragma omp parallel default(none),\
  shared(blist, contacts, beadsPerBin, npolybeads, thres)
	{
#ifdef _OPENMP
	  int id = omp_get_thread_num();
#else
	  int id = 0;
#endif
	  double xi[3], xj[3];
	  int sep;
#pragma omp for schedule(dynamic, 16) 
	  for (int i = 0; i < npolybeads; i++) {
	    xi[0] = blist.getUnwrappedPosition(i,0);
	    xi[1] = blist.getUnwrappedPosition(i,1);
	    xi[2] = blist.getUnwrappedPosition(i,2);
	    for (int j = i; j < npolybeads; j++) {
	      xj[0] = blist.getUnwrappedPosition(j,0);
	      xj[1] = blist.getUnwrappedPosition(j,1);
	      xj[2] = blist.getUnwrappedPosition(j,2);
	      if (dist(xi,xj) < thres) {
		sep = abs(j-i);
		contacts[id][mapToBin(sep,beadsPerBin)] += 1.0;
	      }
	    }
	  } // Close contact loops
	} // Close parallel region
      }
    } // Close time loop
    reader.close();
  } // Close posFile loop
  
  // Combine all contacts
  for (int i = 1; i < nthreads; i++) {
    for (int j = 0; j < nbins; j++) {
      contacts[0][j] += contacts[i][j];
    }
  }
  
  // Determine the number of possible contacts for each bin
  int sep;
  int* count = create1DArray<int>(nbins);
  for (int i = 0; i < npolybeads; i++) {
    for (int j = i; j < npolybeads; j++) {
      sep = abs(j-i);
      count[mapToBin(sep,beadsPerBin)]++;
    }
  }
  
  // Output
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nbins; i++) {
    writer << (useGenomicCoords ? (i+0.5)*binsize : i) << " "
	   << (static_cast<double>(contacts[0][i]) / 
	       static_cast<double>(nsamples*count[i])) << " "
	   << contacts[0][i] << "\n";
  }
  writer.close();

  // Clean up
  deleteArray(count);
  deleteArray(contacts);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}

inline int mapToBin(int sep, int scale) {
  return sep/scale;
}
