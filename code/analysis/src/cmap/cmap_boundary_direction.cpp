/* cmap_boundary_direction.cpp
 * A program which determines the boundaries of TADs using a "directionality
 * index" - comparing the sum of contacts of each bin with its left and those
 * with its right
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

int main(int argc, char* argv[]) {
  if (argc != 11) {
    cout << "Usage: cmap_boundary_direction matStartBp matEndBp startBp endBp "
	 << "bpPerBin minBp maxBp useGenomeCoords cmapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long matStartBp = stol(string(argv[++argi]), nullptr, 10);
  long matEndBp = stol(string(argv[++argi]), nullptr, 10);
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  long minBp = stol(string(argv[++argi]), nullptr, 10);
  long maxBp = stol(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile (argv[++argi]);
  string outFile (argv[++argi]);

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  CMap cmap (matStartBp, matEndBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  }
  
  // Use the directionality algorithm to find TAD boundaries
  uint nbinsMax = cmap.size();
  uint startBin = cmap.bpToBin(startBp);
  uint endBin = cmap.bpToBin(endBp); // endBp is exclusive
  if (endBin > nbinsMax) endBin = nbinsMax;
  uint nbins = endBin-startBin;
  uint minBin = cmap.bpToBin(matStartBp+minBp);
  uint maxBin = cmap.bpToBin(matStartBp+maxBp);
  
  double forward, backward;
  for (uint i = startBin; i < endBin; i++) {
    forward = 0.0;
    backward = 0.0;
    for (uint j = (i-maxBin < 0 ? 0 : i-maxBin); j < i-minBin; j++) {
      backward += cmap.at(i,j);
    }
    for (uint j = i+minBin+1; j < i+maxBin && j < nbins; j++) {
      forward += cmap.at(i,j);
    }
    writer << (useGenomeCoords ? cmap.binToBp(i) : i) << " "
	   << forward << " " << backward << " " 
	   << forward-backward << "\n";
  }
  writer.close();
}
