// cmap_threshold.cpp
// A program which creates a contact map from simulation trajectories based
// on contacts within a threshold distance

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <omp.h>
#include <cmath>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "contact.hpp"

#define OMP_CHUNK 16

using std::cout;
using std::endl;
using std::stringstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc < 14) {
    cout << "Usage: cmap_threshold npolybeads beadsPerBin startBp bpPerBead "
	 << "thres startTime endTime timeInc norm useGenomeCoords useHeader "
	 << "outFile posFile1 [posFiles ...]" 
	 << endl;
    return 1;
  }

  int argi = 0;
  int npolybeads = stoi(string(argv[++argi]), nullptr, 10);
  int beadsPerBin = stoi(string(argv[++argi]), nullptr, 10);
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBead = stol(string(argv[++argi]), nullptr, 10);
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  int norm = stoi(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  int useHeader = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);
  
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif

  BeadStream bstream;
  BeadList blist;
  int nsamples = 0;
  uint nbins = static_cast<uint>(ceil(static_cast<double>(npolybeads)/
				      static_cast<double>(beadsPerBin)));
  long binsize = bpPerBead*beadsPerBin;
  vector<CMap> cmaps (nthreads, 
		      CMap(startBp, binsize, nbins, useGenomeCoords));
  
  string posFile;
  for (int n = ++argi; n < argc; n++) {
    posFile = string(argv[n]);
    if (!bstream.open(posFile, blist)) {
      cout << "Error: cannot open the file " << posFile << endl;
      return 1;
    }
    for (long time = startTime; time <= endTime; time += timeInc) {
      if (!bstream.moveTo(time)) continue;
      cout << "Doing t = " << time << endl;
      nsamples++;
#pragma omp parallel default(none),\
  shared(blist, cmaps, beadsPerBin, npolybeads, thres)
      {
#ifdef _OPENMP
	int id = omp_get_thread_num();
#else
	int id = 0;
#endif
	double xi[3], xj[3];
#pragma omp for schedule(dynamic,OMP_CHUNK)
	for (int i = 0; i < npolybeads; i++) {
	  xi[0] = blist.getUnwrappedPosition(i,0);
	  xi[1] = blist.getUnwrappedPosition(i,1);
	  xi[2] = blist.getUnwrappedPosition(i,2);
	  for (int j = i; j < npolybeads; j++) {
	    xj[0] = blist.getUnwrappedPosition(j,0);
	    xj[1] = blist.getUnwrappedPosition(j,1);
	    xj[2] = blist.getUnwrappedPosition(j,2);
	    if (dist(xi,xj) < thres) {
	      cmaps[id](i/beadsPerBin,j/beadsPerBin) += 1.0;
	    }
	  }
	}
      }
    }
    bstream.close();
  }

  // Combine the maps and clean up
  for (int i = 1; i < nthreads; i++) {
    cmaps[0] += cmaps[i];
    cmaps[i].clear();
  }

  // Normalise the map
  // Need to handle the special case for the final bin of the map
  if (norm) {
    int binSize = beadsPerBin;
    int lastBinSize = npolybeads % beadsPerBin;
    if (lastBinSize == 0) lastBinSize = binSize;
    for (CMap::iter it = cmaps[0].begin(); it != cmaps[0].end(); it++) {
      uint i = it->first.first;
      uint j = it->first.second;
      if (i == nbins-1 && j == nbins-1) {
	it->second /= static_cast<double>(nsamples*lastBinSize*lastBinSize);
      } else if (i == nbins-1 || j == nbins-1) {
	it->second /= static_cast<double>(nsamples*lastBinSize*binSize);
      } else {
	it->second /= static_cast<double>(nsamples*binSize*binSize);
      }
    }
  }

  // Store the command, the seed and the number of threads used 
  // for generating the contact map
  if (useHeader) {
    stringstream ss;
    ss << "# ";
    for (int i = 0; i < argc-1; i++) {
      ss << argv[i] << " ";
    }
    ss << argv[argc-1] << "\n";
    ss << "# Number of threads: " << nthreads << "\n";
    cmaps[0].setHeader(ss.str());
    cmaps[0].enableHeader(true);
  }
  
  // Output the results
  cmaps[0].dumpToGnuplot(outFile);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
