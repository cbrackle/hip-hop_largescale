/* cmap_tad_boundary.cpp
 * A program which identifies TAD boundaries in a contact map
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

int main(int argc, char* argv[]) {
  if (argc != 11) {
    cout << "Usage: cmap_tad_boundary startBp endBp bpPerBin windowSize "
	 << "avgWidth peakFactor useGenomeCoords cmapFile signalFile "
	 << "peakFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  uint windowSize = stoul(string(argv[++argi]), nullptr, 10);
  int avgWidth = stoi(string(argv[++argi]), nullptr, 10);
  double peakFactor = stod(string(argv[++argi]), nullptr);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string cmapFile (argv[++argi]);
  string signalFile (argv[++argi]);
  string peakFile (argv[++argi]);

  CMap cmap (startBp, endBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot open the file " << cmapFile << endl;
    return 1;
  }

  // Use the sliding box algorithm to find TAD boundaries
  uint nbins = cmap.size();
  vector<double> signal (nbins, 0.0);
  double sum = 0.0;
  uint width;
  for (uint i = 0; i < nbins; i++) {
    sum = 0.0;
    if (i < windowSize) {
      width = i;
    } else if (i > nbins-windowSize) {
      width = nbins-i;
    } else {
      width = windowSize;
    }
    for (uint j = i-width; j < i; j++) {
      for (uint k = i; k < i+width; k++) {
	sum += cmap.at(j,k);
      }
    }
    sum /= (width*width);
    signal[i] = sum;
  }
  
  ofstream writer;
  writer.open(signalFile);
  if (!writer) {
    cout << "Error: cannot open the file " << signalFile << endl;
    return 1;
  }
  
  for (uint i = 0; i < nbins; i++) {
    writer << (useGenomeCoords ? cmap.binToBp(i) : i) << " "
	   << signal[i] << "\n";
  }
  writer.close();

  // Identify the possible minima in the signal
  writer.open(peakFile);
  if (!writer) {
    cout << "Error: cannot open the file " << peakFile << endl;
    return 1;
  }
  double avg;
  for (uint i = avgWidth; i < nbins-avgWidth; i++) {
    avg = 0.0;
    for (uint j = i-avgWidth; j < i+avgWidth; j++) {
      avg += signal[j];
    }
    avg /= static_cast<double>(avgWidth*2+1);
    if (signal[i] < peakFactor*avg) {
      writer << (useGenomeCoords ? cmap.binToBp(i) : i) << " " 
	     << signal[i] << "\n";
    }
  }
  writer.close();
}
