// cmap_semistochastic.cpp
// A program to create a contact map from the simulation trajectories based
// on a threshold value

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <random>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "contact.hpp"
#include "contact_kernel.hpp"

#define OMP_CHUNK 16

using std::cout;
using std::endl;
using std::stringstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc < 19) {
    cout << "Usage: cmap_semistochastic npolybeads beadsPerBin startBp "
	 << "bpPerBead multiplier range seed thres startTime endTime timeInc "
	 << "norm kernelFunc kernelArgs useGenomeCoords useHeader outFile "
	 << "posFile1 [posFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  int npolybeads = stoi(string(argv[++argi]), nullptr, 10);
  int beadsPerBin = stoi(string(argv[++argi]), nullptr, 10);
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBead = stol(string(argv[++argi]), nullptr, 10);
  int multiplier = stoi(string(argv[++argi]), nullptr, 10);
  int range = stoi(string(argv[++argi]), nullptr, 10);
  unsigned int seed = stoul(string(argv[++argi]), nullptr, 10);
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  int norm = stoi(string(argv[++argi]), nullptr, 10);
  string kernelFunc (argv[++argi]);
  string kernelArgs (argv[++argi]);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  int useHeader = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);

  ContactKernel kernel (kernelFunc, kernelArgs);

#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif

  BeadStream bstream;
  BeadList blist;
  int nsamples = 0;
  uint nbins = static_cast<uint>(ceil(static_cast<double>(npolybeads)/
				      static_cast<double>(beadsPerBin)));
  long binsize = bpPerBead*beadsPerBin;
  vector<CMap> cmaps (nthreads, 
		      CMap(startBp, binsize, nbins, useGenomeCoords));
  vector<std::mt19937> randGen;
  for (int i = 0; i < nthreads; i++) {
    randGen.push_back(std::mt19937(seed+i));
  }
  std::uniform_int_distribution<int> randInt(0,npolybeads-1);
  std::uniform_real_distribution<double> randDouble(0.0,1.0);
  
  string posFile;
  for (int n = ++argi; n < argc; n++) {
    posFile = string(argv[n]);
    if (!bstream.open(posFile, blist)) {
      cout << "Error: cannot open the file " << posFile << endl;
      return 1;
    }    
    for (long time = startTime; time <= endTime; time += timeInc) {
      if (!bstream.moveTo(time)) continue;
      cout << "Doing t = " << time << endl;
      nsamples++;
#pragma omp parallel default(none),\
  shared(blist, cmaps, npolybeads, beadsPerBin, range, thres, multiplier),\
  shared(randInt, randDouble, randGen, kernel)
      {
#ifdef _OPENMP
	int id = omp_get_thread_num();
#else
	int id = 0;
#endif
	double xi[3], xj[3];
#pragma omp for schedule(dynamic,OMP_CHUNK)
	for (int k = 0; k < npolybeads*multiplier; k++) {
	  int i = k % npolybeads;
	  xi[0] = blist.getUnwrappedPosition(i,0);
	  xi[1] = blist.getUnwrappedPosition(i,1);
	  xi[2] = blist.getUnwrappedPosition(i,2);
	  for (int j = i; j < npolybeads; j++) {
	    if (abs(i-j) <= range) {
	      xj[0] = blist.getUnwrappedPosition(j,0);
	      xj[1] = blist.getUnwrappedPosition(j,1);
	      xj[2] = blist.getUnwrappedPosition(j,2);
	      if (randDouble(randGen[id]) < kernel.prob(dist(xi,xj),thres)) {
		cmaps[id](i/beadsPerBin,j/beadsPerBin) += 1.0;
	      }
	    }
	  }
	}
      }
    }
    bstream.close();
  }

  // Combine the maps and clean up
  for (int i = 1; i < nthreads; i++) {
    cmaps[0] += cmaps[i];
    cmaps[i].clear();
  }
  
  // Normalise the map
  // Need to handle the special case for the final bin of the map
  if (norm) {
    nsamples *= multiplier;
    int binSize = beadsPerBin;
    int lastBinSize = npolybeads % beadsPerBin;
    if (lastBinSize == 0) lastBinSize = binSize;
    for (CMap::iter it = cmaps[0].begin(); it != cmaps[0].end(); it++) {
      uint i = it->first.first;
      uint j = it->first.second;
      if (i == nbins-1 && j == nbins-1) {
	it->second /= static_cast<double>(nsamples*lastBinSize*lastBinSize);
      } else if (i == nbins-1 || j == nbins-1) {
	it->second /= static_cast<double>(nsamples*lastBinSize*binSize);
      } else {
	it->second /= static_cast<double>(nsamples*binSize*binSize);
      }
    }
  }

  // Store the command, the seed and the number of threads used 
  // for generating the contact map
  if (useHeader) {
    stringstream ss;
    ss << "# ";
    for (int i = 0; i < argc-1; i++) {
      ss << argv[i] << " ";
    }
    ss << argv[argc-1] << "\n";
    ss << "# Number of threads: " << nthreads << "\n";
    ss << "# Seed: " << seed << "\n";
    ss << "# Probability kernel: " << kernelFunc << " " << kernelArgs << "\n";
    cmaps[0].setHeader(ss.str());
    cmaps[0].enableHeader(true);
  }

  // Output the results
  cmaps[0].dumpToGnuplot(outFile);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
