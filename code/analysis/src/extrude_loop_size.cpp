// extrude_loop_size.cpp
// A program which outputs the loop sizes of extruders at sampled time frames

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;

int main(int argc, char* argv[]) {
  if (argc < 6) {
    cout << "Usage: extrude_loop_size startTime endTime timeInc "
	 << "outFile extrudePosFile1 [extrudePosFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);
  
  ifstream reader;
  string extrudePosFile;
  string line, str;
  stringstream ss;
  int nextruders, left, right;
  long time;

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  for (int i = ++argi; i < argc; i++) {
    extrudePosFile = string(argv[i]);
    reader.open(extrudePosFile);

    if (!reader) {
      cout << "Error: cannot open the file " << extrudePosFile << endl;
      return 1;
    }

    while (getline(reader, line)) {
      // Read header lines
      ss.str(line);
      ss >> str >> nextruders;
      ss.clear();
      getline(reader, line);
      ss.str(line);
      ss >> str >> time;
      ss.clear();
      if (time > endTime) {
	break;
      } else if (time >= startTime && (time-startTime) % timeInc == 0) {
	for (int j = 0; j < nextruders; j++) {
	  getline(reader, line);
	  ss.str(line);
	  ss >> left >> right;
	  ss.clear();
	  // No need to change indices to zero-based as we only look at
	  // the relative distance of the two heads of the extruder
	  writer << (right-left) << "\n";
	}
      } else {
	for (int j = 0; j < nextruders; j++) {
	  getline(reader, line);
	}
      }
    }
    reader.close();
  }
  writer.close();
}
