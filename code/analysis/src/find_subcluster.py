# find_subcluster.py

import sys
import numpy as np
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage

args = sys.argv

if (len(args) < 3):
    print("Usage: cluster.py order start_leaf dist_thres descriptor_files")
    sys.exit(1)

order = int(args.pop(1))
start_leaf = int(args.pop(1))
dist_thres = float(args.pop(1))

npoints = 0
for n in range(order+1): # Need to include zeroth order descriptor
    for l in range(n%2,n+1,2):
        npoints += 1

nconfigs = len(args)-1
descriptors = np.zeros((nconfigs,npoints),dtype=float)

# Load the data
data_files_tmp = args[1:]

# Sort the data files by run
data_files = ['' for i in range(len(data_files_tmp))]
for df in data_files_tmp:
    # Pick out the run number
    index = int(df[len(df)-df[::-1].index("_"):len(df)-df[::-1].index(".")-1])
    data_files[index-1] = df

for i,df in enumerate(data_files):
    with open(df,'r') as reader:
        for j,line in enumerate(reader):
            data = line.split()
            descriptors[i,j] = float(data[2])

nsamples = len(data_files)

dist_mode = "euclidean"
link_mode = "average"
link_matrix = linkage(descriptors,method=link_mode,metric=dist_mode)

def find_leaves(tree, pos, nsamples, leaf_list):
    pos = int(pos)
    child0 = int(tree[pos,0])
    child1 = int(tree[pos,1])
    print(pos,nsamples,tree[pos])
    if (child0 >= nsamples):
        print(child0-nsamples)
        find_leaves(tree, child0-nsamples, nsamples, leaf_list)
    else:
        leaf_list.append(child0)
    if (child1 >= nsamples):
        print(child1-nsamples)
        find_leaves(tree, child1-nsamples, nsamples, leaf_list)
    else:
        leaf_list.append(child1)

#cluster_list = [start_leaf]
cluster_list = []

# First, find the specified element
pos = None
"""
for i,node in enumerate(link_matrix):
    child0 = int(node[0])
    child1 = int(node[1])
    if (child0 == start_leaf and node[2] < dist_thres):
        print(i,node)
        pos = i
        if (child1 >= nsamples):
            find_leaves(link_matrix, child1-nsamples, cluster_list)
        else:
            cluster_list.append(child1)
    elif (child1 == start_leaf and node[2] < dist_thres):
        print(i,node)
        pos = i
        if (child0 >= nsamples):
            find_leaves(link_matrix, child0-nsamples, cluster_list)
        else:
            cluster_list.append(child0)
    elif (child0-nsamples == pos and node[2] < dist_thres):
        print(i,node)
        pos = i
        if (child1 >= nsamples):
            find_leaves(link_matrix, child1-nsamples, cluster_list)
        else:
            cluster_list.append(child1)
    elif (child1-nsamples == pos and node[2] < dist_thres):
        print(i,node)
        pos = i
        if (child0 >= nsamples):
            find_leaves(link_matrix, child0-nsamples, cluster_list)
        else:
            cluster_list.append(child0)
"""
#for i,node in enumerate(link_matrix):
#    print(i,node)

#print()

for i,node in enumerate(link_matrix):
    child0 = int(node[0])
    child1 = int(node[1])
    if (child0 == start_leaf or child1 == start_leaf):
        if (node[2] < dist_thres):
            #print(i,node)
            pos = i
        else:
            break
    elif (child0-nsamples == pos or child1-nsamples == pos):
        if (node[2] < dist_thres):
            #print(i,node)
            pos = i
        else:
            break

# Find cluster leaves
if (pos != None):
    find_leaves(link_matrix, pos, nsamples, cluster_list)

# Output the resulting list
list.sort(cluster_list)
print(cluster_list)
print(len(cluster_list))
