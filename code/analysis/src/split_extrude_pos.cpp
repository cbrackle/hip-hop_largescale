// split_extrude_pos.cpp
// A program to split the file containing the extruders' positions

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <set>
#include <utility>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::set;
using std::pair;

int main(int argc, char* argv[]) {

  if (argc != 5) {
    cout << "Usage: split_extrude_pos repeatStartIndex extrudePosFile "
	 << "indexFile outFileRoot" << endl;
    return 1;
  }
  
  int argi = 0;
  int repeatStartIndex = stoi(string(argv[++argi]), nullptr, 10);
  string extrudePosFile (argv[++argi]);
  string indexFile (argv[++argi]);
  string outFileRoot (argv[++argi]);
  
  ifstream reader;
  reader.open(indexFile);
  if (!reader) {
    cout << "Error: cannot open the file " << indexFile << endl;
  }

  vector<int> startIndex, endIndex;
  string line;
  stringstream ss;
  int index, start, end;
  while (getline(reader, line)) {
    ss.clear();
    ss.str(line);
    ss >> index >> start >> end;
    startIndex.push_back(start);
    endIndex.push_back(end);
  }
  reader.close();
  
  int nrepeats = static_cast<int>(startIndex.size());
  
  vector<ofstream> writers (nrepeats);
  string outFile;
  for (int i = 0; i < nrepeats; i++) {
    ss.clear();
    ss.str("");
    ss << outFileRoot << "_run_" << (repeatStartIndex+i) << ".dat";
    outFile = ss.str();
    writers[i].open(outFile);
    if (!writers[i]){
      cout << "Error: cannot open the file " << outFile << endl;
      return 1;
    }
  }
  
  string str;
  long time;
  int nextruders;
  int left, right;
  vector<set<pair<int,int> > > extruders (nrepeats,set<pair<int,int> >());
  reader.open(extrudePosFile);
  if (!reader) {
    cout << "Error: cannot open the file " << extrudePosFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    // Read header lines
    ss.str(line);
    ss >> str >> nextruders;
    ss.clear();
    getline(reader,line);
    ss.str(line);
    ss >> str >> time;
    ss.clear();
    for (int i = 0; i < nextruders; i++) {
      getline(reader,line);
      ss.str(line);
      ss >> left >> right;
      ss.clear();
      // Determine which replica the extruder is located within
      // Assume that left and right are always within the same replica
      index = -1; // Meaning that the extruder is lcoated in the gap region
      for (int j = 0; j < nrepeats; j++) {
	if (left >= startIndex[j] && left <= endIndex[j]) {
	  index = j;
	  break;
	}
      }
      if (index != -1) {
	left = left-startIndex[index]+1; // One-based indexing
	right = right-startIndex[index]+1; // One-based indexing
	extruders[index].insert(std::make_pair(left,right));
      }
    }
    for (int i = 0; i < nrepeats; i++) {
      // Write header lines
      writers[i] << "Extruders: " << extruders[i].size() << "\n"
		<< "Timestep: " << time << "\n";
      for (std::set<pair<int,int> >::iterator it = extruders[i].begin();
	   it != extruders[i].end(); it++) {
	writers[i] << it->first << " " << it->second << "\n";
      }
      extruders[i].clear();
    }
  }
  reader.close();
  for (int i = 0; i < nrepeats; i++) {
    writers[i].close();
  }
}
