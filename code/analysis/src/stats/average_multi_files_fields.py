# average_multi_files_fields.py
import sys
from itertools import izip
import math

args = sys.argv

if (len(args) < 6):
    print "Usage: average_multi_files.py ncols ref_col startpt " \
        "output_file data_files"
    sys.exit(1)

args.pop(0) # Ignore self
ncols = int(args.pop(0))
ref_col = int(args.pop(0))
startpt = int(args.pop(0))
output_file = args.pop(0)

if (ref_col != 0 and ref_col != -1):
    print "Error: there can be no reference column (ref_col = -1) or " \
        "it has has to be first column (ref_col = 0)"
    sys.exit(1)

files = [open(i, "r") for i in args]
writer = open(output_file, "w")

for rows in izip(*files):
    ref = 0.0
    avg = [0.0 for i in xrange(ncols)]
    avgSq = [0.0 for i in xrange(ncols)]
    dataOK = False

    for j,line in enumerate(rows):
        if (not line.startswith("#")):
            data = line.split()
            if (data == []): #ignore any lines start with \n
                break
                #print j,data
            # Convert all columns to numerical values
            for i in xrange(ncols):
                data[i] = float(data[i])
            
            if (ref_col != -1):
                ref = data[ref_col]

            if (ref_col == -1 or ref >= startpt):
                for i in xrange(ncols):
                    value = data[i]
                    avg[i] += value
                    avgSq[i] += value*value
                dataOK = True

    if (dataOK):
        n = float(len(rows))
        for i in xrange(ncols):
            if (i != ref_col):
                avg[i] /= n
                avgSq[i] /= n

                # Use un-biased estimate of variance
                if (n > 1):
                    var = n / (n-1) * (avgSq[i] - avg[i]*avg[i])
                    if (var < 0.0):
                        print "Negative variance: var = %.5f" % var
                    sigma = math.sqrt(abs(var)) 
                else:
                    var = 0.0
                    sigma = 0.0
        
                error = sigma / math.sqrt(n)
                output = "%.5e %.5e %.5e " % (avg[i], sigma, error)
                writer.write(output)
            else:
                output = "%.5e " % (ref);
                writer.write(output)
        writer.write("\n")

for f in files:
    f.close()
writer.close()
    
        
    
