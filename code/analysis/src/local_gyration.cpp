// local_gyration.cpp
// A program which computes the local radius of gyration.
// The program assumes that the first Np beads are the polymer beads in the
// position file.

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

double distSq(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 8) {
    cout << "Usage: local_gyration nbeads length startTime endTime timeInc "
	 << "posFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  int length = stoi(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);
  
  BeadStream reader;
  BeadList blist;
  reader.open(posFile, blist);
  if (!reader.isOpen()) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }

  int nthreads = omp_get_max_threads();
  double** gyration = create2DArray<double>(nthreads, nbeads);
  double*** pos = create3DArray<double>(nthreads,length,3);
  
  int nframes = 0;

  for (long time = startTime; time <= endTime; time += timeInc) {
    if (reader.moveTo(time)) {
      cout << "Doing time " << time << endl;
#pragma omp parallel default(none),\
  shared(blist, gyration, length, pos, nbeads)
      {
	int id = omp_get_thread_num();
#pragma omp for schedule(static)
	for (int i = 0; i < nbeads; i++) {
	  int start;
	  if (i < length/2) {
	    start = 0;
	  } else if (i-length/2+length>=nbeads) {
	    start = nbeads-length;
	  } else {
	    start = i-length/2;
	  }
	  // Compute the local centre of mass
	  double cm[3] = {0.0, 0.0, 0.0};
	  for (int j = 0; j < length; j++) {
	    pos[id][j][0] = blist.getUnwrappedPosition(start+j,0);
	    pos[id][j][1] = blist.getUnwrappedPosition(start+j,1);
	    pos[id][j][2] = blist.getUnwrappedPosition(start+j,2);
	    cm[0] += pos[id][j][0];
	    cm[1] += pos[id][j][1];
	    cm[2] += pos[id][j][2];
	  }
	  cm[0] /= static_cast<double>(length);
	  cm[1] /= static_cast<double>(length);
	  cm[2] /= static_cast<double>(length);
	  
	  // Compute the radius of gyration
	  for (int j = 0; j < length; j++) {
	    gyration[id][i] += (distSq(pos[id][j],cm) / 
				static_cast<double>(length));
	  }
	}
      }
    }
    nframes++;
  }
  reader.close();

  // Merge the results from different threads
  for (int i = 1; i < nthreads; i++) {
    for (int j = 0; j < nbeads; j++) {
      gyration[0][j] += gyration[i][j];
    }
  }

  // Normalise
  for (int i = 0; i < nbeads; i++) {
    gyration[0][i] /= static_cast<double>(nframes);
  }
  
  // Output
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nbeads; i++) {
    writer << i << " " << sqrt(gyration[0][i]) << "\n";
  }
  writer.close();

  // Clean up
  deleteArray(pos);
  deleteArray(gyration);
}

double distSq(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sum;
}
