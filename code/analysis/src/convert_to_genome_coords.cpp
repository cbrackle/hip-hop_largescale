// convert_to_genome_coords.cpp
// Convert index column(s) to genome coordinates

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

long binToBp(long startBp, long bpPerBin, long bin);

int main(int argc, char* argv[]) {
  if (argc < 8) {
    cout << "Usage: convert_to_genome_coords startBp endBp bpPerBin " 
	 << "inFile outFile ncols col1 [cols ...]" << endl;
    return 1;
  }

  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  string inFile (argv[++argi]);
  string outFile (argv[++argi]);
  // Number of columns/fields in the file
  int ncols = stoi(string(argv[++argi]), nullptr, 10);
  // Get the columns to be converted
  vector<int> cols;
  for (int i = ++argi; i < argc; i++) {
    cols.push_back(stoi(string(argv[i]), nullptr, 10));
  }

  ifstream reader;
  reader.open(inFile);
  if (!reader) {
    cout << "Error: cannot open the file " << inFile << endl;
    return 1;
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open  the file " << outFile << endl;
    return 1;
  }

  int nbins = static_cast<int>(ceil(static_cast<double>(endBp-startBp)/
				    static_cast<double>(bpPerBin)));

  stringstream ss;
  string line, str;
  int bin;
  while (getline(reader, line)) {
    ss.clear();
    ss.str(line);
    int nextColIndex = 0;
    int nextCol = cols[nextColIndex];
    for (int i = 0; i < ncols; i++) {
      ss >> str;
      if (i == nextCol) {
	bin = stoi(str, nullptr, 10);
	if (bin < 0 || bin >= nbins) {
	  cout << "Warning: bin index out of specified range. "
	       << "Total number of bins = " << nbins << ". Got bin " << bin
	       << endl;
	}
	writer << binToBp(startBp, bpPerBin, bin);
	nextColIndex++;
	if (nextColIndex < static_cast<int>(cols.size())) {
	  nextCol = cols[nextColIndex];
	} else {
	  nextCol = -1;
	}
      } else {
	writer << str;
      }
      if (i < ncols-1) {
	writer << " ";
      } else {
	writer << "\n";
      }
    }
  }
  reader.close();
  writer.close();
}

inline long binToBp(long startBp, long bpPerBin, long bin) {
  return startBp+(bin+0.5)*bpPerBin;
}
