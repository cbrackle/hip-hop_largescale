/* split_lammpstrj.cpp
 * A simple code that splits a lammpstrj file into several lammpstrj files
 * corresponding to each replicate within the polymer chain
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::vector;
using std::string;

int main(int argc, char* argv[]){
  // Read input arguments
  if (argc != 5){
    cout << "Usage: split_lammpstrj repeatStartIndex "
	 << "posFile indexFile outFileRoot" << endl;
    return 1;
  }
  
  int argi = 0;
  int repeatStartIndex = stoi(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string indexFile (argv[++argi]);
  string outFileRoot (argv[++argi]);
  
  ifstream reader;
  reader.open(indexFile);
  if (!reader) {
    cout << "Error: cannot open the file " << indexFile << endl;
  }

  vector<int> startIndex, endIndex;
  string line;
  stringstream ss;
  int index, start, end;
  while (getline(reader, line)) {
    ss.clear();
    ss.str(line);
    ss >> index >> start >> end;
    startIndex.push_back(start);
    endIndex.push_back(end);
  }
  reader.close();
  int nrepeats = static_cast<int>(startIndex.size());

  BeadStream bstream;
  BeadList blist;

  bstream.open(posFile, blist);

  if (!bstream.isOpen()){
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }

  vector<ofstream> writers (nrepeats);
  string outFile;
  for (int i = 0; i < nrepeats; i++) {
    ss.clear();
    ss.str("");
    ss << outFileRoot << "_run_" << (repeatStartIndex+i) << ".lammpstrj";
    outFile = ss.str();
    writers[i].open(outFile);
    if (!writers[i]){
      cout << "Error: cannot open the file " << outFile << endl;
      return 1;
    }
  }

  double boxSize[3], boxLo[3], boxHi[3];
  for (int i = 0; i < 3; i++) {
    boxSize[i] = bstream.getBoxSize(i);
    boxLo[i] = bstream.getBoxLo(i);
    boxHi[i] = bstream.getBoxHi(i);
  }

  while (bstream.nextFrame()){
    for (int i = 0; i < nrepeats; i++) {
      start = startIndex[i];
      end = endIndex[i];
      // Writer header (only works for periodic boundaries)
      writers[i] << "ITEM: TIMESTEP\n";
      writers[i] << bstream.getTime() << "\n";
      writers[i] << "ITEM: NUMBER OF ATOMS\n";
      writers[i] << end-start+1 << "\n";
      writers[i] << "ITEM: BOX BOUNDS pp pp pp\n";
      writers[i] << std::setprecision(16) << std::scientific;
      writers[i] << boxLo[0] << " " << boxHi[0] << "\n";
      writers[i] << boxLo[1] << " " << boxHi[1] << "\n";
      writers[i] << boxLo[2] << " " << boxHi[2] << "\n";
      writers[i].unsetf(std::ios_base::floatfield);
      writers[i] << "ITEM: ATOMS id type xs ys zs ix iy iz\n";
      writers[i] << std::setprecision(6);
      for (int j = start-1; j < end; j++) {
	writers[i] << j-start+2 << " " 
		   << blist.getType(j) << " "
		   << (blist.getPosition(j,0)-boxLo[0])/boxSize[0] << " "
		   << (blist.getPosition(j,1)-boxLo[1])/boxSize[1] << " "
		   << (blist.getPosition(j,2)-boxLo[2])/boxSize[2] << " "
		   << blist.getBoundCount(j,0) << " "
		   << blist.getBoundCount(j,1) << " "
		   << blist.getBoundCount(j,2) << "\n";
      }
      writers[i].unsetf(std::ios_base::floatfield);
    }
  }
  bstream.close();
  for (int i = 0; i < nrepeats; i++) {
    writers[i].close();
  }
}
