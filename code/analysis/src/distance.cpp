// distance.cpp
// A program which computes the end-to-end of a polymer chain
// as a function of its contour length

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <omp.h>
#include "array.hpp"
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;

double distanceSq(double* r1, double* r2);

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: distance nbeads startTime endTime timeInc "
	 << "posfile outfile" << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  int startTime = stoi(string(argv[++argi]), nullptr, 10);
  int endTime = stoi(string(argv[++argi]), nullptr, 10);
  int timeInc = stoi(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  int nbins = ((endTime-startTime)/timeInc)+1;
  double*** pos = create3DDoubleArray(nbins, nbeads, 3);
  
  // Read position data
  BeadStream reader;
  BeadList beadData;
  long time;
  int ibin, sep;
  reader.open(posFile, beadData);
  if (!reader.isOpen()) {
    cout << "Error: cannot open position file " << posFile << endl;
    return 1;
  }
  while (reader.nextFrame()) {
    time = reader.getTime();
    if (time < startTime) {
      continue;
    } else if (time > endTime) {
      break;
    } else if (time >= startTime && time <= endTime) {
      ibin = (time-startTime)/timeInc;
      for (int i = 0; i < nbeads; i++) {
	for (int j = 0; j < 3; j++) {
	  pos[ibin][i][j] = beadData.getUnwrappedPosition(i,j);
	}
      }
    }
  }
  reader.close();
  
  // Compute end-to-end distances
  int i, j;
  int* count = nullptr;
  int* totalCount = create1DIntArray(nbeads);
  double* endToEndDist = nullptr;
  double* endToEndDistAvg = create1DDoubleArray(nbeads);
  
#pragma omp parallel default(none) \
  shared(nbins, nbeads, pos, totalCount, endToEndDistAvg) \
  private(ibin, i, j, sep, count, endToEndDist)
  {
    // Initialise arrays
    count = create1DIntArray(nbeads);
    endToEndDist = create1DDoubleArray(nbeads);
    
#pragma omp for schedule(static)
    for (ibin = 0; ibin < nbins; ibin++) {
      for (i = 0; i < nbeads; i++) {
	for (j = 0; j <= i; j++) {
	  sep = abs(i-j);
	  endToEndDist[sep] += distanceSq(pos[ibin][i], pos[ibin][j]);
	  count[sep]++;
	}
      }
    }
    
    for (i = 0; i < nbeads; i++) {
#pragma omp atomic
      endToEndDistAvg[i] += endToEndDist[i];
#pragma omp atomic
      totalCount[i] += count[i];
    }

    // Free resources
    deleteArray(count);
    deleteArray(endToEndDist);
  }
  
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open output file " << outFile << endl;
    return 1;
  }

  writer << std::setprecision(5) << std::fixed;
  for (i = 0; i < nbeads-1; i++) {
    endToEndDistAvg[i] /= static_cast<double>(totalCount[i]);
    writer << i << " " << endToEndDistAvg[i] << "\n";
  }
  writer.close();

  // Clean up
  deleteArray(totalCount);
  deleteArray(endToEndDistAvg);
}

double distanceSq(double* r1, double* r2) {
  double distSq = 0.0; 
  double diff;
  for (int i = 0; i < 3; i++) {
    diff = r1[i]-r2[i];
    distSq += diff*diff;
  }
  return distSq;
}
