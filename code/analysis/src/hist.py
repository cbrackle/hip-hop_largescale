import numpy as np
from matplotlib import pyplot as plt
import sys

args = sys.argv
infile = args[1]
data = []
with open(infile,'r') as reader:
    for line in reader:
        data.append(float(line))
bins = np.arange(0.0,1.0,0.01)
plt.hist(data,bins=bins,alpha=0.5)
plt.show()
