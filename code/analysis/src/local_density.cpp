// local_density.cpp
// A program which computes the number of (polymer beads) within a spherical
// volume around each bead.
// The program assumes that the first Np beads are the polymer beads in the
// position file.

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 8) {
    cout << "Usage: local_density nbeads radius startTime endTime timeInc "
	 << "posFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  double radius = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);
  
  BeadStream reader;
  BeadList blist;
  reader.open(posFile, blist);
  if (!reader.isOpen()) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }

  int nthreads = omp_get_max_threads();
  vector<double> density[nthreads];
  for (int i = 0; i < nthreads; i++) {
    density[i] = vector<double>(nbeads, 0.0);
  }
  int nframes = 0;

  for (long time = startTime; time <= endTime; time += timeInc) {
    if (reader.moveTo(time)) {
      cout << "Doing time " << time << endl;
#pragma omp parallel default(none),\
  shared(blist, density, radius, nbeads)
      {
	int id = omp_get_thread_num();
	double xi[3], xj[3];
#pragma omp for schedule(dynamic,16)
	for (int i = 0; i < nbeads; i++) {
	  xi[0] = blist.getUnwrappedPosition(i,0);
	  xi[1] = blist.getUnwrappedPosition(i,1);
	  xi[2] = blist.getUnwrappedPosition(i,2);
	  for (int j = 0; j < i; j++) { // Ignore self in density count
	    xj[0] = blist.getUnwrappedPosition(j,0);
	    xj[1] = blist.getUnwrappedPosition(j,1);
	    xj[2] = blist.getUnwrappedPosition(j,2);
	    if (dist(xi,xj) <= radius) {
	      density[id][i] += 1.0;
	      density[id][j] += 1.0;
	    }
	  }
	}
      }
    }
    nframes++;
  }
  reader.close();

  // Merge the results from different threads
  for (int i = 1; i < nthreads; i++) {
    for (int j = 0; j < nbeads; j++) {
      density[0][j] += density[i][j];
    }
  }

  // Normalise
  for (int i = 0; i < nbeads; i++) {
    density[0][i] /= static_cast<double>(nframes);
  }

  double volume = 4.0/3.0*M_PI*radius*radius*radius; 
  
  // Output
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nbeads; i++) {
    writer << i << " " << density[0][i]/volume << "\n";
  }
  writer.close();
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
