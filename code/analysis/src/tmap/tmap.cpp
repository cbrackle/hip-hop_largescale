// tmap.cpp
// A program which correlates the transcription activty of pairs of beads
// (typically ATAC beads) by the simple matching coefficient. A bead is said
// to be transcribed if it is less than a cutoff distance away from a
// protein/TF bead. The coefficient measures the number of times that both
// beads have the same activity (i.e., both being ON or OFF) out of all the
// sampled time frames. 

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <omp.h>
#include <map>
#include <utility>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::map;
using std::pair;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc < 10) {
    cout << "Usage: tmap thres startTime endTime timeInc "
	 << "beadFile proteinFile probFile coeffFile posFile1 [posFiles ...]" 
	 << endl;
    return 1;
  }

  int argi = 0;
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string proteinFile (argv[++argi]);
  string probFile (argv[++argi]);
  string coeffFile (argv[++argi]);

  ifstream reader;
  stringstream ss;
  string line;
  int index;

  // Read the list of chromatin beads
  vector<int> beads;
  reader.open(beadFile);
  if (!reader) {
    cout << "Error: cannot open the file " << beadFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    beads.push_back(index-1); // Change to zero-based
  }
  reader.close();

  // Read the list of chromatin beads
  vector<int> proteins;
  reader.open(proteinFile);
  if (!reader) {
    cout << "Error: cannot open the file " << proteinFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    proteins.push_back(index-1); // Change to zero-based
  }
  reader.close();

  int nbeads = static_cast<int>(beads.size());
  int nproteins = static_cast<int>(proteins.size());

#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif

  int nbins = (endTime-startTime)/timeInc+1;
  int*** state = create3DArray<int>(nthreads, nbins, nbeads);
  vector<map<pair<int,int>,double> > coeffs(nthreads);
  
  double transProb = 0.0; // Overall transcription probability
  
  int nfiles = argc-(++argi);

  vector<BeadStream> bstreams (nthreads);
  vector<BeadList> blists (nthreads);
  int error = 0;

#pragma omp parallel default(none),\
  shared(nfiles, argv, argi, bstreams, blists, startTime, endTime, timeInc),\
  shared(beads, proteins, state, coeffs, thres, cout),\
  shared(nbeads, nproteins, nbins),\
  reduction(+:error), reduction(+:transProb)
  {

#ifdef _OPENMP
    int id = omp_get_thread_num();
#else
    int id = 0;
#endif

#pragma omp for schedule(static)
    for (int n = 0; n < nfiles; n++) {
      string posFile = string(argv[n+argi]);
      cout << "Working on file " << posFile << endl;
      if (!bstreams[id].open(posFile, blists[id])) {
	cout << "Error: cannot open the file " << posFile << endl;
	error++;
	continue;
      }
      for (long time = startTime; time <= endTime; time += timeInc) {
	if (bstreams[id].moveTo(time)) {
	  int ibin = (time-startTime)/timeInc;
	  // Compute transcription activity
	  double xi[3], xj[3];
	  for (int i = 0; i < nbeads; i++) {
	    xi[0] = blists[id].getPosition(beads[i],0);
	    xi[1] = blists[id].getPosition(beads[i],1);
	    xi[2] = blists[id].getPosition(beads[i],2);
	    state[id][ibin][i] = 0;
	    for (int j = 0; j < nproteins; j++) {
	      xj[0] = blists[id].getPosition(proteins[j],0);
	      xj[1] = blists[id].getPosition(proteins[j],1);
	      xj[2] = blists[id].getPosition(proteins[j],2);
	      if (dist(xi,xj) < thres) {
		state[id][ibin][i] = 1;
		transProb += 1.0;
		break;
	      }
	    }
	  }
	}
      }
      // Update coefficient matrix
      int matches = 0;
      for (int i = 0; i < nbeads; i++) {
	for (int j = 0; j <= i; j++) {
	  matches = 0;
	  for (int ibin = 0; ibin < nbins; ibin++) {
	    if (state[id][ibin][i] == state[id][ibin][j]) {
	      matches++;
	    }
	  }
	  coeffs[id][std::make_pair(i,j)] += matches;
	}
      }
      bstreams[id].close();
    }
  } // Close parallel region

  // Combine results
  pair<int,int> coords;
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j <= i; j++) {
      coords = std::make_pair(i,j);
      for (int k = 1; k < nthreads; k++) {
	coeffs[0][coords] += coeffs[k][coords];
      }
    }
  }
  
  // Overall transcription probability
  transProb /= (static_cast<double>(nbins*nfiles)*static_cast<double>(nbeads));

  ofstream writer;
  writer.open(probFile);
  if (!writer) {
    cout << "Error: cannot open the file " << probFile << endl;
    return 1;
  }
  writer << transProb << "\n";
  writer.close();

  writer.open(coeffFile);
  if (!writer) {
    cout << "Error: cannot open the file " << coeffFile << endl;
    return 1;
  }
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j <= i; j++) {
      double val = coeffs[0][std::make_pair(i,j)];
      writer << beads[i] << " " << beads[j] << " "
	     << static_cast<int>(val) << " " 
	     << val/static_cast<double>(nbins*nfiles) << "\n";
    }
  }
  writer.close();

  // Clean up
  deleteArray(state);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
