// transcription.cpp
// A program which computes the transcription activity of a given
// set of beads (typically ATAC beads)
// A bead is to be trascribed if it is less than a cutoff distance
// away from a protein/TF bead
// A program which computes the Pearson correlation for the transcription
// activity between different (ATAC) beads

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <omp.h>
#include <map>
#include <utility>
#include <boost/math/special_functions/beta.hpp>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::map;
using std::pair;

void pcorr(int n, double* x, double* y, double* corr, double* pvalue);
double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc < 9) {
    cout << "Usage: transcription thres startTime endTime timeInc "
	 << "beadFile proteinFile outFile posFile1 [posFiles ...]"
	 << endl;
    return 1;
  }
  
  int argi = 0;
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string proteinFile (argv[++argi]);
  string outFile (argv[++argi]);

  ifstream reader;
  stringstream ss;
  string line;
  int index;
  
  // Read the list of chromatin beads
  vector<int> beads;
  reader.open(beadFile);
  if (!reader) {
    cout << "Error: cannot open the file " << beadFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    beads.push_back(index-1); // Change to zero-based
  }
  reader.close();

  // Read the list of protein beads
  vector<int> proteins;
  reader.open(proteinFile);
  if (!reader) {
    cout << "Error: cannot open the file " << proteinFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    proteins.push_back(index-1); // Change to zero-based
  }
  reader.close();

  int nbeads = static_cast<int>(beads.size());
  int nproteins = static_cast<int>(proteins.size());

  // Read the position file

#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
  int nbins = (endTime-startTime)/timeInc+1;
  int** state = create2DArray<int>(nthreads,nbeads);
  double*** avgState = create3DArray<double>(nthreads,nbeads,nbins);
  
  vector<BeadStream> bstreams (nthreads);
  vector<BeadList> blists (nthreads);
  int nfiles = argc-(++argi);
  int error = 0;

#pragma omp parallel default(none),\
  shared(nbeads, nproteins, bstreams, blists, state, avgState),\
  shared(startTime, endTime, timeInc, argi, argv, nfiles, thres, cout),\
  shared(beads, proteins),\
  reduction(+:error)
  {

#ifdef _OPENMP
    int id = omp_get_thread_num();
#else
    int id = 0;
#endif

#pragma omp for schedule(static)
    for (int n = 0; n < nfiles; n++) {
      string posFile = string(argv[n+argi]);
      cout << "Working on file " << posFile << endl;
      if (!bstreams[id].open(posFile, blists[id])) {
	cout << "Error: cannot open the file " << posFile << endl;
	error++;
      }
      for (long time = startTime; time <= endTime; time += timeInc) {
	if (bstreams[id].moveTo(time)) {
	  int ibin = (time-startTime)/timeInc;
	  // Compute transcription activity
	  double xi[3], xj[3];
	  for (int i = 0; i < nbeads; i++) {
	    state[id][i] = 0;
	    xi[0] = blists[id].getPosition(beads[i],0);
	    xi[1] = blists[id].getPosition(beads[i],1);
	    xi[2] = blists[id].getPosition(beads[i],2);
	    for (int j = 0; j < nproteins; j++) {
	      xj[0] = blists[id].getPosition(proteins[j],0);
	      xj[1] = blists[id].getPosition(proteins[j],1);
	      xj[2] = blists[id].getPosition(proteins[j],2);
	      if (dist(xi,xj) < thres) {
		state[id][i] = 1;
		break;
	      }
	    }
	    avgState[id][i][ibin] += state[id][i];
	  }
	}
      }
      bstreams[id].close();
    }
  } // Close parallel region

  if (error > 0) {
    cout << "There are errors when reading position files - stop proceeding"
	 << endl;
    deleteArray(state);
    deleteArray(avgState);
    return 1;
  }
      
  // Combine results
  for (int i = 1; i < nthreads; i++) {
    for (int j = 0; j < nbeads; j++) {
      for (int k = 0; k < nbins; k++) {
	avgState[0][j][k] += avgState[i][j][k];
      }
    }
  }
  
  // Compute the correlation matrix
  cout << "Computing correlations ..." << endl;
  double corr, pvalue;
  map<pair<int,int>,pair<double,double> > corrMat;
  for (int i = 0; i < nbeads; i++) {
    for (int j = i; j < nbeads; j++) {
      pcorr(nbins, avgState[0][i], avgState[0][j], &corr, &pvalue);
      corrMat[std::make_pair(i,j)] = std::make_pair(corr,pvalue);
    }
  }

  // Output the correlation matrix
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  pair<double,double> corrpair;
  for (int i = 0; i < nbeads; i++) {
    for (int j = 0; j < nbeads; j++) {
      corrpair = corrMat[i <= j ? std::make_pair(i,j) : std::make_pair(j,i)];
      writer << i << " " << j << " " << beads[i] << " " << beads[j] << " "
	     << corrpair.first << " " << corrpair.second << "\n";
    }
    writer << "\n";
  }
  writer.close();

  // Clean up
  deleteArray(state);
  deleteArray(avgState);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}

void pcorr(int n, double* x, double* y, double* corr, double* pvalue) {
  double xsum = 0.0;
  double ysum = 0.0;
  double xsqsum = 0.0;
  double ysqsum = 0.0;
  double xysum = 0.0;
  for (int i = 0; i < n; i++) {
    xsum += x[i];
    ysum += y[i];
    xsqsum += x[i]*x[i];
    ysqsum += y[i]*y[i];
    xysum += x[i]*y[i];
  }
  
  double rho = (n*xysum-xsum*ysum) /
    (sqrt(n*xsqsum-xsum*xsum)*sqrt(n*ysqsum-ysum*ysum));
  
  if (rho > 1.0) rho = 1.0;
  if (rho < -1.0) rho = 1.0;

  double p;
  double df = n-2;
  double tsq;
  if (fabs(rho) == 1.0) {
    p = 0.0;
  } else {
    tsq = rho*rho*(df/((1.0-rho)*(1.0+rho)));
    p = boost::math::ibeta(0.5*df,0.5,df/(df+tsq));
  }
  *corr = rho;
  *pvalue = p;
}
