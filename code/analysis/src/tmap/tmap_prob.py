# tmap_prob.py
# A program which computes the p-value of the transcription matching
# coefficient matrix

import sys
import numpy as np
import scipy.stats as stats

args = sys.argv

if (len(args) != 5):
    print "Usage: transcription_coeff_pvalue.py ntrials trans_prob", \
        "coeff_file out_file"
    sys.exit(1)

ntrials = int(args.pop(1))
trans_prob = float(args.pop(1))
coeff_file = args.pop(1)
out_file = args.pop(1)

z = trans_prob**2 + (1.0-trans_prob)**2
print ntrials, trans_prob, z

# Read the coefficient matrix and compute p-values
with open(coeff_file, 'r') as reader:
    with open(out_file, 'w') as writer:
        for line in reader:
            if (len(line) == 0 or line[0] == '#'): continue
            data = line.split()
            if (len(data) != 4): continue
            beadi = int(data[0])
            beadj = int(data[1])
            count = int(data[2])
            coeff = float(data[3])
            p_above = stats.binom.sf(count-1,ntrials,z)
            p_below = stats.binom.cdf(count,ntrials,z)
            writer.write("{:d} {:d} {:d} {:f} {:e} {:e}\n".format(
                beadi, beadj, count, coeff, p_below, p_above))
        
        
