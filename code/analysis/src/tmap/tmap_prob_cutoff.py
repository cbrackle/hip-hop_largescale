# tmap_prob_cutoff.py
# Find the nearest coefficient for a particular p-value cutoff

import sys
import scipy.stats as stats

args = sys.argv

if (len(args) != 4):
    print "Usage: tmap_prob_cutoff.py ntrials trans_prob cutoff"
    sys.exit(1)

ntrials = int(args.pop(1))
trans_prob = float(args.pop(1))
cutoff = float(args.pop(1))

z = trans_prob**2 + (1.0-trans_prob)**2

above = stats.binom.isf(cutoff, ntrials, z)+1
below = stats.binom.ppf(cutoff,ntrials, z)

print above, above/float(ntrials), below, below/float(ntrials)
