// transcription_prob.cpp
// A program which measures the probability of transcription of each bead
// (i.e., the fraction of time that a bead is within a cutoff distance
// from a TF bead)

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc < 9) {
    cout << "Usage: transcription_prob nbeads thres startTime endTime timeInc "
	 << "proteinFile outFile posFile1 [posFile ...]"
	 << endl;
    return 1;
  }
  
  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string proteinFile (argv[++argi]);
  string outFile (argv[++argi]);

  ifstream reader;
  stringstream ss;
  string line;
  int index;

  // Read the list of protein beads
  vector<int> proteins;
  reader.open(proteinFile);
  if (!reader) {
    cout << "Error: cannot open the file " << proteinFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    proteins.push_back(index-1); // Change to zero-based
  }
  reader.close();
  
  int nproteins = static_cast<int>(proteins.size());
  
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif

  // Read the position files
  vector<BeadStream> bstreams (nthreads);
  vector<BeadList> blists (nthreads);

  double** transProb = create2DArray<double>(nthreads, nbeads);
  double*** protPos = create3DArray<double>(nthreads, nproteins, 3);

  int nbins = (endTime-startTime)/timeInc+1;
  int nfiles = argc-(++argi);
  int error = 0;
  cout << nbins << endl;
#pragma omp parallel default(none),\
  shared(bstreams, blists, startTime, endTime, timeInc, thres, argv, argi),\
  shared(transProb, protPos, proteins, nproteins, nbeads, nfiles, cout),\
  reduction(+:error)
  {

#ifdef _OPENMP
    int id = omp_get_thread_num();
#else
    int id = 0;
#endif
    
#pragma omp for schedule(static)
    for (int n = 0; n < nfiles; n++) {
      string posFile = string(argv[n+argi]);
      cout << "Working on file " << posFile << endl;
      if (!bstreams[id].open(posFile, blists[id])) {
	cout << "Error: cannot open the file " << posFile << endl;
	error++;
	continue;
      }
      for (long time = startTime; time <= endTime; time += timeInc) {
	if (bstreams[id].moveTo(time)) {
	  // Read proteins' positions
	  // Use wrapped coordinates as proteins can diffuse far away
	  for (int i = 0; i < nproteins; i++) {
	    protPos[id][i][0] = blists[id].getPosition(proteins[i],0);
	    protPos[id][i][1] = blists[id].getPosition(proteins[i],1);
	    protPos[id][i][2] = blists[id].getPosition(proteins[i],2);
	  }

	  double beadPos[3];
	  for (int i = 0; i < nbeads; i++) {
	    beadPos[0] = blists[id].getPosition(i,0);
	    beadPos[1] = blists[id].getPosition(i,1);
	    beadPos[2] = blists[id].getPosition(i,2);
	    for (int j = 0; j < nproteins; j++) {
	      if (dist(beadPos,protPos[id][j]) < thres) {
		transProb[id][i] += 1.0;
		break;
	      }
	    }
	  }
	}
      }
      bstreams[id].close();
    }
  } // Close parallel region

  if (error > 0) {
    cout << "There were errors while reading files - stop proceeding" << endl;
    deleteArray(transProb);
    return 1;
  }

  // Combine results
  for (int i = 1; i < nthreads; i++) {
    for (int j = 0; j < nbeads; j++) {
      transProb[0][j] += transProb[i][j];
    }
  }

  // Normalise results
  for (int i = 0; i < nbeads; i++) {
    transProb[0][i] /= static_cast<double>(nfiles*nbins);
  }

  // Output results
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nbeads; i++) {
    writer << i << " " << transProb[0][i] << "\n";
  }
  writer.close();
  
  // Clean up
  deleteArray(transProb);
  deleteArray(protPos);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
