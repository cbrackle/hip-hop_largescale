// tmap_cluster.cpp
// A program which finds all positive/negative correlated
// subgraphs/subclusters in a given transcription network (tmap)
// The code stores a linked-list of bead indices. The list contains
// multiple (cyclic) sub-linked-lists in which each list comprises of
// the indices of all the beads belonging to the same cluster. The code
// is based on the algorithm described in:
// Stoddard J. Comp. Phys. 27, 291, 1977

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <set>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::pair;
using std::map;
using std::set;

using TMap = map<pair<int,int>,double>;

pair<int,int> toCoords(int beadi, int beadj);

int main(int argc, char* argv[]) {
  if (argc != 5) {
    cout << "Usage: tmap_cluster thres clusterType tmapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  double thres = stod(string(argv[++argi]), nullptr);
  // 0 = negative network, 1 = positive network
  int clusterType = stoi(string(argv[++argi]), nullptr, 10);
  string tmapFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the interaction matrix
  ifstream reader;
  reader.open(tmapFile);
  if (!reader) {
    cout << "Error: cannot open the file " << tmapFile << endl;
    return 1;
  }
  
  string line;
  stringstream ss;
  int beadi, beadj, count;
  double coeff, lowPV, highPV;
  TMap tmap;
  set<int> beadset;
  while (getline(reader, line)) {
    ss.str(line);
    // beadi, beadj are zero-based
    ss >> beadi >> beadj >> count >> coeff >> lowPV >> highPV;
    ss.clear();
    if (clusterType == 0) {
      tmap[toCoords(beadi,beadj)] = lowPV;
    } else {
      tmap[toCoords(beadi,beadj)] = highPV;
    }
    beadset.insert(beadi);
  }
  reader.close();
  
  // Cluster lists
  int nbeads = static_cast<int>(beadset.size());
  vector<int> beads (nbeads,0);
  vector<int> list (nbeads,0);
  vector<int> clusterStartIndex;
  vector<int> clusterSize;

  int i, j, k, temp;
  i = 0;
  for (set<int>::iterator it = beadset.begin(); it != beadset.end(); it++) {
    beads[i] = *it;
    i++;
  }
  for (i = 0; i < nbeads; i++) {
    list[i] = i;
  }
  
  // Find clusters
  int beadsInCluster = 0;
  for (i = 0; i < nbeads; i++) {
    if (i == list[i]) {
      j = i;
      beadsInCluster++;
      clusterStartIndex.push_back(i);
      do {
	for (k = i+1; k < nbeads; k++) {
	  if (k != list[k]) continue;
	  if (tmap[toCoords(beads[j],beads[k])] < thres) {
	    temp = list[j];
	    list[j] = list[k];
	    list[k] = temp;
	    beadsInCluster++;
	  }
	}
	j = list[j];
      } while (j != i);
      clusterSize.push_back(beadsInCluster);
      beadsInCluster = 0;
    }
  }
  
  // Output the clusters
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  int index = 0, nextIndex = -1;
  for (size_t n = 0; n < clusterStartIndex.size(); n++) {
    index = clusterStartIndex[n];
    writer << beads[index] << " ";
    nextIndex = list[index];
    while (nextIndex != clusterStartIndex[n]) {
      index = nextIndex;
      writer << beads[index] << " ";
      nextIndex = list[index];
    }
    writer << "\n";
  }
  writer.close();
}

inline pair<int,int> toCoords(int beadi, int beadj) {
  if (beadi <= beadj) {
    return std::make_pair(beadi, beadj);
  } else {
    return std::make_pair(beadj, beadi);
  }
}
