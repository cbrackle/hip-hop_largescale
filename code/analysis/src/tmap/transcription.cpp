// transcription.cpp
// A program which computes the transcription activity of a given
// set of beads (typically ATAC beads)
// A bead is to be trascribed if it is less than a cutoff distance
// away from a protein/TF bead

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 9) {
    cout << "Usage: transcription thres startTime endTime timeInc "
	 << "beadFile proteinFile posFile outFile"
	 << endl;
    return 1;
  }
  
  int argi = 0;
  double thres = stod(string(argv[++argi]), nullptr);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string proteinFile (argv[++argi]);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  ifstream reader;
  stringstream ss;
  string line;
  int index;
  
  // Read the list of chromatin beads
  vector<int> beads;
  reader.open(beadFile);
  if (!reader) {
    cout << "Error: cannot open the file " << beadFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    beads.push_back(index-1); // Change to zero-based
  }
  reader.close();

  // Read the list of protein beads
  vector<int> proteins;
  reader.open(proteinFile);
  if (!reader) {
    cout << "Error: cannot open the file " << proteinFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    proteins.push_back(index-1); // Change to zero-based
  }
  reader.close();

  int nbeads = static_cast<int>(beads.size());
  int nproteins = static_cast<int>(proteins.size());

  // Read the position file
  BeadStream bstream;
  BeadList blist;
  if (!bstream.open(posFile, blist)) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }

  int* state = create1DArray<int>(nbeads);
  double** beadPos = create2DArray<double>(nbeads,3);
  double** protPos = create2DArray<double>(nproteins,3);
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (long time = startTime; time <= endTime; time += timeInc) {
    if (bstream.moveTo(time)) {
      cout << "Doing t = " << time << endl;
      // Read the bead position
      // Use wrapped position as protein beads diffusive far
      for (int i = 0; i < nbeads; i++) {
	beadPos[i][0] = blist.getPosition(beads[i],0);
	beadPos[i][1] = blist.getPosition(beads[i],1);
	beadPos[i][2] = blist.getPosition(beads[i],2);
      }
      // Read the protein position
      for (int i = 0; i < nproteins; i++) {
	protPos[i][0] = blist.getPosition(proteins[i],0);
	protPos[i][1] = blist.getPosition(proteins[i],1);
	protPos[i][2] = blist.getPosition(proteins[i],2);
      }
      // Compute transcription activity
      for (int i = 0; i < nbeads; i++) {
	state[i] = 0;
	for (int j = 0; j < nproteins; j++) {
	  if (dist(beadPos[i],protPos[j]) < thres) {
	    state[i] = 1;
	    break;
	  }
	}
      }
      // Output
      writer << time << " ";
      for (int i = 0; i < nbeads; i++) {
	writer << state[i] << " ";
      }
      writer << "\n";
    }
  }
  bstream.close();
  writer.close();

  // Clean up
  deleteArray(beadPos);
  deleteArray(protPos);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
