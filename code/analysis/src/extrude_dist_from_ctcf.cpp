// extrude_dist_from_ctcf.cpp
// Compute the distances of extruders from the nearest (convergent) ctcf 
// boundary element

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <utility>
#include <iterator>
#include <set>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::vector;
using std::string;
using std::pair;
using std::set;

int distFromNearestCTCF(int extrudeIndex, set<int>& ctcf);

int main(int argc, char* argv[]) {
  if (argc != 4) {
    cout << "Usage: extrude_dist_from_ctcf ctcfFile extrudeFile outFile"
	 << endl;
    return 1;
  }

  int argi = 0;
  string ctcfFile (argv[++argi]);
  string extrudeFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read CTCFs' positions
  ifstream reader;
  reader.open(ctcfFile);
  if (!reader) {
    cout << "Error: cannot open the file " << ctcfFile << endl;
    return 1;
  }
  set<int> forCTCF;
  set<int> backCTCF;
  string line;
  stringstream ss;
  int index, ctcfVal;
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index >> ctcfVal;
    ss.clear();
    switch (ctcfVal) {
    case 1: forCTCF.insert(index-1); break;
    case -1: backCTCF.insert(index-1); break;
    case 2: forCTCF.insert(index-1); backCTCF.insert(index-1); break;
    }
  }
  reader.close();

  // Read extruders' positions and find the distance to the nearest 
  // convergent CTCF
  string str;
  int nextruders, left, right, dist;
  long time;
  reader.open(extrudeFile);
  if (!reader) {
    cout << "Error: cannot open the file " << extrudeFile << endl;
    return 1;
  }
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    // Read header lines
    ss.str(line);
    ss >> str >> nextruders;
    ss.clear();
    getline(reader, line);
    ss.str(line);
    ss >> str >> time;
    ss.clear();
    for (int i = 0; i < nextruders; i++) {
      getline(reader, line);
      ss.str(line);
      ss >> left >> right;
      ss.clear();
      dist = distFromNearestCTCF(left, forCTCF);
      if (dist >= 0) writer << dist << "\n";
      dist = distFromNearestCTCF(right, backCTCF);
      if (dist >= 0) writer << dist << "\n";
    }
  }
  reader.close();
  writer.close();
}

int distFromNearestCTCF(int extrudeIndex, set<int>& ctcf) {
  // This assumes extruders and CTCF cannot occupy the same location
  set<int>::iterator it = (ctcf.insert(extrudeIndex)).first;
  if (ctcf.size() == 1) return -1;
  int up, down, dup, ddown, dist;
  set<int>::iterator nit, pit;
  nit = std::next(it);
  if (it == ctcf.begin()) {
    up = *nit;
    dup = abs(extrudeIndex-up);
    dist = dup;
  } else {
    pit = std::prev(it);
    down = *pit;
    ddown = abs(extrudeIndex-down);
    if (nit == ctcf.end()) {
      dist = ddown;
    } else {
      up = *nit;
      dup = abs(extrudeIndex-up);
      if (dup > ddown) {
	dist = ddown;
      } else {
	dist = dup;
      }
    }
  }
  ctcf.erase(it);
  return dist;
}
