// split_ctcf_map.cpp
// A program to split the file containing the CTCF sites

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

int main(int argc, char* argv[]) {

  if (argc != 6) {
    cout << "Usage: split_ctcf_map nbeads repeatStartIndex ctcfMapFile "
	 << "indexFile outFileRoot" << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  int repeatStartIndex = stoi(string(argv[++argi]), nullptr, 10);
  string ctcfMapFile (argv[++argi]);
  string indexFile (argv[++argi]);
  string outFileRoot (argv[++argi]);
  
  ifstream reader;
  reader.open(indexFile);
  if (!reader) {
    cout << "Error: cannot open the file " << indexFile << endl;
  }

  vector<int> startIndex, endIndex;
  string line;
  stringstream ss;
  int index, start, end;
  while (getline(reader, line)) {
    ss.clear();
    ss.str(line);
    ss >> index >> start >> end;
    startIndex.push_back(start);
    endIndex.push_back(end);
  }
  reader.close();
  
  int nrepeats = static_cast<int>(startIndex.size());
    
  vector<int> ctcf (nbeads,0);
  int ctcfVal;
  reader.open(ctcfMapFile);
  if (!reader) {
    cout << "Error: cannot open the file " << ctcfMapFile << endl;
    return 1;
  }
  for (int i = 0; i < nbeads; i++) {
    getline(reader, line);
    ss.str(line);
    ss >> index >> ctcfVal;
    ss.clear();
    ctcf[index-1] = ctcfVal;
  }
  reader.close();

  ofstream writer;
  string outFile;
  for (int i = 0; i < nrepeats; i++) {
    ss.clear();
    ss.str("");
    ss << outFileRoot << "_run_" << (repeatStartIndex+i) << ".dat";
    outFile = ss.str();
    writer.open(outFile);
    if (!writer){
      cout << "Error: cannot open the file " << outFile << endl;
      return 1;
    }
    for (int j = startIndex[i]; j <= endIndex[i]; j++) {
      writer << j-startIndex[i]+1 << " " << ctcf[j-1] << "\n";
    }
    writer.close();
  }
}
