// extrude_distrb.cpp
// A program which generates a simulated ChIP-seq measuring the frequency of
// finding an extruder at a particular genomic region 

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

int main(int argc, char* argv[]) {
  if (argc < 10) {
    cout << "Usage: extrude_distrb startBp endBp bpPerBead startTime endTime "
	 << "timeInc useGenomeCoords outFile extrudePosFile1 "
	 << "[extrudePosFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBead = stol(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  bool useGenomeCoords = 
    static_cast<bool>(stoi(string(argv[++argi]), nullptr, 10));
  string outFile (argv[++argi]);

  int nbeads = static_cast<int>(ceil(static_cast<double>(endBp-startBp)/
				     static_cast<double>(bpPerBead)));
  int nsamples = argc-(++argi);
  
  ifstream reader;
  string extrudePosFile;
  string line, str;
  stringstream ss;
  int nextruders, left, right;
  long time;
  vector<double> extrudeForFreq (nbeads, 0);
  vector<double> extrudeBackFreq (nbeads, 0);

  for (int i = argi; i < argc; i++) {
    extrudePosFile = string(argv[i]);
    reader.open(extrudePosFile);

    if (!reader) {
      cout << "Error: cannot open the file " << extrudePosFile << endl;
      return 1;
    }
       
    while (getline(reader, line)) {
      // Read header lines
      ss.str(line);
      ss >> str >> nextruders;
      ss.clear();
      getline(reader, line);
      ss.str(line);
      ss >> str >> time;
      ss.clear();
      if (time > endTime) {
	break;
      } else if (time >= startTime && (time-startTime) % timeInc == 0) {
	for (int j = 0; j < nextruders; j++) {
	  getline(reader, line);
	  ss.str(line);
	  ss >> left >> right;
	  ss.clear();
	  extrudeBackFreq[left-1] += 1.0; // Convert to zero-based
	  extrudeForFreq[right-1] += 1.0; // Convert to zero-based
	}
      } else {
	for (int j = 0; j < nextruders; j++) {
	  getline(reader, line);
	}
      }
    }
    reader.close();
  }

  // Normalise
  for (int i = 0; i < nbeads; i++) {
    extrudeBackFreq[i] /= static_cast<double>(nsamples);
    extrudeForFreq[i] /= static_cast<double>(nsamples);
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  for (int i = 0; i < nbeads; i++) {
    writer << (useGenomeCoords?(long)(startBp+(i+0.5)*bpPerBead):i) << " " 
	   << extrudeForFreq[i] << " " << extrudeBackFreq[i] << "\n";
  }
  writer.close();
}
