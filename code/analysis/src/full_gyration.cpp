// full_gyration.cpp
// A program which computes the full radius of gyration.
// The program assumes that the first Np beads are the polymer beads in the
// position file.

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

double dist(double x1[3], double x2[3]);
double distSq(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: full_gyration nbeads startTime endTime timeInc "
	 << "posFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  // This is the number of polymer beads
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);
  
  BeadStream reader;
  BeadList blist;
  reader.open(posFile, blist);
  if (!reader.isOpen()) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }

  int nbins = static_cast<int>(ceil(static_cast<double>(endTime-startTime)/
				    static_cast<double>(timeInc)));
  double* gyration = create1DArray<double>(nbins);
  double** pos = create2DArray<double>(nbeads,3);
  
  for (long time = startTime; time <= endTime; time += timeInc) {
    if (reader.moveTo(time)) {
      cout << "Doing time " << time << endl;
      int ibin = static_cast<int>(floor(static_cast<double>(time-startTime)/
					static_cast<double>(timeInc)));
      // Compute the centre of mass
      double cm[3] = {0.0, 0.0, 0.0};
      for (int i = 0; i < nbeads; i++) {
	pos[i][0] = blist.getUnwrappedPosition(i,0);
	pos[i][1] = blist.getUnwrappedPosition(i,1);
	pos[i][2] = blist.getUnwrappedPosition(i,2);
	cm[0] += pos[i][0];
	cm[1] += pos[i][1];
	cm[2] += pos[i][2];
      }
      cm[0] /= static_cast<double>(nbeads);
      cm[1] /= static_cast<double>(nbeads);
      cm[2] /= static_cast<double>(nbeads);
      
      // Compute the radius of gyration
      for (int i = 0; i < nbeads; i++) {
	gyration[ibin] += distSq(pos[i],cm);
      }
      gyration[ibin] /= static_cast<double>(nbeads);
    }
  }
  reader.close();
  
  // Output
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int ibin = 0; ibin < nbins; ibin++) {
    writer << startTime+ibin*timeInc << " " << sqrt(gyration[ibin]) << "\n";
  }
  writer.close();

  // Clean up
  deleteArray(pos);
  deleteArray(gyration);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}

double distSq(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sum;
}
