/* oci.cpp
 * A program which computes the open chromatin index (OCI) of the contact map
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include "contact.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

using uint = unsigned int;
using CMap = ContactMap<uint,double>;

int main(int argc, char* argv[]) {
  if (argc != 8) {
    cout << "Usage: oci startBp endBp bpPerBin distBp "
	 << "useGenomeCoords cmapFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBin = stol(string(argv[++argi]), nullptr, 10);
  long distBp = stol(string(argv[++argi]), nullptr, 10);
  bool useGenomeCoords = 
    static_cast<bool>(stoi(string(argv[++argi]), nullptr, 10));
  string cmapFile (argv[++argi]);
  string outFile (argv[++argi]);

  cout << "Reading contact map ..." << endl;
  CMap cmap (startBp, endBp, bpPerBin, useGenomeCoords);
  if (!cmap.read(cmapFile)) {
    cout << "Error: cannot load the contact map" << endl;
    return 1;
  }
  
  // Compute local and distal score
  uint nbins = cmap.size();
  int dist = cmap.bpToBin(startBp+distBp);
  vector<double> local (nbins,0.0);
  vector<double> distal (nbins,0.0);
  vector<int> localCount (nbins,0);
  for (uint i = 0; i < nbins; i++) {
    for (uint j = 0; j < nbins; j++) {
      if (abs(i-j) <= dist) {
	local[i] += cmap.at(i,j);
	localCount[i]++;
      } else {
	distal[i] += cmap.at(i,j);
      }
    }
  }

  // Normalise
  double count;
  for (uint i = 0; i < nbins; i++) {
    count = localCount[i];
    local[i] /= count;
    distal[i] /= (nbins-count);
  }

  // Output
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  for (uint i = 0; i < nbins; i++) {
    writer << (useGenomeCoords ? startBp+(i+0.5)*bpPerBin : i)
	   << " " << local[i] << " " << distal[i] << " " 
	   << log2(distal[i]/local[i]) << "\n";
  }
  writer.close();
}
