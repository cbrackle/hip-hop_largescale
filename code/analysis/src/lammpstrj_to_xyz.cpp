/* lammpstrj_to_xyz.cpp
 * A simple code that converts a lammpstrj file to an xyz file which is 
 * readable by vmd
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::istringstream;
using std::vector;
using std::string;
using std::map;

int main(int argc, char* argv[]){
  if (argc != 7){
    cout << "Usage: lammpstrj_to_xyz startBead endBead useUnwrapCoords "
	 << "useIndexAsType posFile xyzFile" << endl;
    return 1;
  }
  
  int argi = 0;
  // startBead and endBead are one-based
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  int useUnwrapCoords = stoi(string(argv[++argi]), nullptr, 10);
  int useIndexAsType = stoi(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string xyzFile (argv[++argi]);
  int numOfOutputBeads = endBead-startBead+1;

  BeadStream bstream;
  BeadList blist;
  ofstream writer;
  bstream.open(posFile, blist);
  writer.open(xyzFile);

  if (!bstream.isOpen()){
    cout << "Problem with opening position file!" << endl;
    return 1;
  }
  if (!writer){
    cout << "Problem wiht opening output file!" << endl;
    return 1;
  }

  while (bstream.nextFrame()){
    // Write position
    writer << numOfOutputBeads << "\n";
    writer << "Atoms. Timestep: " << bstream.getTime() << "\n";
    for (int i = startBead; i <= endBead; i++){
      writer << (useIndexAsType ? i : blist.getType(i-1)) << " ";
      if (useUnwrapCoords) {
	writer << blist.getUnwrappedPosition(i-1,0) << " "
	       << blist.getUnwrappedPosition(i-1,1) << " "
	       << blist.getUnwrappedPosition(i-1,2) << "\n";
      } else {
	writer << blist.getPosition(i-1,0) << " "
	       << blist.getPosition(i-1,1) << " "
	       << blist.getPosition(i-1,2) << "\n";
      }
    }
  }
  bstream.close();
  writer.close();
}
