// symmap_to_fullmap.cpp
// Convert a symmetric map to a full map

#include <iostream>
#include <string>
#include "bead_map.hpp"

using std::cout;
using std::endl;
using std::istream;
using std::ostream;
using std::string;
using std::vector;

struct Data {
  static size_t nargs;
  vector<string> args;
  Data() {args = vector<string>(nargs);}
  friend istream& operator>>(istream& stream, Data& d) {
    for (size_t i = 0; i < nargs; i++) {
      stream >> d.args[i];
    }
    return stream;
  }
  friend ostream& operator<<(ostream& stream, const Data& d) {
    for (size_t i = 0; i < nargs-1; i++) {
      stream << d.args[i] << " ";
    }
    stream << d.args[nargs-1];
    return stream;
  }
};

size_t Data::nargs = 0;

int main(int argc, char* argv[]) {
  if (argc != 5) {
    cout << "Usage: symmap_to_fullmap nargs beadFile inFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int nargs = stoi(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string inFile (argv[++argi]);
  string outFile (argv[++argi]);

  Data::nargs = nargs;

  BeadMap<Data> map;
  if (!map.load(beadFile,inFile)) {
    cout << "Error: cannot load the map data" << endl;
    return 1;
  }
  map.dumpToGnuplot(outFile);
}
