# cluster_small_world.py
# A program which computes statistics on how small-world the network is
# The program uses MPI for speedup

import sys
import numpy as np
from mpi4py import MPI
import networkx as nx

# Set up MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

# Check arguments
args = sys.argv
if (len(args) != 4):
    print("cluster_small_world.py seed edge_file out_file")
    sys.exit(1)

seed = int(args[1])+rank
edge_file = args[2]
out_file = args[3]

# Set up the graph
graph = None
niter = 100
nrand = nprocs #10

if (rank == 0):
    graph = nx.read_edgelist(edge_file)
graph = comm.bcast(graph, root=0)

nnodes = nx.number_of_nodes(graph)
if (nnodes >= 4):
    # Generate random and lattice graphs
    ntasks_per_proc = int(np.ceil(nrand*2.0/float(nprocs)))
    ntasks = ntasks_per_proc*nprocs

    all_task_code = None
    all_clust_coeff = None
    all_short_path_len = None
    task_code = np.zeros(ntasks_per_proc, dtype=int)
    clust_coeff = np.zeros(ntasks_per_proc, dtype=float)
    short_path_len = np.zeros(ntasks_per_proc, dtype=float)
    
    if (rank == 0):
        all_task_code = np.zeros(ntasks,dtype=int)
        all_clust_coeff = np.zeros(ntasks,dtype=float)
        all_short_path_len = np.zeros(ntasks,dtype=float)
        task_no = 0
        for i in range(ntasks_per_proc):
            if (task_no >= nrand*2): break
            for j in range(nprocs):
                if (task_no >= nrand*2): break
                all_task_code[j*ntasks_per_proc+i] = (
                    1 if task_no < nrand else 2)
                task_no += 1

    comm.Scatter(all_task_code, task_code, root=0)

    for i,code in enumerate(task_code):
        new_graph = None
        if (code == 1): # Generate a random graph
            print("Rank {:d}: doing random graph".format(rank))
            new_graph = nx.random_reference(graph, niter=niter, seed=seed)
        elif (code == 2): # Generate a lattice graph
            print("Rank {:d}: doing lattice graph".format(rank))
            new_graph = nx.lattice_reference(graph, niter=niter, seed=seed)
        clust_coeff[i] = nx.transitivity(new_graph)
        short_path_len[i] = nx.average_shortest_path_length(new_graph)

    comm.Gather(clust_coeff, all_clust_coeff, root=0)
    comm.Gather(short_path_len, all_short_path_len, root=0)

    # Compute the final results
    if (rank == 0):
        Cr, Cl, Lr, Ll = [], [], [], []
        for i,code in enumerate(all_task_code):
            if (code == 1): # Random graphs
                Cr.append(all_clust_coeff[i])
                Lr.append(all_short_path_len[i])
            elif (code == 2): # Lattice graphs
                Cl.append(all_clust_coeff[i])
                Ll.append(all_short_path_len[i])
        avgCr = np.mean(Cr)
        avgCl = np.mean(Cl)
        avgLr = np.mean(Lr)
        avgLl = np.mean(Ll)
        C = nx.transitivity(graph)
        L = nx.average_shortest_path_length(graph)
        sigma = (C/avgCr)/(L/avgLr)
        omega = (avgLr/L)-(C/avgCl)
        swi = ((L-avgLl)/(avgLr-avgLl))*((C-avgCr)/(avgCl-avgCr))

        with open(out_file,'w') as writer:
            writer.write("{:f} {:f} {:f}\n".format(sigma, omega, swi))
else:
    if (rank == 0):
        with open(out_file,'w') as writer:
            writer.write("nan nan nan\n")

# Clean up

MPI.Finalize()
