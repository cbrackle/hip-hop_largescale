// cluster_colour.cpp
// A program which colours the interaction matrix by the observed clusters
// with the matrix. Interactions which are not part of any cluster/domain are
// labelled as 0

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::map;

int main(int argc, char* argv[]) {
  if (argc != 5) {
    cout << "Usage: cluster_colour oneBased beadFile clustFile outFile"
	 << endl;
    return 1;
  }
  
  int argi = 0;
  int oneBased = stoi(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string clustFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the list of beads
  ifstream reader;
  reader.open(beadFile);
  if (!reader) {
    cout << "Error: cannot open the file " << beadFile << endl;
    return 1;
  }

  stringstream ss;
  string line;
  int index;
  map<int,int> beadGroup;
  
  while (getline(reader,line)) {
    ss.str(line);
    ss >> index;
    beadGroup[oneBased ? index-1 : index] = 0;
    ss.clear();
  }
  reader.close();

  // Read the interaction clusters
  vector<int> clusterSize;
  clusterSize.push_back(0);
  
  reader.open(clustFile);
  if (!reader) {
    cout << "Error: cannot open the file " << clustFile << endl;
    return 1;
  }

  string token;
  int group = 1;
  while (getline(reader,line)) {
    ss.str(line);
    int n = 0;
    while (getline(ss,token,' ')) {
      index = stoi(token, nullptr, 10);
      beadGroup[index] = group;
      n++;
    }
    ss.clear();
    clusterSize.push_back(n);
    group++; 
  }

  int nbeads = static_cast<int>(beadGroup.size());
  vector<int> beads (nbeads);
  int n = 0;
  for (map<int,int>::iterator it = beadGroup.begin();
       it != beadGroup.end(); it++) {
    beads[n] = it->first;
    n++;
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  int groupi, groupj;
  for (int i = 0; i < nbeads; i++) {
    groupi = beadGroup[beads[i]];
    for (int j = 0; j < nbeads; j++) {
      groupj = beadGroup[beads[j]];
      writer << beads[i] << " " << beads[j] << " "
	     << (groupi == groupj ? groupi : 0) <<  " " 
	     << (groupi == groupj ? clusterSize[groupi] : 0) << "\n";
    }
    writer << "\n";
  }
  writer.close();
}
