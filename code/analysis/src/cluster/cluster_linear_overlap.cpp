// cluster_linear_overlap.cpp
// A program which determines the amount of linear overlap between two
// clusters

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <utility>
#include "mat.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::stringstream;
using std::string;
using std::vector;
using std::pair;

using Mat = mat::SymMat<double>;
using MatStream = mat::SymMatStream<double>;

double fracOverlap(int min1, int max1, int min2, int max2);

int main(int argc, char* argv[]) {
  if (argc != 3) {
    cout << "Usage: cluster_linear_overlap clustFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  string clustFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the clusters
  ifstream reader;
  reader.open(clustFile);
  if (!reader) {
    cout << "Error: cannot open the file " << clustFile << endl;
    return 1;
  }

  vector<pair<int,int> > clustBound;

  stringstream ss;
  string line;
  string token;
  int min, max, index;
  while (getline(reader,line)) {
    ss.clear();
    ss.str(line);
    int n = 0;
    min = 0;
    max = 0;
    while (getline(ss,token,' ')) {
      index = stoi(token, nullptr, 10);
      if (n == 0) {
	min = index;
	max = index;
      } else if (index < min) {
	min = index;
      } else if (index > max) {
	max = index;
      }
      n++;
    }
    clustBound.push_back(std::make_pair(min,max));
  }

  // Compute the amount of overlap
  int nclusters = static_cast<int>(clustBound.size());
  Mat overlap (nclusters);
  int mini, minj, maxi, maxj;
  for (int i = 0; i < nclusters; i++) {
    mini = clustBound[i].first;
    maxi = clustBound[i].second;
    for (int j = 0; j < i; j++) {
      minj = clustBound[j].first;
      maxj = clustBound[j].second;
      overlap(i,j) = fracOverlap(mini,maxi,minj,maxj);
    }
    overlap(i,i) = 1.0; // A cluster is always fully overlapped with itself
  }

  // Output results
  MatStream writer;
  writer.dumpToGnuplot(outFile,overlap);
}

double fracOverlap(int min1, int max1, int min2, int max2) {
  int len1 = max1-min1;
  int len2 = max2-min2;
  double slen = len2 > len1 ? len1 : len2;
  if (min2 > max1 || max2 < min1) {
    return 0.0;
  } else if (min2 < min1 && max2 < max1) {
    return (max2-min1)/slen;
  } else if (min2 > min1 && max2 > max1) {
    return (max1-min2)/slen;
  } else {
    return 1.0;
  }
}
