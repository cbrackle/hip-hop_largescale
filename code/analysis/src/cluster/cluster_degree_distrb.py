# cluster_degree_distrb.py
# A program which measures the degree distribution of networks

import sys
import numpy as np
import networkx as nx
from matplotlib import pyplot as plt

args = sys.argv

if (len(args) < 3):
    print("Usage: cluster_degree_distrb.py out_file edge_file1",
          "[edge_files ...]")
    sys.exit(1)

out_file = args.pop(1)
degs = []
for i in range(len(args)-1):
    edge_file = args.pop(1)
    graph = nx.read_edgelist(edge_file)
    deg = np.array(graph.degree(),dtype=int)
    deg = deg[:,1].tolist()
    degs = degs + deg
plt.hist(degs)
plt.show()
    
