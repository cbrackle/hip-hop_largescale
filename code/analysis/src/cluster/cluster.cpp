// cluster.cpp
// A program which finds all subgraphs/subclusters in a given 
// interaction network (imap)
// The code stores a linked-list of bead indices. The list contains
// multiple (cyclic) sub-linked-lists in which each list comprises of
// the indices of all the beads belonging to the same cluster. The code
// is based on the algorithm described in:
// Stoddard J. Comp. Phys. 27, 291, 1977

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "bead_map.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: cluster thres minSize oneBased keyFile mapFile outFile" 
	 << endl;
    return 1;
  }
  
  int argi = 0;
  int thres = stoi(string(argv[++argi]), nullptr, 10);
  int minSize = stoi(string(argv[++argi]), nullptr, 10);
  int oneBased = stoi(string(argv[++argi]), nullptr, 10);
  string keyFile (argv[++argi]);
  string mapFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the interaction matrix
  BeadMap<int> bmap;
  if (!bmap.load(keyFile, mapFile, oneBased)) {
    cout << "Error: cannot load the matrix data" << endl;
    return 1;
  }
  int nbeads = static_cast<int>(bmap.size());
  
  // Cluster lists
  vector<int> list (nbeads,0);
  vector<int> clusterStartIndex;
  vector<int> clusterSize;

  int i, j, k, temp;
  for (i = 0; i < nbeads; i++) {
    list[i] = i;
  }
  
  // Find clusters
  int beadsInCluster = 0;
  for (i = 0; i < nbeads; i++) {
    if (i == list[i]) {
      j = i;
      beadsInCluster++;
      clusterStartIndex.push_back(i);
      do {
	for (k = i+1; k < nbeads; k++) {
	  if (k != list[k]) continue;
	  if (bmap(j,k) >= thres) {
	    temp = list[j];
	    list[j] = list[k];
	    list[k] = temp;
	    beadsInCluster++;
	  }
	}
	j = list[j];
      } while (j != i);
      clusterSize.push_back(beadsInCluster);
      beadsInCluster = 0;
    }
  }
  
  // Output the clusters
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  int index = 0, nextIndex = -1;
  for (size_t n = 0; n < clusterStartIndex.size(); n++) {
    if (clusterSize[n] < minSize) continue;
    index = clusterStartIndex[n];
    writer << bmap.getBead(index) << " ";
    nextIndex = list[index];
    while (nextIndex != clusterStartIndex[n]) {
      index = nextIndex;
      writer << bmap.getBead(index) << " ";
      nextIndex = list[index];
    }
    writer << "\n";
  }
  writer.close();
}
