# cluster_network_analysis.py

import sys
import networkx as nx

args = sys.argv

if (len(args) != 3):
    print("Usage: clsuter_network_analysis.py edge_file out_file")

edge_file = args.pop(1)
out_file = args.pop(1)

graph = nx.read_edgelist(edge_file)
nnodes = nx.number_of_nodes(graph)
max_clique_size = nx.graph_clique_number(graph)
clust_coeff = nx.transitivity(graph)
avg_clust_coeff = nx.average_clustering(graph)
avg_shortest_length = nx.average_shortest_path_length(graph)

with open(out_file,'w') as writer:
    writer.write("{:d} {:d} {:f} {:f} {:f}\n".format(
        nnodes, max_clique_size, clust_coeff, avg_clust_coeff, 
        avg_shortest_length))
