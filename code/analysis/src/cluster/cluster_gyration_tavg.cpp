// cluster_gyration_avg.cpp
// A program which computes the centre of mass and radius of gyration of
// clusters

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

double distSq(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: cluster_gyration_tavg startTime endTime timeInc " 
	 << "clustFile posFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string clustFile (argv[++argi]);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);
  
  // Read the clusters
  ifstream reader;
  reader.open(clustFile);
  if (!reader) {
    cout << "Error: cannot open the file " << clustFile << endl;
    return 1;
  }
  
  stringstream ss;
  string line;
  string token;
  vector<int> cluster;
  vector<vector<int> > clusters;
  int maxClustSize = 0;
  while (getline(reader,line)) {
    ss.str(line);
    cluster.clear();
    int n = 0;
    while (getline(ss,token,' ')) {
      cluster.push_back(stoi(token, nullptr, 10));
      n++;
    }
    if (n > maxClustSize) maxClustSize = n;
    ss.clear();
    clusters.push_back(cluster);
  }
  reader.close();

  int nclusters = static_cast<int>(clusters.size());
  double** pos = create2DArray<double>(maxClustSize,3);
  double* gyrAvg = create1DArray<double>(nclusters);
  double* gyrAvgSq = create1DArray<double>(nclusters);
  
  // Read the bead positions
  BeadStream bstream;
  BeadList blist;
  if (!bstream.open(posFile, blist)) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }
  
  int nbins = 0;
  for (long time = startTime; time <= endTime; time += timeInc) {
    if (bstream.moveTo(time)) {
      for (int i = 0; i < nclusters; i++) {
	// Compute the centre of mass
	double cm[3] = {0.0, 0.0, 0.0};
	int nbeads = static_cast<int>(clusters[i].size());
	for (int j = 0; j < nbeads; j++) {
	  pos[j][0] = blist.getUnwrappedPosition(clusters[i][j],0);
	  pos[j][1] = blist.getUnwrappedPosition(clusters[i][j],1);
	  pos[j][2] = blist.getUnwrappedPosition(clusters[i][j],2);
	  cm[0] += pos[j][0];
	  cm[1] += pos[j][1];
	  cm[2] += pos[j][2];
	}
	cm[0] /= static_cast<double>(nbeads);
	cm[1] /= static_cast<double>(nbeads);
	cm[2] /= static_cast<double>(nbeads);
	// Compute the radius of gyration
	double rg2 = 0.0;
	for (int j = 0; j < nbeads; j++) {
	  rg2 += distSq(pos[j],cm);
	}
	rg2 /= static_cast<double>(nbeads);
	gyrAvg[i] += sqrt(rg2);
	gyrAvgSq[i] += rg2;
      }
      nbins++;
    }
  }
  bstream.close();

  // Output results
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int i = 0; i < nclusters; i++) {
    double rgavg = 0.0, rgstdev = 0.0, rgstderr = 0.0;
    if (nbins > 1) {
      gyrAvg[i] /= static_cast<double>(nbins);
      gyrAvgSq[i] /= static_cast<double>(nbins);
      rgavg = gyrAvg[i];
      if (nbins > 2) { // Use unbiased estimate of stdev
	double rgvar = gyrAvgSq[i]-gyrAvg[i]*gyrAvg[i];
	rgstdev = sqrt(rgvar*nbins/static_cast<double>(nbins-1));
	rgstderr = rgstdev/sqrt(nbins);
      }
    }
    writer << i << " " << rgavg << " " << rgstdev << " " << rgstderr << "\n";
  }
  writer.close();

  // Clean up
  deleteArray(pos);
  deleteArray(gyrAvg);
  deleteArray(gyrAvgSq);
}

double distSq(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sum;
}
