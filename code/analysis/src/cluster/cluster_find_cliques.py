# cluster_find_cliques.py
# A program which searches for disjoint cliques above a certain size

import sys
import numpy as np
import networkx as nx
import copy
import operator

args = sys.argv

if (len(args) != 4):
    print("Usage: cluster_find_cliques.py edge_file clique_file gml_file")
    sys.exit(1)

edge_file = args.pop(1)
clique_file = args.pop(1)
gml_file = args.pop(1)


graph = nx.read_edgelist(edge_file,nodetype=int,edgetype=int)

clique_list = []

def find_best_clique(sup_graph):
    cliques = sorted(list(nx.find_cliques(sup_graph)),
                     key=lambda x : len(x), reverse=True)
    max_clique_size = len(cliques[0])
    if (max_clique_size == 1):
        return cliques[0]
    best_cliques = []
    for c in cliques:
        if (len(c) == max_clique_size):
            best_cliques.append(c)
        else:
            break
    if (len(best_cliques) == 1):
        return best_cliques[0]
    # Otherwise, find the following:
    # 1. The size of the next biggest clique upon removing the current one
    # 2. The number of cliques of that size
    # 3. The max linear separation of the clique
    stats = [None for i in best_cliques]
    for i,c in enumerate(best_cliques):
        sub_graph = copy.deepcopy(sup_graph)
        sub_graph.remove_nodes_from(c)
        sub_cliques = sorted(list(nx.find_cliques(sub_graph)),
                             key=lambda x: len(x), reverse=True)
        max_size = len(sub_cliques[0])
        nmax = 0
        for sc in sub_cliques:
            if (len(sc) == max_size):
                nmax += 1
            else:
                break
        c.sort()
        sep = c[-1]-c[0]
        stats[i] = [i, max_size, nmax, sep]
    stats.sort(key=operator.itemgetter(1,2,3), reverse=True)
    return best_cliques[stats[0][0]]

with open(clique_file,'w') as writer:
    while (len(graph) > 0):
        best_clique = sorted(find_best_clique(graph))
        for i in best_clique:
            writer.write("{:d} ".format(i))
        writer.write("\n")
        clique_list.append(best_clique)
        graph.remove_nodes_from(best_clique)

graph = nx.read_edgelist(edge_file,nodetype=int,edgetype=int)

colour_dict = {x:0 for x in list(graph)}

for i,clique in enumerate(clique_list):
    for v in clique:
        colour_dict[v] = i+1
nx.set_node_attributes(graph,0,'group')
for k in colour_dict:
    graph.nodes[k]['group'] = colour_dict[k]
nx.write_gml(graph,gml_file)
