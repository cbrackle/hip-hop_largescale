// cluster_volume_overlap.cpp
// A program which determines the amount of volume overlap between two
// clusters

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"
#include "mat.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::stringstream;
using std::string;
using std::vector;

using Mat = mat::SymMat<double>;
using MatStream = mat::SymMatStream<double>;

double dist(double x1[3], double x2[3]);
double distSq(double x1[3], double x2[3]);
double fracVolOverlap(double x1[3], double x2[3], double r1, double r2);

int main(int argc, char* argv[]) {
  if (argc < 7) {
    cout << "Usage: cluster_volume_overlap startTime endTime timeInc "
	 << "clustFile outFile posFile1 [posFiles ...]" << endl;
    return 1;
  }

  int argi = 0;
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string clustFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the clusters
  ifstream reader;
  reader.open(clustFile);
  if (!reader) {
    cout << "Error: cannot open the file " << clustFile << endl;
    return 1;
  }

  stringstream ss;
  string line;
  string token;
  vector<vector<int> > clusters;
  vector<int> cluster;
  int maxClustSize = 0;
  while (getline(reader, line)) {
    ss.clear();
    ss.str(line);
    cluster.clear();
    int n = 0;
    while (getline(ss, token, ' ')) {
      cluster.push_back(stoi(token, nullptr, 10));
      n++;
    }
    if (n > maxClustSize) maxClustSize = n;
    clusters.push_back(cluster);
  }
  reader.close();

  int nclusters = static_cast<int>(clusters.size());
  
  // Read the positions and compute the fractional overlap
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
  
  vector<BeadStream> bstreams (nthreads);
  vector<BeadList> blists (nthreads);
  vector<Mat> overlap (nthreads, Mat(nclusters));
  int nfiles = argc-(++argi);
  int error = 0;
  int nsamples = 0;
  double*** pos = create3DArray<double>(nthreads,maxClustSize,3);
  double*** cm = create3DArray<double>(nthreads,nclusters,3);
  double** gyr = create2DArray<double>(nthreads,nclusters);
  
#pragma omp parallel default(none),\
  shared(nthreads, nclusters, clusters, startTime, endTime, timeInc),\
  shared(bstreams, blists, argv, argi, nfiles, cout, pos, gyr, cm, overlap),\
  reduction(+:error), reduction(+:nsamples)
  {
#ifdef _OPENMP
    int id = omp_get_thread_num();
#else
    int id = 0;
#endif

#pragma omp for schedule(static)
    for (int n = 0; n < nfiles; n++) {
      string posFile (argv[n+argi]);
      if (!bstreams[id].open(posFile, blists[id])) {
	cout << "Error: cannot open the file " << posFile << endl;
	continue;
      }
      cout << "Working on file " << posFile << endl;
      for (long time = startTime; time <= endTime; time += timeInc) {
	if (bstreams[id].moveTo(time)) {
	  for (int i = 0; i < nclusters; i++) {
	    // Compute the centre of mass
	    int nbeads = static_cast<int>(clusters[i].size());
	    cm[id][i][0] = 0.0;
	    cm[id][i][1] = 0.0;
	    cm[id][i][2] = 0.0;
	    for (int j = 0; j < nbeads; j++) {
	      pos[id][j][0] = 
		blists[id].getUnwrappedPosition(clusters[i][j],0);
	      pos[id][j][1] = 
		blists[id].getUnwrappedPosition(clusters[i][j],1);
	      pos[id][j][2] = 
		blists[id].getUnwrappedPosition(clusters[i][j],2);
	      cm[id][i][0] += pos[id][j][0];
	      cm[id][i][1] += pos[id][j][1];
	      cm[id][i][2] += pos[id][j][2];
	    }
	    cm[id][i][0] /= static_cast<double>(nbeads);
	    cm[id][i][1] /= static_cast<double>(nbeads);
	    cm[id][i][2] /= static_cast<double>(nbeads);
	    // Compute the radius of gyration
	    double rg2 = 0.0;
	    for (int j = 0; j < nbeads; j++) {
	      rg2 += distSq(pos[id][j],cm[id][i]);
	    }
	    rg2 /= static_cast<double>(nbeads);
	    gyr[id][i] = sqrt(rg2);
	  }
	  // Compute volume overlap
	  for (int i = 0; i < nclusters; i++) {
	    overlap[id](i,i) += 1.0;
	    for (int j = 0; j < i; j++) {
	      overlap[id](i,j) += fracVolOverlap(cm[id][i], cm[id][j], 
						 gyr[id][i], gyr[id][j]);
	    }
	  }
	  nsamples++;
	}
      } // Close time loop
    } // Close file loop
  } // Close parallel region
  if (error > 0) {
    cout << "There were errors when reading position data - stop proceeding"
	 << endl;
    return 1;
  }
  
  // Combine results
  for (int id = 1; id < nthreads; id++) {
    for (int i = 0; i < nclusters; i++) {
      overlap[0](i,i) += overlap[id](i,i);
      for (int j = 0; j < i; j++) {
	overlap[0](i,j) += overlap[id](i,j);
      }
    }
  }

  // Normalise results
  for (int i = 0; i < nclusters; i++) {
    for (int j = 0; j <= i; j++) {
      overlap[0](i,j) /= static_cast<double>(nsamples);
    }
  }

  // Output results
  MatStream writer;
  writer.dumpToGnuplot(outFile, overlap[0]);

  // Clean up
  deleteArray(pos);
  deleteArray(cm);
  deleteArray(gyr);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}

double distSq(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sum;
}

double fracVolOverlap(double x1[3], double x2[3], double r1, double r2) {
  const double pi = M_PI;
  const double d = dist(x1,x2);
  const double R = r1 > r2 ? r1 : r2;
  const double r = r1 > r2 ? r2 : r1;
  if (d >= r+R) {
    return 0.0;
  } else if (d < R-r) {
    return 1.0;
  } else {
    const double l = (d*d+R*R-r*r)/(2.0*d);
    const double dml = d-l;
    const double Rml = R-l;
    const double pio3 = pi/3.0;
    double V, v;
    const double vr = 4.0/3.0*pi*r*r*r;
    V = pio3*Rml*Rml*(2.0*R+l);
    if (l < d) {
      v = pio3*(r-dml)*(r-dml)*(2.0*r+dml);
      return (V+v)/vr;
    } else { // l > d
      v = pio3*(4.0*r*r*r-(r+dml)*(r+dml)*(2.0*r-dml));
      return (V+v)/vr;
    }
  }
}
