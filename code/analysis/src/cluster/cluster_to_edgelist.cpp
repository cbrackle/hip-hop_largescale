// cluster_to_edgelist.cpp
// A program which exports each imap cluster graph in the edge list format

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "bead_map.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: cluster_to_edgelist thres oneBased "
	 << "keyFile mapFile clustFile outRoot" << endl;
    return 1;
  }

  int argi = 0;
  // Minimum size to be considered as a cluster
  int thres = stoi(string(argv[++argi]), nullptr, 10);
  int oneBased = stoi(string(argv[++argi]), nullptr, 10);
  string keyFile (argv[++argi]);
  string mapFile (argv[++argi]);
  string clustFile (argv[++argi]);
  string outRoot (argv[++argi]);
  
  // Read the interaction matrix
  BeadMap<int> bmap;
  if (!bmap.load(keyFile, mapFile, oneBased)) {
    cout << "Error: cannot load the matrix data" << endl;
    return 1;
  }

  // Read the interaction clusters
  vector<vector<int> > clusters;
  
  ifstream reader;
  reader.open(clustFile);
  if (!reader) {
    cout << "Error: cannot open the file " << clustFile << endl;
    return 1;
  }
  
  stringstream ss;
  string line;
  string token;
  vector<int> cluster;
  while (getline(reader,line)) {
    ss.str(line);
    cluster.clear();
    while (getline(ss,token,' ')) {
      cluster.push_back(stoi(token, nullptr, 10));
    }
    ss.clear();
    clusters.push_back(cluster);
  }

  ofstream writer;
  // Find all connections/edges within each cluster
  for (size_t i = 0; i < clusters.size(); i++) {
    ss.clear();
    ss.str("");
    ss << outRoot << "_clust_" << (i+1) << ".edgelist";
    string outFile = ss.str();
    writer.open(outFile);
    if (!writer) {
      cout << "Error: cannot open the file " << outFile << endl;
      return 1;
    }
    for (size_t j = 0; j < clusters[i].size(); j++) {
      for (size_t k = 0; k < j; k++) { // Ignore self-loops
	if (bmap.at(clusters[i][j],clusters[i][k]) >= thres) {
	  writer << clusters[i][j] << " " << clusters[i][k] << "\n";
	}
      }
    }
    writer.close();
  }
}
