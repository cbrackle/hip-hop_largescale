/* extrude_kymo.cpp
 * A program that converts the extruder position file into a gnuplot friendly
 * kymograph showing extrusion events
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <map>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::pair;
using std::map;

int main(int argc, char* argv[]) {
  if (argc != 6) {
    cout << "Usage: extrude_kymo nbeads startTime endTime "
	 << "inFile outFile" << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  string inFile (argv[++argi]);
  string outFile (argv[++argi]);

  ifstream reader;
  reader.open(inFile);
  if (!reader) {
    cout << "Error: cannot open the file " << inFile << endl;
    return 1;
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  string line, str;
  stringstream ss;
  int nextruders;
  int left, right;
  long time;
  map<int,int> extruders;
  
  while (getline(reader, line)) {
    ss.str(line);
    ss >> str >> nextruders;
    ss.clear();
    getline(reader, line);
    ss.str(line);
    ss >> str >> time;
    ss.clear();
    if (time > endTime) break;
    if (time < startTime) {
      for (int i = 0; i < nextruders; i++) {
	getline(reader, line);
      }
    } else {
      extruders.clear();
      for (int i = 0; i < nextruders; i++) {
	getline(reader, line);
	ss.str(line);
	ss >> left >> right;
	ss.clear();
	extruders[left] = i;
	extruders[right] = i;
      }
      int index = 0;
      for (map<int,int>::iterator it = extruders.begin();
	   it != extruders.end(); it++) {
	for (; index < it->first; index++) {
	  writer << time << " " << index << " " << 0 << "\n";
	}
	writer << time << " " << it->first << " " << it->second << "\n";
      }
      for (; index < nbeads; index++) {
	writer << time << " " << index << " " << 0 << "\n";
      }
      writer << "\n";
    }
  }
  reader.close();
  writer.close();
}
