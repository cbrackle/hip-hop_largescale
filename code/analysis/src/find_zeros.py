# find_zeros.py
# A script to determine the zeros of a set of data points
# It considers pairs of points that straddle across the x-axis and use
# linear interpolation to determine the zeros

import sys

args = sys.argv

if (len(args) != 5):
    print "Usage: find_zeros.py index_col val_col in_file out_file"
    sys.exit(1)

index_col = int(args.pop(1))
val_col = int(args.pop(1))
in_file = args.pop(1)
out_file = args.pop(1)
thres = 1e-16

def zeros(x1, x2, y1, y2):
    return 0.5*((x2+x1)-(y2+y1)*(x2-x1)/(y2-y1))

# Read the data and find zeros
xpre = None
ypre = None
writer = open(out_file, 'w')
reader = open(in_file, 'r')
for line in reader:
    if (len(line) == 0 or line[0] == '' or line[0] == '#'):
        continue
    params = line.split()
    n = len(params)
    if (index_col < n and val_col < n):
        x = float(params[index_col])
        y = float(params[val_col])
        if (xpre != None and ypre != None):
            if (abs(y-ypre) < thres):
                writer.write("{:.5f} {:d}\n".format(xpre,0))
                writer.write("{:.5f} {:d}\n".format(x,0))
            elif (ypre < 0 and y > 0):
                writer.write("{:.5f} {:d}\n".format(zeros(xpre,x,ypre,y),1))
            elif (ypre > 0 and y < 0):
                writer.write("{:.5f} {:d}\n".format(zeros(xpre,x,ypre,y),-1))
        xpre = x
        ypre = y

reader.close()
writer.close()

