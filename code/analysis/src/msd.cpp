// msd.cpp
// A program which computes the mean square displacement (MSD) of each bead
// The program assumes that the first Np beads are the polymer beads in the
// position file.

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <omp.h>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::vector;

int main(int argc, char* argv[]) {
  if (argc != 7) {
    cout << "Usage: msd nbeads startTime endTime timeInc posFile outFile" 
	 << endl;
    return 1;
  }

  int argi = 0;
  int nbeads = stoi(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);
  
  BeadStream reader;
  BeadList blist;
  reader.open(posFile, blist);
  if (!reader.isOpen()) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }
  
  // Find the total number of frames
  int nbins = static_cast<int>((endTime-startTime)/timeInc)+1;
  
  // Read the position data
  cout << "Read the bead positions ..." << endl;
  double*** pos = create3DArray<double>(nbins, nbeads, 3); 
  for (long time = startTime; time <= endTime; time += timeInc) {
    reader.moveTo(time);
    int ibin = static_cast<int>((time-startTime)/timeInc);
    for (int i = 0; i < nbeads; i++) {
      pos[ibin][i][0] = blist.getUnwrappedPosition(i,0);
      pos[ibin][i][1] = blist.getUnwrappedPosition(i,1);
      pos[ibin][i][2] = blist.getUnwrappedPosition(i,2);
    }
  }
  reader.close();

  // Compute the centre of mass at each frame first
  cout << "Computing the CM of polymer ..." << endl;
  double** cm = create2DArray<double>(nbins, 3);
  for (int ibin = 0; ibin < nbins; ibin++) {
    for (int i = 0; i < nbeads; i++) {
      cm[ibin][0] += pos[ibin][i][0];
      cm[ibin][1] += pos[ibin][i][1];
      cm[ibin][2] += pos[ibin][i][2];
    }
    cm[ibin][0] /= static_cast<double>(nbeads);
    cm[ibin][1] /= static_cast<double>(nbeads);
    cm[ibin][2] /= static_cast<double>(nbeads);
  }
  
  // Compute the MSD
  int dbin;
  double r2;
  double dr[3];
  double dcm[3];
  double** r2avg = create2DArray<double>(nbins, nbeads);
  int* count = create1DArray<int>(nbins);
  for (int ibin = 0; ibin < nbins-1; ibin++) {
    long time = ibin*timeInc+startTime;
    cout << "Doing time " << time << endl;
    for (int jbin = ibin+1; jbin < nbins; jbin++) {
      dbin = jbin-ibin;
      dcm[0] = cm[jbin][0]-cm[ibin][0];
      dcm[1] = cm[jbin][1]-cm[ibin][1];
      dcm[2] = cm[jbin][2]-cm[ibin][2];
      for (int i = 0; i < nbeads; i++) {
	dr[0] = pos[jbin][i][0]-pos[ibin][i][0]-dcm[0];
	dr[1] = pos[jbin][i][1]-pos[ibin][i][1]-dcm[1];
	dr[2] = pos[jbin][i][2]-pos[ibin][i][2]-dcm[2];
	r2 = dr[0]*dr[0]+dr[1]*dr[1]+dr[2]*dr[2];
	r2avg[dbin][i] += r2;
      }
      count[dbin]++;
    }
  }

  // Normalise
  for (int ibin = 1; ibin < nbins; ibin++) {
    for (int i = 0; i < nbeads; i++) {
      r2avg[ibin][i] /= static_cast<double>(count[ibin]);
    }
  }
  
  // Output
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }
  for (int ibin = 0; ibin < nbins; ibin++) {
    writer << ibin*timeInc << " ";
    for (int i = 0; i < nbeads; i++) {
      writer << r2avg[ibin][i] << " ";
    }
    writer << "\n";
  }
  writer.close();

  // Clean up
  deleteArray(cm);
  deleteArray(pos);
  deleteArray(r2avg);
}
