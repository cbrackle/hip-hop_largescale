/* lammpstrj_ctcf_extruder_to_xyz.cpp
 * A simple code that converts a lammpstrj file to an xyz file which is 
 * readable by vmd
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include "bead_stream.hpp"
#include "bead_list.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::vector;
using std::string;
using std::map;
using std::set;

int main(int argc, char* argv[]){
  if (argc != 14){
    cout << "Usage: lammpstrj_ctcf_extruder_to_xyz startBead endBead "
	 << "ctcfForType ctcfBackType ctcfBothType extrudeForType "
	 << "extrudeBackType useUnwrapCoords useSameTypeForPolymerBeads "
	 << "posFile ctcfFile extrudeFile xyzFile" << endl;
    return 1;
  }
  
  int argi = 0;
  // startBead and endBead are one-based
  int startBead = stoi(string(argv[++argi]), nullptr, 10);
  int endBead = stoi(string(argv[++argi]), nullptr, 10);
  int ctcfForType = stoi(string(argv[++argi]), nullptr, 10);
  int ctcfBackType = stoi(string(argv[++argi]), nullptr, 10);
  int ctcfBothType = stoi(string(argv[++argi]), nullptr, 10);
  int extrudeForType = stoi(string(argv[++argi]), nullptr, 10);
  int extrudeBackType = stoi(string(argv[++argi]), nullptr, 10);
  int useUnwrapCoords = stoi(string(argv[++argi]), nullptr, 10);
  int useSameTypeForPolymerBeads = stoi(string(argv[++argi]), nullptr, 10);
  string posFile (argv[++argi]);
  string ctcfFile (argv[++argi]);
  string extrudeFile (argv[++argi]);
  string xyzFile (argv[++argi]);
  int nbeads = endBead-startBead+1;

  // Read CTCF locations
  map<int,int> ctcfs;
  string line, str;
  stringstream ss;
  int index, ctcfVal;
  ifstream reader;
  reader.open(ctcfFile);
  if (!reader) {
    cout << "Error: cannot open the file " << ctcfFile << endl;
    return 1;
  }
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index >> ctcfVal;
    ss.clear();
    if (index >= startBead && index <= endBead) {
      ctcfs[index] = ctcfVal;
    }
  }
  reader.close();

  // Read extruder and bead position data
  reader.open(extrudeFile);
  if (!reader) {
    cout << "Error: cannot open the file " << extrudeFile << endl;
    return 1;
  }

  BeadStream bstream;
  BeadList blist;
  ofstream writer;
  bstream.open(posFile, blist);
  writer.open(xyzFile);

  if (!bstream.isOpen()){
    cout << "Problem with opening position file!" << endl;
    return 1;
  }
  if (!writer){
    cout << "Problem wiht opening output file!" << endl;
    return 1;
  }

  set<int> forExtruders;
  set<int> backExtruders;
  int nextruders;
  long time;
  int left, right;

  while (bstream.nextFrame()){
    // Read extruder data
    cout << "Doing timestep = " << bstream.getTime() << endl;
    do {
      // Read header lines
      if (!getline(reader, line)) break;
      ss.str(line);
      ss >> str >> nextruders;
      ss.clear();
      getline(reader, line);
      ss.str(line);
      ss >> str >> time;
      ss.clear();
      cout << "Extrusion timestep = " << time 
	   << " nextruders = " << nextruders << endl;
      forExtruders.clear();
      backExtruders.clear();
      for (int i = 0; i < nextruders; i++) {
	getline(reader, line);
	ss.str(line);
	ss >> left >> right;
	ss.clear();
	if (left >= startBead && left <= endBead) {
	  backExtruders.insert(left);
	}
	if (right >= startBead && right <= endBead) {
	  forExtruders.insert(right);
	}
      }
    } while (time < bstream.getTime());
    // Write position
    int type;
    vector<int> beadTypes (nbeads, 1);
    writer << nbeads << "\n";
    writer << "Atoms. Timestep: " << bstream.getTime() << "\n";
    // Get polymer bead types
    if (!useSameTypeForPolymerBeads) {
      for (int i = startBead; i <= endBead; i++) {
	beadTypes[i-startBead] = blist.getType(i-1);
      }
    }
    // Modify the bead types to add CTCFs and extruders
    for (std::map<int,int>::iterator it = ctcfs.begin(); 
	 it != ctcfs.end(); it++) {
      index = it->first;
      type = it->second;
      switch (type) {
      case 1: beadTypes[index-startBead] = ctcfForType; break;
      case -1: beadTypes[index-startBead] = ctcfBackType; break;
      case 2: beadTypes[index-startBead] = ctcfBothType; break;
      }
    }
    if (time == bstream.getTime()) {
      for (std::set<int>::iterator it = forExtruders.begin();
	   it != forExtruders.end(); it++) {
	beadTypes[*it-startBead] = extrudeForType;
      }
      for (std::set<int>::iterator it = backExtruders.begin();
	   it != backExtruders.end(); it++) {
	beadTypes[*it-startBead] = extrudeBackType;
      }
    }
    for (int i = startBead; i <= endBead; i++){
      writer << beadTypes[i-startBead] << " ";
      if (useUnwrapCoords) {
	writer << blist.getUnwrappedPosition(i-1,0) << " "
	       << blist.getUnwrappedPosition(i-1,1) << " "
	       << blist.getUnwrappedPosition(i-1,2) << "\n";
      } else {
	writer << blist.getPosition(i-1,0) << " "
	       << blist.getPosition(i-1,1) << " "
	       << blist.getPosition(i-1,2) << "\n";
      }
    }
  }
  bstream.close();
  writer.close();
}
