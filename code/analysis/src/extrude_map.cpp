/* extrude_map.cpp
 * A program which generates 1D contact maps based on extruders' positions
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <utility>
#include <map>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::pair;
using std::map;

double getValue(int i, int j, const map<pair<int,int>,double>& contact);
pair<int,int> mapCoords(int i, int j, int scale);
long binToBp(int i, long startBp, long bpPerBin);

int main(int argc, char* argv[]) {
  if (argc < 11) {
    cout << "Usage: extrude_map startBp endBp bpPerBead beadsPerBin "
	 << "startTime endTime timeInc useGenomeCoords outFile "
	 << "extrudePosFile1 [extrudePosFiles ...]" << endl;
    return 1;
  }
  
  int argi = 0;
  long startBp = stol(string(argv[++argi]), nullptr, 10);
  long endBp = stol(string(argv[++argi]), nullptr, 10);
  long bpPerBead = stol(string(argv[++argi]), nullptr, 10);
  long beadsPerBin = stol(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  int useGenomeCoords = stoi(string(argv[++argi]), nullptr, 10);
  string outFile (argv[++argi]);

  int nbeads = static_cast<int>(ceil(static_cast<double>(endBp-startBp)/
				     static_cast<double>(bpPerBead)));
  int nbins = static_cast<int>(ceil(static_cast<double>(nbeads)/
				    static_cast<double>(beadsPerBin)));
  int bpPerBin = bpPerBead*beadsPerBin;
  //int nsamples = argc-(++argi);
  
  ifstream reader;
  string extrudePosFile;
  string line, str;
  stringstream ss;
  int nextruders, left, right;
  long time;
  map<pair<int,int>,double> extrudeMap;

  for (int i = ++argi; i < argc; i++) {
    extrudePosFile = string(argv[i]);
    reader.open(extrudePosFile);
    if (!reader) {
      cout << "Error: cannot open the file " << extrudePosFile << endl;
      return 1;
    }
    while (getline(reader, line)) {
      // Read header lines
      ss.str(line);
      ss >> str >> nextruders;
      ss.clear();
      getline(reader, line);
      ss.str(line);
      ss >> str >> time;
      ss.clear();
      if (time > endTime) {
	break;
      } else if (time < startTime || (time % timeInc != 0)) {
	for (int j = 0; j < nextruders; j++) {
	  getline(reader, line);
	}
      } else {
	for (int j = 0; j < nextruders; j++) {
	  getline(reader, line);
	  ss.str(line);
	  ss >> left >> right;
	  ss.clear();
	  extrudeMap[mapCoords(left,right,beadsPerBin)] += 1.0;
	}
      }
    }
    reader.close();
  }

  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  for (int i = 0; i < nbins; i++) {
    for (int j = 0; j < nbins; j++) {
      writer << (useGenomeCoords ? binToBp(i,startBp,bpPerBin) : i) << " " 
	     << (useGenomeCoords ? binToBp(j,startBp,bpPerBin) : j) << " " 
	     << getValue(i,j,extrudeMap) << "\n"; 
      writer.unsetf(std::ios_base::floatfield);
    }
    writer << "\n";
  }
  writer.close();
}

inline double getValue(int i, int j, const map<pair<int,int>,double>& contact) {
  pair<int,int> coords = mapCoords(i,j,1);
  if (contact.find(coords) != contact.end()) {
    return contact.at(coords);
  } else {
    return 0.0;
  }
}

inline pair<int,int> mapCoords(int i, int j, int scale) {
  pair<int,int> binij;
  if (i <= j) {
    binij.first = i/scale;
    binij.second = j/scale;
  } else {
    binij.first = j/scale;
    binij.second = i/scale;
  }
  return binij;
}

inline long binToBp(int i, long startBp, long bpPerBin) {
  return startBp+(i+0.5)*bpPerBin;
}
