// protein_cluster_time.cpp
// A program which finds all protein clusters in the simulation
// The code stores a linked-list of bead indices. The list contains
// multiple (cyclic) sub-linked-lists in which each list comprises of
// the indices of all the beads belonging to the same cluster. The code
// is based on the algorithm described in:
// Stoddard J. Comp. Phys. 27, 291, 1977

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include "bead_stream.hpp"
#include "bead_list.hpp"
#include "array.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;

double dist(double x1[3], double x2[3]);

int main(int argc, char* argv[]) {
  if (argc != 9) {
    cout << "Usage: protein_cluster_time thres minClust startTime endTime "
	 << "timeInc beadFile posFile outFile" << endl;
    return 1;
  }
  
  int argi = 0;
  int thres = stoi(string(argv[++argi]), nullptr, 10);
  int minClust = stoi(string(argv[++argi]), nullptr, 10);
  long startTime = stol(string(argv[++argi]), nullptr, 10);
  long endTime = stol(string(argv[++argi]), nullptr, 10);
  long timeInc = stol(string(argv[++argi]), nullptr, 10);
  string beadFile (argv[++argi]);
  string posFile (argv[++argi]);
  string outFile (argv[++argi]);

  // Read the beads
  ifstream reader;
  reader.open(beadFile);
  if (!reader) {
    cout << "Error: cannot open the file " << beadFile << endl;
    return 1;
  }

  string line;
  stringstream ss;
  int index;
  vector<int> beads;
  
  while (getline(reader, line)) {
    ss.str(line);
    ss >> index;
    beads.push_back(index-1); // Convert to zero-based
    ss.clear();
  }
  reader.close();

  // Cluster lists
  int nbeads = static_cast<int>(beads.size());
  vector<int> list (nbeads,0);
  vector<int> clusterSizes;
  double** pos = create2DArray<double>(nbeads,3);
  
  // Open output file
  ofstream writer;
  writer.open(outFile);
  if (!writer) {
    cout << "Error: cannot open the file " << outFile << endl;
    return 1;
  }

  // Read the position data
  BeadStream bstream;
  BeadList blist;
  if (!bstream.open(posFile, blist)) {
    cout << "Error: cannot open the file " << posFile << endl;
    return 1;
  }
  for (long time = startTime; time <= endTime; time += timeInc) {
    if (bstream.moveTo(time)) {
      cout << "Doing t = " << time << endl;
      // Get the positions of beads
      for (int i = 0; i < nbeads; i++) {
	// Use wrapped position as proteins are far apart otherwise
	pos[i][0] = blist.getPosition(beads[i],0);
	pos[i][1] = blist.getPosition(beads[i],1);
	pos[i][2] = blist.getPosition(beads[i],2);
      }
      
      // Reset the cluster list
      clusterSizes.clear();
      for (int i = 0; i < nbeads; i++) {
	list[i] = i;
      }
      
      // Find clusters
      int beadsInCluster = 0;
      int temp;
      for (int i = 0; i < nbeads; i++) {
	if (i == list[i]) {
	  int j = i;
	  beadsInCluster++;
	  do {
	    for (int k = i+1; k < nbeads; k++) {
	      if (k != list[k]) continue;
	      if (dist(pos[j],pos[k]) < thres) {
		temp = list[j];
		list[j] = list[k];
		list[k] = temp;
		beadsInCluster++;
	      }
	    }
	    j = list[j];
	  } while (j != i);
	  clusterSizes.push_back(beadsInCluster);
	  beadsInCluster = 0;
	}
      }
      
      // Compute cluster statistics
      std::sort(clusterSizes.rbegin(), clusterSizes.rend());
      vector<int> bigClusterSizes;
      for (size_t i = 0; i < clusterSizes.size(); i++) {
	if (clusterSizes[i] >= minClust) {
	  bigClusterSizes.push_back(clusterSizes[i]);
	} else {
	  break;
	}
      }
      std::sort(bigClusterSizes.begin(), bigClusterSizes.end());
      double avgClusterSize = 0.0;
      double avgSqClusterSize = 0.0;
      int size;
      int n = static_cast<int>(bigClusterSizes.size());
      for (int i = 0; i < n; i++) {
	size = bigClusterSizes[i];
	avgClusterSize += size;
	avgSqClusterSize += size*size;
      }
      avgClusterSize /= static_cast<double>(n);
      avgSqClusterSize /= static_cast<double>(n);
      int medianClusterSize = bigClusterSizes[n/2];
      int minClusterSize = bigClusterSizes[0];
      int maxClusterSize = bigClusterSizes[n-1];
      double var;
      double sigma;

      if (n > 1) {
	var = n/(n-1)*(avgSqClusterSize - avgClusterSize*avgClusterSize);
	sigma = sqrt(var);
      } else {
	var = 0.0;
	sigma = 0.0;
      }

      // Output cluster statistics
      writer << time << " " << n << " " 
	     << minClusterSize << " " << maxClusterSize << " "
	     << medianClusterSize << " " << avgClusterSize << " "
	     << sigma << "\n"; 
    }
  }
  bstream.close();
  writer.close();

  // Clean up
  deleteArray(pos);
}

double dist(double x1[3], double x2[3]) {
  double sum = 0.0;
  double dx;
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}
