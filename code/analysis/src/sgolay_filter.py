# sgolay_filter.py
# A program which smooths a set of data points using the Savitzky-Golay filter

import sys
import scipy.signal
import numpy as np

args = sys.argv

if (len(args) != 8):
    print "Usage: sgolay_filter index_col val_col window_len poly_order",\
        "deriv in_file out_file"
    sys.exit(1)

index_col = int(args.pop(1))
val_col = int(args.pop(1))
window_len = int(args.pop(1))
poly_order = int(args.pop(1))
deriv = int(args.pop(1))
in_file = args.pop(1)
out_file = args.pop(1)

if (val_col < 0):
    print "Error: val_col must be greater or equal to zero"
    sys.exit(1)

# Read the data
values = []
indexes = []
with open(in_file, 'r') as reader:
    for line in reader:
        if (len(line) == 0 or line[0] == "" or line[0] == "#"):
            continue
        data = line.split()
        if (val_col < len(data)):
            values.append(float(data[val_col]))
        if (index_col != -1 and index_col < len(data)):
            indexes.append(float(data[index_col]))

# Apply the filter
values = np.array(values)
indexes = np.array(indexes)
smoothed = scipy.signal.savgol_filter(values, window_len, poly_order, deriv=deriv)

# Output the data
with open(out_file, 'w') as writer:
    for x,y in zip(indexes,smoothed):
        writer.write("{:.5f} {:.5f}\n".format(x,y));
