#!/bin/bash
# local_gyration.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 5 ]]; then
    echo "Usage: local_density.sh run_start run_end run_inc in_dir out_dir"
    exit 1
fi

run_start=$1
run_end=$2
run_inc=$3
in_dir=$4
out_dir=$5

cell="GM12878"
chrom="chr18"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
tstart=20000000
tend=50000000
tinc=200000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")
#use_genome_coords=1

length=100 # Local segment length

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

gyr_exe="${sh_dir}/../bin/analysis/exe/local_gyration"
to_genome_coords_exe="${sh_dir}/../bin/analysis/exe/convert_to_genome_coords"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# Compute the local gyration
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}_run_${run}"
    pos_file="${in_dir}/config_${name}.lammpstrj"
    gyr_file="${out_dir}/gyr_${name}.dat"
    cmd[$jobid]="$gyr_exe $nbeads $length $tstart $tend $tinc $pos_file ${gyr_file}.tmp"
    jobid=$(($jobid+1))
done
export OMP_NUM_THREADS=1
max_jobs=8
total_jobs=$jobid
parallel_run

# Convert to genome coords
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}_run_${run}"
    gyr_file="${out_dir}/gyr_${name}.dat"
    cmd[$jobid]="$to_genome_coords_exe $start_bp $end_bp $bp_per_bead ${gyr_file}.tmp $gyr_file 2 0"
    jobid=$(($jobid+1))
done
max_jobs=8
total_jobs=$jobid
parallel_run

for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}_run_${run}"
    gyr_file="${out_dir}/gyr_${name}.dat"
    rm ${gyr_file}.tmp
    jobid=$(($jobid+1))
done
