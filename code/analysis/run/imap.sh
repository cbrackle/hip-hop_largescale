#!/bin/bash
# imap_cluster_atac.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 7 ]]; then
    echo "Usage: imap_cluster_atac.sh run_start run_end run_inc" \
	 "bead_type bead_list pos_dir out_dir"
    exit 1
fi

run_start=$1
run_end=$2
run_inc=$3
bead_type=$4
bead_list=$5
pos_dir=$6
out_dir=$7

cell="GM12878"
#chrom="chr19"
chrom="chr18"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
bp_per_bead=1000
tstart=20000000
tend=30000000
tinc=200000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")
rc=3.5 # Threshold on the spatial separation to be in contact
fq=1530 # Threshold on the interaction frequency for beads to be connected
nc=1 # Threshold on the size of a cluster to be idenitified as a cluster

#if [[ ! -d $raw_dir ]]; then
#    echo "Error: cannot find the directory " $raw_dir
#    exit 1
#fi

if [[ ! -d $pos_dir ]]; then
    echo "Error: cannot find the directory " $pos_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Programs required
exe_dir="../../../../code/analysis/bin/analysis/exe/"
imap_exe="${exe_dir}/imap/imap"
imap_clust_exe="${exe_dir}/cluster/cluster"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

function compute_indv_imap() {
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
    pos_file="${pos_dir}/config_${name}_run_${run}.lammpstrj"
    imap_file="${out_dir}/imap-${bead_type}_${name}_rc_${rc}_t_${tstart}-${tend}_run_${run}.dat"
    cmd[$jobid]="$imap_exe $rc $tstart $tend $tinc $bead_list $imap_file $pos_file"
    jobid=$(($jobid+1))
done
export OMP_NUM_THREADS=1
max_jobs=8
total_jobs=$jobid
parallel_run
}

function compute_full_imap() {
pos_files=""
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
    pos_files="${pos_files} ${pos_dir}/config_${name}_run_${run}.lammpstrj"
done
imap_file="${out_dir}/imap-${bead_type}_${name}_rc_${rc}_t_${tstart}-${tend}.dat"
export OMP_NUM_THREADS=64
echo "$imap_exe $rc $tstart $tend $tinc $bead_list $imap_file $pos_files"
$imap_exe $rc $tstart $tend $tinc $bead_list $imap_file $pos_files
}

function compute_indv_imap_cluster() {
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
    imap_file="${out_dir}/imap-${bead_type}_${name}_rc_${rc}_t_${tstart}-${tend}_run_${run}.dat"
    imap_clust_file="${out_dir}/imap-cluster-${bead_type}_${name}_rc_${rc}_nc_${nc}_t_${tstart}-${tend}_run_${run}.dat"
    cmd[$jobid]="$imap_clust_exe $nc $imap_file $imap_clust_file"
    jobid=$(($jobid+1))
done
export OMP_NUM_THREADS=1
max_jobs=8
total_jobs=$jobid
parallel_run
}

function compute_full_imap_cluster() {
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
imap_file="${out_dir}/imap-${bead_type}_${name}_rc_${rc}_t_${tstart}-${tend}.dat"
imap_clust_file="${out_dir}/imap-cluster-${bead_type}_${name}_rc_${rc}_fq_${fq}_nc_${nc}_t_${tstart}-${tend}.dat"
echo "$imap_clust_exe $fq $nc 1 $bead_list $imap_file $imap_clust_file"
$imap_clust_exe $fq $nc 1 $bead_list $imap_file $imap_clust_file
}

#compute_indv_imap
#compute_indv_imap_cluster
compute_full_imap
#compute_full_imap_cluster
