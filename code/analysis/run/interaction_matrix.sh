#!/bin/bash

if [[ "$#" != 8 ]]; then
    echo "Usage: interaction_matrix.sh thres run_start run_end run_inc trial bed_file in_dir out_dir"
    exit 1
fi

thres=$1
run_start=$2
run_end=$3
run_inc=$4
trial=$5
bed_file=$6
in_dir=$7
out_dir=$8

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

intrxn_exe="../bin/analysis/exe/imap/interaction_matrix"

cell="GM12878"
genome="hg19"
chrom="chr19"
chr=${chrom:3}
tstart=20000000
tend=50000000
tinc=200000

chromsizes="../../../genome/${genome}/${genome}.chrom.sizes"
start=0
end=$(awk -v cc=$chrom '{if($1==cc){print $2}}' $chromsizes)
bp_per_bead=1000

name="${cell}_${genome}_chr_${chr}"
out_file="${out_dir}/interaction_${name}_run_${run_start}-${run_end}-${run_inc}_rc_${thres}_${trial}.dat"

# Get the bead file (for DNase)
bead_file="beads.dat.tmp"
bead_list="${in_dir}/${name}_run_${run_start}/bead_list.dat"
echo $bead_list
awk '{if(NF==6){print $1-1,$3}}' $bead_list > $bead_file

traj_files=""
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    traj_files="${traj_files} ${in_dir}/${name}_run_${run}/*.lammpstrj"
done

echo "$intrxn_exe $chrom $start $end $bp_per_bead $thres $tstart $tend $tinc $bed_file $bead_file $out_file $traj_files"
$intrxn_exe $chrom $start $end $bp_per_bead $thres $tstart $tend $tinc $bed_file $bead_file $out_file $traj_files
