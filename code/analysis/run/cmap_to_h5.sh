#!/bin/bash
# A script to convert contact maps in gnuplot friendly format into the h5
# format via the intermediate hicpro format

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Script to convert contact maps in gnuplot friendly format into "
    echo "the h5 format that can be read by HiCExplorer"
    echo "Usage: cmap_to_h5.sh in_file out_file"
    exit 1
fi

in_file=$1
out_file=$2

genome="hg19"
chrom="chr19" #"chr19"
#chrom="chr14"
chr=${chrom:3}
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
nbp=$(awk -v ch=$chrom '{if($1==ch) {print $2}}' $chromsizes)
start_bp=0
end_bp=$nbp
#start_bp=50000000 #6500000 #55000000 #6500000
#end_bp=60000000 #16500000 #65000000 #16500000 # exclusive
bp_per_bead=1000
bead_per_bin=10
bp_per_bin=$(bc <<< "$bp_per_bead*$bead_per_bin")

hic_pro_bin="${out_file}.hicpro.bed"
hic_pro_mat="${out_file}.hicpro.matrix"

# Create the bin index file
echo "Creating hicpro matrix bin index ..."
awk -v cc=$chrom -v sbp=$start_bp -v ebp=$end_bp -v bppb=$bp_per_bin 'BEGIN{
OFS="\t"
bin=1
for (i=sbp;i<ebp;i+=bppb) {
print cc,i,((i+bppb)>ebp?ebp:i+bppb),bin; bin++;
}}' > $hic_pro_bin

# Convert the contact map file (selecting the region of interest)
echo "Converting from gnuplot to hicpro format ..."
awk -v sbp=$start_bp -v ebp=$end_bp -v bppb=$bp_per_bin \
    -v ff=$hic_pro_bin 'BEGIN{
while ( (getline<ff)>0 ) {bin[$2]=$4;}
}{
if (NF==3 && substr($1,1,1)!="#" && $1!="" && $1>=sbp && $1<ebp && 
$2>=sbp && $2<ebp && $1<=$2) {OFS="\t";
ii=int($1-0.5*bppb);
jj=int($2-0.5*bppb);
print bin[ii],bin[jj],$3
}}' $in_file | sort -k1,1n -k2,2n > $hic_pro_mat

echo "Converting from hicpro to h5 format ..."
source activate myPy36
hicConvertFormat --matrices $hic_pro_mat --bedFileHicpro $hic_pro_bin \
                 --inputFormat hicpro --outFileName $out_file \
                 --outputFormat h5
conda deactivate

echo "Cleaning up ..."
rm $hic_pro_mat $hic_pro_bin
