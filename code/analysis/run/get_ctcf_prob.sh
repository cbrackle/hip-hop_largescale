#!/bin/bash
# get_ctcf_prob.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 7 ]]; then
    echo "Usage: chrom start_bp end_bp bp_per_bead ctcf_prob_file" \
	"repeat_index_file out_file"
    exit 1
fi

chrom=$1
start_bp=$2
end_bp=$3
bp_per_bead=$4
ctcf_prob_file=$5
repeat_index_file=$6
out_file=$7
for_bigwig=${out_file%.*}_forward.bigWig
back_bigwig=${out_file%.*}_backward.bigWig
both_bigwig=${out_file%.*}_both.bigWig

# Extract the first replica starting and ending index
start_bead=$(awk '{if(NR==1){print $2}}' $repeat_index_file)
end_bead=$(awk '{if(NR==1){print $3}}' $repeat_index_file)

awk -v sbp=$start_bp -v sb=$start_bead -v eb=$end_bead -v bppb=$bp_per_bead '
BEGIN{
  nb=eb-sb+1
  for (i=0;i<nb;i++){
    forward[i]=0
    backward[i]=0
    both[i]=0
  }
}{
  if ($1>=sb && $1<=eb) {
    forward[$1-sb]=$2
    backward[$1-sb]=$3
    both[$1-sb]=$4
  }
}END{
  for (i=0;i<nb;i++) {
    print (i+0.5)*bppb+sbp,forward[i],backward[i],both[i]
  }
}' $ctcf_prob_file > $out_file


# Convert the output file into bigwig format
bigwig_sh="${sh_dir}/dat_to_bigwig.sh"
$bigwig_sh $chrom $start_bp $end_bp $bp_per_bead 1 4 $out_file $both_bigwig
$bigwig_sh $chrom $start_bp $end_bp $bp_per_bead 1 3 $out_file $back_bigwig
$bigwig_sh $chrom $start_bp $end_bp $bp_per_bead 1 2 $out_file $for_bigwig

