#!/bin/bash
# cmap_fixed_read_stochastic.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ "$#" != 9 ]]; then
    echo "Usage: cmap_fixed_read_stochastic.sh thres nreads multiplier run_start run_end run_inc trial in_dir out_dir"
    exit 1
fi

thres=$1
nreads=$2
multiplier=$3
run_start=$4
run_end=$5
run_inc=$6
trial=$7
in_dir=$8
out_dir=$9

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

#cmap_exe="${sh_dir}/../bin/analysis/exe/cmap/cmap_fixed_read_stochastic"
cmap_exe="${sh_dir}/../bin/analysis/exe/new_cmap/cmap_fixedread.1"
#cmap_exe="${sh_dir}/../bin/analysis/exe/new_cmap/cmap_fixedread"

#cell="GM12878"
cell="HUVEC"
genome="hg19"
#chrom="chr18" #"chr19"
chrom="chr14"
chr=${chrom:3}
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
nbp=$(awk -v ch=$chrom '{if($1==ch) {print $2}}' $chromsizes)
regstart=50000000 #55000000 #6500000
regend=60000000 #65000000 #16500000
#regstart=0
#regend=$nbp
tstart=20000000 # 20000000
tend=50000000 # 50000000
tinc=200000
beads_per_bin=10 # 10
bp_per_bead=1000
npolybeads=10000 # 59129
#npolybeads=$(python -c "
#import math
#print int(math.ceil(${nbp}/float(${bp_per_bead})))")
norm=1 # Normalise the contacts or not
use_genome_coords=1 # Output map in bin index or genome coords
use_header=1 # Output a header containing the run info
range=$npolybeads

# Probability kernel for counting contacts
kernel_func="tanh"
kernel_args="1.0"

# Generate a 4-byte random integer using urandom
function get_rand(){
    rand=$(python -c "print $(od -vAn -N4 -tu4 < /dev/urandom)")
    echo $rand
}
seed=$(get_rand)

#name="${cell}_${genome}_chr_${chr}"
name="${cell}_${genome}_chr_${chr}_${regstart}-${regend}"
out_file="${out_dir}/cmap-fixedread_${name}_run_${run_start}-${run_end}-${run_inc}_rc_${thres}_n_${multiplier}_nreads_${nreads}_t_${tstart}-${tend}_trial_${trial}.dat"

traj_files=""
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    #traj_files="${traj_files} ${in_dir}/${name}_run_${run}/*.lammpstrj"
    traj_files="${traj_files} ${in_dir}/config_${name}_run_${run}.lammpstrj"
done
#traj_files="${traj_files} ${in_dir}/*.lammpstrj"

#echo "$cmap_exe $npolybeads $beads_per_bin $regstart $bp_per_bead $nreads $multiplier $range $seed $thres $tstart $tend $tinc $norm $use_genome_coords $out_file $traj_files"
#$cmap_exe $npolybeads $beads_per_bin $regstart $bp_per_bead $nreads $multiplier $range $seed $thres $tstart $tend $tinc $norm $use_genome_coords $out_file $traj_files
echo "$cmap_exe $npolybeads $beads_per_bin $regstart $bp_per_bead $nreads $multiplier $range $seed $thres $tstart $tend $tinc $norm $kernel_func $kernel_args $use_genome_coords $use_header $out_file $traj_files"
$cmap_exe $npolybeads $beads_per_bin $regstart $bp_per_bead $nreads $multiplier $range $seed $thres $tstart $tend $tinc $norm $kernel_func $kernel_args $use_genome_coords $use_header $out_file $traj_files
