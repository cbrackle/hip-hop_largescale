#!/bin/bash
# cmap_bound_dir_corr.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 3 ]]; then
    echo "Usage: cmap_bound_dir_corr.sh sim_map exp_map out_dir"
    exit
fi

sim_map=$1
exp_map=$2
out_dir=$3

cell="GM12878"
genome="hg19"
chrom="chr19"
chr=${chrom:3}

thres=500000
window=5000000
step=500000

mat_start_bp=0
mat_end_bp=59128983
#start_bp=48000000
#end_bp=58000000
start_bp=0
start_bp=27740000
#end_bp=24630000
end_bp=59128983
bp_per_bin=10000

# Required programs and scripts
exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
pcorr_py="${src_path}/stats/pearson_correlation.py"
scorr_py="${src_path}/stats/spearman_correlation.py"
cmap_bound_dir_exe="${exe_path}/cmap/cmap_boundary_direction"

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

full_pcorr_file="${out_dir}/cmap-bound-dir-roll-pcorr_thres_${thres}_win_${window}_step_${step}.dat"
full_scorr_file="${out_dir}/cmap-bound-dir-roll-scorr_thres_${thres}_win_${window}_step_${step}.dat"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

cmd=()
jobid=0

# Determine the window regions
function get_bound_signal() {
winhalf=$(python -c "print $window/2")
win_sbp=$start_bp
win_ebp=$(python -c "print $start_bp+$window")
win_mbp=$(python -c "print $start_bp+$winhalf")

while (( $(python -c "print int($win_ebp < $end_bp)") ))
do
    sim_bound_file="${out_dir}/cmap-bound-dir_sim_${thres}_${win_sbp}-${win_ebp}.dat"
    exp_bound_file="${out_dir}/cmap-bound-dir_exp_${thres}_${win_sbp}-${win_ebp}.dat"
    pcorr_file="${out_dir}/cmap-bound-dir-pcorr_${thres}_${win_sbp}-${win_ebp}.dat"
    scorr_file="${out_dir}/cmap-bound-dir-scorr_${thres}_${win_sbp}-${win_ebp}.dat"
    cmd[$jobid]="$cmap_bound_dir_exe $mat_start_bp $mat_end_bp $win_sbp $win_ebp $bp_per_bin 0 $thres 1 $sim_map $sim_bound_file"
    jobid=$(($jobid+1))
    cmd[$jobid]="$cmap_bound_dir_exe $mat_start_bp $mat_end_bp $win_sbp $win_ebp $bp_per_bin 0 $thres 1 $exp_map $exp_bound_file"
    jobid=$(($jobid+1))
    echo $win_sbp $win_mbp $win_ebp
    win_sbp=$(python -c "print $win_sbp+$step")
    win_ebp=$(python -c "print $win_sbp+$window")
    win_mbp=$(python -c "print $win_sbp+$winhalf")
done

max_jobs=64
total_jobs=$jobid
parallel_run
}

function get_bound_corr() {
winhalf=$(python -c "print $window/2")
win_sbp=$start_bp
win_ebp=$(python -c "print $start_bp+$window")
win_mbp=$(python -c "print $start_bp+$winhalf")

> $full_pcorr_file
> $full_scorr_file

while (( $(python -c "print int($win_ebp < $end_bp)") ))
do
    sim_bound_file="${out_dir}/cmap-bound-dir_sim_${thres}_${win_sbp}-${win_ebp}.dat"
    exp_bound_file="${out_dir}/cmap-bound-dir_exp_${thres}_${win_sbp}-${win_ebp}.dat"
    pcorr_file="${out_dir}/cmap-bound-dir-pcorr_${thres}_${win_sbp}-${win_ebp}.dat"
    scorr_file="${out_dir}/cmap-bound-dir-scorr_${thres}_${win_sbp}-${win_ebp}.dat"
    python $pcorr_py $(wc -l $sim_bound_file | awk '{print $1}') -1 3 $sim_bound_file $exp_bound_file $pcorr_file
    python $scorr_py $(wc -l $sim_bound_file | awk '{print $1}') -1 3 $sim_bound_file $exp_bound_file $scorr_file
    echo $win_mbp $(awk '{if(NR==1){print}}' $pcorr_file) >> $full_pcorr_file
    echo $win_mbp $(awk '{if(NR==1){print}}' $scorr_file) >> $full_scorr_file
    win_sbp=$(python -c "print $win_sbp+$step")
    win_ebp=$(python -c "print $win_sbp+$window")
    win_mbp=$(python -c "print $win_sbp+$winhalf")
done
}

get_bound_signal
get_bound_corr
