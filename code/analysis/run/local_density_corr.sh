#!/bin/bash
# local_density_corr.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: local_density_corr.sh sim_dir den_dir"
    exit 1
fi

sim_dir=$1
den_dir=$2

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

# Centromere region
cytoband="${sh_dir}/../../../genome/${genome}/${genome}.cytoBand.txt"
cen_start=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==1){print $2}}')
cen_end=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==2){print $3}}')
echo $cen_start $cen_end

rc=5.0 # Cutoff radius

if [[ ! -d $sim_dir ]]; then
    echo "Error: cannot find the directory " $sim_dir
    exit 1
fi

if [[ ! -d $den_dir ]]; then
    echo "Error: cannot find the directory " $den_dir
    exit 1
fi

exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
sh_path="${sh_dir}/"
avg_py="${src_path}/stats/average_multi_files.py"
dat_to_bigwig="${sh_path}/dat_to_bigwig.sh"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# File names
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
raw_bead_list="${sim_dir}/raw/${name}_run_1/bead_list.dat"
bead_list="${den_dir}/bead_list.dat"
den_avg_file="${den_dir}/density-avg_${name}.dat"
den_bw="${den_dir}/density_${name}.bigWig"
den_corr_file="${den_dir}/density-corr_${name}.dat"
den_atac_file="${den_dir}/density-ATAC_${name}.dat"
den_k9me3_file="${den_dir}/density-H3K9me3_${name}.dat"
den_k27ac_file="${den_dir}/density-H3K27ac_${name}.dat"
den_k27me3_file="${den_dir}/density-H3K27me3_${name}.dat"
den_unmark_file="${den_dir}/density-unmark_${name}.dat"

# Average the density results
function average_density() {
python $avg_py 0 1 -1 -1 $den_avg_file ${den_dir}/density_${name}_run_*.dat
}

# Convert data to bigwig files
function to_bigwig() {
$dat_to_bigwig $chrom $start_bp $end_bp $bp_per_bead 1 2 $den_avg_file $den_bw
}

# Map bead density to bead type
function map_density_to_bead_type() {
cp $raw_bead_list $den_dir
tail -n+3 $bead_list > ${bead_list}.tmp
mv ${bead_list}.tmp $bead_list
paste -d" " $bead_list $den_avg_file > $den_corr_file

# Remove the centromere region
awk -v scen=$cen_start -v ecen=$cen_end '{
if ($2<scen || $2>ecen) {print}}' $den_corr_file > ${den_corr_file}.tmp
mv ${den_corr_file}.tmp $den_corr_file

# Output beads
awk -v atac=$den_atac_file -v k9me3=$den_k9me3_file -v k27ac=$den_k27ac_file \
    -v k27me3=$den_k27me3_file -v unmark=$den_unmark_file '{
  if ($3==1) {print $8 > atac}
  if ($4==1) {print $8 > k9me3}
  if ($5==1) {print $8 > k27ac}
  if ($6==1) {print $8 > k27me3}
  if ($3==0 && $4==0 && $5==0 && $6==0) {print $8 > unmark}
}' $den_corr_file
#rm $den_corr_file
}

# Display in gnuplot
function plot() {
plot_gp="${den_dir}/density-plot_${name}.gp"
echo "
stats '${den_atac_file}' u 1
natac = STATS_records
matac = STATS_median
stats '${den_k27ac_file}' u 1
nk27ac = STATS_records
mk27ac = STATS_median
stats '${den_k27me3_file}' u 1
nk27me3 = STATS_records
mk27me3 = STATS_median
stats '${den_k9me3_file}' u 1
nk9me3 = STATS_records
mk9me3 = STATS_median
stats '${den_unmark_file}' u 1
nunmark = STATS_records
munmark = STATS_median

set xrange[0.0:0.6]
set table 'den_atac'
plot '${den_atac_file}' u 1:(1/natac) smooth kdensity
set table 'den_k27ac'
plot '${den_k27ac_file}' u 1:(1/nk27ac) smooth kdensity
set table 'den_k27me3'
plot '${den_k27me3_file}' u 1:(1/nk27me3) smooth kdensity
set table 'den_k9me3'
plot '${den_k9me3_file}' u 1:(1/nk9me3) smooth kdensity
set table 'den_unmark'
plot '${den_unmark_file}' u 1:(1/nunmark) smooth kdensity

unset table

set xrange [0.0:200.0]
set yrange [0.0:0.6]

# Set positions of individual violin plots
atac_pos=20.0
k27ac_pos=60.0
k27me3_pos=100.0
k9me3_pos=140.0
unmark_pos=180.0
buf=0.0

set border 15 lw 1
unset key

p 'den_atac' u (atac_pos+\$2):1 w filledcurves x1=atac_pos-buf ls 1,\
'den_atac' u (atac_pos-\$2):1 w filledcurve x1=atac_pos+buf ls 1,\
'den_k27ac' u (k27ac_pos+\$2):1 w filledcurves x1=k27ac_pos-buf ls 2,\
'den_k27ac' u (k27ac_pos-\$2):1 w filledcurve x1=k27ac_pos+buf ls 2,\
'den_k27me3' u (k27me3_pos+\$2):1 w filledcurves x1=k27me3_pos-buf ls 3,\
'den_k27me3' u (k27me3_pos-\$2):1 w filledcurve x1=k27me3_pos+buf ls 3,\
'den_k9me3' u (k9me3_pos+\$2):1 w filledcurves x1=k9me3_pos-buf ls 4,\
'den_k9me3' u (k9me3_pos-\$2):1 w filledcurve x1=k9me3_pos+buf ls 4,\
'den_unmark' u (unmark_pos+\$2):1 w filledcurves x1=unmark_pos-buf ls 5,\
'den_unmark' u (unmark_pos-\$2):1 w filledcurve x1=unmark_pos+buf ls 5

" > $plot_gp
gnuplot -p $plot_gp
rm den_atac den_k27ac den_k27me3 den_k9me3 den_unmark
}

average_density
to_bigwig
map_density_to_bead_type
plot
