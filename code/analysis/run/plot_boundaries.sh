#!/bin/bash
# plot_boundaries.sh
# A script to plot TAD boundaries

if [[ $# != 3 ]]; then
    echo "Usage: plot_boundaries.sh boundary_file cmap_file out_dir"
    exit 1
fi

boundary_file=$1
cmap_file=$2
out_dir=$3

start_bp=6500000
end_bp=16500000
cbmin=0.1
cbmax=10000

plot_gp="${out_dir}/${boundary_file}.gp"
echo "
set logscale cb 10
set palette defined (0 'white', 0.333 'yellow', 0.666 'red', 1 'black')
set cbrange[${cbmin}:${cbmax}]
" > $plot_gp
# Add the boundary lines
awk -v sbp=$start_bp -v ebp=$end_bp 'BEGIN{
  pt[0] = sbp
  n = 1
}{
  if($2 >= 0){pt[n]=$1;n+=1}
} END {
  pt[n] = ebp
  n += 1
  for (i = 0; i < n-1; i++) {
    printf("set arrow from %.5f,%.5f to %.5f,%.5f nohead front\n",\
           pt[i],pt[i],pt[i],pt[i+1])
    printf("set arrow from %.5f,%.5f to %.5f,%.5f nohead front\n",\
           pt[i],pt[i+1],pt[i+1],pt[i+1])
  }
}' $boundary_file >> $plot_gp
echo "
p [${start_bp}:${end_bp}][${start_bp}:${end_bp}]'${cmap_file}' u 1:2:3 w image
" >> $plot_gp

#gnuplot $plot_gp
