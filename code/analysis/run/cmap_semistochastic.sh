#!/bin/bash

if [[ "$#" != 8 ]]; then
    echo "Usage: cmap_semistochastic.sh thres multiplier run_start run_end run_inc trial in_dir out_dir"
    exit 1
fi

thres=$1
multiplier=$2
run_start=$3
run_end=$4
run_inc=$5
trial=$6
in_dir=$7
out_dir=$8

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

cmap_exe="../bin/analysis/exe/cmap/cmap_semistochastic"

cell="GM12878"
genome="hg19"
chrom="chr19"
chr=${chrom:3}
regstart=6500000
regend=16500000
tstart=20000000 # 20000000
tend=38000000 # 50000000
tinc=200000
beads_per_bin=10
npolybeads=10000 # 59129
bp_per_bead=1000
norm=1 # Normalise the contacts or not
use_genome_coords=1 # Output map in bin index or genome coords
range=$npolybeads

# Generate a 4-byte random integer using urandom
function get_rand(){
    rand=$(python -c "print $(od -vAn -N4 -tu4 < /dev/urandom)")
    echo $rand
}
seed=$(get_rand)

#name="${cell}_${genome}_chr_${chr}"
name="${cell}_${genome}_chr_${chr}_${regstart}-${regend}"
out_file="${out_dir}/cmap-semistochastic_${name}_run_${run_start}-${run_end}-${run_inc}_rc_${thres}_n_${multiplier}_${tstart}-${tend}_${trial}.dat"

traj_files=""
#for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
#do
#    traj_files="${traj_files} ${in_dir}/${name}_run_${run}/*.lammpstrj"
#done
traj_files="${traj_files} ${in_dir}/*.lammpstrj"

echo "$cmap_exe $npolybeads $beads_per_bin $regstart $bp_per_bead $multiplier $range $seed $thres $tstart $tend $tinc $norm $use_genome_coords $out_file $traj_files"
$cmap_exe $npolybeads $beads_per_bin $regstart $bp_per_bead $multiplier $range $seed $thres $tstart $tend $tinc $norm $use_genome_coords $out_file $traj_files
