#!/bin/bash
# msd.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 5 ]]; then
    echo "Usage: msd.sh run_start run_end run_inc in_dir out_dir"
    exit 1
fi

run_start=$1
run_end=$2
run_inc=$3
in_dir=$4
out_dir=$5

cell="GM12878"
chrom="chr18"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
tstart=20000000
tend=50000000
tinc=200000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")
#use_genome_coords=1

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
avg_py="${src_path}/stats/average_multi_files_fields.py"
msd_exe="${exe_path}/msd"

# File names
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
msd_avg_dir="${out_dir}/msd_avg"
msd_avg_file="${msd_avg_dir}/msd_${name}_avg.dat"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# Compute the msd
function compute_msd() {
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    run_name="${name}_run_${run}"
    pos_file="${in_dir}/config_${run_name}.lammpstrj"
    msd_file="${out_dir}/msd_${run_name}.dat"
    cmd[$jobid]="$msd_exe $nbeads $tstart $tend $tinc $pos_file ${msd_file}"
    jobid=$(($jobid+1))
done
export OMP_NUM_THREADS=1
max_jobs=20 #8
total_jobs=$jobid
parallel_run
}

# Average the msd results
function average_msd() {
mkdir -p $msd_avg_dir
python $avg_py $(($nbeads+1)) 0 -1 $msd_avg_file ${out_dir}/msd_${name}_run_*.dat
}

function split_avg_msd() {
mkdir -p $msd_avg_dir
msd_split_sh="${msd_avg_dir}/msd_split.sh"
echo \
"#!/bin/bash
avg_col=\$1
std_col=\$2
ste_col=\$3
msd_avg_file=\$4
out_file=\$5
awk -v avgcol=\$avg_col -v stdcol=\$std_col -v stecol=\$ste_col '{
print \$1,\$avgcol,\$stdcol,\$stecol'} \$msd_avg_file > \$out_file
" > $msd_split_sh
cmd=()
jobid=0
for (( i=1; $i<=$nbeads; i++ ))
do
    echo "Doing $i"
    file="${msd_avg_dir}/msd_${name}_avg_bead_${i}.dat"
    col=$(bc <<< "($i-1)*3+2")
    cmd[$jobid]="bash $msd_split_sh $col $(($col+1)) $(($col+2)) $msd_avg_file $file"
    jobid=$(($jobid+1))
done
max_jobs=64
total_jobs=$jobid
parallel_run
}

#compute_msd
#average_msd
split_avg_msd
