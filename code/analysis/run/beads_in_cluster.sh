#!/bin/bash
# beads_in_cluster.sh
# A script which finds the number of selected beads in each cluster 
# (TAD/clique/cluster)

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 3 ]]; then
    echo "Usage beads_in_cluster.sh bead_file cluster_file out_file"
    exit 1
fi

bead_file=$1
cluster_file=$2
out_file=$3

if [[ ! -f $bead_file ]]; then
    echo "Error: cannot find the file ${bead_file}"
    exit 1
fi

if [[ ! -f $cluster_file ]]; then
    echo "Error: cannot find the file ${cluster_file}"
    exit 1
fi

out_dir=$(dirname $out_file)
if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
bp_per_bead=1000

# Read the bead file and the cluster file
# Assume that the bead indices are one-based
awk -v cfile=$cluster_file -v sbp=$start_bp -v bppb=$bp_per_bead 'BEGIN {
# Determine the boundary of each region
n = 0
while ((getline < cfile) > 0) {
  start[n] = int(($1-sbp)/bppb)
  end[n] = int(($2-sbp)/bppb)
  count[n] = 0
  n++
}} {
# Find where each bead belongs to (lazy inefficient way)
bead = int($1-1)
for (i = 0; i < n; i++) {
  if (bead >= start[i] && bead <= end[i]) {
    count[i]++
  }
}} END {
# Output the result
for (i = 0; i < n; i++) {
  print (i+1), count[i]
}}' $bead_file > $out_file
