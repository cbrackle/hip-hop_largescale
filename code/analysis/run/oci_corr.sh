#!/bin/bash
# oci_corr.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: oci_corr.sh sim_dir out_dir"
    exit 1
fi

sim_dir=$1
out_dir=$2

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
#thres=1000000 #500000 2000000
bp_per_bin=10000

# Centromere region
cytoband="${sh_dir}/../../../genome/${genome}/${genome}.cytoBand.txt"
cen_start=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==1){print $2}}')
cen_end=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==2){print $3}}')
echo $cen_start $cen_end

if [[ ! -d $sim_dir ]]; then
    echo "Error: cannot find the directory " $sim_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

sh_path="${sh_dir}"
exe_path="${sh_dir}/../bin/analysis/exe/"
main_exe_path="${sh_dir}/../../sim/bin/main/exe/"
src_path="${sh_dir}/../src/"
to_genome_coords_exe="${exe_path}/convert_to_genome_coords"
dat_to_bigwig="${sh_path}/dat_to_bigwig.sh"
data2beads_exe="${main_exe_path}/data_to_beads"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# File names
#name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}_sd_${thres}"
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
raw_bead_list="${sim_dir}/raw/${name}_run_1/bead_list.dat"
bead_list="${out_dir}/bead_list.dat"
oci_file="${out_dir}/oci-sim_${name}.dat"
oci_bw="${out_dir}/oci-sim_${name}.bigWig"
oci_corr_file="${out_dir}/oci-corr_${name}.dat"
oci_atac_file="${out_dir}/oci-ATAC_${name}.dat"
oci_k27ac_file="${out_dir}/oci-H3K27ac_${name}.dat"
oci_k27me3_file="${out_dir}/oci-H3K27me3_${name}.dat"
oci_k9me3_file="${out_dir}/oci-H3K9me3_${name}.dat"
oci_unmark_file="${out_dir}/oci-unmark_${name}.dat"

# Convert data to bigwig files
function to_bigwig() {
$dat_to_bigwig $chrom $start_bp $end_bp $bp_per_bin 1 4 $oci_file $oci_bw
}

# Create bead type map for 10kb resolution
function get_bead_type() {
path2data="${sh_dir}/../../../data_GM12878/${genome}/"
k27ac_bed="${path2data}/histone/H3K27ac_epicpeaks.bed"
k27me3_bed="${path2data}/histone/H3K27me3_epicpeaks.bed"
k9me3_bed="${path2data}/histone/H3K9me3_epicpeaks.bed"
atac_bed="${path2data}/atac/ATAC_finalpeaks.bed"
config_file="${out_dir}/config_${name}.txt"
echo "${chrom}
${start_bp}
${end_bp}
${bp_per_bin}
ACT ${atac_bed}
OPEN ${k27ac_bed}
POLY ${k27me3_bed}
HET ${k9me3_bed}" > $config_file
$data2beads_exe $config_file $out_dir
}

# Map bead msd to baed type
function map_oci_to_bead_type() {
#cp $raw_bead_list $out_dir
tail -n+3 $bead_list > ${bead_list}.tmp
mv ${bead_list}.tmp $bead_list
oci_file_tmp=$oci_file.tmp
awk '{print $1,$4}' $oci_file > $oci_file_tmp
paste -d" " $bead_list $oci_file_tmp > $oci_corr_file
rm $oci_file_tmp

# Remove the centromere region
awk -v scen=$cen_start -v ecen=$cen_end '{
if ($2<scen || $2>ecen) {print}}' $oci_corr_file > ${oci_corr_file}.tmp
mv ${oci_corr_file}.tmp $oci_corr_file

# Output beads
awk -v atac=$oci_atac_file -v k9me3=$oci_k9me3_file -v k27ac=$oci_k27ac_file -v k27me3=$oci_k27me3_file -v unmark=$oci_unmark_file '{
  if ($3==1) {print $8 > atac}
  if ($4==1) {print $8 > k9me3}
  if ($5==1) {print $8 > k27ac}
  if ($6==1) {print $8 > k27me3}
  if ($3==0 && $4==0 && $5==0 && $6==0) {print $8 > unmark}
}' $oci_corr_file
#rm $oci_corr_file
}

# Display in gnuplot
function plot() {
min=-8.5
max=-5.5
width=2.0
plot_gp="${out_dir}/oci_${name}.gp"
echo "
stats '${oci_atac_file}' u 1
natac = STATS_records
matac = STATS_median
stats '${oci_k27ac_file}' u 1
nk27ac = STATS_records
mk27ac = STATS_median
stats '${oci_k27me3_file}' u 1
nk27me3 = STATS_records
mk27me3 = STATS_median
stats '${oci_k9me3_file}' u 1
nk9me3 = STATS_records
mk9me3 = STATS_median
stats '${oci_unmark_file}' u 1
nunmark = STATS_records
munmark = STATS_median

set xrange[${min}:${max}]
width = ${width}

set table 'oci_atac'
plot '${oci_atac_file}' u 1:(1/natac) smooth kdensity
set table 'oci_k27ac'
plot '${oci_k27ac_file}' u 1:(1/nk27ac) smooth kdensity
set table 'oci_k27me3'
plot '${oci_k27me3_file}' u 1:(1/nk27me3) smooth kdensity
set table 'oci_k9me3'
plot '${oci_k9me3_file}' u 1:(1/nk9me3) smooth kdensity
set table 'oci_unmark'
plot '${oci_unmark_file}' u 1:(1/nunmark) smooth kdensity

unset table

set xrange [0:10]
set yrange [${min}:${max}]

# Set positions of individual violin plots
atac_pos=1
k27ac_pos=3
k27me3_pos=5
k9me3_pos=7
unmark_pos=9
buf=0.0

set border 15 lw 1
unset key

p 'oci_atac' u (atac_pos+\$2/width):1 w filledcurves x1=atac_pos-buf ls 1,\
'oci_atac' u (atac_pos-\$2/width):1 w filledcurve x1=atac_pos+buf ls 1,\
'oci_k27ac' u (k27ac_pos+\$2/width):1 w filledcurves x1=k27ac_pos-buf ls 2,\
'oci_k27ac' u (k27ac_pos-\$2/width):1 w filledcurve x1=k27ac_pos+buf ls 2,\
'oci_k27me3' u (k27me3_pos+\$2/width):1 w filledcurves x1=k27me3_pos-buf ls 3,\
'oci_k27me3' u (k27me3_pos-\$2/width):1 w filledcurve x1=k27me3_pos+buf ls 3,\
'oci_k9me3' u (k9me3_pos+\$2/width):1 w filledcurves x1=k9me3_pos-buf ls 4,\
'oci_k9me3' u (k9me3_pos-\$2/width):1 w filledcurve x1=k9me3_pos+buf ls 4,\
'oci_unmark' u (unmark_pos+\$2/width):1 w filledcurves x1=unmark_pos-buf ls 5,\
'oci_unmark' u (unmark_pos-\$2/width):1 w filledcurve x1=unmark_pos+buf ls 5

" > $plot_gp
gnuplot -p $plot_gp
rm oci_atac oci_k27ac oci_k27me3 oci_k9me3 oci_unmark
}

get_bead_type
to_bigwig
map_oci_to_bead_type
plot
