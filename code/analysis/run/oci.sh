#!/bin/bash
# oci.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 3 ]]; then
    echo "Usage: oci.sh sim_map exp_map out_dir"
    exit 1
fi

sim_map=$1
exp_map=$2
out_dir=$3

cell="GM12878"
chrom="chr18"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bin=10000
use_genome_coords=1

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
oci_exe="${exe_path}/oci"
to_genome_coords_exe="${exe_path}/convert_to_genome_coords"

# File names
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# Compute the msd
function compute_oci() {
thres=1000000 #2000000 #500000
oci_sim_file="${out_dir}/oci-sim_${name}.dat"
oci_exp_file="${out_dir}/oci-exp_${name}.dat"
$oci_exe $start_bp $end_bp $bp_per_bin $thres $use_genome_coords $sim_map $oci_sim_file
$oci_exe $start_bp $end_bp $bp_per_bin $thres $use_genome_coords $exp_map $oci_exp_file
#$to_genome_coords_exe $start_bp $end_bp $bp_per_bin $oci_sim_file ${oci_sim_file}.tmp 4 0
#$to_genome_coords_exe $start_bp $end_bp $bp_per_bin $oci_exp_file ${oci_exp_file}.tmp 4 0
#mv ${oci_sim_file}.tmp $oci_sim_file
#mv ${oci_exp_file}.tmp $oci_exp_file
}

compute_oci
