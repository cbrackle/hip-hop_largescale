#!/bin/bash
# local_gyration_corr.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: local_gyration_corr.sh sim_dir gyr_dir"
    exit 1
fi

sim_dir=$1
gyr_dir=$2

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

# Centromere region
cytoband="${sh_dir}/../../../genome/${genome}/${genome}.cytoBand.txt"
cen_start=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==1){print $2}}')
cen_end=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==2){print $3}}')
echo $cen_start $cen_end

length=100 # Local segment length

if [[ ! -d $sim_dir ]]; then
    echo "Error: cannot find the directory " $sim_dir
    exit 1
fi

if [[ ! -d $gyr_dir ]]; then
    echo "Error: cannot find the directory " $gyr_dir
    exit 1
fi

exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
sh_path="${sh_dir}/"
avg_py="${src_path}/stats/average_multi_files.py"
dat_to_bigwig="${sh_path}/dat_to_bigwig.sh"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# File names
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
raw_bead_list="${sim_dir}/raw/${name}_run_1/bead_list.dat"
bead_list="${gyr_dir}/bead_list.dat"
gyr_avg_file="${gyr_dir}/gyr-avg_${name}.dat"
gyr_bw="${gyr_dir}/gyr_${name}.bigWig"
gyr_corr_file="${gyr_dir}/gyr-corr_${name}.dat"
gyr_atac_file="${gyr_dir}/gyr-ATAC_${name}.dat"
gyr_k27ac_file="${gyr_dir}/gyr-H3K27ac_${name}.dat"
gyr_k27me3_file="${gyr_dir}/gyr-H3K27me3_${name}.dat"
gyr_k9me3_file="${gyr_dir}/gyr-H3K9me3_${name}.dat"
gyr_unmark_file="${gyr_dir}/gyr-unmark_${name}.dat"

# Average the gyration results
function average_gyration() {
python $avg_py 0 1 -1 -1 $gyr_avg_file ${gyr_dir}/gyr_${name}_run_*.dat
}

# Convert data to bigwig files
function to_bigwig() {
$dat_to_bigwig $chrom $start_bp $end_bp $bp_per_bead 1 2 $gyr_avg_file $gyr_bw
}

# Map bead gyration to baed type
function map_gyration_to_bead_type() {
cp $raw_bead_list $gyr_dir
tail -n+3 $bead_list > ${bead_list}.tmp
mv ${bead_list}.tmp $bead_list
paste -d" " $bead_list $gyr_avg_file > $gyr_corr_file

# Remove the initial and final length/2 sections
half_length=$(python -c "print $length/2")
tail -n+$(($half_length+1)) $gyr_corr_file | head -n-$half_length > ${gyr_corr_file}.tmp
mv ${gyr_corr_file}.tmp $gyr_corr_file

# Remove the centromere region
awk -v scen=$cen_start -v ecen=$cen_end '{
if ($2<scen || $2>ecen) {print}}' $gyr_corr_file > ${gyr_corr_file}.tmp
mv ${gyr_corr_file}.tmp $gyr_corr_file

# Output beads
awk -v atac=$gyr_atac_file -v k9me3=$gyr_k9me3_file -v k27ac=$gyr_k27ac_file \
    -v k27me3=$gyr_k27me3_file -v unmark=$gyr_unmark_file '{
  if ($3==1) {print $8 > atac}
  if ($4==1) {print $8 > k9me3}
  if ($5==1) {print $8 > k27ac}
  if ($6==1) {print $8 > k27me3}
  if ($3==0 && $4==0 && $5==0 && $6==0) {print $8 > unmark}
}' $gyr_corr_file
#rm $gyr_corr_file
}

# Display in gnuplot
function plot() {
plot_gp="${gyr_dir}/gyr-plot_${name}.gp"
echo "
stats '${gyr_atac_file}' u 1
natac = STATS_records
matac = STATS_median
stats '${gyr_k27ac_file}' u 1
nk27ac = STATS_records
mk27ac = STATS_median
stats '${gyr_k27me3_file}' u 1
nk27me3 = STATS_records
mk27me3 = STATS_median
stats '${gyr_k9me3_file}' u 1
nk9me3 = STATS_records
mk9me3 = STATS_median
stats '${gyr_unmark_file}' u 1
nunmark = STATS_records
munmark = STATS_median

set xrange[3.0:7.0]
set table 'gyr_atac'
plot '${gyr_atac_file}' u 1:(1/natac) smooth kdensity
set table 'gyr_k27ac'
plot '${gyr_k27ac_file}' u 1:(1/nk27ac) smooth kdensity
set table 'gyr_k27me3'
plot '${gyr_k27me3_file}' u 1:(1/nk27me3) smooth kdensity
set table 'gyr_k9me3'
plot '${gyr_k9me3_file}' u 1:(1/nk9me3) smooth kdensity
set table 'gyr_unmark'
plot '${gyr_unmark_file}' u 1:(1/nunmark) smooth kdensity

unset table

set xrange [0.0:25.0]
set yrange [3.0:7.0]

# Set positions of individual violin plots
atac_pos=2.5
k27ac_pos=7.5
k27me3_pos=12.5
k9me3_pos=17.5
unmark_pos=22.5
buf=0.0

set border 15 lw 1
unset key

p 'gyr_atac' u (atac_pos+\$2):1 w filledcurves x1=atac_pos-buf ls 1,\
'gyr_atac' u (atac_pos-\$2):1 w filledcurve x1=atac_pos+buf ls 1,\
'gyr_k27ac' u (k27ac_pos+\$2):1 w filledcurves x1=k27ac_pos-buf ls 2,\
'gyr_k27ac' u (k27ac_pos-\$2):1 w filledcurve x1=k27ac_pos+buf ls 2,\
'gyr_k27me3' u (k27me3_pos+\$2):1 w filledcurves x1=k27me3_pos-buf ls 3,\
'gyr_k27me3' u (k27me3_pos-\$2):1 w filledcurve x1=k27me3_pos+buf ls 3,\
'gyr_k9me3' u (k9me3_pos+\$2):1 w filledcurves x1=k9me3_pos-buf ls 4,\
'gyr_k9me3' u (k9me3_pos-\$2):1 w filledcurve x1=k9me3_pos+buf ls 4,\
'gyr_unmark' u (unmark_pos+\$2):1 w filledcurves x1=unmark_pos-buf ls 5,\
'gyr_unmark' u (unmark_pos-\$2):1 w filledcurve x1=unmark_pos+buf ls 5
" > $plot_gp
gnuplot -p $plot_gp
rm gyr_atac gyr_k27ac gyr_k27me3 gyr_k9me3 gyr_unmark
}

average_gyration
to_bigwig
map_gyration_to_bead_type
plot
