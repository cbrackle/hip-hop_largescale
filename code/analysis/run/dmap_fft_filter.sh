#!/bin/bash

if [[ "$#" != 5 ]]; then
    echo "Usage: dmap_fft_filter.sh run_start run_end run_inc in_dir out_dir"
    exit 1
fi

run_start=$1
run_end=$2
run_inc=$3
in_dir=$4
out_dir=$5

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

fft_exe="${sh_dir}/../bin/analysis/exe/rca/dmap_fft_filter"
start_bead=349 #429
end_bead=739 #619
radius_factor=10

cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    echo "Doing $run"
    in_name="bead_${start_bead}-${end_bead}_run_${run}"
    dmap_file="${in_dir}/dmap_${in_name}.dat"
    out_name="bead_${start_bead}-${end_bead}_rf_${radius_factor}_run_${run}"
    fft_file="${out_dir}/dmap-fft_${out_name}.dat"
    out_file="${out_dir}/dmap-fft_${out_name}.out"
    cmd[$jobid]="$fft_exe $start_bead $end_bead $radius_factor $dmap_file $fft_file $out_file"
    jobid=$(($jobid+1))
done

# Parallel runs
max_jobs=8
total_jobs=$jobid
jobid=0
while (( $(bc <<< "$jobid < $total_jobs") ))
do
    for (( i=0; i<$max_jobs && $jobid < $total_jobs; i++))
    do
        echo "${cmd[jobid]} &"
        ${cmd[jobid]} &
        jobid=$(($jobid+1))
    done
    wait
done
