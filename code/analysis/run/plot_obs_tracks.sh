#!/bin/bash
# plot_obv_tracks.sh
# A script to plot the measured structural/dynamical observables linearly
# along the genome alongside ChIP-seq signals

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: plot_obs_tracks.sh in_dir out_dir"
    exit 1
fi

in_dir=$1
out_dir=$2

if [[ ! -f $out_dir ]]; then
    mkdir -p $out_dir
fi

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
tstart=20000000
tend=50000000
tinc=200000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

# Plot region
reg_start=0 #27740000 #32000000 #14000000 #2000000 #6500000
reg_end=24630000 #59128983 #42000000 #24000000 #12000000 #16500000

# Required programs
path_to_exe="${sh_dir}/../bin/analysis/exe/"
path_to_sh="${sh_dir}/"

# Files
# Genomic data sets
cell_data_path="${sh_dir}/../../../data_${cell}/${genome}/"
k9me3_bed="${cell_data_path}/histone/H3K9me3_epicpeaks.bed"
k27me3_bed="${cell_data_path}/histone/H3K27me3_epicpeaks.bed"
k27ac_bed="${cell_data_path}/histone/H3K27ac_epicpeaks.bed"
atac_bed="${cell_data_path}/atac/ATAC_finalpeaks.bed"
ctcf_rad21_bed="${cell_data_path}/ctcf/CTCF_Rad21_peaks_withstrand.bed"

# Simulation observable tracks
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
den_bw="${in_dir}/local_density/density_${name}.bigWig"
gyr_bw="${in_dir}/local_gyration/gyr_${name}.bigWig"
msd_mob_bw="${in_dir}/msd/msd_mobility_0-1000000/msd-mobility_${name}.bigWig"
msd_exp_bw="${in_dir}/msd/msd_exp_10000000-20000000/msd-exp-slope_${name}.bigWig"
oci_bw="${in_dir}/oci/oci_sd_1000000/oci-sim_${name}.bigWig"

ini_file="${out_dir}/obs-track_${name}_region_${reg_start}-${reg_end}.ini"
out_file="${out_dir}/obs-track_${name}_region_${reg_start}-${reg_end}.pdf"

function plot() {
echo "

[gyration]
file = ${gyr_bw}
file_type = bigwig
title = Local radius of gyration
color = black
height = 2
min_value = 3.0
max_value = 7.0
summary_method = max
type = line

[density]
file = ${den_bw}
file_type = bigwig
title = Local density
color = black
height = 2
min_value = 0.1
max_value = 0.5
summary_method = max
type = line

[oci]
file = ${oci_bw}
file_type = bigwig
title = OCI
color = black
height = 2
min_value = -7.5
max_value = -5.5
summary_method = max
type = line

[msd exponent]
file = ${msd_exp_bw}
file_type = bigwig
title = Anomalous exponent
color = black
height = 2
min_value = 0.0
max_value = 1.0
summary_method = max
type = line

[msd mobility]
file = ${msd_mob_bw}
file_type = bigwig
title = Mobility
color = black
height = 2
min_value = 50
max_value = 150
summary_method = max
type = line

[spacer]

[h3k9me3]
file = ${k9me3_bed}
display = collapsed
labels = false
title = H3K9me3
color = blue
border_color = blue
file_type = bed

[h3k27me3]
file = ${k27me3_bed}
display = collapsed
labels = false
title = H3K27me3
color = blue
border_color = blue
file_type = bed

[h3k27ac]
file = ${k27ac_bed}
display = collapsed
labels = false
title = H3K27ac
color = blue
border_color = blue
file_type = bed

[spacer]

[atac]
file = ${atac_bed}
display = collapsed
labels = false
title = ATAC
color = blue
border_color = blue
file_type = bed

[x-axis]
where = bottom
" > $ini_file

source ~/conda/activate_conda.sh
conda activate myPy36
pyGenomeTracks --tracks $ini_file --region ${chrom}:${reg_start}-${reg_end} -o $out_file
conda deactivate

rm $ini_file
}

plot
