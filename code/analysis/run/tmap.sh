#!/bin/bash
# tmap.sh
# A script to compute a matrix of simple matching coefficients between
# all pairs of ATAC beads' transcritpion activity time series

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ "$#" != 7 ]]; then
    echo "Usage: tmap.sh thres run_start run_end run_inc raw_dir pos_dir out_dir"
    exit 1
fi

thres=$1
run_start=$2
run_end=$3
run_inc=$4
raw_dir=$5
pos_dir=$6
out_dir=$7

if [[ ! -d $pos_dir ]]; then
    echo "Error: cannot find the directory " $pos_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

tmap_exe="${sh_dir}/../bin/analysis/exe/tmap/tmap"

cell="GM12878"
genome="hg19"
chrom="chr19"
chr=${chrom:3}
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
nbp=$(awk -v ch=$chrom '{if($1==ch) {print $2}}' $chromsizes)
#regstart=6500000
#regend=16500000
regstart=0
regend=$nbp
tstart=20000000 # 20000000
tend=30000000 # 50000000
tinc=200000
bp_per_bead=1000
npolybeads=$(python -c "
import math
print int(math.ceil(${nbp}/float(${bp_per_bead})))")

name="${cell}_${genome}_chr_${chr}_${regstart}-${regend}"
out_name="${name}_run_${run_start}-${run_end}-${run_inc}_rc_${thres}_t_${tstart}-${tend}"
prob_file="${out_dir}/tmap-prob_${out_name}.dat"
coeff_file="${out_dir}/tmap_${out_name}.dat"

# Retrieve ATAC bead and active protein files
raw_bead_list="${raw_dir}/${name}_run_1/bead_list.dat"
bead_atac_file="${out_dir}/bead_atac.dat"
awk '{if($1!="#" && NF==6 && $3==1){print $1}}' $raw_bead_list > $bead_atac_file
prot_act_file="${out_dir}/bead_act.dat"
nact=1500
act_start=$(($npolybeads+1))
act_end=$(($act_start+$nact-1))
awk -v start=$act_start -v end=$act_end 'BEGIN {
for (i=start;i<=end;i++) {print i}}' > $prot_act_file

traj_files=""
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    traj_files="${traj_files} ${pos_dir}/config_${name}_run_${run}.lammpstrj"
done

echo "$tmap_exe $thres $tstart $tend $tinc $bead_atac_file $prot_act_file $prob_file $coeff_file $traj_files"
$tmap_exe $thres $tstart $tend $tinc $bead_atac_file $prot_act_file $prob_file $coeff_file $traj_files
