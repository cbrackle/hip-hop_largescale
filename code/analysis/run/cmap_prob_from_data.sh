#!/bin/bash
# cmap_prob_from_data.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"


if [[ "$#" != 7 ]]; then
    echo "Usage: cmap_threshold.sh thres run_start run_end run_inc trial in_dir out_dir"
    exit 1
fi

thres=$1
run_start=$2
run_end=$3
run_inc=$4
trial=$5
in_dir=$6
out_dir=$7

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

prob_exe="${sh_dir}/../bin/analysis/exe/cmap/cmap_prob_from_data"

cell="GM12878"
genome="hg19"
chrom="chr19"
chr=${chrom:3}
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
nbp=$(awk -v ch=$chrom '{if($1==ch) {print $2}}' $chromsizes)
#regstart=6500000
#regend=16500000
regstart=0
regend=$nbp
tstart=20000000
tend=50000000
tinc=200000
beads_per_bin=10
bp_per_bead=1000
#npolybeads=10000 #10000 #59129
npolybeads=$(python -c "
import math
print int(math.ceil(${nbp}/float(${bp_per_bead})))")
use_genome_coords=0 # Output map in bin index or genome coords
range=$npolybeads

name="${cell}_${genome}_chr_${chr}_${regstart}-${regend}"
out_file="${out_dir}/cmap-threshold-prob_${name}_run_${run_start}-${run_end}-${run_inc}_rc_${thres}_t_${tstart}-${tend}_trial_${trial}.dat"

traj_files=""
#for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
#do
#    traj_files="${traj_files} ${in_dir}/${name}_run_${run}/*.lammpstrj"
#done
traj_files="${traj_files} ${in_dir}/*.lammpstrj"

echo "$prob_exe $npolybeads $beads_per_bin $bp_per_bead $thres $tstart $tend $tinc $use_genome_coords $out_file $traj_files"
$prob_exe $npolybeads $beads_per_bin $bp_per_bead $thres $tstart $tend $tinc $use_genome_coords $out_file $traj_files
