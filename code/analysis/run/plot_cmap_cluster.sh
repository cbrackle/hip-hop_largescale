#!/bin/bash
# plot_cmap_cluster.sh
# A script which creates the gnuplot script to plot cluster domains 
# on top of a contact map

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 3 ]]; then
    echo "Usage: plot_cmap_cluster.sh map_file clust_file plot_gp"
    exit 1
fi

map_file=$1
clust_file=$2 # Indices assumed to be zero-based
plot_gp=$3

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

# Plot region
reg_start=45000000 #32000000 #14000000 #2000000 #6500000
reg_end=55000000 #42000000 #24000000 #12000000 #16500000

bp_per_bead=1000
bead_per_bin=10
bp_per_bin=$(python -c "print $bp_per_bead*$bead_per_bin")

# Create the gnuplot script
# Load the palette file 
palette_pal=$(cat ${sh_dir}/../../../gnuplot-palettes/inferno.pal)
echo \
"
# Load the palette
$palette_pal

set palette negative
set logscale cb 10
set xrange [$start_bp:$end_bp]
set yrange [$start_bp:$end_bp]
set size square
set style rect back fs empty border lc rgb 'cyan'
" > $plot_gp

# Read the cluster file and identify the min and max bead of each cluster
awk -v sbp=$start_bp -v ebp=$end_bp -v bppb=$bp_per_bead '{
min = $1
max = $1
for (i = 2; i <= NF; i++) {
  if ($i < min) {min = $i}
  else if ($i > max) {max = $i}
}
# Convert to genome coords
minbp = bppb*min+sbp
maxbp = bppb*max+sbp

printf("set object %d rect from %d,%d to %d,%d lw 3 front\n",NR+500,minbp,minbp,maxbp,maxbp)
}' $clust_file >> $plot_gp

echo \
"

p '$map_file' u 1:2:3 w image
" >> $plot_gp

