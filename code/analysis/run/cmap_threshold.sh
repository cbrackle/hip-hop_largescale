#!/bin/bash
# cmap_threshold.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ "$#" != 7 ]]; then
    echo "Usage: cmap_threshold.sh thres run_start run_end run_inc trial in_dir out_dir"
    exit 1
fi

thres=$1
run_start=$2
run_end=$3
run_inc=$4
trial=$5
in_dir=$6
out_dir=$7

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

cmap_exe="${sh_dir}/../bin/analysis/exe/cmap/cmap_threshold"

cell="GM12878"
#cell="HUVEC"
genome="hg19"
#chrom="chr18" #"chr19"
chrom="chr22"
chr=${chrom:3}
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
nbp=$(awk -v ch=$chrom '{if($1==ch) {print $2}}' $chromsizes)
#regstart=50000000 #6500000 #55000000 #6500000
#regend=60000000 #16500000 #65000000 #16500000
#regstart=0
#regend=$nbp
regstart=17900000 #14300000 55700000 0
regend=$nbp #135006516 #51600000 #$nbp
nbp=$(python -c "print $regend-$regstart")
echo $nbp
tstart=20000000
tend=30000000 #50000000
tinc=200000
beads_per_bin=10 # 10
bp_per_bead=1000
#npolybeads=10000 #59129
npolybeads=$(python -c "
import math
print int(math.ceil(${nbp}/float(${bp_per_bead})))")
norm=1 # Normalise the contacts or not
use_genome_coords=1 # Output map in bin index or genome coords
use_header=1 # Output a header containing the run info
range=$npolybeads

name="${cell}_${genome}_chr_${chr}_${regstart}-${regend}"
out_file="${out_dir}/cmap-threshold_${name}_run_${run_start}-${run_end}-${run_inc}_rc_${thres}_t_${tstart}-${tend}_trial_${trial}.dat"

pos_files=""
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    pos_files="${pos_files} ${in_dir}/config_${name}_run_${run}.lammpstrj"
done

echo "$cmap_exe $npolybeads $beads_per_bin $regstart $bp_per_bead $thres $tstart $tend $tinc $norm $use_genome_coords $use_header $out_file $pos_files"
$cmap_exe $npolybeads $beads_per_bin $regstart $bp_per_bead $thres $tstart $tend $tinc $norm $use_genome_coords $use_header $out_file $pos_files
