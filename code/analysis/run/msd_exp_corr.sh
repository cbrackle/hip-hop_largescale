#!/bin/bash
# msd_corr.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 3 ]]; then
    echo "Usage: msd_exp_corr.sh sim_dir msd_dir out_dir"
    exit 1
fi

sim_dir=$1
msd_dir=$2
out_dir=$3

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

# Centromere region
cytoband="${sh_dir}/../../../genome/${genome}/${genome}.cytoBand.txt"
cen_start=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==1){print $2}}')
cen_end=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==2){print $3}}')
echo $cen_start $cen_end

if [[ ! -d $sim_dir ]]; then
    echo "Error: cannot find the directory " $sim_dir
    exit 1
fi

if [[ ! -d $msd_dir ]]; then
    echo "Error: cannot find the directory " $msd_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

sh_path="${sh_dir}"
exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
to_genome_coords_exe="${exe_path}/convert_to_genome_coords"
dat_to_bigwig="${sh_path}/dat_to_bigwig.sh"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# File names
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
raw_bead_list="${sim_dir}/raw/${name}_run_1/bead_list.dat"
bead_list="${out_dir}/bead_list.dat"
msd_avg_file="${msd_dir}/msd_avg/msd_${name}_avg.dat"
msd_exp_file="${out_dir}/msd-exp_${name}.dat"
msd_exp_slope_file="${out_dir}/msd-exp-slope_${name}.dat"
msd_exp_slope_bw="${out_dir}/msd-exp-slope_${name}.bigWig"
msd_exp_corr_file="${out_dir}/msd-exp-corr_${name}.dat"
msd_exp_atac_file="${out_dir}/msd-exp-ATAC_${name}.dat"
msd_exp_k27ac_file="${out_dir}/msd-exp-H3K27ac_${name}.dat"
msd_exp_k27me3_file="${out_dir}/msd-exp-H3K27me3_${name}.dat"
msd_exp_k9me3_file="${out_dir}/msd-exp-H3K9me3_${name}.dat"
msd_exp_unmark_file="${out_dir}/msd-exp-unmark_${name}.dat"

# Compute the anormolous exponent
function fit_msd_exp() {
tmin=10000000
tmax=20000000
tmin=$(python -c "import math; print math.log10($tmin)")
tmax=$(python -c "import math; print math.log10($tmax)")
#tmin=7 # 10^7 = 10000000
#tmax=7.5 # 10^7.5 ~ 31622776
#tmax=7.544068 # 10^7.544068 ~ 35000000
cmd=()
jobid=0

# Create the fitting script
fit_sh="${out_dir}/msd-exp-fit_${name}.sh"
echo \
"#!/bin/bash
bead=\$1
#index=\$(bc <<< \"(2+\$bead*3)\")
fit_gp=${out_dir}/msd-fit_${name}_bead_\${bead}.gp
fit_log=${out_dir}/msd-fit_${name}_bead_\${bead}.log
msd_file=${msd_dir}/msd_avg/msd_${name}_avg_bead_\${bead}.dat
msd_fit_file=${out_dir}/msd-exp_${name}_bead_\${bead}.dat
> \$fit_gp
> \$fit_log
echo \"
f(x)=a*x+b
a=1.0
b=1.0
set fit logfile '\${fit_log}'
set fit quiet
fit [${tmin}:${tmax}] f(x) '\${msd_file}' u (log10(\\\$1)):(log10(\\\$2)) via a,b
\" > \$fit_gp
gnuplot \$fit_gp
rm \$fit_gp
slope=\$(grep -E \"a +[=] [0-9.e-]* +[+][/][-]\" \$fit_log | awk '{print \$3}')
slope_err=\$(grep -E \"a +[=] [0-9.e-]* +[+][/][-]\" \$fit_log | awk '{print \$5}')
intercept=\$(grep -E \"b +[=] [0-9.e-]* +[+][/][-]\" \$fit_log | awk '{print \$3}')
intercept_err=\$(grep -E \"b +[=] [0-9.e-]* +[+][/][-]\" \$fit_log | awk '{print \$5}')
echo \$((\$bead-1)) \$slope \$slope_err \$intercept \$intercept_err > \$msd_fit_file
rm \$fit_log
" > $fit_sh

for (( i=1; $i<=$nbeads; i++ ))
do
    cmd[$jobid]="bash $fit_sh $i"
    jobid=$(($jobid+1))
done
max_jobs=62
total_jobs=$jobid
parallel_run

> $msd_exp_file
for (( i=1; $i<=$nbeads; i++ ))
do
    echo "Doing bead $i ..."
    msd_fit_file="${out_dir}/msd-exp_${name}_bead_${i}.dat"
    cat $msd_fit_file >> $msd_exp_file
    rm $msd_fit_file
done
$to_genome_coords_exe $start_bp $end_bp $bp_per_bead $msd_exp_file ${msd_exp_file}.tmp 4 0
mv ${msd_exp_file}.tmp $msd_exp_file
#rm $fit_sh
}

# Convert data to bigwig files
function to_bigwig() {
# Slope
awk '{print $1,$2}' $msd_exp_file > $msd_exp_slope_file
$dat_to_bigwig $chrom $start_bp $end_bp $bp_per_bead 1 2 $msd_exp_slope_file $msd_exp_slope_bw
}

# Map bead msd to beadd type
function map_msd_to_bead_type() {
cp $raw_bead_list $out_dir
tail -n+3 $bead_list > ${bead_list}.tmp
mv ${bead_list}.tmp $bead_list
awk '{print $1,$2}' $msd_exp_file > $msd_exp_slope_file
paste -d" " $bead_list $msd_exp_slope_file > $msd_exp_corr_file

# Remove the centromere region
awk -v scen=$cen_start -v ecen=$cen_end '{
if ($2<scen || $2>ecen) {print}}' $msd_exp_corr_file > ${msd_exp_corr_file}.tmp
mv ${msd_exp_corr_file}.tmp $msd_exp_corr_file

# Output beads
awk -v atac=$msd_exp_atac_file -v k9me3=$msd_exp_k9me3_file -v k27ac=$msd_exp_k27ac_file -v k27me3=$msd_exp_k27me3_file -v unmark=$msd_exp_unmark_file '{
  if ($3==1) {print $8 > atac}
  if ($4==1) {print $8 > k9me3}
  if ($5==1) {print $8 > k27ac}
  if ($6==1) {print $8 > k27me3}
  if ($3==0 && $4==0 && $5==0 && $6==0) {print $8 > unmark}
}' $msd_exp_corr_file
#rm $msd_exp_corr_file
}

# Display in gnuplot
function plot() {
plot_gp="${out_dir}/msd-exp-plot_${name}.gp"
echo "
stats '${msd_exp_atac_file}' u 1
natac = STATS_records
matac = STATS_median
stats '${msd_exp_k27ac_file}' u 1
nk27ac = STATS_records
mk27ac = STATS_median
stats '${msd_exp_k27me3_file}' u 1
nk27me3 = STATS_records
mk27me3 = STATS_median
stats '${msd_exp_k9me3_file}' u 1
nk9me3 = STATS_records
mk9me3 = STATS_median
stats '${msd_exp_unmark_file}' u 1
nunmark = STATS_records
munmark = STATS_median

set xrange[-0.2:1.0]
max = 5.0

set table 'msd_atac'

plot '${msd_exp_atac_file}' u 1:(1/natac) smooth kdensity
set table 'msd_k27ac'
plot '${msd_exp_k27ac_file}' u 1:(1/nk27ac) smooth kdensity
set table 'msd_k27me3'
plot '${msd_exp_k27me3_file}' u 1:(1/nk27me3) smooth kdensity
set table 'msd_k9me3'
plot '${msd_exp_k9me3_file}' u 1:(1/nk9me3) smooth kdensity
set table 'msd_unmark'
plot '${msd_exp_unmark_file}' u 1:(1/nunmark) smooth kdensity

unset table

set xrange [0:10]
set yrange [-0.2:1.0]

# Set positions of individual violin plots
atac_pos=1
k27ac_pos=3
k27me3_pos=5
k9me3_pos=7
unmark_pos=9
buf=0.0

set border 15 lw 1
unset key

p 'msd_atac' u (atac_pos+\$2/max):1 w filledcurves x1=atac_pos-buf ls 1,\
'msd_atac' u (atac_pos-\$2/max):1 w filledcurve x1=atac_pos+buf ls 1,\
'msd_k27ac' u (k27ac_pos+\$2/max):1 w filledcurves x1=k27ac_pos-buf ls 2,\
'msd_k27ac' u (k27ac_pos-\$2/max):1 w filledcurve x1=k27ac_pos+buf ls 2,\
'msd_k27me3' u (k27me3_pos+\$2/max):1 w filledcurves x1=k27me3_pos-buf ls 3,\
'msd_k27me3' u (k27me3_pos-\$2/max):1 w filledcurve x1=k27me3_pos+buf ls 3,\
'msd_k9me3' u (k9me3_pos+\$2/max):1 w filledcurves x1=k9me3_pos-buf ls 4,\
'msd_k9me3' u (k9me3_pos-\$2/max):1 w filledcurve x1=k9me3_pos+buf ls 4,\
'msd_unmark' u (unmark_pos+\$2/max):1 w filledcurves x1=unmark_pos-buf ls 5,\
'msd_unmark' u (unmark_pos-\$2/max):1 w filledcurve x1=unmark_pos+buf ls 5

" > $plot_gp
gnuplot -p $plot_gp
rm msd_atac msd_k27ac msd_k27me3 msd_k9me3 msd_unmark
}

#fit_msd_exp
#to_bigwig
map_msd_to_bead_type
plot
