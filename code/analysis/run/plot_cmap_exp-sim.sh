#!/bin/bash
# plot_cmaps_exp-sim.sh
# A script to plot experimental contact maps against simulation ones 
# using the HiC-Explorer/pyGenomeTracks packages

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage plot_cmap_exp-sim.sh cmap_sim_file out_dir"
    exit 1
fi

cmap_sim_file=$1
out_dir=$2

if [[ ! -f $out_dir ]]; then
    mkdir -p $out_dir
fi

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
tstart=20000000
tend=50000000
tinc=200000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

# Plot region
reg_start=40000000 #60000000 #45000000 #32000000 #14000000 #2000000 #6500000
reg_end=$end_bp #60000000 #55000000 #42000000 #24000000 #12000000 #16500000

min_count=10
max_count=1000
log_min_count=$(python -c "import math; print math.log($min_count)")
log_max_count=$(python -c "import math; print math.log($max_count)")
depth=2000000

# Required programs
path_to_exe="${sh_dir}/../bin/analysis/exe/"
path_to_sh="${sh_dir}/"
cmap_to_h5_sh="${path_to_sh}/cmap_to_h5.sh"

# Files
cmap_name=$(basename ${cmap_sim_file})
cmap_name=${cmap_name%.*}
cmap_sim_name=$cmap_name
prefix=${cmap_name%%_*}
rest=${cmap_name#*_*}
name="${prefix}-hic_${rest}"
cmap_sim_h5="${out_dir}/${cmap_sim_name}.h5"
cmap_exp_h5="${sh_dir}/../../../hic_data/cmap-hic_${cell}_${genome}_chr_${chr}_res_10kb_norm_vcsq.h5"
ini_file="${out_dir}/${name}_region_${reg_start}-${reg_end}.ini"
out_file="${out_dir}/${name}_region_${reg_start}-${reg_end}.pdf"

# Convert the contact map from gnuplot to h5 format
function convert_to_h5() {
if [[ ! -f $cmap_sim_h5 ]]; then
    $cmap_to_h5_sh $cmap_sim_file $cmap_sim_h5
fi
}

function plot() {
echo "
[exp hic matrix]
file = ${cmap_exp_h5}
file_type = hic_matrix
transform = log
# log(10) to log(1000)
min_value = $log_min_count
max_value = $log_max_count
depth = $depth
colormap = inferno_r

[sim hic matrix]
file = ${cmap_sim_h5}
file_type = hic_matrix
transform = log
# log(10) to log(1000)
min_value = $log_min_count
max_value = $log_max_count
colormap = inferno_r
depth = $depth
orientation = inverted

[x-axis]
where = bottom
" > $ini_file

source ~/conda/activate_conda.sh
conda activate myPy36
pyGenomeTracks --tracks $ini_file --region ${chrom}:${reg_start}-${reg_end} -o $out_file
conda deactivate

rm $ini_file
}

convert_to_h5
plot
