#!/bin/bash
# cmap_bound_dir_corr.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 3 ]]; then
    echo "Usage: cmap_bound_box_corr.sh sim_map exp_map out_dir"
    exit
fi

sim_map=$1
exp_map=$2
out_dir=$3

cell="GM12878"
#cell="HUVEC"
genome="hg19"
chrom="chr22" # "chr18" "chr19"
#chrom="chr14"
chr=${chrom:3}

pyx=python3

thres_start=100000
thres_end=500000
thres_inc=10000

mat_start_bp=17900000 #14300000 0 55700000 0 55000000 35500000 0 6500000
mat_end_bp=51304566 #48129895 63025520 135006516 51600000 78077248 65000000 45500000 59128983 16500000
start_bp=17900000 #14300000 55700000 0 55000000 35500000 0 27740000 48000000 6500000
end_bp=51304566 #48129895 63025520 135006516 51600000 78077248 65000000 45500000 24630000 59128983 58000000 16500000
bp_per_bin=10000

# Required programs and scripts
exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
pcorr_py="${src_path}/stats/pearson_correlation.py"
scorr_py="${src_path}/stats/spearman_correlation.py"
cmap_bound_box_exe="${exe_path}/cmap/cmap_boundary_box"

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

full_pcorr_file="${out_dir}/cmap-bound-box-pcorr.dat"
full_scorr_file="${out_dir}/cmap-bound-box-scorr.dat"
> $full_pcorr_file
> $full_scorr_file

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

function compute_box() {
cmd=()
jobid=0
for (( thres=$thres_start; $thres<=$thres_end; thres+=$thres_inc ))
do
    echo "Doing $thres"
    sim_bound_file="${out_dir}/cmap-bound-box_sim_${thres}.dat"
    exp_bound_file="${out_dir}/cmap-bound-box_exp_${thres}.dat"
    cmd[$jobid]="$cmap_bound_box_exe $start_bp $end_bp $bp_per_bin $thres 1 $sim_map $sim_bound_file"
    jobid=$(($jobid+1))
    cmd[$jobid]="$cmap_bound_box_exe $start_bp $end_bp $bp_per_bin $thres 1 $exp_map $exp_bound_file"
    jobid=$(($jobid+1))
done
max_jobs=7
total_jobs=$jobid
parallel_run
}

function compute_corr() {
for (( thres=$thres_start; $thres<=$thres_end; thres+=$thres_inc ))
do
    sim_bound_file="${out_dir}/cmap-bound-box_sim_${thres}.dat"
    exp_bound_file="${out_dir}/cmap-bound-box_exp_${thres}.dat"
    pcorr_file="${out_dir}/cmap-bound-box-pcorr_${thres}.dat"
    scorr_file="${out_dir}/cmap-bound-box-scorr_${thres}.dat"
    $pyx $pcorr_py $(wc -l $sim_bound_file | awk '{print $1}') -1 1 -1 1 $sim_bound_file $exp_bound_file $pcorr_file
    $pyx $scorr_py $(wc -l $sim_bound_file | awk '{print $1}') -1 1 -1 1 $sim_bound_file $exp_bound_file $scorr_file
    echo $thres $(awk '{if(NR==1){print}}' $pcorr_file) >> $full_pcorr_file
    echo $thres $(awk '{if(NR==1){print}}' $scorr_file) >> $full_scorr_file
    rm $pcorr_file
    rm $scorr_file
done
}

compute_box
compute_corr
