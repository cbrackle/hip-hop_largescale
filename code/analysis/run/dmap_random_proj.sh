#!/bin/bash

if [[ "$#" != 5 ]]; then
    echo "Usage: dmap_random_proj.sh run_start run_end run_inc in_dir out_dir"
    exit 1
fi

run_start=$1
run_end=$2
run_inc=$3
in_dir=$4
out_dir=$5

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

proj_py="${sh_dir}/../src/rca/dmap_random_proj.py"
start_bead=429
end_bead=619
ndim=5000

# Generate a 4-byte random integer using urandom
function get_rand(){
    rand=$(python -c "print $(od -vAn -N4 -tu4 < /dev/urandom)")
    echo $rand
}
seed=$(get_rand)

cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    echo "Doing $run"
    in_name="bead_${start_bead}-${end_bead}_run_${run}"
    dmap_file="${in_dir}/dmap_${in_name}.dat"
    out_name="bead_${start_bead}-${end_bead}_n_${ndim}_run_${run}"
    proj_file="${out_dir}/dmap-proj_${out_name}.dat"
    cmd[$jobid]="python3 $proj_py $start_bead $end_bead $ndim $seed $dmap_file $proj_file"
    jobid=$(($jobid+1))
done

# Parallel runs
max_jobs=8
total_jobs=$jobid
jobid=0
while (( $(bc <<< "$jobid < $total_jobs") ))
do
    for (( i=0; i<$max_jobs && $jobid < $total_jobs; i++))
    do
        echo "${cmd[jobid]} &"
        ${cmd[jobid]} &
        jobid=$(($jobid+1))
    done
    wait
done
