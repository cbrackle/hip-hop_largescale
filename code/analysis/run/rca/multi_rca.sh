#!/bin/bash
# multi_rca.sh
# A script to do clustering analysis for multiple regions

{

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 4 ]]; then
    echo "Usage: multi_rca.sh regions_file sim_dir pos_dir out_dir"
    exit 1
fi

regions_file=$1
sim_dir=$2
pos_dir=$3
out_dir=$4

if [[ ! -f $regions_file ]]; then
    echo "Error: cannot find the file ${regions_file}"
    exit 1
fi

if [[ ! -d $sim_dir ]]; then
    echo "Error: cannot find the directory ${sim_dir}"
    exit 1
fi

if [[ ! -d $pos_dir ]]; then
    echo "Error: cannot find the directory ${pos_dir}"
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Required scripts and programs
src_dir="${sh_dir}/../../src/"
rca_sh="${sh_dir}/rca.sh"
dip_R="${src_dir}/rca/dip_test.R"
kurt_py="${src_dir}/stats/kurtosis.py"

# Parallel runs
nthreads=72
max_jobs=$nthreads
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}


# Read the list of regions to do
start=()
end=()
i=0
while IFS='' read -r line || [[ -n "$line" ]]; do
    IFS=' ' read -r -a args <<< "$line"
    start[$i]=${args[0]}
    end[$i]=${args[1]}
    i=$(($i+1))
done < $regions_file
nregions=$i

function do_rca() {
for (( i=0; $i<$nregions; i++ ))
do
    sbp=${start[i]}
    ebp=${end[i]}
    echo "Working on region: ${sbp}-${ebp} ..."
    region_name="region_$(($i+1))_${sbp}-${ebp}"
    $rca_sh $sbp $ebp $region_name $sim_dir $pos_dir $out_dir
done
}

function do_rca_parallel() {
cmd=()
jobid=0
for (( i=0; $i<$nregions; i++ ))
do
    sbp=${start[i]}
    ebp=${end[i]}
    echo "Working on region: ${sbp}-${ebp} ..."
    region_name="region_$(($i+1))_${sbp}-${ebp}"
    cmd[$jobid]="$rca_sh $sbp $ebp $region_name $sim_dir $pos_dir $out_dir"
    jobid=$(($jobid+1))
done
total_jobs=$jobid
parallel_run
}

function do_shape_dip_test() {
dip_file="${out_dir}/shape_dip_test.dat"
> $dip_file
for (( i=0; $i<$nregions; i++ ))
do
    sbp=${start[i]}
    ebp=${end[i]}
    echo "Doing dip test on shape info for region: ${sbp}-${ebp} ..."
    region_name="region_$(($i+1))_${sbp}-${ebp}"
    shape_file=${out_dir}/${region_name}/analysis/bead*/shape/shape*.dat
    # Dip test on radius of gyration (squared) Rg2
    data_rg2=$(Rscript $dip_R 5 $shape_file | awk '{print $2,$3}')
    # Dip test on asphericity b
    data_b=$(Rscript $dip_R 6 $shape_file | awk '{print $2,$3}')
    # Dip test on acylindricity c
    data_c=$(Rscript $dip_R 7 $shape_file | awk '{print $2,$3}')
    # Dip test on anisotropy k2
    data_k2=$(Rscript $dip_R 8 $shape_file | awk '{print $2,$3}')
    echo "$((i+1)) $sbp $ebp $data_rg2 $data_b $data_c $data_k2" >> $dip_file
done
}

function do_dmap_dip_test() {
#name="dmap-dist-euclid"
#name="dmap-dist-contact"
#name="dmap-dist-beads"
name="dmap-dist-beads-binary"
dip_file="${out_dir}/${name}_dip-test.dat"
> $dip_file
for (( i=0; $i<$nregions; i++ ))
do
    sbp=${start[i]}
    ebp=${end[i]}
    echo "Doing dip test on dmap info for region: ${sbp}-${ebp} ..."
    region_name="region_$(($i+1))_${sbp}-${ebp}"
    dist_file=${out_dir}/${region_name}/analysis/bead*/dmap/${name}_*.dat
    data=$(Rscript $dip_R 3 $dist_file | awk '{print $2,$3}')
    echo "$((i+1)) $sbp $ebp $data" >> $dip_file
done
}

function do_dmap_kurtosis() {
#name="dmap-dist-euclid"
#name="dmap-dist-contact"
#name="dmap-dist-beads"
name="dmap-dist-beads-binary"
kurt_file="${out_dir}/${name}_kurtosis.dat"
> $kurt_file
for (( i=0; $i<$nregions; i++ ))
do
    sbp=${start[i]}
    ebp=${end[i]}
    echo "Doing kurtosis on dmap info for region: ${sbp}-${ebp} ..."
    local region_name="region_$(($i+1))_${sbp}-${ebp}"
    local dist_file=${out_dir}/${region_name}/analysis/bead*/dmap/${name}_*.dat
    local dist_file_name=$(basename $dist_file)
    local dist_file_dir=$(dirname $dist_file)
    local out_file=${dist_file_dir}/${dist_file_name%%.*}_kurtosis.dat
    python $kurt_py 2 $dist_file $out_file
    echo "$((i+1)) $sbp $ebp $(cat $out_file)" >> $kurt_file
    rm $out_file
done
}

#do_rca
#do_rca_parallel
#do_shape_dip_test
#do_dmap_dip_test
do_dmap_kurtosis
exit

}
