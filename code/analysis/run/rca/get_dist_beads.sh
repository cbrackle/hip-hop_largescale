#!/bin/bash
# get_dist_beads.sh
# Get the matrix of distance/dissimilarity between configurations computed
# based on a comparison of binary contacts between a specific set of beads

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 9 ]]; then
    echo "Usage: get_dist_beads.sh nbeads run_start run_end run_inc bead_file in_name out_name in_dir out_dir"
    exit
fi

nbeads=$1
run_start=$2
run_end=$3
run_inc=$4
bead_file=$5
in_name=$6
out_name=$7
in_dir=$8
out_dir=$9

in_path=${in_dir}/${in_name}
out_path=${out_dir}/${out_name}

# Check the required files exist
if [[ ! -f $bead_file ]]; then
    echo "Error: missing the file ${bead_file}"
    exit 1
fi
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    dat_file=$(echo ${in_dir}/${in_name}_run_${run}.dat)
    if [[ ! -f $dat_file ]]; then
	echo "Error: missing the file for run ${run}"
	exit 1
    fi
done

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Required programs
dist_exe="${sh_dir}/../../bin/analysis/exe/rca/dist_beads"

echo "Computing dissimilarity between configurations ..."

function compute_dist() {
dat_files=""
# Need to make sure the dat files are ordered by the run number
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    dat_file="${in_path}_run_${run}.dat"
    dat_files="${dat_files} ${dat_file}"
done
dist_file="${out_path}.dat"
$dist_exe $nbeads $bead_file $dist_file $dat_files
}

compute_dist
