#!/bin/bash
# rca.sh (region clustering analysis)
# A script to do clustering analysis for a specified region of interest

{ # Ensure the entire script is parsed before running

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 6 ]]; then
    echo "Usage: rca.sh reg_start_bp reg_end_bp reg_name sim_dir in_dir out_dir"
    exit 1
fi

reg_start_bp=$1
reg_end_bp=$2
reg_name=$3
sim_dir=$4
in_dir=$5
out_dir=$6
out_dir=${out_dir}/${reg_name}

####################

# Sub-scripts

get_traj_sh="${sh_dir}/get_traj.sh"
get_shape_sh="${sh_dir}/get_shape.sh"
get_dmap_sh="${sh_dir}/get_dmap.sh"
get_voxel_sh="${sh_dir}/get_voxel.sh"
get_zdscript_sh="${sh_dir}/get_zdscript.sh"

get_dist_euclid_sh="${sh_dir}/get_dist_euclid.sh"
get_dist_contact_sh="${sh_dir}/get_dist_contact.sh"
get_dist_beads_sh="${sh_dir}/get_dist_beads.sh"
get_dist_beads_binary_sh="${sh_dir}/get_dist_beads_binary.sh"

get_spatial_cluster_beads_sh="${sh_dir}/get_spatial_cluster_beads.sh"

####################

# Analysis options

get_traj=0                # Get the region trajectories from dump files
do_shape_analysis=0       # Compute shape info
do_dmap_analysis=1        # Compute distance maps
do_zdscript_analysis=0    # Compute Zernike descriptors

####################

# Set up parameters

# For parallel execution

export RCA_NTHREADS=64 # 8

# For extracting the trajectories from dump files
sim_start_bp=0 #6500000
sim_end_bp=59128983 #16500000
bp_per_bead=1000
tstart=30000000 #20000000
tend=30000000 #50000000
tinc=10000000
run_start=1
run_end=300
run_inc=1

# For voxelisation and computing Zernike functions
nvoxels=64
bead_size=2.0
thickness=1.0
cutoff=0.5
order=25

# For clustering analysis
metric="euclidean" # Options: euclidean correlation (not implemented yet)

####################

# Set up variables

# Determine the start and end beads
# Check region bounds
if (( ${reg_start_bp} < ${sim_start_bp} )); then
    echo "Error: region start bp is less than simulation start bp"
    exit 1
fi
if (( ${reg_end_bp} > ${sim_end_bp} )); then
    echo "Error: region end bp is greater than simulation end bp"
    exit 1
fi

start_bead=$(python -c "print (${reg_start_bp}-${sim_start_bp})/${bp_per_bead}")
end_bead=$(python -c "print (${reg_end_bp}-${sim_start_bp})/${bp_per_bead}")
nbeadsm1=$(($end_bead-$start_bead))
nbeads=$(($nbeadsm1+1))

echo "Region corresponds to bead ${start_bead}-${end_bead}"

####################

# Make the output directory

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

config_dir="${out_dir}/config/bead_${start_bead}-${end_bead}"
ana_dir="${out_dir}/analysis/bead_${start_bead}-${end_bead}"
mkdir -p $config_dir
mkdir -p $ana_dir

####################

# Running jobs in parallel

max_jobs=$nthreads
total_jobs=0
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

####################

# Get region trajectories

function rca_get_traj() {
    local out_name="config_bead_${start_bead}-${end_bead}"
    local out_dir="${config_dir}"
    #local dump_files=$(ls -v ${in_dir}/*.lammpstrj)
    #echo $dump_files
    $get_traj_sh $start_bead $end_bead $tstart $tend $tinc $run_start $run_end $run_inc $out_name $in_dir $out_dir
}

# Check region trajectories exist

function rca_check_traj() {
    for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
    do
	local traj_file="${config_dir}/config_bead_${start_bead}-${end_bead}_run_${run}.lammpstrj"
	if [[ ! -f $traj_file ]]; then
	    echo "Error: missing the trajectory file for run ${run}"
	    echo "Need to first extract the trajectories from raw dump files"
	    exit 1
	fi
    done
#    if ! test -n "$(find ${config_dir} -maxdepth 1 -name '*.lammpstrj' -print -quit)"; then
#	echo "Error: cannot find the region's trajectories"
#	echo "Need to first extract the trajectories from raw dump files"
#	exit 1
#    fi
}

####################

# Compute shape info

function rca_get_shape() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="config_${name}"
    local out_name="shape_${name}"
    local in_dir=$config_dir
    local out_dir=${ana_dir}/shape
    $get_shape_sh 0 $nbeadsm1 $run_start $run_end $run_inc $in_name $out_name $in_dir $out_dir
}

####################

# Compute distance maps

function rca_get_dmap() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="config_${name}"
    local out_name="dmap_${name}"
    local in_dir=$config_dir
    local out_dir=${ana_dir}/dmap
#    local out_dir=${ana_dir}/full_dmap
    $get_dmap_sh 0 $nbeadsm1 $run_start $run_end $run_inc $in_name $out_name $in_dir $out_dir
}

function rca_get_dmap_dist_euclid() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="dmap_${name}"
    local out_name="dmap-dist-euclid_${name}"
    local in_dir=${ana_dir}/dmap
    local out_dir=${ana_dir}/dmap
    local column=2
    $get_dist_euclid_sh $column $run_start $run_end $run_inc $in_name $out_name $in_dir $out_dir
}

function rca_get_dmap_dist_contact() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="dmap_${name}"
    local out_name="dmap-dist-contact_${name}"
    local in_dir=${ana_dir}/dmap
    local out_dir=${ana_dir}/dmap
    local thres=3.5
    $get_dist_contact_sh $nbeads $thres $run_start $run_end $run_inc $in_name $out_name $in_dir $out_dir
}

function rca_get_dmap_dist_beads() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="dmap_${name}"
    local out_name="dmap-dist-beads_${name}"
    local in_dir=${ana_dir}/dmap
    local out_dir=${ana_dir}/dmap
    # Find the ATAC beads in the region
    local bead_atac="${sim_dir}/bead_list/bead_atac.dat"
    local bead_file="${ana_dir}/dmap/bead_atac.dat"
    # Start bead and end bead are zero-based
    awk -v sb=$start_bead -v eb=$end_bead '{
if ($1 >= sb+1 && $1 <= eb+1) {print $1-sb}}' $bead_atac > $bead_file
    echo "Number of ATAC beads: $(wc -l $bead_file | awk '{print $1}')"
    $get_dist_beads_sh $nbeads $run_start $run_end $run_inc $bead_file $in_name $out_name $in_dir $out_dir
    #rm $bead_file
}

function rca_get_dmap_dist_beads_binary() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="dmap_${name}"
    local out_name="dmap-dist-beads-binary_${name}"
    local in_dir=${ana_dir}/dmap
    local out_dir=${ana_dir}/dmap
    local thres=3.5
    # Find the ATAC beads in the region
    local bead_atac="${sim_dir}/bead_list/bead_atac.dat"
    local bead_file="${ana_dir}/dmap/bead_atac.dat"
    # Start bead and end bead are zero-based
    awk -v sb=$start_bead -v eb=$end_bead '{
if ($1 >= sb+1 && $1 <= eb+1) {print $1-sb}}' $bead_atac > $bead_file
    echo "Number of ATAC beads: $(wc -l $bead_file | awk '{print $1}')"
    $get_dist_beads_binary_sh $nbeads $thres $run_start $run_end $run_inc $bead_file $in_name $out_name $in_dir $out_dir
    #rm $bead_file
}

function rca_get_dmap_spatial_cluster_beads() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="dmap_${name}"
    local out_name="dmap-cluster-ATAC_${name}"
    local in_dir=${ana_dir}/dmap
    local out_dir=${ana_dir}/dmap/cluster/
    local thres=3.5
    # Find the ATAC beads in the region
    local bead_atac="${sim_dir}/bead_list/bead_atac.dat"
    local bead_file="${ana_dir}/dmap/bead_atac.dat"
    # Start bead and end bead are zero-based
    awk -v sb=$start_bead -v eb=$end_bead '{
if ($1 >= sb+1 && $1 <= eb+1) {print $1-sb}}' $bead_atac > $bead_file
    echo "Number of ATAC beads: $(wc -l $bead_file | awk '{print $1}')"
    $get_spatial_cluster_beads_sh $nbeads $thres $run_start $run_end $run_inc $bead_file $in_name $out_name $in_dir $out_dir
    # Determine the number of clusters in each file
    local clust_all_file="${out_dir}/${out_name}.dat"
    > $clust_all_file
    for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
    do
	clust_file="${out_dir}/${out_name}_run_${run}.dat"
	local nclust=$(wc -l $clust_file | awk '{print $1}')
	echo $run $nclust >> $clust_all_file
    done
    #rm $bead_file
}

####################

# Zernike descriptors

# Determine region size

function rca_get_max_dist() {
    local max_dist_file=${ana_dir}/max_dist.dat
    local max_dist_exe="${sh_dir}/../../bin/analysis/exe/rca/size_maxdist"
    $max_dist_exe 0 $nbeadsm1 -1 -1 -1 $max_dist_file $config_dir/*.lammpstrj
}

# Voxelize data

function rca_get_voxel() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="config_${name}"
    local out_name="voxel_L_${nvoxels}_r_${bead_size}_d_${thickness}_rc_${cutoff}"
    local in_dir=$config_dir
    local out_dir=${ana_dir}/voxel

    # Determine the box size and scale factor
    if [[ ! -d $out_dir ]]; then
	mkdir -p $out_dir
    fi

    local max_dist_file=${ana_dir}/max_dist.dat
    if [[ ! -f $max_dist_file ]]; then
	rca_get_max_dist
    fi
    local max_dist=$(awk '{a+=$1;n++}END{printf("%.10f",a/n)}' $max_dist_file)
    local box_size=$(python -c "print '%.16f'%(2.0/3.0**0.5)")
    local scale=$(python -c "print '%.16f'%(${box_size}/(${max_dist}*2.0))")
    
    $get_voxel_sh 0 $nbeadsm1 $nvoxels $bead_size $thickness $cutoff $box_size $scale $in_name $out_name $in_dir $out_dir
}

# Compute Zernike descriptors

function rca_get_zdscript() {
    local name="bead_${start_bead}-${end_bead}"
    local in_name="voxel_L_${nvoxels}_r_${bead_size}_d_${thickness}_rc_${cutoff}"
    local out_name="zdscript_n_${order}_L_${nvoxels}_r_${bead_size}_d_${thickness}_rc_${cutoff}"
    local in_dir=${ana_dir}/voxel
    local out_dir=${ana_dir}/zdscript
    
    local box_size=$(python -c "print '%.16f'%(2.0/3.0**0.5)")
    local min_pt=$(python -c "print '%.16f'%(-${box_size}*0.5)")
    local voxel_size=$(python -c "print '%.16f'%(${box_size}/${nvoxels})")

    $get_zdscript_sh $nvoxels $min_pt $voxel_size $order $in_name $out_name $in_dir $out_dir
}

####################

# Do analysis

if [[ $get_traj == 1 ]]; then
    rca_get_traj
fi

if [[ $do_shape_analysis == 1 ]]; then
    rca_check_traj
    rca_get_shape
fi

if [[ $do_dmap_analysis == 1 ]]; then
    #rca_check_traj
    #rca_get_dmap
    #rca_get_dmap_dist_euclid
    #rca_get_dmap_dist_contact
    #rca_get_dmap_dist_beads
    rca_get_dmap_dist_beads_binary
    #rca_get_dmap_spatial_cluster_beads
fi

if [[ $do_zdscript_analysis == 1 ]]; then
    rca_check_traj
    rca_get_voxel
    rca_get_zdscript
fi

####################

exit

}
