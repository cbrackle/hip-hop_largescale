#!/bin/bash
# get_voxel.sh
# Voxelise the region's trajectories

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 12 ]]; then
    echo "Usage: get_voxel.sh start_bead end_bead nvoxels bead_size thickness cutoff box_size scale in_name out_name in_dir out_dir"
    exit 1
fi

start_bead=$1
end_bead=$2
nvoxels=$3
bead_size=$4
thickness=$5
cutoff=$6
box_size=$7
scale=$8
in_name=$9
out_name=${10}
in_dir=${11}
out_dir=${12}

in_path=${in_dir}/${in_name}
out_path=${out_dir}/${out_name}

if ! test -n "$(find ${in_dir} -maxdepth 1 -name ${in_name}'*.lammpstrj' -print -quit)"; then
    echo "Error: cannot find the trajectory files"
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Required programs
vox_exe="${sh_dir}/../../bin/analysis/exe/rca/voxel_thick_gauss"

echo "Voxelising the trajectories ..."

# Get the number of trajectories
nconfigs=$(ls -1f ${in_path}*.lammpstrj | wc -l )

cmd=()
for (( i=1; $i<=$nconfigs; i++ ))
do
    traj_file="${in_path}_run_${i}.lammpstrj"
    vox_file="${out_path}_run_${i}.dat"
    cmd[$(($i-1))]="$vox_exe $start_bead $end_bead $nvoxels $bead_size $thickness $cutoff $box_size $scale -1 $traj_file $vox_file"
done

# Parallel run
max_jobs=1
if [[ $RCA_NTHREADS && $RCA_NTHREADS > 1 ]]; then
    max_jobs=$RCA_NTHREADS
fi
total_jobs=$nconfigs
jobid=0
while (( $(bc <<< "$jobid < $total_jobs") ))
do
    for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
    do
	echo "${cmd[jobid]} &"
	${cmd[jobid]} &
	jobid=$(($jobid+1))
    done
    wait
done
