#!/bin/bash
# get_traj.sh
# Get the trajectories of the region of interest from the dump files

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 11 ]]; then
    echo "Usage: get_traj.sh start_bead end_bead tstart tend tinc run_start run_end run_inc out_name in_dir out_dir"
    exit 1
fi

start_bead=$1
end_bead=$2
tstart=$3
tend=$4
tinc=$5
run_start=$6
run_end=$7
run_inc=$8
out_name=$9
in_dir=${10}
out_dir=${11}

out_path="${out_dir}/${out_name}"

# Check the required files exist
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    pos_file=$(echo ${in_dir}/*run_${run}.lammpstrj)
    if [[ ! -f $pos_file ]]; then
	echo "Error: missing trajectory file for run ${run}"
	exit 1
    fi
done

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Required programs
region_traj_exe="${sh_dir}/../../bin/analysis/exe/rca/region_lammpstrj"

echo "Extracting the region of interest from dump files ..."

# Get the simulation dump files
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    pos_file=$(echo ${in_dir}/*run_${run}.lammpstrj)
    out_file=${out_path}_run_${run}.lammpstrj
    cmd[$jobid]="$region_traj_exe $start_bead $end_bead $tstart $tend $tinc $pos_file $out_file"
    jobid=$(($jobid+1))
done

# Parallel run
max_jobs=1
if [[ $RCA_NTHREADS && $RCA_NTHREADS > 1 ]]; then
    max_jobs=$RCA_NTHREADS
fi
total_jobs=$jobid
jobid=0
while (( $(bc <<< "$jobid < $total_jobs") ))
do
    for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
    do
	echo "${cmd[jobid]} &"
	${cmd[jobid]} &
	jobid=$(($jobid+1))
    done
    wait
done
