#!/bin/bash
# get_dist_contact.sh
# Get the matrix of distance/dissimilarity between configurations computed
# based on the pairwise distances of bead pairs that are interacting in one
# configuration (i.e. within a contact threshold) and not interacting in the
# other configuration

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 9 ]]; then
    echo "Usage: get_dist_contact.sh nbeads thres run_start run_end run_inc in_name out_name in_dir out_dir"
    exit
fi

nbeads=$1
thres=$2
run_start=$3
run_end=$4
run_inc=$5
in_name=$6
out_name=$7
in_dir=$8
out_dir=$9

in_path=${in_dir}/${in_name}
out_path=${out_dir}/${out_name}

# Check the required files exist
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    dat_file=$(echo ${in_dir}/${in_name}_run_${run}.dat)
    if [[ ! -f $dat_file ]]; then
	echo "Error: missing the file for run ${run}"
	exit 1
    fi
done

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Required programs
dist_exe="${sh_dir}/../../bin/analysis/exe/rca/dist_contact"

echo "Computing dissimilarity between configurations ..."

function compute_dist() {
dat_files=""
# Need to make sure the dat files are ordered by the run number
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    dat_file="${in_path}_run_${run}.dat"
    dat_files="${dat_files} ${dat_file}"
done
dist_file="${out_path}.dat"
$dist_exe $nbeads $thres $dist_file $dat_files
}

compute_dist
