#!/bin/bash
# cluster_to_region_list.sh
# A script which converts the identified clusters into a region list

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: cluster_to_region_list.sh clust_file out_file"
    exit 1
fi

clust_file=$1
out_file=$2

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
bp_per_bead=1000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

out_dir=$(dirname $out_file)
echo $out_dir

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

awk -v sbp=$start_bp -v bppb=$bp_per_bead '{
min = $1
max = $1
for ( i = 2; i <= NF; i++) {
  if ($i < min) {min = $i}
  else if ($i > max) {max = $i}
}
min = sbp+min*bppb
max = sbp+max*bppb
print min,max 
}' $clust_file > $out_file
