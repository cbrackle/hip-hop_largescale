#!/bin/bash
# get_zdscript.sh
# Get the Zernike descriptors of the region's trajectories

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 8 ]]; then
    echo "Usage: get_zdscript.sh nvoxels min_pt voxel_size order in_name out_name in_dir out_dir"
    exit 1
fi

nvoxels=$1
min_pt=$2
voxel_size=$3
order=$4
in_name=$5
out_name=$6
in_dir=$7
out_dir=$8

in_path=${in_dir}/${in_name}
out_path=${out_dir}/${out_name}

if ! test -n "$(find ${in_dir} -maxdepth 1 -name ${in_name}'*.dat' -print -quit)"; then
    echo "Error: cannot find the voxel files"
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Required programs
zd_exe="${sh_dir}/../../bin/analysis/exe/rca/zernike"

echo "Computing the Zernike descriptors of the voxelised trajectories ..."

# Get the number of trajectories
nconfigs=$(ls ${in_path}*.dat | wc -l )

cmd=()
for (( i=1; $i<=$nconfigs; i++ ))
do
    vox_file="${in_path}_run_${i}.dat"
    zd_file="${out_path}_run_${i}.dat"
    cmd[$(($i-1))]="$zd_exe $nvoxels $min_pt $voxel_size $order $vox_file $zd_file"
done

# Parallel run
max_jobs=1
if [[ $RCA_NTHREADS && $RCA_NTHREADS > 1 ]]; then
    max_jobs=$RCA_NTHREADS
fi
total_jobs=$nconfigs
jobid=0
while (( $(bc <<< "$jobid < $total_jobs") ))
do
    for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
    do
	echo "${cmd[jobid]} &"
	${cmd[jobid]} &
	jobid=$(($jobid+1))
    done
    wait
done
