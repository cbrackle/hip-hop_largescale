#!/bin/bash
# get_dmap.sh
# Get the distance maps of the region's trajectories

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 9 ]]; then
    echo "Usage: get_dmap.sh start_bead end_bead run_start run_end run_inc in_name out_name in_dir out_dir"
    exit
fi

start_bead=$1
end_bead=$2
run_start=$3
run_end=$4
run_inc=$5
in_name=$6
out_name=$7
in_dir=$8
out_dir=$9

in_path=${in_dir}/${in_name}
out_path=${out_dir}/${out_name}

# Check the required files exist
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    pos_file=$(echo ${in_dir}/*run_${run}.lammpstrj)
    if [[ ! -f $pos_file ]]; then
	echo "Error: missing the trajectory file for run ${run}"
	exit 1
    fi
done

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Required programs
dmap_exe="${sh_dir}/../../bin/analysis/exe/rca/config_to_dmap"
dist_exe="${sh_dir}/../../bin/analysis/exe/rca/dist_euclid"

# Parallel run
max_jobs=1
if [[ $RCA_NTHREADS && $RCA_NTHREADS > 1 ]]; then
    max_jobs=$RCA_NTHREADS
fi
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

echo "Computing distance maps ..."

function compute_dmap() {
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    traj_file="${in_path}_run_${run}.lammpstrj"
    dmap_file="${out_path}_run_${run}.dat"
    cmd[$jobid]="$dmap_exe $start_bead $end_bead -1 0 $traj_file $dmap_file"
#   cmd[$jobid]="$dmap_exe $start_bead $end_bead -1 1 $traj_file $dmap_file"
    jobid=$(($jobid+1))
done

total_jobs=$jobid
parallel_run
}

function compute_dmap_dist() {
dmap_files=""
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    dmap_file="${out_path}_run_${run}.dat"
    dmap_files="${dmap_files} ${dmap_file}"
done
dmap_file="${out_path}_run_${run_start}.dat"
info=${out_name##*_}
dist_file="${out_dir}/dmap-dist_${info}.dat"
$dist_exe $(wc -l $dmap_file | awk '{print $1}') 2 $dist_file $dmap_files
}

compute_dmap
compute_dmap_dist
