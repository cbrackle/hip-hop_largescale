#!/bin/bash
# protein_cluster_time.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 5 ]]; then
    echo "Usage: protein_cluster_time.sh run_start run_end run_inc in_dir out_dir"
    exit 1
fi

run_start=$1
run_end=$2
run_inc=$3
in_dir=$4
out_dir=$5

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
tstart=0
tend=50000000
tinc=200000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

thres=3.5 # Cutoff radius
min_clust=2 # Minimum size of each cluster

# Protein bead indices
nact=1500
npcmb=750
nhet=3750
act_start=$(($nbeads+1))
act_end=$(($nbeads+$nact))
pcmb_start=$(($nbeads+$nact+1))
pcmb_end=$(($nbeads+$nact+$npcmb))
het_start=$(($nbeads+$nact+$npcmb+1))
het_end=$(($nbeads+$nact+$npcmb+$nhet))

act_file="${out_dir}/bead_act.dat"
pcmb_file="${out_dir}/bead_pcomb.dat"
het_file="${out_dir}/bead_het.dat"

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

clust_exe="${sh_dir}/../bin/analysis/exe/protein_cluster_time"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

function make_bead_list() {
echo "Creating active TF bead list ..."
echo "start = $act_start end = $act_end"
> $act_file
for (( i=$act_start; $i<=$act_end; i++ ))
do
    echo $i >> $act_file
done

echo "Creating polycomb TF bead list ..."
echo "start = $pcmb_start end = $pcmb_end"
> $pcmb_file
for (( i=$pcmb_start; $i<=$pcmb_end; i++ ))
do
    echo $i >> $pcmb_file
done

echo "Creating heterochromatin TF bead list ..."
echo "start = $het_start end = $het_end"
> $het_file
for (( i=$het_start; $i<=$het_end; i++ ))
do
    echo $i >> $het_file
done
}

function compute_cluster() {
# Compute the protein cluster time series
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
    pos_file="${in_dir}/config_${name}_run_${run}.lammpstrj"
    clust_act_file="${out_dir}/prot-clust-act_${name}_rc_${thres}_nc_${min_clust}_run_${run}.dat"
    clust_pcmb_file="${out_dir}/prot-clust-pcomb_${name}_rc_${thres}_nc_${min_clust}_run_${run}.dat"
    clust_het_file="${out_dir}/prot-clust-het_${name}_rc_${thres}_nc_${min_clust}_run_${run}.dat"
    cmd[$jobid]="$clust_exe $thres $min_clust $tstart $tend $tinc $act_file $pos_file $clust_act_file"
    jobid=$(($jobid+1))
    cmd[$jobid]="$clust_exe $thres $min_clust $tstart $tend $tinc $pcmb_file $pos_file $clust_pcmb_file"
    jobid=$(($jobid+1))
    cmd[$jobid]="$clust_exe $thres $min_clust $tstart $tend $tinc $het_file $pos_file $clust_het_file"
    jobid=$(($jobid+1))
done
max_jobs=8
total_jobs=$jobid
parallel_run
}

#make_bead_list
compute_cluster
