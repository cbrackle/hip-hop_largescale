#!/bin/bash
# oci_corr_vary_thres.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: oci_corr.sh sim_map out_dir"
    exit 1
fi

sim_map=$1
out_dir=$2

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
#chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
#chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
#start_bp=0
#end_bp=$chrom_size_bp
start_bp=6500000
end_bp=16500000
bp_per_bin=10000
use_genome_coords=1

sd_start=100000
sd_end=5000000
sd_inc=100000
sd_range="${sd_start}-${sd_end}-${sd_inc}"

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

sh_path="${sh_dir}"
exe_path="${sh_dir}/../bin/analysis/exe/"
main_exe_path="${sh_dir}/../../sim/bin/main/exe/"
src_path="${sh_dir}/../src/"
to_genome_coords_exe="${exe_path}/convert_to_genome_coords"
dat_to_bigwig="${sh_path}/dat_to_bigwig.sh"
data2beads_exe="${main_exe_path}/data_to_beads"
avg_py="${src_path}/average_one_file.py"
oci_exe="${exe_path}/oci"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# File names
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
bead_list="${out_dir}/bead_list.dat"
oci_thres_atac_file="${out_dir}/oci-ATAC_${name}_sd_${sd_range}.dat"
oci_thres_k27ac_file="${out_dir}/oci-H3K27ac_${name}_sd_${sd_range}.dat"
oci_thres_k27me3_file="${out_dir}/oci-H3K27me3_${name}_sd_${sd_range}.dat"
oci_thres_k9me3_file="${out_dir}/oci-H3K9me3_${name}_sd_${sd_range}.dat"
oci_thres_unmark_file="${out_dir}/oci-unmark_${name}_sd_${sd_range}.dat"

# Create bead type map for 10kb resolution
function get_bead_type() {
path2data="${sh_dir}/../../../data_GM12878/${genome}/"
k27ac_bed="${path2data}/histone/H3K27ac_epicpeaks.bed"
k27me3_bed="${path2data}/histone/H3K27me3_epicpeaks.bed"
k9me3_bed="${path2data}/histone/H3K9me3_epicpeaks.bed"
atac_bed="${path2data}/atac/ATAC_finalpeaks.bed"
config_file="${out_dir}/config_${name}.txt"
echo "${chrom}
${start_bp}
${end_bp}
${bp_per_bin}
ACT ${atac_bed}
OPEN ${k27ac_bed}
POLY ${k27me3_bed}
HET ${k9me3_bed}" > $config_file
$data2beads_exe $config_file $out_dir
}

function oci_vary_thres() {
bead_list_tmp=${bead_list}.tmp
tail -n+3 $bead_list > $bead_list_tmp
for (( sd=$sd_start; $sd<=$sd_end; sd+=$sd_inc ))
do
    echo "Doing sd = ${sd}"
    # Compute the OCI
    run_name="${name}_sd_${sd}"
    oci_file="${out_dir}/oci_${run_name}.dat"
    oci_file_tmp=${oci_file}.tmp
    $oci_exe $start_bp $end_bp $bp_per_bin $sd $use_genome_coords $sim_map $oci_file
    
    # Map OCI to bead type
    oci_corr_file="${out_dir}/oci-corr_${run_name}.dat"
    oci_atac_file="${out_dir}/oci-ATAC_${run_name}.dat"
    oci_k27ac_file="${out_dir}/oci-H3K27ac_${run_name}.dat"
    oci_k27me3_file="${out_dir}/oci-H3K27me3_${run_name}.dat"
    oci_k9me3_file="${out_dir}/oci-H3K9me3_${run_name}.dat"
    oci_unmark_file="${out_dir}/oci-unmark_${run_name}.dat"
    awk '{print $1,$4}' $oci_file > $oci_file_tmp
    paste -d" " $bead_list $oci_file_tmp > $oci_corr_file
    awk -v atac=$oci_atac_file -v k9me3=$oci_k9me3_file -v k27ac=$oci_k27ac_file -v k27me3=$oci_k27me3_file -v unmark=$oci_unmark_file '{
  if ($3==1) {print $8 > atac}
  if ($4==1) {print $8 > k9me3}
  if ($5==1) {print $8 > k27ac}
  if ($6==1) {print $8 > k27me3}
  if ($3==0 && $4==0 && $5==0 && $6==0) {print $8 > unmark}
}' $oci_corr_file
    
    # Average the OCI values for each bead type
    python $avg_py 0 -1 -1 -1 $oci_atac_file ${oci_atac_file}.avg
    python $avg_py 0 -1 -1 -1 $oci_k27ac_file ${oci_k27ac_file}.avg
    python $avg_py 0 -1 -1 -1 $oci_k27me3_file ${oci_k27me3_file}.avg
    python $avg_py 0 -1 -1 -1 $oci_k9me3_file ${oci_k9me3_file}.avg
    python $avg_py 0 -1 -1 -1 $oci_unmark_file ${oci_unmark_file}.avg
    echo $sd $(cat ${oci_atac_file}.avg) >> $oci_thres_atac_file
    echo $sd $(cat ${oci_k27ac_file}.avg) >> $oci_thres_k27ac_file
    echo $sd $(cat ${oci_k27me3_file}.avg) >> $oci_thres_k27me3_file
    echo $sd $(cat ${oci_k9me3_file}.avg) >> $oci_thres_k9me3_file
    echo $sd $(cat ${oci_unmark_file}.avg) >> $oci_thres_unmark_file

    # Clean up
    rm $oci_file $oci_file_tmp
    rm $oci_corr_file
    rm $oci_atac_file*
    rm $oci_k27ac_file*
    rm $oci_k27me3_file*
    rm $oci_k9me3_file*
    rm $oci_unmark_file*
done
rm $bead_list_tmp
}

get_bead_type
oci_vary_thres
