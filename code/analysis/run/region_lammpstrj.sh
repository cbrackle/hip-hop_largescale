#!/bin/bash
# region_lampmpstrj.sh

if (( $# < 7 )); then
    echo "Usage: region_lammpstrj.sh start_bead end_bead tstart tend tinc config_dir dump_file1 [dump_files ...]"
    exit 1
fi

start_bead=$1
end_bead=$2
tstart=$3
tend=$4
tinc=$5
config_dir=$6
dump_files=${@:7}

# Directory of this script - needed for running the script from somewhere else
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ ! -d $config_dir ]]; then
    mkdir -p $config_dir
fi

echo "Extracting the region of interest from trajectory files ..."

region_traj_exe="${sh_dir}/../bin/analysis/exe/rca/region_lammpstrj"
config_path="${config_dir}/config_bead_${start_bead}-${end_bead}_run"
echo "$region_traj_exe $start_bead $end_bead $tstart $tend $tinc $config_path $dump_files"
