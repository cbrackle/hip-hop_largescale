#!/bin/bash
# imap_cluster_analysis.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 5 ]]; then
    echo "Usage: imap_cluster_network_analysis.sh bead_type bead_list pos_dir in_dir out_dir"
    exit 1
fi

bead_type=$1
bead_list=$2
pos_dir=$3
in_dir=$4
out_dir=$5

cell="GM12878"
#chrom="chr19"
chrom="chr18"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
bp_per_bead=1000
tstart=20000000
tend=30000000
tinc=200000
run_start=1
run_end=300
run_inc=1
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")
rc=3.5 # Threshold on the spatial separation to be in contact
fq=1530 # Threshold on the interaction frequency for beads to be connected
nc=2 # Threshold on the size of a cluster to be idenitified as a cluster
one_based=1 # Whether the indices in the bead list is one-based or not

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

# Required programs and scripts
exe_dir="${sh_dir}/../bin/analysis/exe/"
src_dir="${sh_dir}/../src/"
clust_exe="${exe_dir}/cluster/cluster"
clust_to_edge_exe="${exe_dir}/cluster/cluster_to_edgelist"
clust_colour_exe="${exe_dir}/cluster/cluster_colour"
clust_lin_olap_exe="${exe_dir}/cluster/cluster_linear_overlap"
clust_vol_olap_exe="${exe_dir}/cluster/cluster_volume_overlap"
clust_gyr_exe="${exe_dir}/cluster/cluster_gyration"
clust_gyr_tavg_exe="${exe_dir}/cluster/cluster_gyration_tavg"
clust_network_py="${src_dir}/cluster/cluster_network_analysis.py"
clust_smallworld_py="${src_dir}/cluster/cluster_small_world.py"
clust_cliques_py="${src_dir}/cluster/cluster_find_cliques.py"
avg_multi_py="${src_dir}/stats/average_multi_files.py"

# Variables
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
clust_name="${name}_rc_${rc}_fq_${fq}_nc_${nc}_t_${tstart}-${tend}"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# Generate a 4-byte random integer using urandom
function get_rand(){
    rand=$(python -c "print $(od -vAn -N4 -tu4 < /dev/urandom)")
    echo $rand
}

function get_cluster() {
imap_file="${in_dir}/imap-${bead_type}_${name}_rc_${rc}_t_${tstart}-${tend}.dat"
imap_clust_file="${out_dir}/imap-cluster-${bead_type}_${clust_name}.dat"
echo "$clust_exe $fq $nc $one_based $bead_list $imap_file $imap_clust_file"
$clust_exe $fq $nc $one_based $bead_list $imap_file $imap_clust_file
}

function get_edgelist() {
imap_file="${in_dir}/imap-${bead_type}_${name}_rc_${rc}_t_${tstart}-${tend}.dat"
imap_clust_file="${out_dir}/imap-cluster-${bead_type}_${clust_name}.dat"
out_root="${out_dir}/imap-cluster-${bead_type}_${clust_name}"
$clust_to_edge_exe $fq $one_based $bead_list $imap_file $imap_clust_file $out_root
}

function analyse_edgelist() {
nclusters=$(ls ${in_dir}/*clust_*.edgelist | wc -l | awk '{print $1}')
ana_file="${out_dir}/imap-cluster-network-${bead_type}_${clust_name}.dat"
> $ana_file
for (( i=1; $i<=$nclusters; i++ ))
do
    edge_file="${in_dir}/imap-cluster-${bead_type}_${clust_name}_clust_${i}.edgelist"
    out_file="${out_dir}/imap-cluster-network-${bead_type}_${clust_name}_clust_${i}.dat"
    python3 $clust_network_py $edge_file $out_file
    data=$(cat $out_file) 
    echo $i $data >> $ana_file
    rm $out_file
done
}

function small_world_analysis() {
module load openmpi/1.10
nprocs=16
nclusters=$(ls ${in_dir}/*clust_*.edgelist | wc -l | awk '{print $1}')
ana_file="${out_dir}/imap-cluster-smallworld-${bead_type}_${clust_name}.dat"
> $ana_file
for (( i=1; $i<=$nclusters; i++ ))
#for (( i=44; $i<=$nclusters; i++ ))
do
    seed=$(get_rand)
    edge_file="${in_dir}/imap-cluster-${bead_type}_${clust_name}_clust_${i}.edgelist"
    out_file="${out_dir}/imap-cluster-smallworld-${bead_type}_${clust_name}_clust_${i}.dat"
    echo "mpirun -np $nprocs python3 $clust_smallworld_py $seed $edge_file $out_file"
    mpirun -np $nprocs python3 $clust_smallworld_py $seed $edge_file $out_file
    data=$(cat $out_file) 
    echo $i $data $seed >> $ana_file
    rm $out_file
done

}

function find_cliques() {
nclusters=$(ls ${in_dir}/*clust_*.edgelist | wc -l | awk '{print $1}')
clique_files=""
for (( i=1; $i<=$nclusters; i++ ))
do
    echo "Doing cluster $i"
    edge_file="${in_dir}/imap-cluster-${bead_type}_${clust_name}_clust_${i}.edgelist"
    clique_file="${out_dir}/imap-clique-${bead_type}_${clust_name}_clust_${i}.dat"
    gml_file="${in_dir}/imap-cluster-${bead_type}_${clust_name}_clust_${i}.gml"
    python3 $clust_cliques_py $edge_file $clique_file $gml_file
    clique_files="$clique_files $clique_file"
done
# Combine all clique files
imap_all_clique_file="${out_dir}/imap-clique-${bead_type}_${clust_name}.dat"
awk -v nc=$nc '{if(NF>=nc){print}}' $clique_files > $imap_all_clique_file
imap_all_clique_colour_file="${out_dir}/imap-clique-colour-${bead_type}_${clust_name}.dat"
$clust_colour_exe $one_based $bead_list $imap_all_clique_file $imap_all_clique_colour_file
}

function cluster_vol_overlap() {
imap_clust_file="${in_dir}/imap-cluster-${bead_type}_${clust_name}.dat"
imap_clust_vol_olap_file="${out_dir}/imap-cluster-volume-overlap-${bead_type}_${clust_name}.dat"
export OMP_NUM_THREADS=64
$clust_vol_olap_exe $tstart $tend $tinc $imap_clust_file $imap_clust_vol_olap_file ${pos_dir}/config*.lammpstrj
}

function cluster_lin_overlap() {
imap_clust_file="${in_dir}/imap-cluster-${bead_type}_${clust_name}.dat"
imap_clust_lin_olap_file="${out_dir}/imap-cluster-linear-overlap-${bead_type}_${clust_name}.dat"
$clust_lin_olap_exe $imap_clust_file $imap_clust_lin_olap_file
}

function clique_vol_overlap() {
imap_clique_file="${in_dir}/imap-clique-${bead_type}_${clust_name}.dat"
imap_clique_vol_olap_file="${out_dir}/imap-clique-volume-overlap-${bead_type}_${clust_name}.dat"
export OMP_NUM_THREADS=64
$clust_vol_olap_exe $tstart $tend $tinc $imap_clique_file $imap_clique_vol_olap_file ${pos_dir}/config*.lammpstrj
}

function clique_lin_overlap() {
imap_clique_file="${in_dir}/imap-clique-${bead_type}_${clust_name}.dat"
imap_clique_lin_olap_file="${out_dir}/imap-clique-linear-overlap-${bead_type}_${clust_name}.dat"
$clust_lin_olap_exe $imap_clique_file $imap_clique_lin_olap_file
}

function cluster_gyr_distrb() {
imap_clust_file="${in_dir}/imap-cluster-${bead_type}_${clust_name}.dat"
imap_clust_gyr_distrb_file="${out_dir}/imap-cluster-gyr-distrb-${bead_type}_${clust_name}.dat"
cmd=()
jobid=0
imap_clust_gyr_tavg_files=""
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    pos_file="${pos_dir}/config_${name}_run_${run}.lammpstrj"
    imap_clust_gyr_tavg_file="${out_dir}/imap-cluster-gyr-tavg-${bead_type}_${clust_name}_run_${run}.dat"
    imap_clust_gyr_tavg_files="$imap_clust_gyr_tavg_files $imap_clust_gyr_tavg_file"
    cmd[$jobid]="$clust_gyr_tavg_exe $tstart $tend $tinc $imap_clust_file $pos_file $imap_clust_gyr_tavg_file"
    jobid=$(($jobid+1))
done
max_jobs=64
total_jobs=$jobid
parallel_run
# Average over all runs
python $avg_multi_py 0 1 -1 -1 $imap_clust_gyr_distrb_file $imap_clust_gyr_tavg_files
#rm $imap_clust_gyr_tavg_files
}

function cluster_lin_distrb() {
imap_clust_file="${in_dir}/imap-cluster-${bead_type}_${clust_name}.dat"
imap_clust_lin_distrb_file="${out_dir}/imap-cluster-linear-distrb-${bead_type}_${clust_name}.dat"
awk '{
min = $1
max = $1
for (i = 1; i <= NF; i++) {
  if ($i < min) {min = $i}
  else if ($i > max) {max = $i}
}
print max-min
}' $imap_clust_file > $imap_clust_lin_distrb_file
}

function clique_gyr_distrb() {
imap_clique_file="${in_dir}/imap-clique-${bead_type}_${clust_name}.dat"
imap_clique_gyr_distrb_file="${out_dir}/imap-clique-gyr-distrb-${bead_type}_${clust_name}.dat"
cmd=()
jobid=0
imap_clique_gyr_tavg_files=""
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    pos_file="${pos_dir}/config_${name}_run_${run}.lammpstrj"
    imap_clique_gyr_tavg_file="${out_dir}/imap-clique-gyr-tavg-${bead_type}_${clust_name}_run_${run}.dat"
    imap_clique_gyr_tavg_files="$imap_clique_gyr_tavg_files $imap_clique_gyr_tavg_file"
    cmd[$jobid]="$clust_gyr_tavg_exe $tstart $tend $tinc $imap_clique_file $pos_file $imap_clique_gyr_tavg_file"
    jobid=$(($jobid+1))
done
max_jobs=64
total_jobs=$jobid
parallel_run
# Average over all runs
python $avg_multi_py 0 1 -1 -1 $imap_clique_gyr_distrb_file $imap_clique_gyr_tavg_files
#rm $imap_clique_gyr_tavg_files
}

function clique_lin_distrb() {
imap_clique_file="${in_dir}/imap-clique-${bead_type}_${clust_name}.dat"
imap_clique_lin_distrb_file="${out_dir}/imap-clique-linear-distrb-${bead_type}_${clust_name}.dat"
awk '{
min = $1
max = $1
for (i = 1; i <= NF; i++) {
  if ($i < min) {min = $i}
  else if ($i > max) {max = $i}
}
print max-min
}' $imap_clique_file > $imap_clique_lin_distrb_file
}

function analyse_indv_runs() {
cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    clust_name="${clust_name}_run_${run}"
    cmd[$jobid]=get_cluster
    jobid=$(($jobid+1))
done
export OMP_NUM_THREADS=1
max_job=8
total_jobs=$jobid
parallel_run
}

# Choose which function(s) to run
#get_cluster
#in_dir=$out_dir # Needed when running all functions at once
#get_edgelist
#analyse_edgelist
#small_world_analysis
#find_cliques
#cluster_vol_overlap
#cluster_lin_overlap
#cluster_gyr_distrb
#cluster_lin_distrb
#clique_vol_overlap
#clique_lin_overlap
clique_gyr_distrb
#clique_lin_distrb
#analyse_indv_runs
