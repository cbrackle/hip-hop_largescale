#!/bin/bash
# dat_to_bigwig.sh
# Convert two-column dat files to bigWig files

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

ucsc_bin="/home/s1309877/.local/bin/ucsc/v385/"
chromsizes="${sh_dir}/../../../genome/hg19/hg19.chrom.sizes"

if [[ $# != 8 ]]; then
    echo "Usage: datToBigWig.sh chrom start_bp end_bp bp_per_bin" \
	"index_col val_col dat_file bigwig_file"
    exit 1
fi

chrom=$1
start_bp=$2
end_bp=$3
bp_per_bin=$4
index_col=$5 # Index and value column are one-based
val_col=$6
dat_file=$7
bigwig_file=$8

# Assume the index column records the midpt bp of each bin
# Make the wig file
awk -v icol=$index_col -v vcol=$val_col -v chr=$chrom -v sbp=$start_bp \
-v ebp=$end_bp -v bppb=$bp_per_bin 'BEGIN {
printf("variableStep chrom=%s span=%d\n",chr,bppb)
}{
if ($icol>=(sbp+bppb) && $icol<=(ebp-bppb)) {
  s=($icol-bppb/2)
  print s,$vcol
}}' $dat_file > $dat_file.wig

# Convert the wig file to bigWig format
$ucsc_bin/wigToBigWig $dat_file.wig $chromsizes $bigwig_file
rm $dat_file.wig
