#!/bin/bash
# msd_corr.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: msd_corr.sh sim_dir gyr_dir"
    exit 1
fi

sim_dir=$1
msd_dir=$2

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
#chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
#chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
#start_bp=0
#end_bp=$chrom_size_bp
start_bp=6500000
end_bp=16500000
bp_per_bead=1000
tstart=20000000
tend=50000000
tinc=200000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

rc=5.0 # Cutoff radius

if [[ ! -d $sim_dir ]]; then
    echo "Error: cannot find the directory " $sim_dir
    exit 1
fi

if [[ ! -d $msd_dir ]]; then
    echo "Error: cannot find the directory " $msd_dir
    exit 1
fi

exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
avg_py="${src_path}/stats/average_multi_files_fields.py"
to_genome_coords_exe="${exe_path}/convert_to_genome_coords"

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

# File names
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
raw_bead_list="${sim_dir}/raw/${name}_run_1/bead_list.dat"
bead_list="${msd_dir}/bead_list.dat"
msd_avg_file="${msd_dir}/msd_${name}_avg.dat"
msd_exp_file="${msd_dir}/msd-exp_${name}.dat"
msd_exp_slope_file="${msd_dir}/msd-exp-slope_${name}.dat"
msd_corr_file="${msd_dir}/msd-exp-corr_${name}.dat"
msd_atac_file="${msd_dir}/msd-exp-ATAC_${name}.dat"
msd_k27ac_file="${msd_dir}/msd-exp-H3K27ac_${name}.dat"
msd_k27me3_file="${msd_dir}/msd-exp-H3K27me3_${name}.dat"
msd_k9me3_file="${msd_dir}/msd-exp-H3K9me3_${name}.dat"
msd_unmark_file="${msd_dir}/msd-exp-unmark_${name}.dat"

# Average the msd results
function average_msd() {
python $avg_py $(($nbeads+1)) 0 -1 $msd_avg_file ${msd_dir}/msd_${name}_run_*.dat   
}

# Compute the anormolous exponent
function fit_msd_exp() {
tmin=7 # 10^7 = 10000000
tmax=7.5 # 10^7.5 ~ 31622776
cmd=()
jobid=0

# Create the fitting script
fit_sh="${msd_dir}/msd-exp-fit_${name}.sh"
echo \
"#!/bin/bash
bead=\$1
index=\$(python -c \"print 2+\$bead*3\")
fit_gp=${msd_dir}/msd-fit_${name}_bead_\${bead}.gp
fit_log=${msd_dir}/msd-fit_${name}_bead_\${bead}.log
> \$fit_gp
> \$fit_log
echo \"
f(x)=a*x+b
a=1.0
b=1.0
set fit logfile '\${fit_log}'
set fit quiet
fit [${tmin}:${tmax}] f(x) '${msd_avg_file}' u (log10(\\\$1)):(log10(\\$\${index})) via a,b
\" > \$fit_gp
gnuplot \$fit_gp
rm \$fit_gp
" > $fit_sh

#for (( i=0; $i<$nbeads; i++ ))
#do
#    cmd[$jobid]="bash $fit_sh $i"
#    jobid=$(($jobid+1))
#done
#max_jobs=12
#total_jobs=$jobid
#parallel_run

#> $msd_exp_file
#for (( i=0; $i<$nbeads; i++ ))
#do
#    echo "Doing bead $i ..."
#    # Grab the fitted slope and intercept
#    fit_log="${msd_dir}/msd-fit_${name}_bead_${i}.log"
#    slope=$(grep -E "a +[=] [0-9.e-]* +[+][/][-]" $fit_log | awk '{print $3}')
#    slope_err=$(grep -E "a +[=] [0-9.e-]* +[+][/][-]" $fit_log | awk '{print $5}')
#    intercept=$(grep -E "b +[=] [0-9.e-]* +[+][/][-]" $fit_log | awk '{print $3}')
#    intercept_err=$(grep -E "b +[=] [0-9.e-]* +[+][/][-]" $fit_log | awk '{print $5}')
#    echo $i $slope $slope_err $intercept $intercept_err >> $msd_exp_file
#done
$to_genome_coords_exe $start_bp $end_bp $bp_per_bead $msd_exp_file ${msd_exp_file}.tmp 4 0
mv ${msd_exp_file}.tmp $msd_exp_file
}

# Map bead msd to baed type
function map_msd_to_bead_type() {
cp $raw_bead_list $msd_dir
tail -n+3 $bead_list > ${bead_list}.tmp
mv ${bead_list}.tmp $bead_list
awk '{print $1,$2}' $msd_exp_file > $msd_exp_slope_file
paste -d" " $bead_list $msd_exp_slope_file > $msd_corr_file
# Output beads
awk -v atac=$msd_atac_file -v k9me3=$msd_k9me3_file -v k27ac=$msd_k27ac_file \
    -v k27me3=$msd_k27me3_file -v unmark=$msd_unmark_file '{
  if ($3==1) {print $8,$9,$10 > atac}
  if ($4==1) {print $8,$9,$10 > k9me3}
  if ($5==1) {print $8,$9,$10 > k27ac}
  if ($6==1) {print $8,$9,$10 > k27me3}
  if ($3==0 && $4==0 && $5==0 && $6==0) {print $8,$9,$19 > unmark}
}' $msd_corr_file
#rm $msd_corr_file
}

# Display in gnuplot
function plot() {
plot_gp="${msd_dir}/msd-exp-plot_${name}.gp"
echo "
stats '${msd_atac_file}' u 1
natac = STATS_records
matac = STATS_median
stats '${msd_k27ac_file}' u 1
nk27ac = STATS_records
mk27ac = STATS_median
stats '${msd_k27me3_file}' u 1
nk27me3 = STATS_records
mk27me3 = STATS_median
stats '${msd_k9me3_file}' u 1
nk9me3 = STATS_records
mk9me3 = STATS_median
stats '${msd_unmark_file}' u 1
nunmark = STATS_records
munmark = STATS_median

set xrange[0.0:0.7]
set table 'msd_atac'
plot '${msd_atac_file}' u 1:(1/natac) smooth kdensity
set table 'msd_k27ac'
plot '${msd_k27ac_file}' u 1:(1/nk27ac) smooth kdensity
set table 'msd_k27me3'
plot '${msd_k27me3_file}' u 1:(1/nk27me3) smooth kdensity
set table 'msd_k9me3'
plot '${msd_k9me3_file}' u 1:(1/nk9me3) smooth kdensity
set table 'msd_unmark'
plot '${msd_unmark_file}' u 1:(1/nunmark) smooth kdensity

unset table

set xrange [0.0:50.0]
set yrange [0.0:0.7]

# Set positions of individual violin plots
atac_pos=5.0
k27ac_pos=15.0
k27me3_pos=25.0
k9me3_pos=35.0
unmark_pos=45.0
buf=0.0

set border 15 lw 1
unset key

p 'msd_atac' u (atac_pos+\$2):1 w filledcurves x1=atac_pos-buf ls 1,\
'msd_atac' u (atac_pos-\$2):1 w filledcurve x1=atac_pos+buf ls 1,\
'msd_k27ac' u (k27ac_pos+\$2):1 w filledcurves x1=k27ac_pos-buf ls 2,\
'msd_k27ac' u (k27ac_pos-\$2):1 w filledcurve x1=k27ac_pos+buf ls 2,\
'msd_k27me3' u (k27me3_pos+\$2):1 w filledcurves x1=k27me3_pos-buf ls 3,\
'msd_k27me3' u (k27me3_pos-\$2):1 w filledcurve x1=k27me3_pos+buf ls 3,\
'msd_k9me3' u (k9me3_pos+\$2):1 w filledcurves x1=k9me3_pos-buf ls 4,\
'msd_k9me3' u (k9me3_pos-\$2):1 w filledcurve x1=k9me3_pos+buf ls 4,\
'msd_unmark' u (unmark_pos+\$2):1 w filledcurves x1=unmark_pos-buf ls 5,\
'msd_unmark' u (unmark_pos-\$2):1 w filledcurve x1=unmark_pos+buf ls 5

" > $plot_gp
gnuplot -p $plot_gp
rm msd_atac msd_k27ac msd_k27me3 msd_k9me3 msd_unmark
}

#average_msd
#fit_msd_exp
map_msd_to_bead_type
plot
