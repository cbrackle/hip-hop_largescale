#!/bin/bash
# extract_timeframe.sh
# Extract a specific time frame from a dump file

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 3 ]]; then
    echo "Usage: extract_timeframe.sh time in_dir out_dir"
    exit 1
fi

time=$1
in_dir=$2
out_dir=$3

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory $in_dir"
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

cell="GM12878"
#chrom="chr19"
#chrom="chr18"
chrom="chr11"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
#start_bp=0
#end_bp=$chrom_size_bp
start_bp=55700000 # 0 6500000
end_bp=135006516 # 51600000 16500000
bp_per_bead=1000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")
#nproteins=6000 # chr19
#nproteins=7810 # chr18
#nproteins=5170 # chr11:0-51600000
nproteins=7930 # chr11:55700000-135006516

run_start=1
run_end=300
run_inc=1
run_shift=300

# For parallel runs
function parallel_run() {
    jobid=0
    while (( $(bc <<< "$jobid < $total_jobs") ))
    do
	for (( i=0; $i<$max_jobs && $jobid < $total_jobs; i++ ))
	do
	    echo "${cmd[jobid]} &"
	    ${cmd[jobid]} &
	    jobid=$(($jobid+1))
	done
	wait
    done
}

name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"

extract_sh="${out_dir}/extract.sh"
echo \
"#!/bin/bash
in_file=\$1
out_file=\$2
grep -B1 -A$(($nbeads+$nproteins+7)) -w '${time}' \$in_file > \$out_file
" > $extract_sh

cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    pos_file="${in_dir}/config_${name}_run_${run}.lammpstrj"
    out_file="${out_dir}/config_${name}_run_$(($run+$run_shift)).lammpstrj"
    cmd[$jobid]="bash $extract_sh $pos_file $out_file"
    jobid=$(($jobid+1))
done

max_jobs=72 #8
total_jobs=$jobid
parallel_run

# Clean up
rm $extract_sh
