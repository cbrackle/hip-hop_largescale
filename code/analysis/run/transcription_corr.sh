#!/bin/bash
# transcription_corr.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: transcription_corr.sh sim_dir trans_dir"
    exit 1
fi

sim_dir=$1
trans_dir=$2

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
tstart=20000000
tend=30000000 #50000000
tinc=200000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

rc=3.5 # Cutoff radius

if [[ ! -d $sim_dir ]]; then
    echo "Error: cannot find the directory " $sim_dir
    exit 1
fi

if [[ ! -d $trans_dir ]]; then
    echo "Error: cannot find the directory " $trans_dir
    exit 1
fi

# Required programs
exe_path="${sh_dir}/../bin/analysis/exe/"
src_path="${sh_dir}/../src/"
sh_path="${sh_dir}/"
avg_py="${src_path}/stats/average_multi_files.py"
dat_to_bigwig="${sh_path}/dat_to_bigwig.sh"
to_genome_coords_exe="${exe_path}/convert_to_genome_coords"

# File names
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
trans_name="${name}_rc_${rc}_t_${tstart}-${tend}"
raw_bead_list="${sim_dir}/raw/${name}_run_1/bead_list.dat"
bead_list="${trans_dir}/bead_list.dat"
trans_file="${trans_dir}/trans_${trans_name}.dat"
trans_bw="${trans_dir}/trans_${trans_name}.bigWig"
trans_corr_file="${trans_dir}/trans_${trans_name}_corr.dat"
trans_atac_file="${trans_dir}/trans-ATAC_${trans_name}.dat"
trans_k9me3_file="${trans_dir}/trans-H3K9me3_${trans_name}.dat"
trans_k27ac_file="${trans_dir}/trans-H3K27ac_${trans_name}.dat"
trans_k27me3_file="${trans_dir}/trans-H3K27me3_${trans_name}.dat"
trans_unmark_file="${trans_dir}/trans-unmark_${trans_name}.dat"

# Convert data to bigwig files
function to_bigwig() {
$dat_to_bigwig $chrom $start_bp $end_bp $bp_per_bead 1 2 $trans_file $trans_bw
}

# Map bead density to baed type
function map_trans_to_bead_type() {
cp $raw_bead_list $trans_dir
tail -n+3 $bead_list > ${bead_list}.tmp
mv ${bead_list}.tmp $bead_list
paste -d" " $bead_list $trans_file > $trans_corr_file
# Output beads
awk -v atac=$trans_atac_file -v k9me3=$trans_k9me3_file -v k27ac=$trans_k27ac_file \
    -v k27me3=$trans_k27me3_file -v unmark=$trans_unmark_file '{
  if ($3==1) {print $8 > atac}
  if ($4==1) {print $8 > k9me3}
  if ($5==1) {print $8 > k27ac}
  if ($6==1) {print $8 > k27me3}
  if ($3==0 && $4==0 && $5==0 && $6==0) {print $8 > unmark}
}' $trans_corr_file
#rm $trans_corr_file
}

# Display in gnuplot
function plot() {
plot_gp="${trans_dir}/trans-plot_${trans_name}.gp"
echo "
stats '${trans_atac_file}' u 1
natac = STATS_records
matac = STATS_median
stats '${trans_k27ac_file}' u 1
nk27ac = STATS_records
mk27ac = STATS_median
stats '${trans_k27me3_file}' u 1
nk27me3 = STATS_records
mk27me3 = STATS_median
stats '${trans_k9me3_file}' u 1
nk9me3 = STATS_records
mk9me3 = STATS_median
stats '${trans_unmark_file}' u 1
nunmark = STATS_records
munmark = STATS_median

set xrange[0.0:0.6]
set table 'trans_atac'
plot '${trans_atac_file}' u 1:(1/natac) smooth kdensity
set table 'trans_k27ac'
plot '${trans_k27ac_file}' u 1:(1/nk27ac) smooth kdensity
set table 'trans_k27me3'
plot '${trans_k27me3_file}' u 1:(1/nk27me3) smooth kdensity
set table 'trans_k9me3'
plot '${trans_k9me3_file}' u 1:(1/nk9me3) smooth kdensity
set table 'trans_unmark'
plot '${trans_unmark_file}' u 1:(1/nunmark) smooth kdensity

unset table

set xrange [0.0:200.0]
set yrange [0.0:1.0]

# Set positions of individual violin plots
atac_pos=20.0
k27ac_pos=60.0
k27me3_pos=100.0
k9me3_pos=140.0
unmark_pos=180.0
buf=0.0

set border 15 lw 1
unset key

p 'trans_atac' u (atac_pos+\$2):1 w filledcurves x1=atac_pos-buf ls 1,\
'trans_atac' u (atac_pos-\$2):1 w filledcurve x1=atac_pos+buf ls 1,\
'trans_k27ac' u (k27ac_pos+\$2):1 w filledcurves x1=k27ac_pos-buf ls 2,\
'trans_k27ac' u (k27ac_pos-\$2):1 w filledcurve x1=k27ac_pos+buf ls 2,\
'trans_k27me3' u (k27me3_pos+\$2):1 w filledcurves x1=k27me3_pos-buf ls 3,\
'trans_k27me3' u (k27me3_pos-\$2):1 w filledcurve x1=k27me3_pos+buf ls 3,\
'trans_k9me3' u (k9me3_pos+\$2):1 w filledcurves x1=k9me3_pos-buf ls 4,\
'trans_k9me3' u (k9me3_pos-\$2):1 w filledcurve x1=k9me3_pos+buf ls 4,\
'trans_unmark' u (unmark_pos+\$2):1 w filledcurves x1=unmark_pos-buf ls 5,\
'trans_unmark' u (unmark_pos-\$2):1 w filledcurve x1=unmark_pos+buf ls 5

" > $plot_gp
gnuplot -p $plot_gp
rm trans_atac trans_k27ac trans_k27me3 trans_k9me3 trans_unmark
}

#to_bigwig
map_trans_to_bead_type
plot

