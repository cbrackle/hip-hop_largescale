#!/bin/bash
# config_to_dmap.sh

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ "$#" != 8 ]]; then
    echo "Usage: config_to_dmap.sh start_bead end_bead time_pt run_start run_end run_inc traj_dir dmap_dir"
    exit 1
fi

start_bead=$1
end_bead=$2
time_pt=$3
run_start=$4
run_end=$5
run_inc=$6
traj_dir=$7
dmap_dir=$8

if [[ ! -d $in_dir ]]; then
    echo "Error: cannot find the directory " $in_dir
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

dmap_exe="${sh_dir}/../bin/analysis/exe/rca/config_to_dmap"

cmd=()
jobid=0
for (( run=$run_start; $run<=$run_end; run+=$run_inc ))
do
    echo "Doing $run"
    name="bead_${start_bead}-${end_bead}_run_${run}"
    #traj_file="${traj_dir}/dump.${run}.DNA"
    traj_file="${traj_dir}/config_${name}.lammpstrj"
    dmap_file="${dmap_dir}/dmap_${name}.dat"
    cmd[$jobid]="$dmap_exe $start_bead $end_bead $time_pt $traj_file $dmap_file"
    jobid=$(($jobid+1))
done

# Parallel runs
max_jobs=8
if (( $_NTHREADS )); then
    max_jobs=$_NTHREADS
fi
total_jobs=$jobid
jobid=0
while (( $(bc <<< "$jobid < $total_jobs") ))
do
    for (( i=0; i<$max_jobs && $jobid < $total_jobs; i++))
    do
        echo "${cmd[jobid]} &"
        ${cmd[jobid]} &
        jobid=$(($jobid+1))
    done
    wait
done
