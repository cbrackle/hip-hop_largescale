#!/bin/bash
# signal_corr.sh
# A script which correlates the signal from different properties

# Directory of this script
sh_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ $# != 2 ]]; then
    echo "Usage: signal_corr.sh sim_dir out_dir"
    exit 1
fi

sim_dir=$1
out_dir=$2

if [[ ! -d $sim_dir ]]; then
    echo "Error: cannot find the directory $sim_dir"
    exit 1
fi

if [[ ! -d $out_dir ]]; then
    mkdir -p $out_dir
fi

cell="GM12878"
chrom="chr19"
chr=${chrom:3}
genome="hg19" # hg38
chromsizes="${sh_dir}/../../../genome/${genome}/${genome}.chrom.sizes"
chrom_size_bp=$(awk -v chr=$chrom '{if($1==chr){print $2}}' $chromsizes)
start_bp=0
end_bp=$chrom_size_bp
#start_bp=6500000
#end_bp=16500000
bp_per_bead=1000
bp_per_bin=10000
nbeads=$(python -c "
import math
print int(math.ceil((${end_bp}-${start_bp})/float(${bp_per_bead})))")

# Centromere region
cytoband="${sh_dir}/../../../genome/${genome}/${genome}.cytoBand.txt"
cen_start=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==1){print $2}}')
cen_end=$(grep $chrom $cytoband | grep 'acen' | awk '{if(NR==2){print $3}}')
echo $cen_start $cen_end

corr_type="pcorr" # "scorr"

# Required programs
src_dir="${sh_dir}/../src/"
pcorr_py="${src_dir}/stats/pearson_correlation.py"
scorr_py="${src_dir}/stats/spearman_correlation.py"

# Data files
name="${cell}_${genome}_chr_${chr}_${start_bp}-${end_bp}"
den_file="${sim_dir}/local_density/density-avg_${name}.dat"
gyr_file="${sim_dir}/local_gyration/gyr-avg_${name}.dat"
oci_file="${sim_dir}/oci/oci_sd_1000000/oci-sim_${name}.dat"
msd_exp_file="${sim_dir}/msd/msd_exp_10000000-20000000/msd-exp_${name}.dat"
msd_mob_file="${sim_dir}/msd/msd_mobility_0-1000000/msd-mobility_${name}.dat"

out_file="${out_dir}/signal_correlation.dat"

# Set up the correlation matrix
# den -> 0
# gyr -> 1
# oci -> 2
# msd_exp -> 3
# msd_mob -> 4
nfiles=5
oci_index=2
dat_file[0]=$den_file
dat_file[1]=$gyr_file
dat_file[2]=$oci_file
dat_file[3]=$msd_exp_file
dat_file[4]=$msd_mob_file
index_col[0]=0
index_col[1]=0
index_col[2]=0
index_col[3]=0
index_col[4]=0
value_col[0]=1
value_col[1]=1
value_col[2]=3
value_col[3]=1
value_col[4]=1

# Check all data files exist
for (( i=0; $i<$nfiles; i++ ))
do
    if [[ ! -f ${dat_file[$i]} ]]; then 
	echo "Error: cannot open the file ${dat_file[$i]}"
	exit 1
    fi
done

declare -A corr
declare -A pval

for (( i=0; $i<$nfiles; i++ ))
do
    corr[$i,$i]=1.0
    pval[$i,$i]=0.0
    for (( j=0; $j<$nfiles; j++ ))
    do
	if [[ $i != $j ]]; then
	    corr[$i,$j]=0.0
	    pval[$i,$j]=0.0
	fi
    done
done

# Python scripts
# A script to transform data to z-scores
zscore_py="${out_dir}/zscore.py"
echo "
import sys
from scipy import stats

args = sys.argv
data_file = args.pop(1)

values = []

with open(data_file, 'r') as reader:
    for line in reader:
        data = line.split()
        values.append(float(data[0]))
values = stats.zscore(values, ddof=1)

for val in values:
    print('{:f}'.format(val))
" > $zscore_py

# A script to rebin the data
rebin_py="${out_dir}/rebin.py"
echo "
import sys
import math
import numpy as np

args = sys.argv
data_file = args.pop(1)

sbp = int($start_bp)
ebp = int($end_bp)
bppb = int($bp_per_bin)
nbins = int(math.ceil((ebp-sbp)/float(bppb)))

rebin_data = [0.0 for i in xrange(nbins)]
rebin_count = [0 for i in xrange(nbins)]

with open(data_file, 'r') as reader:
    for line in reader:
        data = line.split()
        bp = int(float(data[0]))
        value = float(data[1])
        bin = (bp-sbp)/bppb
        rebin_data[bin] += value
        rebin_count[bin] += 1

for i in range(nbins):
    if (rebin_count[i] > 0):
        rebin_data[i] /= float(rebin_count[i])

for i in range(nbins):
    print('{:d} {:f}'.format(sbp+int((i+0.5)*bppb),rebin_data[i]))
" > $rebin_py

function compute_corr() {
    local ctype=$1
    local index_col1=$2
    local index_col2=$3
    local value_col1=$4
    local value_col2=$5
    local file1=$6
    local file2=$7
    local out_file=$8

    local signal1="${out_dir}/signal1.dat"
    local signal2="${out_dir}/signal2.dat"
    
    awk -v icol=$index_col1 -v vcol=$value_col1 '{
print $icol,$vcol}' $file1 > $signal1
    awk -v icol=$index_col2 -v vcol=$value_col2 '{
print $icol,$vcol}' $file2 > $signal2
    local npts1=$(wc -l $signal1 | awk '{print $1}')
    local npts2=$(wc -l $signal2 | awk '{print $1}')
    if [[ $npts1 != $npts2 ]]; then
	echo "Error: different number of points in the two signal files"
	exit 1
    fi

    # Remove the centromere region
    awk -v scen=$cen_start -v ecen=$cen_end '{
if ($1<scen || $1>ecen) {print $2}}' $signal1 > ${signal1}.tmp
    awk -v scen=$cen_start -v ecen=$cen_end '{
if ($1<scen || $1>ecen) {print $2}}' $signal2 > ${signal2}.tmp

    # Transform data to z-scores
    python $zscore_py ${signal1}.tmp > $signal1
    python $zscore_py ${signal2}.tmp > $signal2

    local npts1=$(wc -l $signal1 | awk '{print $1}')
    local npts2=$(wc -l $signal2 | awk '{print $1}')

    # Compute the correlation
    if [[ $ctype == "pcorr" ]]; then
	python $pcorr_py $npts1 -1 0 $signal1 $signal2 $out_file
    else
	python $scorr_py $npts1 -1 0 $signal1 $signal2 $out_file
    fi

    rm $signal1 $signal2 ${signal1}.tmp ${signal2}.tmp
}

function compute_norm_corr() {
    for (( i=0; $i<$nfiles; i++ ))
    do
	if [[ $i != $oci_index ]]; then
	for (( j=0; $j<=$i; j++ ))
	do
	    if [[ $j != $oci_index ]]; then
	    echo "Computing correlation between ${dat_file[$i]} and ${dat_file[$j]} ..."
	    local corr_out_file="${out_dir}/${corr_type}_${i}-${j}.dat"
	    compute_corr $corr_type ${index_col[$i]} ${index_col[$j]} ${value_col[$i]} ${value_col[$j]} ${dat_file[$i]} ${dat_file[$j]} $corr_out_file
	    corr[$i,$j]=$(awk '{print $1}' $corr_out_file)
	    pval[$i,$j]=$(awk '{print $2}' $corr_out_file)
	    corr[$j,$i]=${corr[$i,$j]}
	    pval[$j,$i]=${pval[$i,$j]}
	    echo $i $j ${corr[$i,$j]} ${pval[$i,$j]} ${corr[$j,$i]} ${pval[$j,$i]}
	    rm $corr_out_file
	    fi
	done
	fi
    done
}

function compute_oci_corr() {
    for (( i=0; $i<$nfiles; i++ ))
    do
	local rebin_file="${out_dir}/rebin_${i}.dat"
	local corr_out_file="${out_dir}/${corr_type}_${oci_index}-${i}.dat"
	if [[ $i != $oci_index ]]; then
	    python $rebin_py ${dat_file[$i]} > $rebin_file
	else
	    rebin_file=${dat_file[$oci_index]}
	fi
	compute_corr $corr_type ${index_col[$oci_index]} ${index_col[$i]} ${value_col[$oci_index]} ${value_col[$i]} ${dat_file[$oci_index]} $rebin_file $corr_out_file
	corr[$oci_index,$i]=$(awk '{print $1}' $corr_out_file)
	pval[$oci_index,$i]=$(awk '{print $2}' $corr_out_file)
	corr[$i,$oci_index]=${corr[$oci_index,$i]}
	pval[$i,$oci_index]=${pval[$oci_index,$i]}
	echo $oci_index $i ${corr[$oci_index,$i]} ${pval[$oci_index,$i]} ${corr[$i,$oci_index]} ${pval[$i,$oci_index]}
	rm $corr_out_file
    done
}

function print_corr() {
    echo \
"# Correlation of the measured properties tracks
# track 0: $(basename $den_file)
# track 1: $(basename $gyr_file)
# track 2: $(basename $oci_file)
# track 3: $(basename $msd_exp_file)
# track 4: $(basename $msd_mob_file)
" > $out_file 

    for (( i=0; $i<$nfiles; i++ ))
    do
	for (( j=0; $j<$nfiles; j++ ))
	do
	    echo $i $j ${corr[$i,$j]} ${pval[$i,$j]} >> $out_file
	done
	echo "" >> $out_file
    done
}

function plot_corr() {
plot_gp="${out_dir}/plot_signal_corr.gp"
echo \
"
set terminal x11 enhanced font 'Arial,14'
load '${sh_dir}/../../../gnuplot-palettes/moreland.pal'
set cbrange [-1:1]
set xrange [-0.5:4.5]
set yrange [-0.5:4.5]

set xtics ('{/Symbol r}' 0, 'R_g' 1, 'OCI' 2, '{/Symbol a}' 3, 'M' 4)
set ytics ('{/Symbol r}' 0, 'R_g' 1, 'OCI' 2, '{/Symbol a}' 3, 'M' 4)

p '${out_file}' u 1:2:3 w image
" > $plot_gp
gnuplot -p $plot_gp
}

function clean_up() {
    rm $zscore_py
    rm $rebin_py
}

#compute_norm_corr
#compute_oci_corr
#print_corr
plot_corr
#clean_up
