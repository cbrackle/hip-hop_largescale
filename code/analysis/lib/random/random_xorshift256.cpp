// random_xorshift256.cpp

#include <cstdint>
#include "random_xorshift256.hpp"

using RXOR = RandomXORShift256;

RXOR::RandomXORShift256(unsigned long seed) {
  // Initialise the state using a splitmix algorithm
  uint64_t x = (uint64_t) seed;
  for (int i = 0; i < 4; i++) {
    uint64_t z = (x += 0x9e3779b97f4a7c15);
    z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
    z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
    s[i] = z ^ (z >> 31);
  }
}

RXOR::~RandomXORShift256() {}

double RXOR::randDouble() {
  return ((double) next() / (uint64_t) UINT64_MAX);
}

int RXOR::randInt(int min, int max) {
  return min + static_cast<int>((max-min)*randDouble());
}

inline uint64_t rotl(const uint64_t x, int k) {
  return (x << k) | (x >> (64 - k));
}

uint64_t RXOR::next() {
  const uint64_t result = rotl(s[0] + s[3], 23) + s[0];
  const uint64_t t = s[1] << 17;
  s[2] ^= s[0];
  s[3] ^= s[1];
  s[1] ^= s[2];
  s[0] ^= s[3];
  s[2] ^= t;
  s[3] = rotl(s[3], 45);
  return result;
}
