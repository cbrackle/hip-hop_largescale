// random.hpp

#ifndef RANDOM_HPP
#define RANDOM_HPP

class Random {
public:
  virtual ~Random() {}
  virtual double randDouble() = 0; // Return a random float in [0,1)
  // Return a random int in [min,max)
  virtual int randInt(int min, int max) = 0;
  
  // Static factory methods
  static Random* createMT19937(unsigned long seed);
  static Random* createMTwister(unsigned long seed);
  static Random* createXORShift256(unsigned long seed);

  // Handle random object destruction
  static void deleteRandom(Random* random);
};

#endif

