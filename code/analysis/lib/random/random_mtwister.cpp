// random_mtwister.cpp

#include <cstdlib>
#include <ctime>
#include "random_mtwister.hpp"
#include "mtwister.h"

using RMT = RandomMTwister;

RMT::RandomMTwister(unsigned long s) {
  seed = s;
  srand(seed);
  mtrand = (MTRand*) malloc(sizeof(MTRand));
  *mtrand = seedRand(rand());
}

RMT::~RandomMTwister() {
  free(mtrand);
}

double RMT::randDouble() {
  return genRand(mtrand);
}

int RMT::randInt(int min, int max) {
  return min + static_cast<int>((max-min)*genRand(mtrand));
}
