// random_mt19937.cpp

#include <random>
#include "random_mt19937.hpp"

using RMT = RandomMT19937;

RMT::RandomMT19937(unsigned long s) {
  seed = s;
  mtrand = std::mt19937(s);
  realDistrb = std::uniform_real_distribution<double>(0.0,1.0);
}

RMT::~RandomMT19937() {}

double RMT::randDouble() {
  return realDistrb(mtrand);
}

int RMT::randInt(int min, int max) {
  return min + static_cast<int>((max-min)*realDistrb(mtrand));
}
