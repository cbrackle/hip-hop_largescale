// random_xorshift256.hpp
// Wrapper file to the xorshift256++ generator

#ifndef RANDOM_XORSHIFT256_HPP
#define RANDOM_XORSHIFT256_HPP

#include <cstdint>
#include "random.hpp"

class RandomXORShift256 : public Random {
private:
  uint64_t s[4];

  // Internal functions
  uint64_t next();

public:
  RandomXORShift256(unsigned long seed);
  ~RandomXORShift256();
  double randDouble();
  int randInt(int min, int max);
  
};

#endif
