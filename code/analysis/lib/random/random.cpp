#include "random.hpp"
#include "random_mt19937.hpp"
#include "random_mtwister.hpp"
#include "random_xorshift256.hpp"

Random* Random::createMT19937(unsigned long seed) {
  return new RandomMT19937(seed);
}

Random* Random::createMTwister(unsigned long seed) {
  return new RandomMTwister(seed);
}

Random* Random::createXORShift256(unsigned long seed) {
  return new RandomXORShift256(seed);
}

void Random::deleteRandom(Random* rand) {
  delete rand;
}
