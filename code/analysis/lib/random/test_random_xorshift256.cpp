#include <cstdio>
#include <string>
#include "random.hpp"

using std::string;

int main(int argc, char* argv[]) {
  int argi = 0;
  unsigned long seed = stoul(string(argv[++argi]), nullptr, 10);
  int num = stoi(string(argv[++argi]), nullptr, 10);
  
  Random* rand = Random::createXORShift256(seed);

  for (int i = 0; i < num; i++) {
    printf("%.10f\n", rand->randDouble());
  }
  Random::deleteRandom(rand);
}
