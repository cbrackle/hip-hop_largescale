// random_mtwister.hpp
// Wrapper file to a meserenne twister random generator

#include "random.hpp"
#include "mtwister.h"

class RandomMTwister : public Random {
private:
  unsigned long seed;
  MTRand* mtrand;

public:
  RandomMTwister(unsigned long seed);
  ~RandomMTwister();
  double randDouble();
  int randInt(int min, int max);
};
