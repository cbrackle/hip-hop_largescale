// random_mt19937.hpp

#include <random>
#include "random.hpp"

class RandomMT19937 : public Random {
private:
  unsigned long seed;
  std::mt19937 mtrand;
  std::uniform_real_distribution<double> realDistrb;
  
public:
  RandomMT19937(unsigned long seed);
  ~RandomMT19937();
  double randDouble();
  int randInt(int min, int max);
};
