// factorial.cpp

#include <cassert>
#include "factorial.hpp"

template<class T>
int Factorial<T>::max = 19;

// Retrieve the factorial of i (i!). All factorials are computed only in the 
// first time this function is called. After this thy are just read from
// the store
template<class T>
inline T Factorial<T>::get(int i) {
  assert(i >= 0 && i <= max);
  if (!factorials.size()) {
    computeFactorials();
  }
  if (!i) return 1; // For factorial 0! = 1
  return factorials[i-1];
}

// Return the result i*(i+1)*...*(j-1)*j
template<class T>
inline T Factorial<T>::get(int i, int j) {
  T result = static_cast<T>(1);
  for (int n = j; n >= i; n--) {
    result *= static_cast<T>(n);
  }
  return result;
}

// Modify the maximum factorial input parameter. All factorials are 
// recomputed here
template<class T>
inline void Factorial<T>::setMax(int m) {
  assert(m >= static_cast<T>(0));

  // In fact, the previously computed factorials could be reused here;
  // however, since this is rarely performed and takes only a couple of
  // multiplications, we don't care
  max = m;
  if (max <= m) {
    computeFactorials();
  }
}

template<class T>
inline int Factorial<T>::getMax() {
  return max;
}

// Compute factorials up to max and store them internally
template<class T>
inline void Factorial<T>::computeFactorials() {
  factorials.resize(max);
  // The zeroth element stores the factorial 1!
  // The first element stores the factorial 2! and so on
  factorials[0] = static_cast<T>(1);
  for (int i = 1; i < max; i++) {
    factorials[i] = factorials[i-1]*static_cast<T>(i+1);
  }
}
