// geometric_moments.hpp

#ifndef GEOMETRIC_MOMENTS_HPP
#define GEOMETRIC_MOMENTS_HPP

#include <vector>

// Class for computing the scaled, pre-integrated geometric moments.
// These tricks are needed to make the computation numerically stable.
// \param VoxelT type of the voxel values
// \param MomentT type of the moments -- recommended to be double

template<class VoxelT, class MomentT>
class GeometricMoments {

public: 
  // Public typedefs
  typedef MomentT T; // The moment type
  typedef std::vector<T> T1D; // 1D array scalar type
  typedef std::vector<T1D> T2D; // 2D array scalar type
  typedef std::vector<T2D> T3D; // 3D array scalar type
  typedef typename T1D::iterator T1DIter;

  // Public member functions
  // Construction/init
  // Constructor
  GeometricMoments
  (const VoxelT* voxels, // Input voxel grid
   int xDim, // x-dimension of the input voxel grid
   int yDim, // y-dimension of the input voxel grid
   int zDim, // z-dimension of the input voxel grid
   double xMin, // x-coord of starting point
   double yMin, // y-coord of starting point
   double zMin, // z-coord of starting point
   double scale, // Grid spacing
   int maxOrder // Maximal order to compute moments for
   );

  // Default constructor
  GeometricMoments();

  // The init function used by the constructors
  void init(const VoxelT* voxels, int xDim, int yDim, int zDim, double xMin, 
	    double yMin, double zMin, double scale, int maxOrder);

  // Access function
  T getMoment
  (int i, // Order along x
   int j, // Order along y
   int k // Order along z
   ) const;
  
private:
  // Private attributes
  int xDim, yDim, zDim; // Dimensions
  
  int maxOrder; // Maximal order of the moments
  T2D samples; // Samples of the scaled and translated grid in x, y, z
               // i.e. the scaled LAB coordiantes of each grid point
               // This gives the sample vector in each dimension
               // i.e. x = (x_0,x_1,...,x_N)^T, y = (y_0,y_1,...,y_N)^T etc.
  T1D voxels; // Array containing the voxel grid
  T3D moments; // Array containing the cumulative moments

  // Private member functions
  // Main function to compute moments
  void compute();
  // Compute the scaled and translated grid in x, y, z
  void computeSamples(double xMin, double yMin, double zMin, double scale);
  // Compute the diff vector F_0' = (-f_0,f_0-f_1,...,f_{N-1})^T as described
  // in the paper
  void computeDiffFunction(T1DIter iter, T1DIter diffIter, int dim);
  // Do the successive element-wise multiplication of the sample vector 
  // with the diff vector and return the value of the moment for the order 
  // after the multiplication
  T multiply(T1DIter diffIter, T1DIter sampleIter, int dim);   
};

#include "geometric_moments.tpp"

#endif
