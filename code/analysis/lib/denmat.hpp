// denmat.hpp
// A template class for storing a dense matrix of objects

#ifndef DENMAT_HPP
#define DENMAT_HPP

#include <iostream>
#include <string>
#include <vector>

namespace mat {

  template<class T>
  class DenMat {
  private:
    size_t matSize;
    std::vector<T> mat;
    
  public:
    DenMat() : matSize(0) {}
    DenMat(size_t n) : matSize(n) {
      mat = std::vector<T>(n*n);
    }
    inline size_t size() const {return matSize;}
    inline T& operator()(size_t i, size_t j) {
      return mat[i*matSize+j];
    }
    inline const T& operator()(size_t i, size_t j) const {
      return mat[i*matSize+j];
    }
  };

  //typedef DenMat Mat;
}

#endif
