// denmat_stream.hpp
// A helper class to read and write dense matrices

#ifndef DENMAT_STREAM_HPP
#define DENMAT_STREAM_HPP

#include <string>
#include <functional>
#include "denmat.hpp"

namespace mat {

  template<class T>
  class DenMatStream {
  public:
    DenMatStream();
    bool read(const std::string& file, DenMat<T>& mat);
    template<class K>
    bool read(const std::string& file, DenMat<T>& mat,
	      const std::function<size_t(const K&)> index);
    
    bool dump(const std::string& file, const DenMat<T>& mat);
    template<class K>
    bool dump(const std::string& file, const DenMat<T>& mat,
	      const std::function<K(const size_t&)> key);
    
    bool dumpToGnuplot(const std::string& file, const DenMat<T>& mat);
    template<class K>
    bool dumpToGnuplot(const std::string& file, const DenMat<T>& mat,
		       const std::function<K(const size_t&)> key);
  };

}

#include "denmat_stream.tpp"

#endif
