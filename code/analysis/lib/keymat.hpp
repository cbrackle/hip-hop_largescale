// keysymmat.hpp
// A template class for storing a symmetric matrix of objects

#ifndef KEYMAT_HPP
#define KEYMAT_HPP

#include <vector>
#include <map>

namespace mat {

  template<template<class> class M, class K, class T>
  class KeyMat {
  private:
    M<T> mat;
    std::map<K,size_t> indexmap;
    std::vector<K> keymap;
    
  public:
    KeyMat() {}
    KeyMat(size_t n) {
      mat = M<T>(n);
      keymap = std::vector<K>(n);
      for (size_t i = 0; i < n; i++) {
	indexmap[i] = i;
	keymap[i] = i;
      }
    }
    KeyMat(size_t n, const std::vector<K>& keys) {
      mat = M<T>(n);
      keymap = std::vector<K>(n);
      for (size_t i = 0; i < n; i++) {
	indexmap[keys[i]] = i;
	keymap[i] = keys[i];
      }
    }
    void setKeys(const std::vector<K>& keys) {
      indexmap.clear();
      keymap.clear();
      keymap = std::vector<K>(mat.size());
      for (size_t i = 0; i < mat.size(); i++) {
	indexmap[keys[i]] = i;
	keymap[i] = keys[i];
      }
    }
    inline size_t getIndex(const K& key) const {return indexmap.at(key);}
    inline K getKey(const size_t& i) const {return keymap.at(i);}
    inline const std::vector<K>& getKeys() const {return keymap;}
    inline M<T>& getMat() {return mat;}
    inline const M<T>& getConstMat() const {return mat;}
    inline size_t size() const {return mat->size();}
    inline T& operator()(size_t i, size_t j) {
      return mat(i,j);
    }
    inline const T& operator()(size_t i, size_t j) const {
      return mat(i,j);
    }
    inline T& at(K k1, K k2) {
      return mat(indexmap.at(k1),indexmap.at(k2));
    }
    inline const T& at(K k1, K k2) const {
      return mat(indexmap.at(k1),indexmap.at(k2));
    }
  };

}

#endif
