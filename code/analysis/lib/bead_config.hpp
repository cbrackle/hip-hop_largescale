/* bead_config.hpp
 * An interface for all classes modelling bead data
 */

#ifndef BEAD_CONFIG_HPP
#define BEAD_CONFIG_HPP

class BeadConfig {
public:
  virtual ~BeadConfig() {}
  virtual void setData(int nbeads, const double boxSize[3]) = 0;
  virtual void setPosition(int index, int comp, double val) = 0;
  virtual void setBoundCount(int index, int comp, int val) = 0;
  virtual void setType(int index, int val) = 0;
  virtual double getPosition(int index, int comp) const = 0;
  virtual int getBoundCount(int index, int comp) const = 0;
  virtual int getType(int index) const = 0;
};

#endif
