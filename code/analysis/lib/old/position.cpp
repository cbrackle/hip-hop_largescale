// position.cpp

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <string>
#include "array.hpp"
#include "position.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::stringstream;
using std::string;

// Constructor
PositionReader::PositionReader() : fileOpen(false) {}

// Destructor
PositionReader::~PositionReader() {
  deleteData();
}

void PositionReader::initData() {
  position = create2DDoubleArray(nbeads, 3);
  boundaryCount = create2DIntArray(nbeads, 3);
  type = create1DIntArray(nbeads);
}

void PositionReader::deleteData() {
  if (position) deleteArray(position);
  if (boundaryCount) deleteArray(boundaryCount);
  if (type) deleteArray(type);
}

bool PositionReader::open(const string& posFile) {
  // Peek the header lines to get the number of atoms and the box size
  // It assumes these parameters do not change over the time frames

  // Close the stream to the previous file
  if (fileOpen) close();
  
  reader.open(posFile);
  if (!reader) {
    cout << "Error: canont open the file " << posFile << endl;
    return false;
  }

  string line;
  stringstream ss;
  getline(reader, line); // ITEM: TIMESTEP
  getline(reader, line); // timestep
  getline(reader, line); // ITEM: NUMBER OF ATOMS
  getline(reader, line);
  ss.str(line); ss >> nbeads; ss.clear();
  getline(reader, line); // ITEM: BOX BOUNDS
  for (int i = 0; i < 3; i++) {
    getline(reader, line);
    ss.str(line); ss >> boxLo[i] >> boxHi[i]; ss.clear();
    boxSize[i] = boxHi[i]-boxLo[i];
  }
  initData();
  reader.clear();
  reader.seekg(0, std::ios::beg); // Go back to the start

  // Count the number of frames in the file
  FILE* file = fopen(posFile.c_str(), "r");
  int ch;
  int nlines = 0; 
  while (EOF != (ch=getc(file))) {
    if (ch == '\n') nlines++;
  }
  nframes = nlines/(nbeads+9); // There are 9 header lines
  fileOpen = true;
  return true;
}

void PositionReader::close(){
  if (reader.is_open()){
    reader.close();
  }
  deleteData();
  fileOpen = false;
}

double PositionReader::getPosition(int index, int comp) const {
  return position[index][comp];
}

double PositionReader::getUnwrappedPosition(int index, int comp) const {
  return position[index][comp] + 
    boxSize[comp] * boundaryCount[index][comp];
}

int PositionReader::getType(int index) const {
  return type[index];
}

bool PositionReader::nextFrame() {
  if (!fileOpen || reader.eof()) return false;
  
  string line;
  double xs, ys, zs;
  int index, ix, iy, iz, t;
  stringstream ss;
  
  // Read header lines
  getline(reader, line); // ITEM: TIMESTEP
  getline(reader, line); // timestep
  ss.str(line); ss >> time; ss.clear();
  getline(reader, line); // ITEM: NUMBER OF ATOMS
  getline(reader, line);
  getline(reader, line); // ITEM: BOX BOUNDS
  for (int i = 0; i < 3; i++) {
    getline(reader, line);
  }
  getline(reader, line); // ITEM: ATOMS id type xs ys zs ix iy iz
  
  // Read bead position data
  for (int i {}; i < nbeads; i++){
    getline(reader, line);
    ss.str(line);
    ss >> index >> t >> xs >> ys >> zs >> ix >> iy >> iz;
    ss.clear();
    position[i][0] = xs*boxSize[0]+boxLo[0];
    position[i][1] = ys*boxSize[1]+boxLo[1];
    position[i][2] = zs*boxSize[2]+boxLo[2];
    boundaryCount[i][0] = ix;
    boundaryCount[i][1] = iy;
    boundaryCount[i][2] = iz;
    type[i] = t;
  }
  
  return true;
}

long PositionReader::getTime() const {
  return time;
}

bool PositionReader::isOpen() const {
  return fileOpen;
}

int PositionReader::frames() const {
  return nframes;
}
