// factorial.hpp
// A template class for precomputation and subsequent retrieval of 
// factorials of an integer number

#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <vector>

template<class T>
class Factorial {

public:
  // Get the factorial of i
  static T get(int i);
  // Get i*(i+1)*...*(j-1)*j
  static T get(int i, int j);
  // Set the maximal stored factorial value to max
  static void setMax(int max);
  // Get the maximal stored factorial value
  static int getMax();

private:
  // Compute factorials of numbers [1...max] (both inclusive)
  static void computeFactorials();
  static int max;
  static std::vector<T> factorials;

};

#include "factorial.tpp"

#endif
