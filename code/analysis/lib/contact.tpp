// contact.tpp

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <map>
#include <functional>
#include <cmath>
#include "contact.hpp"

template<class K, class T>
ContactMap<K,T>::ContactMap(K n) : 
  nbins(n), startBp(0), endBp(n), bpPerBin(1) {}

template<class K, class T>
ContactMap<K,T>::ContactMap(long sbp, long binsize, K n, bool useGenome) : 
  nbins(n), hasGenomeCoords(true), useGenomeCoords(useGenome), 
  startBp(sbp), bpPerBin(binsize) {
  endBp = sbp+nbins*binsize;
}

template<class K, class T>
ContactMap<K,T>::ContactMap(long sbp, long ebp, long binsize, bool useGenome) :
  hasGenomeCoords(true), useGenomeCoords(useGenome), 
  startBp(sbp), bpPerBin(binsize) {
  nbins = static_cast<K>(ceil(static_cast<double>(ebp-sbp)/
			      static_cast<double>(binsize)));
  endBp = sbp+nbins*binsize;
}

template<class K, class T>
T& ContactMap<K,T>::operator()(K i, K j) {
  return contact[mapCoords(i,j)];
}

template<class K, class T>
const T& ContactMap<K,T>::operator()(K i, K j) const {
  typename std::map<std::pair<K,K>,T>::const_iterator it =
    contact.find(mapCoords(i,j));
  if (it != contact.end()) {
    return it->second;
  } else {
    return zero;
  }
}

template<class K, class T>
const T& ContactMap<K,T>::at(K i, K j) const {
  return (*this)(i,j);
}

template<class K, class T>
inline K ContactMap<K,T>::size() const { return nbins;}

template<class K, class T>
inline void ContactMap<K,T>::clear() {contact.clear();}

template<class K, class T>
bool ContactMap<K,T>::read(const std::string& file) {
  if (hasGenomeCoords && useGenomeCoords) {
    return read<long>
      (file, [this] (const long& i) -> K {return this->bpToBin(i);});
  } else {
    return read<K>(file, [] (const K& i) -> K {return i;});
  }
}

template<class K, class T>
template<class U>
bool ContactMap<K,T>::read(const std::string& file,
			   std::function<K(const U&)> tobin) {
  std::ifstream reader;
  reader.open(file);
  if (!reader) {
    std::cout << "Error: cannot open the file " << file << std::endl;
    return false;
  }
  
  // File should be in i j value format
  std::string line;
  std::stringstream ss;
  U ui, uj;
  K i, j;
  T value;

  bool firstline = true;
  bool readHeader = false;
  std::stringstream sshead;
  while (getline(reader, line)) {
    // Read header lines
    if (firstline && line.size() > 0 && line[0] == '#') {
      readHeader = true;
      firstline = false;
      sshead.clear();
    }
    if (readHeader) {
      if (line[0] == '#') {
	sshead << line << "\n";
	continue;
      } else {
	header = sshead.str();
	readHeader = false;
      }
    }
    // Skip comments and empty lines
    if (line.size() == 0 || line[0] == '#') continue;
    ss.str(line);
    ss >> ui >> uj >> value;
    ss.clear();
    i = tobin(ui);
    j = tobin(uj);
    if (i < 0 || i >= nbins) {
      std::cout << "Error: index out of bound: i = " << i << std::endl;
      return false;
    }
    if (j < 0 || j >= nbins) {
      std::cout << "Error: index out of bound: j = " << j << std::endl;
      return false;
    }
    if (value > 0.0) {
      contact[mapCoords(i,j)] = value;
    }
  }
  
  reader.close();
  return true;
}

template<class K, class T>
bool ContactMap<K,T>::dump(const std::string& file) const {
  if (hasGenomeCoords && useGenomeCoords) {
    return dump<long>(file, [this] (const K& i) -> long {
	return this->binToBp(i);});
  } else {
    return dump<K>(file, [] (const K& i) -> K {return i;});
  }
}

template<class K, class T>
template<class U>
bool ContactMap<K,T>::dump(const std::string& file, 
			   std::function<U(const K&)> key) const {
  std::ofstream writer;
  writer.open(file);
  if (!writer) {
    std::cout << "Error: cannot open the file " << file << std::endl;
    return false;
  }
  
  if (useHeader) {
    writer << header << "\n";
  }

  for (typename std::map<std::pair<K,K>,T>::iterator it = contact.begin();
       it != contact.end(); it++) {
    writer << key(it->first.first) << " " << key(it->first.second) << " "
	   << it->second << "\n";
  }
  writer.close();
  return true;
}

template<class K, class T>
bool ContactMap<K,T>::dumpToGnuplot(const std::string& file) const {
  return dumpToGnuplot(0, nbins, file);
}

template<class K, class T>
bool ContactMap<K,T>::dumpToGnuplot(K sbin, K ebin, 
				    const std::string& file) const {
  if (hasGenomeCoords && useGenomeCoords) {
    return dumpToGnuplot<long>(sbin, ebin, file, [this] (const K& i) -> long {
	return this->binToBp(i);});
  } else {
    return dumpToGnuplot<K>(sbin, ebin, file, 
			    [] (const K& i) -> K {return i;});
  };
}

template<class K, class T>
template<class U>
bool ContactMap<K,T>::dumpToGnuplot(K sbin, K ebin, const std::string& file,
				    std::function<U(const K&)> key) const {
  if (ebin < sbin) {
    std::cout << "Error: start index must be greater than end index" 
	      << std::endl;
    return false;
  }

  std::ofstream writer;
  writer.open(file);
  if (!writer) {
    std::cout << "Error: cannot open the file " << file << std::endl;
    return false;
  }

  if (useHeader) {
    writer << header << "\n";
  }
  
  // NOTE: end index is exclusive
  for (K i = sbin; i < ebin; i++) {
    for (K j = sbin; j < ebin; j++) {
      writer << key(i) << " " << key(j) << " " << (*this)(i,j) << "\n";
    }
    writer << "\n";
  }
  writer.close();
  return true;
}

template<class K, class T>
bool ContactMap<K,T>::dumpToGnuplot(const ContactMap<K,T>& cmap1,
				    const ContactMap<K,T>& cmap2,
				    const std::string& file) {
  /*std::function<K(const K&)> func = 
    [] (const K& i) -> K {return i;};*/
  return dumpToGnuplot(0, cmap1.nbins, cmap1, cmap2, file);
}

template<class K, class T>
bool ContactMap<K,T>::dumpToGnuplot(K sbin, K ebin,
				    const ContactMap<K,T>& cmap1,
				    const ContactMap<K,T>& cmap2,
				    const std::string& file) {
  if (cmap1.hasGenomeCoords && cmap1.useGenomeCoords &&
      cmap2.hasGenomeCoords && cmap2.useGenomeCoords) {
    return dumpToGnuplot<long>(sbin, ebin, cmap1, cmap2, file, 
			       [cmap1] (const K& i) -> long {
				 return cmap1.binToBp(i);});
  } else {
    return dumpToGnuplot<K>(sbin, ebin, cmap1, cmap2, file, 
			    [] (const K& i) -> K {return i;});
  };  
}

template<class K, class T>
template<class U>
bool ContactMap<K,T>::dumpToGnuplot(K sbin, K ebin,
				    const ContactMap<K,T>& cmap1,
				    const ContactMap<K,T>& cmap2,
				    const std::string& file,
				    std::function<U(const K&)> key) {
  if (cmap1.nbins != cmap2.nbins) {
    std::cout << "Error: maps have different dimensions" << std::endl;
    return false;
  }

  std::ofstream writer;
  writer.open(file);
  if (!writer) {
    std::cout << "Error: cannot open the file " << file << std::endl;
    return false;
  }
  
  K nb = cmap1.nbins;
  T value;
  for (K i = 0; i < nb; i++) {
    for (K j = 0; j < nb; j++) {
      if (j < i) {
	value = cmap2(i,j);
      } else if (j == i) {
	value = 0;
      } else {
	value = cmap1(i,j);
      }
      writer << key(i) << " " << key(j) << " " << value << "\n";
    }
    writer << "\n";
  }
  writer.close();
  return true;
}

template<class K, class T>
inline void ContactMap<K,T>::enableGenomeCoords(bool value) {
  if (hasGenomeCoords) useGenomeCoords = value;
}

template<class K, class T>
inline void ContactMap<K,T>::enableHeader(bool value) {
  useHeader = value;
}

template<class K, class T>
inline void ContactMap<K,T>::setHeader(const std::string& value) {
  header = value;
}

template<class K, class T>
ContactMap<K,T>& ContactMap<K,T>::operator+=(const ContactMap<K,T>& cmap) {
  if (cmap.nbins != nbins) {
    std::cout << "Warning: cannot add contact maps of different dimensions"
	      << " - ignoring addition" << std::endl;
  } else {
    for (typename std::map<std::pair<K,K>,T>::const_iterator it = 
	   cmap.contact.begin(); it != cmap.contact.end(); it++) {
      this->contact[it->first] += it->second;
    }
  }
  return *this;
}

template<class K, class T>
ContactMap<K,T>& ContactMap<K,T>::operator*=(const T& value) {
  for (typename std::map<std::pair<K,K>,T>::iterator it = contact.begin(); 
       it != contact.end(); it++) {
    contact[it->first] *= value;
  }  
  return *this;
}

template<class K, class T>
ContactMap<K,T>& ContactMap<K,T>::operator/=(const T& value) {
  for (typename std::map<std::pair<K,K>,T>::iterator it = contact.begin(); 
       it != contact.end(); it++) {
    contact[it->first] /= value;
  }  
  return *this;
}

template<class K, class T>
K ContactMap<K,T>::bpToBin(const long& bp) const {
  return static_cast<K>((bp-startBp)/bpPerBin);
}

template<class K, class T>
long ContactMap<K,T>::binToBp(const K& i) const {
  return static_cast<long>(startBp+(i+0.5)*bpPerBin);
}

// Internal helper functions
template<class K, class T>
inline std::pair<K,K> ContactMap<K,T>::mapCoords(K i, K j) {
  return (i <= j ? std::make_pair(i,j) : std::make_pair(j,i));
}

// Iterators and constant iterators (wrappers around map iterators)
template<class K, class T>
inline typename ContactMap<K,T>::iter ContactMap<K,T>::begin() {
  return contact.begin();
}

template<class K, class T>
inline typename ContactMap<K,T>::iter ContactMap<K,T>::end() {
  return contact.end();
}

template<class K, class T>
inline typename ContactMap<K,T>::const_iter ContactMap<K,T>::begin() const {
  return contact.begin();
}

template<class K, class T>
inline typename ContactMap<K,T>::const_iter ContactMap<K,T>::end() const {
  return contact.end();
}
