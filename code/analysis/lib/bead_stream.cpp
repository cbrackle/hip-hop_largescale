// position.cpp

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <string>
#include "bead_config.hpp"
#include "bead_stream.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::stringstream;
using std::string;

// Constructor
BeadStream::BeadStream() : fileOpen(false) {}

// Destructor
BeadStream::~BeadStream() {}

bool BeadStream::open(const string& posFile, BeadConfig& conf) {
  // Peek the header lines to get the number of atoms and the box size
  // It assumes these parameters do not change over the time frames

  // Close the stream to the previous file
  if (fileOpen) close();
  
  reader.open(posFile);
  if (!reader) {
    cout << "Error: cannot open the file " << posFile << endl;
    return false;
  }

  config = &conf;

  string line;
  stringstream ss;
  getline(reader, line); // ITEM: TIMESTEP
  getline(reader, line); // timestep
  ss.str(line); ss >> startTime; ss.clear();
  getline(reader, line); // ITEM: NUMBER OF ATOMS
  getline(reader, line);
  ss.str(line); ss >> nbeads; ss.clear();
  getline(reader, line); // ITEM: BOX BOUNDS
  for (int i = 0; i < 3; i++) {
    getline(reader, line);
    ss.str(line); ss >> boxLo[i] >> boxHi[i]; ss.clear();
    boxSize[i] = boxHi[i]-boxLo[i];
  }
  config->setData(nbeads, boxSize); 
  reader.close();
  frame = 0;
  reader.open(posFile);
  fileOpen = true;
  return true;
}

void BeadStream::close(){
  if (reader.is_open()){
    reader.close();
  }
  fileOpen = false;
}

bool BeadStream::nextFrame() {
  if (!fileOpen || reader.peek() == EOF) return false;
  readHeader();
  readBeadData();
  frame++;
  return true;
}

bool BeadStream::skipFrame(int n) {
  if (!fileOpen || reader.peek() == EOF) return false;
  for (int i = 0; i < n; i++) {
    readHeader();
    skipBeadData();
    frame++;
    if (reader.peek() == EOF) return false;
  }
  return nextFrame();
}

bool BeadStream::moveTo(long t) {
  while (fileOpen && reader.peek() != EOF) {
    readHeader();
    frame++;
    if (time == t) {
      readBeadData();
      return true;
    } else {
      skipBeadData();
    }
  }
  return false;
}

void BeadStream::readHeader() {
  string line;
  stringstream ss;
  // Read header lines
  getline(reader, line); // ITEM: TIMESTEP
  getline(reader, line); // timestep
  ss.str(line); ss >> time; ss.clear();
  getline(reader, line); // ITEM: NUMBER OF ATOMS
  getline(reader, line);
  getline(reader, line); // ITEM: BOX BOUNDS
  for (int i = 0; i < 3; i++) {
    getline(reader, line);
  }
  getline(reader, line); // ITEM: ATOMS id type xs ys zs ix iy iz
}

void BeadStream::skipBeadData() {
  string line;
  for (int i = 0; i < nbeads; i++) {
    getline(reader, line);
  }
}

void BeadStream::readBeadData() {
  string line;
  double xs, ys, zs;
  int index, ix, iy, iz, t;
  stringstream ss;
  for (int i = 0; i < nbeads; i++){
    getline(reader, line);
    ss.str(line);
    ss >> index >> t >> xs >> ys >> zs >> ix >> iy >> iz;
    ss.clear();
    index--; // Use zero base indexing
    config->setPosition(index, 0, xs*boxSize[0]+boxLo[0]);
    config->setPosition(index, 1, ys*boxSize[1]+boxLo[1]);
    config->setPosition(index, 2, zs*boxSize[2]+boxLo[2]);
    config->setBoundCount(index, 0, ix);
    config->setBoundCount(index, 1, iy);
    config->setBoundCount(index, 2, iz);
    config->setType(index, t);
  }
}

long BeadStream::getTime() const {
  return time;
}

long BeadStream::getStartTime() const {
  return startTime;
}

bool BeadStream::isOpen() const {
  return fileOpen;
}

int BeadStream::getFrame() const {
  return frame;
}

int BeadStream::getNumOfBeads() const {
  return nbeads;
}

double BeadStream::getBoxLo(int comp) const {
  return boxLo[comp];
}

double BeadStream::getBoxHi(int comp) const {
  return boxHi[comp];
}

double BeadStream::getBoxSize(int comp) const {
  return boxSize[comp];
}
