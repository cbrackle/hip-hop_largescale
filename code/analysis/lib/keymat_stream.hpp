// keymat_stream.hpp
// A helper class to read and write dense matrices

#ifndef KEYMAT_STREAM_HPP
#define KEYMAT_STREAM_HPP

#include <string>
#include "keymat.hpp"

namespace mat {

  template<template<class> class S, template<class> class M, class K, class T>
  class KeyMatStream {
    S<T> stream;
  public:
    KeyMatStream();
    bool read(const std::string& file, KeyMat<M,K,T>& mat);
    bool dump(const std::string& file, const KeyMat<M,K,T>& mat);
    bool dumpToGnuplot(const std::string& file, const KeyMat<M,K,T>& mat);
  };

}

#include "keymat_stream.tpp"

#endif
