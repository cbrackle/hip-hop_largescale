// zernike_descriptor.hpp
// This class serves as a wrapper around the geometric and Zernike moments. 
// It provides also the implementation of invariant Zernike descriptors,
// means of reconstruction of original function, etc.

#ifndef ZERNIKE_DESCRIPTOR_HPP
#define ZERNIKE_DESCRIPTOR_HPP

#include <vector>
#include <complex>
#include "geometric_moments.hpp"
#include "zernike_moments.hpp"

template<class T, class TIn>
class ZernikeDescriptor {

public:
  // Public typedefs
  typedef std::complex<T> ComplexT; // Complex type
  typedef std::vector<std::vector<std::vector<ComplexT> > > ComplexT3D;
  typedef std::vector<T> T1D; // 1D array of T type  
  typedef GeometricMoments<T,T> GeometricMomentsT;
  typedef ZernikeMoments<T,T> ZernikeMomentsT;

  // Public functions
  ZernikeDescriptor();
  ZernikeDescriptor(T* voxels, int dim, int order, double min, double scale);
  ZernikeDescriptor(T* voxels, int dim, int order, double* min, double scale);

  // Reconstruct the original object from the 3D Zernike moments
  void reconstruct(ComplexT3D& grid, int minN, int maxN, int minL, int maxL,
		   double min, double scale);
  
  // Access to invariants
  T getInvariants(int n, int l);

private:
  // Private helper functions
  void computeMoments(double min, double scale);
  void computeMoments(double* min, double scale);
  void computeInvariants();
  
  // Private member variables
  int dim;
  int order;
  T* voxels;
  T scale;
  T1D invariants;
  ZernikeMomentsT zm;
  GeometricMomentsT gm;
};

#include "zernike_descriptor.tpp"

#endif
