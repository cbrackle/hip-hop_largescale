/* bead_data.hpp
 * Classes which handle the position of each bead
 */

#ifndef BEAD_DATA_HPP
#define BEAD_DATA_HPP

#include <vector>
#include <map>
#include <set>
#include "bead_config.hpp"

struct Bead {

  int id, type;
  int boundCount[3];
  double x[3];
  bool unwrapped;
  
  // Constructor
  Bead();
  Bead(int index);
  
  // Functions
  double wrappedDist(const Bead& bead, const double boxSize[3]) const;
  double unwrappedDist(const Bead& bead, const double boxSize[3]) const;
  bool operator<(const Bead& bead) const;
  void operator+=(const Bead& bead);
  void operator-=(const Bead& bead);
  void operator*=(double num);
  void operator/=(double num);
  Bead operator+(const Bead& bead) const;
  Bead operator-(const Bead& bead) const;
  Bead operator*(double num) const;
  Bead operator/(double num) const;
  double dot(const Bead& bead) const;
  void unwrap(const double boxSize[3]);
  void wrap(const double boxSize[3]);
  double length() const;
};

class BeadData : public BeadConfig {

private:
  std::vector<Bead> beads;
  double boxSize[3];
  
public:
  // Constructor
  BeadData();

  // Destructor
  ~BeadData();

  Bead& operator[](int index);
  void unwrap();
  void wrap();
  void setData(int nbeads, const double boxSize[3]);
  void clearData();
  std::set<int> getBeadIDsOfTypes(const std::set<int>& types);
  std::map<int,Bead> getBeadsOfTypes(const std::set<int>& types);
  Bead getUnwrappedCM(const std::set<int>& list);
  double unwrappedDist(int index1, int index2) const;
  double wrappedDist(int index1, int index2) const;
  void setPosition(int index, int comp, double val);
  void setBoundCount(int index, int comp, int val);
  void setType(int index, int val);
  double getPosition(int index, int comp) const;
  int getBoundCount(int index, int comp) const;
  int getType(int index) const;
};

#endif
