// zernike_descriptor.cpp

#include <iostream>
#include <vector>
#include <cmath>
#include <complex>
#include "zernike_descriptor.hpp"

using std::vector;

template<class T, class TIn>
ZernikeDescriptor<T,TIn>::ZernikeDescriptor
(T* _voxels, int _dim, int _order, double _min, double _scale) :
  dim(_dim), order(_order), voxels(_voxels) {
  std::cout << "Computing moments ..." << std::endl;
  computeMoments(_min, _scale);
  std::cout << "Computing invariants ..." << std::endl;
  computeInvariants();
}

template<class T, class TIn>
ZernikeDescriptor<T,TIn>::ZernikeDescriptor
(T* _voxels, int _dim, int _order, double* _min, double _scale) :
  dim(_dim), order(_order), voxels(_voxels) {
  std::cout << "Computing moments ..." << std::endl;
  computeMoments(_min, _scale);
  std::cout << "Computing invariants ..." << std::endl;
  computeInvariants();
}

template<class T, class TIn>
void ZernikeDescriptor<T,TIn>::computeMoments(double min, double _scale) {
  std::cout << "Computing geometric moments ... " << std::endl;
  gm.init(voxels, dim, dim, dim, min, min, min, _scale, order);
  // Zernike moment
  std::cout << "Initialise Zernike moments ... " << std::endl;
  zm.setOrder(order);
  std::cout << "Computing Zernike moments ... " << std::endl;
  zm.compute(gm);
}

template<class T, class TIn>
void ZernikeDescriptor<T,TIn>::computeMoments(double* min, double _scale) {
  std::cout << "Computing geometric moments ... " << std::endl;
  gm.init(voxels, dim, dim, dim, min[0], min[1], min[2], _scale, order);
  // Zernike moment
  std::cout << "Initialise Zernike moments ... " << std::endl;
  zm.setOrder(order);
  std::cout << "Computing Zernike moments ... " << std::endl;
  zm.compute(gm);
}

template<class T, class TIn>
void ZernikeDescriptor<T,TIn>::reconstruct
(ComplexT3D& grid, int minN, int maxN, int minL, int maxL, 
 double min, double _scale) {
  zm.reconstruct(grid, min, min, min, _scale, minN, maxN, minL, maxL);
}

// Compute the Zernike moment beased invariants, i.e. the norms of vectors with
// components of Z_{nl}^m with m being the running index
template<class T, class TIn>
void ZernikeDescriptor<T,TIn>::computeInvariants() {
  invariants.clear();
  for (int n = 0; n < order+1; n++) {
    T sum = static_cast<T>(0);
    for (int l = n%2; l <= n; l+=2) {
      for (int m = -l; m <= l; m++) {
	ComplexT moment = zm.getMoment(n,l,m);
	sum += std::norm(moment);
      }
      invariants.push_back(sqrt(sum));
    }
  }
}

template<class T, class TIn>
T ZernikeDescriptor<T,TIn>::getInvariants(int n, int l) {
  if (n >= 0 && n <= order && l >= 0 && l <= n && (n-l)%2 == 0) {
    int index = 0;
    for (int i = 0; i < n; i++) {
      index += (i/2+1);
    }
    return invariants[index+l/2];
  } else {
    return static_cast<T>(0);
  }
}
