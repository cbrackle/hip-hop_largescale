/* bead_list.hpp
 * Simple arrays storing beads' positions, boundary counts and types
 */

#ifndef BEAD_LIST_HPP
#define BEAD_LIST_HPP

#include "bead_config.hpp"

class BeadList : public BeadConfig {
  
private:
  int nbeads;
  double boxSize[3];
  double** position = nullptr;
  int** boundCount = nullptr;
  int* type = nullptr;

public:
  BeadList();
  ~BeadList();
  void setData(int nbeads, const double boxSize[3]);
  void clearData();
  void setPosition(int index, int comp, double val);
  void setBoundCount(int index, int comp, int val);
  void setType(int index, int val);
  double getPosition(int index, int comp) const;
  double getUnwrappedPosition(int index, int comp) const;
  int getBoundCount(int index, int comp) const;
  int getType(int index) const;
};

#endif
