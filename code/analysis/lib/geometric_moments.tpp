// geometric_moments.cpp

#include <cstddef>
#include <iostream>
#include "geometric_moments.hpp"

template<class VoxelT, class MomentT>
GeometricMoments<VoxelT,MomentT>::GeometricMoments() {}

template<class VoxelT, class MomentT>
GeometricMoments<VoxelT,MomentT>::GeometricMoments
(const VoxelT* _voxels, int _xDim, int _yDim, int _zDim,
 double _xMin, double _yMin, double _zMin, double _scale, int _maxOrder) {
  init(_voxels, _xDim, _yDim, _zDim, _xMin, _yMin, _zMin, _scale, _maxOrder);
}

template<class VoxelT, class MomentT>
void GeometricMoments<VoxelT,MomentT>::init
(const VoxelT* _voxels, int _xDim, int _yDim, int _zDim,
 double _xMin, double _yMin, double _zMin, double _scale, int _maxOrder) {
  xDim = _xDim;
  yDim = _yDim;
  zDim = _zDim;
  
  maxOrder = _maxOrder;
  
  size_t totalSize = xDim*yDim*zDim;
  voxels.resize(totalSize);
  for (size_t i = 0; i < totalSize; i++) {
    voxels[i] = _voxels[i];
  }
  
  moments.resize(maxOrder+1);
  for (size_t i = 0; i <= maxOrder; i++) {
    moments[i].resize(maxOrder-i+1);
    for (size_t j = 0; j <= maxOrder-i; j++) {
      moments[i][j].resize(maxOrder-i-j+1);
    }
  }

  computeSamples(_xMin, _yMin, _zMin, _scale);
  compute();
}

template<class VoxelT, class MomentT>
void GeometricMoments<VoxelT,MomentT>::computeSamples
(double xMin, double yMin, double zMin, double scale) {
  samples.resize(3); // 3 dimensions
  
  int dim[3];
  dim[0] = xDim;
  dim[1] = yDim;
  dim[2] = zDim;
  
  double min[3];
  min[0] = xMin;
  min[1] = yMin;
  min[2] = zMin;

  for (int i = 0; i < 3; i++) {
    samples[i].resize(dim[i]+1);
    for (int j = 0; j <= dim[i]; j++) {
      samples[i][j] = min[i]+j*scale;
    }
  }
}

template<class VoxelT, class MomentT>
void GeometricMoments<VoxelT,MomentT>::compute() {
  int arrayDim = zDim;
  int layerDim = yDim*zDim;
  
  int diffArrayDim = zDim+1;
  int diffLayerDim = (yDim+1)*zDim;
  int diffGridDim = (xDim+1)*layerDim;

  T1D diffGrid (diffGridDim);
  T1D diffLayer (diffLayerDim);
  T1D diffArray (diffArrayDim);
  
  T1D layer (layerDim);
  T1D array (arrayDim);
  T moment;

  typename T1D::iterator iter = voxels.begin();
  typename T1D::iterator diffIter = diffGrid.begin();

  // Generate the diff version of the voxel grid in the x direction
  for (int x = 0; x < layerDim; x++) {
    computeDiffFunction(iter, diffIter, xDim);
    iter += xDim;
    diffIter += (xDim+1);
  }

  for (int i = 0; i <= maxOrder; i++) {
    diffIter = diffGrid.begin();
    for (int p = 0; p < layerDim; p++) {
      // Multiply the diff function with the sample values
      T1DIter sampleIter (samples[0].begin());
      layer[p] = multiply(diffIter, sampleIter, xDim+1);
      diffIter += (xDim+1);
    }

    iter = layer.begin();
    diffIter = diffLayer.begin();
    for (int y = 0; y < arrayDim; y++) {
      computeDiffFunction(iter, diffIter, yDim);
      iter += yDim;
      diffIter += (yDim+1);
    }

    for (int j = 0; j < maxOrder+1-i; j++) {
      diffIter = diffLayer.begin();
      for (int p = 0; p < arrayDim; p++) {
	T1DIter sampleIter (samples[1].begin());
	array[p] = multiply(diffIter, sampleIter, yDim+1);
	diffIter += (yDim+1);
      }
      iter = array.begin();
      diffIter = diffArray.begin();
      computeDiffFunction(iter, diffIter, zDim);
      
      for (int k = 0; k < maxOrder+1-i-j; k++) {
	T1DIter sampleIter (samples[2].begin());
	moment = multiply(diffIter, sampleIter, zDim+1);
	moments[i][j][k] = moment / ((1+i)*(1+j)*(1+k));
      }
    }
  }
}

template<class VoxelT, class MomentT>
void GeometricMoments<VoxelT,MomentT>::computeDiffFunction
(T1DIter iter, T1DIter diffIter, int dim) {
  diffIter[0] = -iter[0];
  for (int i = 1; i < dim; i++) {
    diffIter[i] = iter[i-1]-iter[i];
  }
  diffIter[dim] = iter[dim-1];
}

template<class VoxelT, class MomentT>
MomentT GeometricMoments<VoxelT,MomentT>::multiply
(T1DIter diffIter, T1DIter sampleIter, int dim) {
  T sum (0); // T is MomentT type
  for (int i = 0; i < dim; i++) {
    diffIter[i] *= sampleIter[i];
    sum += diffIter[i];
  }
  return sum;
}

template<class VoxelT, class MomentT>
MomentT GeometricMoments<VoxelT,MomentT>::getMoment
(int i, int j, int k) const {
  return moments[i][j][k];
}
