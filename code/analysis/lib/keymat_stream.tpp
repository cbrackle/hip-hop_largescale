// keymat_stream.tpp

#include <string>
#include <functional>
#include "keymat.hpp"
#include "keymat_stream.hpp"

template<template<class> class S, template<class> class M, class K, class T>
mat::KeyMatStream<S,M,K,T>::KeyMatStream() {}

template<template<class> class S, template<class> class M, class K, class T>
bool mat::KeyMatStream<S,M,K,T>::read(const std::string& file, 
				      mat::KeyMat<M,K,T>& mat) {
  std::function<size_t(const K& k)> func = 
    [&] (const K& k) -> size_t {return mat.getIndex(k);};
  return stream.read(file, mat.getMat(), func);
}

template<template<class> class S, template<class> class M, class K, class T>
bool mat::KeyMatStream<S,M,K,T>::dump(const std::string& file, 
				      const mat::KeyMat<M,K,T>& mat) {
  std::function<K(const size_t&)> func = 
    [&] (const size_t& i) -> K {return mat.getKey(i);};
  return stream.dump(file, mat.getConstMat(), func);
}

template<template<class> class S, template<class> class M, class K, class T>
bool mat::KeyMatStream<S,M,K,T>::dumpToGnuplot(const std::string& file, 
					       const mat::KeyMat<M,K,T>& mat) {
  std::function<K(const size_t&)> func = 
    [&] (const size_t& i) -> K {return mat.getKey(i);};  
  return stream.dumpToGnuplot(file, mat.getConstMat(), func);
}
