// binomial.hpp
// A template class for fast computation and retrieval of binomials.
// The binomials are computed only once at first call of 'get' function
// using Pascal's triangle

#ifndef BINOMIAL_HPP
#define BINOMIAL_HPP

#include <vector>

template<class T>
class Binomial {

public:
  typedef std::vector<T> VecT;
  typedef std::vector<std::vector<T> > VVecT;

  // Retrieve the binomial i 'over' j
  static T get(int i, int j);
  // Set the maximal value of the upper binomial param to max
  static void setMax(int max);
  // Get the maximal value of the upper binomial param
  static int getMax();

private:
  // Compute Pascal's triangle
  static void computePascalsTriangle();
  static VVecT pascalsTriangle;
  static int max;
};

#include "binomial.tpp"

#endif
