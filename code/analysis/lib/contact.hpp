// contact.hpp
// A class which handles contact map data

#ifndef CONTACT_HPP
#define CONTACT_HPP

#include <string>
#include <utility>
#include <map>
#include <functional>

template<class K, class T>
class ContactMap {
private:
  K nbins;
  std::map<std::pair<K,K>,T> contact;
  // Zero object for returning zero when an entry is not found
  const T zero = static_cast<T>(0.0);

  // Internal helper functions
  static std::pair<K,K> mapCoords(K i, K j);
  
  // For a specific genomic region
  bool hasGenomeCoords = false;
  bool useGenomeCoords = false;
  long startBp;
  long endBp;
  long bpPerBin;
  
  // For storing metadata
  std::string header;
  bool useHeader = false;
  
public:
  ContactMap(K nbins);
  ContactMap(long startBp, long bpPerBin, K nbins, 
	     bool useGenomeCoords = true);
  ContactMap(long startBp, long endBp, long bpPerBin,
	     bool useGenomeCoords = true);

  // Accessor methods
  T& operator()(K i, K j);
  const T& operator()(K i, K j) const;
  const T& at(K i, K j) const;
  
  K size() const;

  void clear(); // Remove all entries in the contact map

  // Read contact map data
  bool read(const std::string& file);
  template<class U>
  bool read(const std::string& file, std::function<K(const U&)> key);

  // Write contact map data
  bool dump(const std::string& file) const;
  template<class U>
  bool dump(const std::string& file, std::function<U(const K&)> key) const;
  
  bool dumpToGnuplot(const std::string& file) const;
  bool dumpToGnuplot(K start, K end, const std::string& file) const;
  template<class U>
  bool dumpToGnuplot(K start, K end, const std::string& file, 
		     std::function<U(const K&)> key) const;
  
  static bool dumpToGnuplot(const ContactMap<K,T>& cmap1, 
			    const ContactMap<K,T>& cmap2,
			    const std::string& file);
  static bool dumpToGnuplot(K sbin, K ebin,
			    const ContactMap<K,T>& cmap1,
			    const ContactMap<K,T>& cmap2,
			    const std::string& file);
  template<class U>
  static bool dumpToGnuplot(K sbin, K ebin,
			    const ContactMap<K,T>& cmap1, 
			    const ContactMap<K,T>& cmap2,
			    const std::string& file,
			    std::function<U(const K&)> key);

  // Output settings
  void enableGenomeCoords(bool value);
  void enableHeader(bool value);
  void setHeader(const std::string& value);

  // Add another contact map
  ContactMap<K,T>& operator+=(const ContactMap<K,T>& cmap);

  // Multiply/divide by a constant factor
  ContactMap<K,T>& operator*=(const T& value);
  ContactMap<K,T>& operator/=(const T& value);

  // Handle genomic coordinates
  K bpToBin(const long& bp) const;
  long binToBp(const K& i) const;
  static bool sameMapRegion(const ContactMap& cmap1, const ContactMap& cmap2);

  // Iterators and constant iterators (wrappers around map iterators)
  using iter = typename std::map<std::pair<K,K>,T>::iterator;
  using const_iter = typename std::map<std::pair<K,K>,T>::const_iterator;
  iter begin();
  iter end();
  const_iter begin() const;
  const_iter end() const;
  
};

#include "contact.tpp"

#endif
