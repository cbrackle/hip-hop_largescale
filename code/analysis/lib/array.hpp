// array.h

#ifndef ARRAY_HPP
#define ARRAY_HPP

#include "arralloc.h"

template<typename T>
T* create1DArray(int len) {
  T* arr = (T*) arralloc(sizeof(T), 1, len);
  for (int i = 0; i < len; i++) {
    arr[i] = T();
  }
  return arr;
}

template<typename T>
T** create2DArray(int len1, int len2) {
  T** arr = (T**) arralloc(sizeof(T), 2, len1, len2);
  for (int i = 0; i < len1; i++) {
    for (int j = 0; j < len2; j++) {
      arr[i][j] = T();
    }
  }
  return arr;
}

template<typename T>
T*** create3DArray(int len1, int len2, int len3) {
  T*** arr = (T***) arralloc(sizeof(T), 3, len1, len2, len3);
  for (int i = 0; i < len1; i++) {
    for (int j = 0; j < len2; j++) {
      for (int k = 0; k < len3; k++) {
	arr[i][j][k] = T();
      }
    }
  }
  return arr;
}

// For backward compatibility
double* create1DDoubleArray(int len);
double** create2DDoubleArray(int len1, int len2);
double*** create3DDoubleArray(int len1, int len2, int len3);
int* create1DIntArray(int len);
int** create2DIntArray(int len1, int len2);
int*** create3DIntArray(int len1, int len2, int len3);

void deleteArray(void* array);

#endif
