// zernike_moments.hpp

#ifndef ZERNIKE_MOMENTS_HPP
#define ZERNIKE_MOMENTS_HPP

#include <complex>
#include <vector>
#include "geometric_moments.hpp"

#define PI 3.14159265358979323846264338327950288

// Struct representing a complex coefficient of a moment of order (p,q,r)
template<class T>
struct ComplexCoeff {
  typedef std::complex<T> ValueT;
  ComplexCoeff(int p, int q, int r, const ValueT& coeff);
  ComplexCoeff(const ComplexCoeff<T>& coeff);
  ComplexCoeff();
  int p, q, r;
  ValueT value;
};

// Class representing the Zernike moments
template<class VoxelT, class MomentT>
class ZernikeMoments {

public: 
  // Public typedefs
  typedef MomentT T;
  typedef std::vector<T> T1D; // 1D array of scalar type
  typedef std::vector<T1D> T2D; // 2D array of scalar type
  typedef std::vector<T2D> T3D; // 3D array of scalar type
  typedef std::complex<T> ComplexT; // Complex type
  typedef std::vector<std::vector<std::vector<ComplexT> > >
  ComplexT3D; // 3D array of complex type
  typedef ComplexCoeff<T> ComplexCoeffT;
  typedef std::vector<std::vector<std::vector<std::vector<ComplexCoeffT> > > >
  ComplexCoeffT4D;

  // Public member functions
  ZernikeMoments();
  ZernikeMoments(int order);
  void setOrder(int order);
  int getOrder() const;
  void compute(const GeometricMoments<VoxelT,MomentT>& gm);
  ComplexT getMoment(int n, int l, int m);
  
  // Debug functions/arguments
  void reconstruct(ComplexT3D& grid, T xMin, T yMin, T zMin, T scale,
		   int minN, int maxN, int minL, int maxL);
  void checkOrthonormality(int n1, int l1, int m1, int n2, int l2, int m2);  

private: 
  // Static attributes (precomputed coefficients)
  static int maxOrder; // max order for which coefficients are computed to
  static T2D cs; // c coefficients (harmonic polynomial normalisation)
  static T3D qs; // q coefficients (radial polynomial normalisation)
  static ComplexCoeffT4D gCoeffs; // Coefficients of the geometric moments

  // Private attributes
  int order; // := max{n} according to indexing of Zernike polynomials
  ComplexT3D zernikeMoments; // Nomen est omen

  // Private member functions
  void computeCoeff();
  static void computeCs();
  static void computeQs();
  static void computeGCoefficients();
  
  // Debug functions/arguments
  T evalMonomialIntegral(int p, int q, int r, int dim);
};

#include "zernike_moments.tpp"

#endif
