// bead_map.hpp

#ifndef BEAD_MAP_HPP
#define BEAD_MAP_HPP

#include <string>
#include <vector>
#include <set>
#include "mat.hpp"

template<class T>
class BeadMap {

private:
  int nbeads;
  mat::KeyMat<mat::SymMat,int,T> mat;

public:
  BeadMap();
  BeadMap(int nbeads, const std::vector<int>& beads);

  T& operator()(int i, int j);
  const T& operator()(int i, int j) const;
  T& at(int beadi, int beadj);
  const T& at(int beadi, int beadj) const;
  size_t size() const;
  int getBead(size_t i) const;

  bool load(const std::string& beadFile, const std::string& matFile,
	    bool oneBased = true);  
  bool load(const std::set<int>& beads, const std::string& matFile,
	    bool oneBased = true);
  bool load(size_t nbeads, const std::string& matFile);
  bool dump(const std::string& outFile);
  bool dumpToGnuplot(const std::string& outFile);

};

#include "bead_map.tpp"

#endif
