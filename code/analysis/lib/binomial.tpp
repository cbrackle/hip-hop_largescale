// binomial.cpp

#include <vector>
#include <cassert>
#include "binomial.hpp"

template<class T>
int Binomial<T>::max = 60; // Default size of Pascal's triangle

template<class T>
typename Binomial<T>::VVecT Binomial<T>::pascalsTriangle;

template<class T>
inline void Binomial<T>::setMax(int m) {
  max = m;
  computePascalsTriangle();
}

template<class T>
inline int Binomial<T>::getMax() {
  return max;
}

template<class T>
void Binomial<T>::computePascalsTriangle() {
  // Store Pascal's triangle in a 'rotated' format: i.e.,
  // 1 1 1 1 1 1
  // 1 2 3 4 5
  // 1 3 6 10 
  // 1 4 10 
  // 1 5
  // 1
  pascalsTriangle.resize(max+1);
  for (int i = 0; i < max+1; i++) {
    pascalsTriangle[i].resize(max+1-i);
    for (int j = 0; j < max+1-i; j++) {
      // The values are ones on the edges of the triangle
      if (!i || !j) {
	pascalsTriangle[i][j] = static_cast<T>(1);
      } else { // Use the familiar addition to generate values on lower levels
	pascalsTriangle[i][j] = 
	  pascalsTriangle[i][j-1] + pascalsTriangle[i-1][j];
      }
    }
  }
}

template<class T>
T Binomial<T>::get(int i, int j) {
  if (!pascalsTriangle.size()) {
    computePascalsTriangle();
  }
  assert(i >= 0 && j >= 0 && i >= j);
  return pascalsTriangle[j][i-j];
}
