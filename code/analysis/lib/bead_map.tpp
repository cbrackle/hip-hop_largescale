// bead_map.tpp

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <set>
#include "mat.hpp"
#include "bead_map.hpp"

template<class T>
BeadMap<T>::BeadMap() {}

template<class T>
BeadMap<T>::BeadMap(int n, const std::vector<int>& blist) : 
  nbeads(n) {
  mat = mat::KeyMat<mat::SymMat,int,T>(n,blist);
}

template<class T>
inline T& BeadMap<T>::operator()(int i, int j) {
  return mat(i,j);
}

template<class T>
inline const T& BeadMap<T>::operator()(int i, int j) const {
  return mat(i,j);
}

template<class T>
inline T& BeadMap<T>::at(int beadi, int beadj) {
  return mat.at(beadi,beadj);
}

template<class T>
inline const T& BeadMap<T>::at(int beadi, int beadj) const {
  return mat.at(beadi,beadj);
}

template<class T>
inline size_t BeadMap<T>::size() const {return nbeads;}

template<class T>
inline int BeadMap<T>::getBead(size_t i) const {return mat.getKey(i);}

template<class T>
bool BeadMap<T>::load(const std::string& beadFile, const std::string& matFile,
		      bool oneBased) {
  // Read the bead index file
  std::ifstream reader;
  reader.open(beadFile);
  if (!reader) {
    std::cout << "Error: cannot open the file " << beadFile << std::endl;
    return false;
  }
  std::stringstream ss;
  std::string line;
  std::set<int> beadset;
  int index;
  while (getline(reader,line)) {
    ss.str(line);
    ss >> index;
    ss.clear();
    beadset.insert(oneBased ? index-1 : index); // Convert to zero-based
  }
  reader.close();
  nbeads = beadset.size();
  std::vector<int> beads (beadset.begin(), beadset.end());
  beadset.clear();
  mat = mat::KeyMat<mat::SymMat,int,T>(nbeads, beads);
  beads.clear();

  // Read the matrix
  mat::KeyMatStream<mat::SymMatStream,mat::SymMat,int,T> stream;
  return stream.read(matFile, mat);
}

template<class T>
bool BeadMap<T>::load(const std::set<int>& beadset, const std::string& matFile,
		      bool oneBased) {
  nbeads = beadset.size();
  std::vector<int> beads (beadset.begin(), beadset.end());
  if (oneBased) { // Convert to zero-based if one-based
    for (size_t i = 0; i < nbeads; i++) {
      beads[i]--;
    }
  }
  
  // Read the matrix
  mat = mat::KeyMat<mat::SymMat,int,T>(nbeads, beads);
  beads.clear();
  mat::KeyMatStream<mat::SymMatStream,mat::SymMat,int,T> stream;
  return stream.read(matFile, mat);
}

template<class T>
bool BeadMap<T>::load(size_t n, const std::string& matFile) {
  std::vector<int> beads(n);
  nbeads = n;
  for (size_t i = 0; i < n; i++) {
    beads[i] = static_cast<int>(i);
  }
  mat = mat::KeyMat<mat::SymMat,int,T>(nbeads, beads);
  beads.clear();
  mat::KeyMatStream<mat::SymMatStream,mat::SymMat,int,T> stream;
  return stream.read(matFile, mat);
}

template<class T>
bool BeadMap<T>::dump(const std::string& outFile) {
  mat::KeyMatStream<mat::SymMatStream,mat::SymMat,int,T> stream;
  return stream.dump(outFile, mat);
}

template<class T>
bool BeadMap<T>::dumpToGnuplot(const std::string& outFile) {
  mat::KeyMatStream<mat::SymMatStream,mat::SymMat,int,T> stream;
  return stream.dumpToGnuplot(outFile, mat);
}
