// zernike_moments.cpp

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <omp.h>
#include "zernike_moments.hpp"
#include "geometric_moments.hpp"
#include "factorial.hpp"
#include "binomial.hpp"

// Implementation of ComplexCoeff struct
// Copy constructor
template<class T>
ComplexCoeff<T>::ComplexCoeff(const ComplexCoeff<T>& coeff) :
  p(coeff.p), q(coeff.q), r(coeff.r), value(coeff.value) {}

// Constructor with scalar args
template<class T>
ComplexCoeff<T>::ComplexCoeff(int _p, int _q, int _r, const ValueT& _value) :
  p(_p), q(_q), r(_r), value(_value) {}

// Default constructor
template<class T>
ComplexCoeff<T>::ComplexCoeff() :
  p(0), q(0), r(0) {}


// Implementation of ZernikeMoments class
// Give access to static variables
template<class VoxelT, class MomentT>
int ZernikeMoments<VoxelT,MomentT>::maxOrder = 0;

template<class VoxelT, class MomentT>
typename ZernikeMoments<VoxelT,MomentT>::T2D 
ZernikeMoments<VoxelT,MomentT>::cs;

template<class VoxelT, class MomentT>
typename ZernikeMoments<VoxelT,MomentT>::T3D 
ZernikeMoments<VoxelT,MomentT>::qs;

template<class VoxelT, class MomentT>
typename ZernikeMoments<VoxelT,MomentT>::ComplexCoeffT4D
ZernikeMoments<VoxelT,MomentT>::gCoeffs;

template<class VoxelT, class MomentT>
ZernikeMoments<VoxelT,MomentT>::ZernikeMoments() : order(0) {
  computeCoeff();
}

template<class VoxelT, class MomentT>
ZernikeMoments<VoxelT,MomentT>::ZernikeMoments(int _order) : order(_order) {
  computeCoeff();
}

// Compute all coefficients that are input data independent
template<class VoxelT, class MomentT>
void ZernikeMoments<VoxelT,MomentT>::setOrder(int _order) {
  order = _order;
  computeCoeff();
}

template<class VoxelT, class MomentT>
int ZernikeMoments<VoxelT,MomentT>::getOrder() const {
  return order;
}

template<class VoxelT, class MomentT>
void ZernikeMoments<VoxelT,MomentT>::computeCoeff() {
  // Need to recompute coefficients if the order exceeds the max order
  if (order >= maxOrder) {
    maxOrder = order;
    computeCs();
    computeQs();
    computeGCoefficients();
  }
}

// Compute all the normalising factors $c_l^m$ for harmonic polynomials e
template<class VoxelT, class MomentT>
void ZernikeMoments<VoxelT,MomentT>::computeCs() {

  // Indexing:
  // l goes from 0 to n (inclusive)
  // m goes from -l to l, in fact from 0 to l, since c(l,-m) propto c(l,m)
  
  cs.clear();
  cs.resize(maxOrder+1);
  for (int l = 0; l <= maxOrder; l++) {
    cs[l].resize(l+1);
    for (int m = 0; m <= l; m++) {
      T nsq = (static_cast<T>(2) * l + static_cast<T>(1)) *
	Factorial<T>::get(l+1,l+m);
      T dsq = Factorial<T>::get(l-m+1,l);
      cs[l][m] = sqrt(nsq/dsq);
    }
  }
}

// Compute all coefficients q for orthormalisation of radial polynomials
// in Zernike polynomials
template<class VoxelT, class MomentT>
void ZernikeMoments<VoxelT,MomentT>::computeQs() {
  
  // Indexing:
  // n goes from 0 to order
  // l goes from 0 to n, so that n-l is even
  // nu goes from 0 to (n-l)/2
  
  qs.clear();
  qs.resize(maxOrder+1); // There is order+1 n's
  for (int n = 0; n <= maxOrder; n++) {
    qs[n].resize(n/2+1); // There is floor(n/2)+1 l's
    int l0 = n%2;
    for (int l = l0; l <= n; l+=2) {
      int k = (n-l)/2;
      qs[n][l/2].resize(k+1); // There is k+1 nu's
      for (int nu = 0; nu <= k; nu++) {
	T nom = Binomial<T>::get(2*k,k) *
	  Binomial<T>::get(k,nu) *
	  Binomial<T>::get(2*(k+l+nu)+1,2*k);
	if ((k+nu)%2) {
	  nom *= static_cast<T>(-1);
	}
	T den = std::pow(static_cast<T>(2), static_cast<T>(2*k)) *
	  Binomial<T>::get(k+l+nu,k); // Denominator of straight part
	T nsq = static_cast<T>(2*l+4*k+3); // Nominator of sqrt part
	T dsq = static_cast<T>(3); // Denominator of sqrt part
	qs[n][l/2][nu] = nom/den*sqrt(nsq/dsq);
      }
    }
  }
}

// Compute the coefficients of geometric moments in linear combinations
// yielding the Zernike moments for each applicable [n,l,m] for n<=order.
// For each such combination the coefficients are stored with acccording
// geometric moment order (see ComplexCoeff)
template<class VoxelT, class MomentT>
void ZernikeMoments<VoxelT,MomentT>::computeGCoefficients() {
  //int countCoeffs = 0; // For debugging
  gCoeffs.clear();
  gCoeffs.resize(maxOrder+1);
  for (int n = 0; n <= maxOrder; n++) {
    gCoeffs[n].resize(n/2+1);
    int li = 0, l0 = n%2;
    for (int l = l0; l <= n; li++, l+=2) {
      gCoeffs[n][li].resize(l+1);
      for (int m = 0; m <= l; m++) {
	T w = cs[l][m] / std::pow(static_cast<T>(2), static_cast<T>(m));
	int k = (n-l)/2;
	for (int nu = 0; nu <= k; nu++) {
	  T wNu = w * qs[n][li][nu];
	  for (int alpha = 0; alpha <= nu; alpha++) {
	    T wNuA = wNu * Binomial<T>::get(nu,alpha);
	    for (int beta = 0; beta <= nu-alpha; beta++) {
	      T wNuAB = wNuA * Binomial<T>::get(nu-alpha,beta);
	      for (int p = 0; p <= m; p++) {
		T wNuABP = wNuAB * Binomial<T>::get(m,p);
		for (int mu = 0; mu <= (l-m)/2; mu++) {
		  T wNuABPMu = wNuABP * 
		    Binomial<T>::get(l,mu) * Binomial<T>::get(l-mu,m+mu) /
		    static_cast<T>(std::pow(2.0,static_cast<double>(2*mu)));
		  for (int q = 0; q <= mu; q++) {
		    // The absolute value of the coefficient
		    T wNuABPMuQ = wNuABPMu * Binomial<T>::get(mu,q);
		    // The sign
		    if ((m-p+mu)%2) wNuABPMuQ *= static_cast<T>(-1);
		    // * i^p
		    int rest = p%4;
		    ComplexT c;
		    T zero = static_cast<T>(0);
		    T neg1 = static_cast<T>(-1);
		    switch (rest) {
		    case 0: c = ComplexT(wNuABPMuQ, zero); break;
		    case 1: c = ComplexT(zero, wNuABPMuQ); break;
		    case 2: c = ComplexT(neg1*wNuABPMuQ, zero); break;
		    case 3: c = ComplexT(zero, neg1*wNuABPMuQ); break;
		    }

		    // Determination of the order of the according moment
		    int zi = l-m+2*(nu-alpha-beta-mu);
		    int yi = 2*(mu-q+beta)+m-p;
		    int xi = 2*q+p+2*alpha;
		    
		    ComplexCoeffT coeff (xi,yi,zi,c);
		    gCoeffs[n][li][m].push_back(coeff);
		    //countCoeffs++;
		  } // q
		} // mu
	      } // p
	    } // beta
	  } // alpha
	} // nu
      } // m
    } // l
  } // n
  //std::cout << countCoeffs << std::endl;
}

// Compute the Zernike moments. This computation is data dependent and
// has to be performed for each new object and/or transformation
template<class VoxelT, class MomentT>
void ZernikeMoments<VoxelT,MomentT>::compute
(const GeometricMoments<VoxelT,MomentT>& gm) {
  // Geometric moments have to be computed first
  if (!order) {
    std::cerr << "ZernikeMoments<VoxelT,MomentT>::compute(): attempting to "
	      << "compute Zernike moments without setting valid moment order "
	      << "first. \nExiting ...\n";
    exit(1);
  }

  // Indexing
  // n goes from 0 to order
  // l goes from 0 to n so that n-l is even
  // m goes from -l to l

  zernikeMoments.resize(order+1);
  for (int n = 0; n <= order; n++) {
    zernikeMoments[n].resize(n/2+1);
    int li = 0, l0 = n%2;
    for (int l = l0; l <= n; li++, l+=2) {
      zernikeMoments[n][li].resize(l+1);
      for (int m = 0; m <= l; m++) {
	// Zernike moment of according index [n,l,m]
	ComplexT zm (static_cast<T>(0), static_cast<T>(0));
	int nCoeffs = static_cast<int>(gCoeffs[n][li][m].size());
	for (int i = 0; i < nCoeffs; i++) {
	  ComplexCoeffT cc = gCoeffs[n][li][m][i];
	  zm += std::conj(cc.value) * gm.getMoment(cc.p,cc.q,cc.r);
	}
	zm *= static_cast<T>(3.0/(4.0*PI));
	zernikeMoments[n][li][m] = zm;
      }
    }
  }
}

template<class VoxelT, class MomentT>
typename ZernikeMoments<VoxelT,MomentT>::ComplexT 
ZernikeMoments<VoxelT,MomentT>::getMoment(int n, int l, int m) {
  if (n >= 0 && n <= order && l >= 0 && l <= n && 
      m >= -l && m <= l && (n-l)%2 == 0) {
    int absm = abs(m);
    ComplexT moment = zernikeMoments[n][l/2][absm];
    if (m < 0) moment = static_cast<T>(m%2?-1.0:1.0)*std::conj(moment);
    return moment;
  } else {
    return ComplexT(static_cast<T>(0),static_cast<T>(0));
  }
}

template<class VoxelT, class MomentT>
void ZernikeMoments<VoxelT,MomentT>::reconstruct
(ComplexT3D& grid, T xMin, T yMin, T zMin, T cellSize, 
 int minN, int maxN, int minL, int maxL) {
  
  int dim[3];
  dim[0] = static_cast<int>(grid.size());
  dim[1] = static_cast<int>(grid[0].size());
  dim[2] = static_cast<int>(grid[0][0].size());

  T min[3];
  min[0] = xMin;
  min[1] = yMin;
  min[2] = zMin;

  // Precompute all the relevant powers
  T3D powers;
  T val;
  powers.resize(3);
  for (int i = 0; i < 3; i++) {
    powers[i].resize(dim[i]);
    for (int j = 0; j < dim[i]; j++) {
      powers[i][j].resize(maxN+1);
      val = static_cast<T>(min[i]+j*cellSize);
      powers[i][j][0] = static_cast<T>(1);
      for (int k = 1; k <= maxN; k++) {
	powers[i][j][k] = powers[i][j][k-1]*val;
      }
    }
  }

  for (int x = 0; x < dim[0]; x++) {
    std::cout << "Doing layer x = " << x << std::endl;
#pragma omp parallel for default(none)			\
  shared(minN, maxN, minL, maxL, powers, dim, grid, x)	\
  schedule(static) collapse(1)
    for (int y = 0; y < dim[1]; y++) {
      for (int z = 0; z < dim[2]; z++) {
	// Function value at the point
	ComplexT fVal (0,0);
	for (int n = minN; n <= maxN; n++) {
	  if (n > maxN) continue;
	  int maxK = n/2;
	  for (int k = 0; k <= maxK; k++) {
	    for (int nu = 0; nu <= k; nu++) {
	      int l = n-2*k;
	      //for (int l = n%2; l <= n; l+=2) {
	    // Check whether l is within bounds
	      if (l < minL || l > maxL) continue;
	      for (int m = -l; m <= l; m++) {
		// Zernike polynomial evaluated at point
		ComplexT zp (0,0);
		int absM = std::abs(m);
		int nCoeffs = static_cast<int>(gCoeffs[n][l/2][absM].size());
		for (int i = 0; i < nCoeffs; i++) {
		  ComplexCoeffT cc = gCoeffs[n][l/2][absM][i];
		  ComplexT cvalue = cc.value;
		  // Conjugate if m is negative and take care of the sign
		  if (m < 0) cvalue = (m%2?-1.0:1.0) * std::conj(cvalue);
		  zp += cvalue * powers[0][x][cc.p] * powers[1][y][cc.q] *
		    powers[2][z][cc.r];
		}
		fVal += zp * getMoment(n,l,m);
	      }
	    }
	  }
	}
	grid[x][y][z] = fVal;
      }
    }
  }
}

template<class VoxelT, class MomentT>
void ZernikeMoments<VoxelT,MomentT>::checkOrthonormality
(int n1, int l1, int m1, int n2, int l2, int m2) {
  int li1 = l1/2;
  int li2 = l2/2;
  int dim = 64;

  // The total sum of the scalar product
  ComplexT sum (0,0);

  int nCoeffs1 = static_cast<int>(gCoeffs[n1][li1][m1].size());
  int nCoeffs2 = static_cast<int>(gCoeffs[n2][li2][m2].size());

  for (int i = 0; i < nCoeffs1; i++) {
    ComplexCoeffT cc1 = gCoeffs[n1][li1][m1][i];
    for (int j = 0; j < nCoeffs2; j++) {
      ComplexCoeffT cc2 = gCoeffs[n2][li2][m2][j];
      int p = cc1.p + cc2.p;
      int q = cc1.q + cc2.q;
      int r = cc1.r + cc2.r;
      sum += cc1.value * std::conj(cc2.value) * 
	evalMonomialIntegral(p,q,r,dim);
    }
  }
  std::cout << "\nInner product of [" << n1 << "," << l1 << "," << m1 << "]";
  std::cout << " and  [" << n2 << "," << l2 << "," << m2 << "]: ";
  std::cout << sum << "\n\n";
}

// Evaluate the integral of a monomial 
template<class VoxelT, class MomentT>
MomentT ZernikeMoments<VoxelT,MomentT>::evalMonomialIntegral
(int p, int q, int r, int dim) {
  T radius = static_cast<T>(dim-1)/static_cast<T>(2);
  T scale = std::pow(static_cast<T>(1)/radius,3);
  T center = static_cast<T>(dim-1)/static_cast<T>(2);
  T result = static_cast<T>(0);
  T point[3];
  
  for (int x = 0; x < dim; x++) {
    point[0] = (static_cast<T>(x)-center)/radius;
    for (int y = 0; y < dim; y++) {
      point[1] = (static_cast<T>(y)-center)/radius;
      for (int z = 0; z < dim; z++) {
	point[2] = (static_cast<T>(z)-center)/radius;
	if (point[0]*point[0]+point[1]*point[1]+point[2]*point[2] > 1.0) {
	  continue;
	}
	result += std::pow(point[0], static_cast<T>(p)) *
	  std::pow(point[1], static_cast<T>(q)) *
	  std::pow(point[2], static_cast<T>(r));
      }
    }
  }
  result *= static_cast<T>(3.0/(4.0*PI))*scale;
  return result;
}
