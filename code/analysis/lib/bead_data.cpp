// bead_data.cpp

#include <cmath>
#include <set>
#include <map>
#include <vector>
#include "bead_data.hpp"

using std::set;
using std::map;
using std::vector;

// Member functions of Bead
Bead::Bead() : Bead(0) {}

Bead::Bead(int index) : id(index), unwrapped(false) {
  x[0] = 0.0;
  x[1] = 0.0;
  x[2] = 0.0;
  boundCount[0] = 0;
  boundCount[1] = 0;
  boundCount[2] = 0;
  type = 0;
}

double Bead::wrappedDist(const Bead& bead, const double box[3]) const {
  double dx[3];
  double sum = 0.0;
  for (int i = 0; i < 3; i++) {
    dx[i] = x[i]-bead.x[i];
    if (dx[i] > 0.5*box[i]) {
      dx[i] -= box[i];
    } else if (dx[i] < -0.5*box[i]) {
      dx[i] += box[i];
    }
    sum += dx[i]*dx[i];
  }
  return sqrt(sum);
}

double Bead::unwrappedDist(const Bead& bead, const double box[3]) const {
  double x1[3];
  double x2[3];
  double dx;
  double sum = 0.0;
  if (!unwrapped) {
    for (int i = 0; i < 3; i++) {
      x1[i] = x[i] + boundCount[i]*box[i];
    }
  }
  if (!bead.unwrapped) {
    for (int i = 0; i < 3; i++) {
      x2[i] = bead.x[i] + bead.boundCount[i]*box[i];
    }
  }
  for (int i = 0; i < 3; i++) {
    dx = x1[i]-x2[i];
    sum += dx*dx;
  }
  return sqrt(sum);
}

bool Bead::operator<(const Bead& bead) const {
  return id < bead.id;
}

void Bead::operator+=(const Bead& bead) {
  for (int i = 0; i < 3; i++) {
    x[i] += bead.x[i];
  }
}

void Bead::operator-=(const Bead& bead) {
  for (int i = 0; i < 3; i++) {
    x[i] -= bead.x[i];
  }
}

void Bead::operator*=(double num) {
  for (int i = 0; i < 3; i++) {
    x[i] += num;
  }
}

void Bead::operator/=(double num) {
  for (int i = 0; i < 3; i++) {
    x[i] /= num;
  }
}

Bead Bead::operator+(const Bead& bead) const {
  Bead newBead = *this;
  for (int i = 0; i < 3; i++) {
    newBead.x[i] += bead.x[i];
  }
  return newBead;
}

Bead Bead::operator-(const Bead& bead) const {
  Bead newBead = *this;
  for (int i = 0; i < 3; i++) {
    newBead.x[i] -= bead.x[i];
  }
  return newBead;
}

Bead Bead::operator*(double num) const {
  Bead newBead = *this;
  for (int i = 0; i < 3; i++) {
    newBead.x[i] *= num;
  }
  return newBead;
}

Bead Bead::operator/(double num) const {
  Bead newBead = *this;
  for (int i = 0; i < 3; i++) {
    newBead.x[i] /= num;
  }
  return newBead;
}

double Bead::dot(const Bead& bead) const {
  double sum = 0.0;
  for (int i = 0; i < 3; i++) {
    sum += x[i]*bead.x[i];
  }
  return sum;
}

void Bead::unwrap(const double box[3]) {
  if (!unwrapped) {
    unwrapped = true;
    for (int i = 0; i < 3; i++) {
      x[i] += boundCount[i]*box[i];
    }
  }
}

void Bead::wrap(const double box[3]) {
  if (unwrapped) {
    unwrapped = false;
    for (int i = 0; i < 3; i++) {
      x[i] -= boundCount[i]*box[i];
    }
  }
}

double Bead::length() const {
  double sum = 0.0;
  for (int i = 0; i < 3; i++) {
    sum += x[i]*x[i];
  }
  return sqrt(sum);
}


// Member functions of BeadData
BeadData::BeadData() {} // Constructor

BeadData::~BeadData() {} // Destructor

void BeadData::setData(int nbeads, const double dim[3]) {
  clearData();
  beads.reserve(nbeads);
  for (int i = 0; i < nbeads; i++) {
    beads.push_back(Bead(i));
  }
  boxSize[0] = dim[0];
  boxSize[1] = dim[1];
  boxSize[2] = dim[2];
}

void BeadData::clearData() {
  beads.clear();
}

Bead& BeadData::operator[](int index) {
  return beads[index];
}

void BeadData::unwrap() {
  for (size_t i = 0; i < beads.size(); i++) {
    beads[i].unwrap(boxSize);
  }
}

void BeadData::wrap() {
  for (size_t i = 0; i < beads.size(); i++) {
    beads[i].wrap(boxSize);
  }
}

set<int> BeadData::getBeadIDsOfTypes(const set<int>& types) {
  set<int> list;
  for (size_t i = 0; i < beads.size(); i++) {
    if (types.find(beads[i].type) != types.end()) {
      list.insert(i);
    }
  }
  return list;
}

map<int,Bead> BeadData::getBeadsOfTypes(const set<int>& types) {
  map<int,Bead> list;
  for (size_t i = 0; i < beads.size(); i++) {
    if (types.find(beads[i].type) != types.end()) {
      list[i] = beads[i];
    }
  }
  return list;
}

Bead BeadData::getUnwrappedCM(const set<int>& list) {
  Bead cm, toAdd;
  cm.unwrapped = true;
  for (set<int>::const_iterator it = list.begin(); it != list.end(); it++) {
    if (beads[*it].unwrapped) {
      cm += beads[*it];
    } else {
      toAdd = beads[*it];
      toAdd.unwrap(boxSize);
      cm += toAdd;
    }
  }
  cm /= list.size();
  return cm;
}

double BeadData::wrappedDist(int index1, int index2) const {
  return beads[index1].wrappedDist(beads[index2], boxSize);
}

double BeadData::unwrappedDist(int index1, int index2) const {
  return beads[index1].unwrappedDist(beads[index2], boxSize);
}

void BeadData::setPosition(int index, int comp, double val) {
  beads[index].x[comp] = val;
}

void BeadData::setBoundCount(int index, int comp, int val) {
  beads[index].boundCount[comp] = val;
}

void BeadData::setType(int index, int val) {
  beads[index].type = val;
}

double BeadData::getPosition(int index, int comp) const {
  return beads[index].x[comp];
}

int BeadData::getBoundCount(int index, int comp) const {
  return beads[index].boundCount[comp];
}

int BeadData::getType(int index) const {
  return beads[index].type; 
}
