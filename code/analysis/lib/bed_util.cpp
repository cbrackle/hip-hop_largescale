// bed_util.cpp

#include <sstream>
#include <string>
#include <cmath>
#include "bed_util.hpp"

using std::stringstream;
using std::string;

Bedline::Bedline() {}

Bedline::Bedline(const Bedline& b) :
  chrom(b.chrom), start(b.start), end(b.end) {}

Bedline::Bedline(const string& line) {
  stringstream ss;
  ss.str(line);
  ss >> chrom >> start >> end;
}

bool Bedline::operator<(const Bedline& b) const {
  int chromCompare = chrom.compare(b.chrom);
  if (chromCompare == 0) {
    return start < b.start;
  } else if (chromCompare < 0) {
    return true;
  } else {
    return false;
  }
}

Region::Region(string chr, long s, long e, long bp) :
  chrom(chr), start(s), end(e), bpPerBead(bp) {
  nbp = end-start;
  nbeads = static_cast<int>(ceil(nbp/static_cast<double>(bpPerBead)));
}

bool Region::inRegion (const Bedline& bline) const {
  return (bline.chrom == chrom && bline.end > start && bline.start < end);
}

int Region::bpToBead(const long& bp) const {
  return static_cast<int>(floor(static_cast<double>(bp-start)/
				static_cast<double>(bpPerBead)));
}

long Region::pos(const int& bead) const {
  return start+bpPerBead*bead+0.5*bpPerBead;
}

long Region::beadStart(const int& bead) const {
  return start+bpPerBead*bead;
}

long Region::beadEnd(const int& bead) const {
  return start+bpPerBead*(bead+1);
}
