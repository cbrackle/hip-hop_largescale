// symmat.hpp
// A template class for storing a symmetric matrix of objects

#ifndef SYMMAT_HPP
#define SYMMAT_HPP

#include <iostream>
#include <string>
#include <vector>

namespace mat {

  template<class T>
  class SymMat {
  private:
    size_t matSize;
    std::vector<T> mat;
    
  public:
    SymMat() : matSize(0) {}
    SymMat(size_t n) : matSize(n) {
      mat = std::vector<T>(n*(n+1)/2);
    }
    inline size_t size() const {return matSize;}
    inline T& operator()(size_t i, size_t j) {
      return i > j ? mat[(i+1)*i/2+j] : mat[(j+1)*j/2+i];
    }
    inline const T& operator()(size_t i, size_t j) const {
      return i > j ? mat[(i+1)*i/2+j] : mat[(j+1)*j/2+i];
    }
  };

}

#endif
