// symmat_stream.hpp
// A helper class to read and write symmetric matrices

#ifndef SYMMAT_STREAM_HPP
#define SYMMAT_STREAM_HPP

#include <string>
#include <functional>
#include "symmat.hpp"

namespace mat {

  template<class T>
  class SymMatStream {
  public:
    SymMatStream();
    bool read(const std::string& file, SymMat<T>& mat);
    template<class K>
    bool read(const std::string& file, SymMat<T>& mat,
	      const std::function<size_t(const K&)> index);
    
    bool dump(const std::string& file, const SymMat<T>& mat, 
	      bool dumpDiag = true);
    template<class K>
    bool dump(const std::string& file, const SymMat<T>& mat,
	      const std::function<K(const size_t&)> key,
	      bool dumpDiag = true);
    
    bool dumpToGnuplot(const std::string& file, const SymMat<T>& mat);
    template<class K>
    bool dumpToGnuplot(const std::string& file, const SymMat<T>& mat,
		       const std::function<K(const size_t&)> key);
  };

}

#include "symmat_stream.tpp"

#endif
