// bed_util.hpp

#ifndef BED_UTIL_HPP
#define BED_UTIL_HPP

#include <string>

struct Bedline {
  std::string chrom;
  long start, end;

  Bedline(); // Default constructor
  Bedline(const Bedline& bline); // Copy constructor
  Bedline(const std::string& str);
  bool operator<(const Bedline& bline) const; // Comparison for sorting
};

struct Region {
  std::string chrom;
  long start, end;
  int bpPerBead, nbp, nbeads; 

  Region(std::string chrom, long start, long end, long bpPerBead);
  bool inRegion(const Bedline& bline) const;
  int bpToBead(const long& bp) const;
  long pos(const int& bead) const;
  long beadStart(const int& bead) const;
  long beadEnd(const int& bead) const;
};

#endif
