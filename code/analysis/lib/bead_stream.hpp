/* bead_stream.hpp
 * A program that reads the dump/position files generated by LAMMPS
 */

#ifndef BEAD_STREAM_HPP
#define BEAD_STREAM_HPP

#include <string>
#include <fstream>
#include "bead_config.hpp"

class BeadStream {

private:
  
  int nbeads;
  double boxLo[3];
  double boxHi[3];
  double boxSize[3];
  long time;
  int frame; 
  long startTime;
  bool fileOpen;
  std::ifstream reader;
  BeadConfig* config;

  void readHeader();
  void readBeadData();
  void skipBeadData();
  
public:
  // Constructor
  BeadStream();
  // Destructor
  ~BeadStream();
  
  // Load a new position file
  bool open(const std::string& posFile, BeadConfig& config);
  void close();

  // Retrieve the bead position data in the next time frame
  // Return false if there is no next frame
  bool nextFrame();
  bool skipFrame(int frames);
  bool moveTo(long time);
  
  // Get the time associated with the frame
  long getTime() const;

  long getStartTime() const;

  // Check if there is a file open
  bool isOpen() const;

  int getFrame() const;

  int getNumOfBeads() const;
  double getBoxLo(int comp) const;
  double getBoxHi(int comp) const;
  double getBoxSize(int comp) const;
};

#endif
