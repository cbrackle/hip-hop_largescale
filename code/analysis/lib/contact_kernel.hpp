// contact_kernel.hpp
// A list of probability kernels for computing the contact map

#ifndef CONTACT_KERNEL_HPP
#define CONTACT_KERNEL_HPP

#include <sstream>
#include <cmath>
#include <functional>

class ContactKernel {

  std::function<double(double,double)> probKernel;

public:
  // Default is the step function kernel
  ContactKernel() {setStep();}
  ContactKernel(const std::string& type, const std::string& args,
		char delim = ' ') {
    std::stringstream ss;
    std::string token;
    if (type == "exp") {
      setExp();
    } else if (type == "expn") {
      if (getline(ss,token,delim)) {
	setExpn(stod(token,nullptr));
      } else {
	setExpn();
      }
    } else if (type == "tanh") {
      if (getline(ss,token,delim)) {
	setTanh(stod(token,nullptr));
      } else {
	setTanh();
      }
    } else {
      setStep();
    }
  }

  void setExp() {
    probKernel = [] (double d, double rc) {return exp(-d/rc);};
  }
  void setExpn(double n = 1.0) {
    probKernel = [n] (double d, double rc) {return exp(-pow(d/rc,n));};
  }
  void setTanh(double width = 1.0) {
    probKernel = [width] (double d, double rc) {
      return 0.5*(1.0+tanh((rc-d)/width));};
  }
  void setStep() {
    probKernel = [] (double d, double rc) {return (d < rc ? 1.0 : 0.0);};
  }

  inline double prob(double sep, double thres) {
    return probKernel(sep,thres);
  }
};

#endif
