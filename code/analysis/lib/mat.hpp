// mat.hpp
// A template class for storing a dense matrix of objects

#ifndef MAT_HPP
#define MAT_HPP

#include "symmat.hpp"
#include "denmat.hpp"
#include "keymat.hpp"
#include "symmat_stream.hpp"
#include "denmat_stream.hpp"
#include "keymat_stream.hpp"

#endif
