// symmat_stream.tpp

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <functional>
#include "symmat.hpp"
#include "symmat_stream.hpp"

template<class T>
mat::SymMatStream<T>::SymMatStream() {}

template<class T>
bool mat::SymMatStream<T>::read(const std::string& file, mat::SymMat<T>& mat) {
  std::function<size_t(const size_t&)> func = 
    [] (const size_t& i) -> size_t {return i;};
  return read(file, mat, func);
}

template<class T>
template<class K>
bool mat::SymMatStream<T>::read(const std::string& file, mat::SymMat<T>& mat,
				std::function<size_t(const K&)> index) {
  std::ifstream reader;
  reader.open(file);
  if (!reader) {
    std::cout << "Error: cannot open the file " << file << std::endl;
    return false;
  }
  std::string line, args;
  std::stringstream ss, ss2;
  K i, j;
  while (getline(reader,line)) {
    ss.clear();
    ss.str(line);
    ss >> i >> j;
    getline(ss,args);
    ss2.clear();
    ss2.str(args);
    ss2 >> mat(index(i),index(j));
  }
  reader.close();
  return true;
}

template<class T>
bool mat::SymMatStream<T>::dump(const std::string& file, 
				const mat::SymMat<T>& mat, bool dumpDiag) {
  std::function<size_t(const size_t&)> func = 
	   [] (const size_t& i) -> size_t {return i;};
  return dump(file, mat, func, dumpDiag);
}

template<class T>
template<class K>
bool mat::SymMatStream<T>::dump(const std::string& file, 
				const mat::SymMat<T>& mat,
				std::function<K(const size_t&)> key,
				bool dumpDiag) {
  std::ofstream writer;
  writer.open(file);
  if (!writer) {
    std::cout << "Error: cannot open the file " << file << std::endl;
    return false;
  }
  size_t n = mat.size();
  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < (dumpDiag ? i+1 : i); j++) {
      writer << key(i) << " " << key(j) << " " << mat(i,j) << "\n";
    }
  }
  writer.close();
  return true;
}

template<class T>
bool mat::SymMatStream<T>::dumpToGnuplot(const std::string& file, 
					 const mat::SymMat<T>& mat) {
  std::function<size_t(const size_t&)> func = 
    [] (const size_t& i) -> size_t {return i;};
  return dumpToGnuplot(file, mat, func);
}

template<class T>
template<class K>
bool mat::SymMatStream<T>::dumpToGnuplot(const std::string& file, 
					 const mat::SymMat<T>& mat,
					 std::function<K(const size_t&)> key) {
  std::ofstream writer;
  writer.open(file);
  if (!writer) {
    std::cout << "Error: cannot open the file " << file << std::endl;
    return false;
  }
  size_t n = mat.size();
  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < n; j++) {
      writer << key(i) << " " << key(j) << " " << mat(i,j) << "\n";
    }
    writer << "\n";
  }
  writer.close();
  return true;
}
