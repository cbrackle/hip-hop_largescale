// bead_list.cpp

#include "array.hpp"
#include "bead_list.hpp"

BeadList::BeadList() {}

BeadList::~BeadList() {
  clearData();
}

void BeadList::setData(int nb, const double dim[3]) {
  clearData();
  nbeads = nb;
  position = create2DDoubleArray(nbeads, 3);
  boundCount = create2DIntArray(nbeads, 3);
  type = create1DIntArray(nbeads);
  boxSize[0] = dim[0];
  boxSize[1] = dim[1];
  boxSize[2] = dim[2];
}

void BeadList::clearData() {
  if (position != nullptr) deleteArray(position);
  if (boundCount != nullptr) deleteArray(boundCount);
  if (type != nullptr) deleteArray(type);
}

void BeadList::setPosition(int index, int comp, double val) {
  position[index][comp] = val;
}

void BeadList::setBoundCount(int index, int comp, int val) {
  boundCount[index][comp] = val;
}

void BeadList::setType(int index, int val) {
  type[index] = val;
}

double BeadList::getPosition(int index, int comp) const {
  return position[index][comp];
}

double BeadList::getUnwrappedPosition(int index, int comp) const {
  return position[index][comp] + boundCount[index][comp]*boxSize[comp];
}

int BeadList::getBoundCount(int index, int comp) const {
  return boundCount[index][comp];
}

int BeadList::getType(int index) const {
  return type[index];
}
