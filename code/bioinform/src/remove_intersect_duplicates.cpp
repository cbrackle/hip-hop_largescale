// remove_intersect_duplicates.cpp
//
// Read the bed file resulting from intersection of two more bed files
// Keep only one copy of the region from the control bed files 
// (-a in bedtools intersect) that intersects with multiple regions from
// other bed files (-b in bedtools) based on the intersected region with the
// highest peak score
//
// The bed file must have the following columns
//     chrom1 start1 end1 name1 peakscore1 strand1 
//     chrom2 start2 end2 name2 peakscore2 strand2

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::string;
using std::vector;
using std::sort;
using std::map;
using std::pair;

struct Bedline {
  string chrom, name, strand;
  long int start, end;
  double peakscore;
  
  Bedline(); // Default constructor
  Bedline(const Bedline&); // Copy constructor
  Bedline(const string&);
  bool operator<(const Bedline&) const; // Comparison for sorting
  bool operator==(const Bedline&) const;
};

void lineToBedlines(const string& line, Bedline* b1, Bedline* b2);
void writeBedlines(ofstream& ouf, const Bedline& b1, const Bedline& b2);

int main(int argc, char* argv[]) {  
  if (argc != 3) {
    cout << "Usage: remove_intersect_duplicates infile outfile" << endl;
    cout << "where  infile   is the input bed file." << endl;
    cout << "       outfile  is the output bed file." << endl;
    cout << "The input bed file must have the following columns:" << endl;
    cout << "    chrom1 start1 end1 name1 peakscore1 strand1" << endl;
    cout << "    chrom2 start2 end2 name2 peakscore2 strand2" << endl;
    cout << "and must be sorted by name first, then by chrom, and " << endl;
    cout << "finally by end before being parsed to this program." << endl;
    return 1;
  }

  int argi = 0;
  string infile = string(argv[++argi]);
  string outfile = string(argv[++argi]);

  ifstream inf; 
  ofstream ouf;

  inf.open(outfile);
  if (inf.good()) {
    cout << "Error: file " << outfile << " already exists. Will not overwrite."
	 << endl;
    inf.close();
    return 1;
  }

  inf.open(infile);
  if (!inf) {
    cout << "Error when trying to open the input bed file " << infile << endl;
    return 1;
  }

  ouf.open(outfile);
  if (!ouf) {
    cout << "Error when trying to open the output bed file " 
	 << outfile << endl;
    return 1;
  }

  string line;
  Bedline bedline[2], nextBedline[2];
  
  if (getline(inf, line)) {
    lineToBedlines(line, &bedline[0], &bedline[1]);
  } else {
    cout << "No lines in the bed file." << endl;
    return 1;
  }

  while (getline(inf, line)) {
    lineToBedlines(line, &nextBedline[0], &nextBedline[1]);
    if (bedline[0] == nextBedline[0]) {
      if (bedline[1] < nextBedline[1]) {
	bedline[0] = nextBedline[0];
	bedline[1] = nextBedline[1];
      }
    } else {
      writeBedlines(ouf, bedline[0], bedline[1]);
      bedline[0] = nextBedline[0];
      bedline[1] = nextBedline[1];	
    }
  }
  writeBedlines(ouf, bedline[0], bedline[1]);
  inf.close();
  ouf.close();
}

void lineToBedlines(const string& line, Bedline* b1, Bedline* b2) {
  stringstream ss;
  string chrom, name, strand;
  long int start, end;
  double peakscore;
  ss.str(line);
  ss >> chrom >> start >> end >> name >> peakscore >> strand;
  b1->chrom = chrom;
  b1->start = start;
  b1->end = end;
  b1->name = name;
  b1->peakscore = peakscore;
  b1->strand = strand;
  ss >> chrom >> start >> end >> name >> peakscore >> strand;
  b2->chrom = chrom;
  b2->start = start;
  b2->end = end;
  b2->name = name;
  b2->peakscore = peakscore;
  b2->strand = strand;
  ss.clear();
}

void writeBedlines(ofstream& ouf, const Bedline& b1, const Bedline& b2) {
  ouf << b1.chrom << "\t" << b1.start << "\t" << b1.end << "\t" 
      << b1.name << "\t" << b1.peakscore << "\t" << b1.strand << "\t" 
      << b2.chrom << "\t" << b2.start << "\t" << b2.end << "\t" 
      << b2.name << "\t" << b2.peakscore << "\t" << b2.strand << "\n";
}


// Member functions of Bedline
Bedline::Bedline() {}

Bedline::Bedline(const Bedline& b) :
  chrom(b.chrom), name(b.name), strand(b.strand), start(b.start), end(b.end),
  peakscore(b.peakscore) {}

Bedline::Bedline(const string& line) {
  stringstream ss; 
  ss.str(line);
  ss >> chrom >> start >> end >> name >> peakscore >> strand;
}

bool Bedline::operator<(const Bedline& b) const {
  return peakscore < b.peakscore;
}

bool Bedline::operator==(const Bedline& b) const {
  return chrom == b.chrom &&
    start == b.start &&
    end == b.end &&
    name == b.name &&
    strand == b.strand;
  // Ignore peakscore comparison
}
