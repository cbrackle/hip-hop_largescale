# data_to_beads.py

import sys
import math

args = sys.argv

if (len(args) != 7):
    print("Usage: data_to_beads.py chrom start_bp end_bp bp_per_bead",
          "bed_file out_file")
    sys.exit(1)

chrom = args.pop(1)
start_bp = int(args.pop(1))
end_bp = int(args.pop(1))
bp_per_bead = int(args.pop(1))
bed_file = args.pop(1)
out_file = args.pop(1)

cover_frac = 0.25

nbeads = int(math.ceil((end_bp-start_bp)/bp_per_bead))

beads = {}

print("Number of beads: {:d}".format(nbeads))

# Helper functions
def bead_start_bp(bead):
    return start_bp+bead*bp_per_bead

def bead_end_bp(bead):
    return start_bp+(bead+1)*bp_per_bead

def bp_to_bead(bp):
    return int((bp-start_bp)/bp_per_bead)

def bead_to_bp(bead):
    return start(bead)

def overlap(sbp1,ebp1,sbp2,ebp2):
    if (sbp1 >= sbp2 and ebp1 <= ebp2):
        return ebp1-sbp1
    elif (sbp2 >= sbp1 and ebp2 <= ebp1):
        return ebp2-sbp2
    elif (ebp1 > sbp2 and ebp1 < ebp2):
        return ebp1-sbp2
    elif (sbp1 > sbp2 and sbp1 < ebp2):
        return ebp2-sbp1
    else:
        return 0

with open(bed_file,'r') as reader:
    for line in reader:
        data = line.split()
        ch = data[0]
        if (ch != chrom): continue
        sbp = int(data[1])
        ebp = int(data[2])
        name = data[3]
        width = ebp-sbp
        mid = int(math.floor(0.5*(sbp+ebp)))
        if (width < (1.0+cover_frac)*bp_per_bead):
            beads[bp_to_bead(mid)] = name
        else:
            first = bp_to_bead(sbp)
            last = bp_to_bead(ebp)
            if (last < 0 or first >= nbeads): continue
            if (last >= nbeads): last = nbeads-1
            if (first < 0): first = 0
            for i in range(first,last+1):
                if (overlap(bead_start_bp(i),bead_end_bp(i),
                            sbp,ebp) > cover_frac*bp_per_bead):
                    beads[i] = name

# Output results (sorted by bead index)
with open(out_file,'w') as writer:
    for bead,name in sorted(beads.items(), key = lambda item: item[0]):
        writer.write("{:d} {:s}\n".format(bead,name))
